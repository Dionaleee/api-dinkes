class CreatePatients < ActiveRecord::Migration[6.1]
  def change
    create_table :patients do |t|
      t.string :nik
      t.date :kkNumber
      t.string :name
      t.string :gender
      t.string :placeOfBirth
      t.date :dateOfBirth
      t.string :religion
      t.text :address
      t.string :provinceCode
      t.string :regencyCode
      t.string :districtCode
      t.string :villageCode
      t.string :postalCode
      t.string :rt
      t.string :rw
      t.string :inssurance
      t.string :inssuranceNumber
      t.string :phoneNumber
      t.string :email
      t.string :occupation
      t.string :education
      t.string :tribe
      t.string :motherName
      t.attachment :maritalStatus
      t.string :patientCompanionRelationship
      t.string :patientCompanionName
      t.string :patientCompanionGender
      t.string :patientCompanionPhoneNumber
      t.date :patientCompanionDateOfBirth
      t.string :patientCompanionAddress
      t.attachment :identity
      t.text :disabilities

      t.timestamps
    end
  end
end
