# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_02_25_090129) do

  create_table "$DOKTERTMP", id: false, force: :cascade do |t|
    t.varchar "KDDOKTER", limit: 20
    t.varchar "NMDOKTER", limit: 200
    t.varchar "DOKTERSMF", limit: 200
    t.varchar "NOREKBANK", limit: 20
    t.varchar "NMREKBANK", limit: 200
  end

  create_table "$PELAYANANTMP2", id: false, force: :cascade do |t|
    t.varchar "KDPELAYANAN", limit: 20
    t.varchar "NMPELAYANAN", limit: 255
    t.varchar "DESKRIPSI", limit: 100
    t.varchar "GRUPPELAYANAN", limit: 100
    t.varchar "IDGRUPPELAYANAN", limit: 100
    t.varchar "IDGRUPPEL", limit: 100
    t.integer "XCOUNT"
  end

  create_table "$RUANGANTMP", id: false, force: :cascade do |t|
    t.integer "INDEKS"
    t.varchar "KDINSTALASI", limit: 10
    t.varchar "NMINSTALASI", limit: 100
    t.varchar "KDRUANGAN", limit: 10
    t.varchar "NMRUANGAN", limit: 100
    t.varchar "GRUPRUANGAN", limit: 100
    t.varchar "GRUPINSTALASI", limit: 100
    t.varchar "AKTIF", limit: 1
  end

  create_table "$TARIFIDIRELASI1002TMP", id: false, force: :cascade do |t|
    t.varchar "SMF_NAME", limit: 255
    t.varchar "KDJNSPELAYANAN", limit: 100
    t.varchar "KDPELAYANAN", limit: 20
    t.varchar "NMPELAYANAN", limit: 1000
    t.varchar "KTRFIDI", limit: 10
    t.varchar "IDISMF", limit: 255
    t.varchar "IDINMTINDAKAN", limit: 255
    t.varchar "ICDIX", limit: 255
    t.float "F1"
    t.float "F2#1"
    t.float "F2#2"
    t.float "F3"
    t.float "F4"
    t.float "TOTALSCORE"
    t.float "RVU"
    t.float "JASA"
  end

  create_table "Agama", primary_key: "KdAgama", id: { type: :char, limit: 2 }, force: :cascade do |t|
    t.varchar "Agama", limit: 20, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 30
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "Antenatal", primary_key: "id_antenatal", id: :integer, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 50
    t.varchar "NoCm", limit: 10
    t.varchar "IdPegawai", limit: 50
    t.varchar "KdRuangan", limit: 3
    t.varchar "UmurKehamilan", limit: 50
    t.varchar "Anamnesa", limit: 50
    t.varchar "BeratBadan", limit: 50
    t.varchar "TD", limit: 50
    t.varchar "LILA", limit: 50
    t.varchar "EDEMA", limit: 50
    t.varchar "StatusGiziANC", limit: 50
    t.varchar "HPHT", limit: 50
    t.varchar "HPL", limit: 50
    t.varchar "GolonganDarah", limit: 50
    t.varchar "PenggunaanAlatKontrasepsi", limit: 50
    t.varchar "RiwayatKehamilan", limit: 50
    t.varchar "Edukasi", limit: 50
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
  end

  create_table "AntrianManager", id: false, force: :cascade do |t|
    t.datetime "TglMasuk"
    t.varchar "KdRuangan", limit: 10
    t.varchar "IdDokter", limit: 10
    t.varchar "JamMulai", limit: 5
    t.varchar "NoAntrian", limit: 10
  end

  create_table "AntrianRawatJalan", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10
    t.char "NoCM", limit: 15
    t.char "KdSubInstalasi", limit: 3
    t.char "KdRuangan", limit: 3
    t.datetime "TglMasuk"
    t.char "KdKelas", limit: 2
    t.char "NoAntrian", limit: 3
    t.varchar "StatusPasien", limit: 10
    t.varchar "StatusPeriksa", limit: 10
    t.datetime "WaktuMasukPoli"
  end

  create_table "ApotikJual", primary_key: ["KdBarang", "KdAsal", "KdRuangan", "SatuanJml", "NoStruk", "NoTerima"], force: :cascade do |t|
    t.varchar "KdBarang", limit: 9, null: false
    t.char "KdAsal", limit: 2, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "SatuanJml", limit: 1, null: false
    t.decimal "JmlBarang", precision: 9, scale: 2, null: false
    t.money "HargaSatuan", precision: 19, scale: 4, null: false
    t.money "PPn", precision: 19, scale: 4, null: false
    t.money "Discount", precision: 19, scale: 4, null: false
    t.char "NoStruk", limit: 10, null: false
    t.money "HargaBeli", precision: 19, scale: 4, null: false
    t.char "KdJenisObat", limit: 2
    t.integer "JmlService", null: false
    t.money "TarifService", precision: 19, scale: 4, null: false
    t.money "JmlHutangPenjamin", precision: 19, scale: 4, default: 0.0, null: false
    t.money "JmlTanggunganRS", precision: 19, scale: 4, default: 0.0, null: false
    t.money "BiayaAdministrasi", precision: 19, scale: 4, default: 0.0, null: false
    t.money "JmlPembebasan", precision: 19, scale: 4, default: 0.0, null: false
    t.char "NoClosing", limit: 10
    t.char "NoBKMClaim", limit: 10
    t.varchar "HariTglPelayanan", limit: 2
    t.varchar "BlnTglPelayanan", limit: 2
    t.varchar "ThnTglPelayanan", limit: 2
    t.decimal "JmlHarusDiBayar", precision: 35, scale: 6
    t.decimal "TotalBiaya", precision: 31, scale: 6
    t.float "TotalHutangPenjamin"
    t.float "TotalTanggunganRS"
    t.float "TotalPembebasan"
    t.char "NoTerima", limit: 10, null: false
    t.char "NoPostingJurnal", limit: 10
    t.char "NoRacikan", limit: 10
  end

  create_table "AsalBarang", primary_key: "QAsal", id: { type: :integer, limit: 1 }, force: :cascade do |t|
    t.char "KdAsal", limit: 2, null: false
    t.varchar "NamaAsal", limit: 25, null: false
    t.varchar "Keterangan", limit: 100
    t.char "KdInstalasi", limit: 2
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 30
    t.integer "StatusEnabled", limit: 1, null: false
    t.integer "KdAccount"
  end

  create_table "AsalRujukanPasienMaternal", id: false, force: :cascade do |t|
    t.char "Kdrujukan", limit: 3, null: false
    t.text_basic "AsalRujukan", null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 30
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "AsesmenGeriartri", primary_key: "id_geriartri", id: :integer, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 50
    t.varchar "NoCm", limit: 10
    t.varchar "IdPegawai", limit: 50
    t.varchar "KdRuangan", limit: 3
    t.varchar "hobby", limit: 50
    t.varchar "keanggotaanorganisasi", limit: 50
    t.varchar "liburanperjalanan", limit: 50
    t.varchar "alattransportasi", limit: 50
    t.varchar "pendapatan", limit: 50
    t.varchar "pbdf", limit: 50
    t.varchar "jdrkrs", limit: 50
    t.varchar "jdrkk", limit: 50
    t.varchar "pkdr", limit: 50
    t.varchar "mkdh", limit: 50
    t.varchar "pssydk", limit: 50
    t.varchar "kebiasaanritual", limit: 50
    t.varchar "lainnya", limit: 50
    t.varchar "sstt", limit: 50
    t.varchar "skulk", limit: 50
    t.varchar "cemas", limit: 50
    t.varchar "depresi", limit: 50
    t.varchar "menangis", limit: 50
    t.varchar "gugup", limit: 50
    t.varchar "insomnia", limit: 50
    t.varchar "takut", limit: 50
    t.varchar "mdmp", limit: 50
    t.varchar "pkf", limit: 50
    t.varchar "kdmk", limit: 50
    t.varchar "kbk", limit: 50
    t.varchar "stres", limit: 50
    t.varchar "gpss", limit: 50
    t.varchar "sd", limit: 50
    t.varchar "smp", limit: 50
    t.varchar "smu", limit: 50
    t.varchar "perguruantinggi", limit: 50
    t.varchar "tbsht", limit: 50
    t.varchar "has", limit: 50
    t.varchar "anti", limit: 50
    t.varchar "bnfa", limit: 50
    t.varchar "daa", limit: 50
    t.varchar "bua", limit: 50
    t.varchar "kal", limit: 50
    t.varchar "spis", limit: 50
    t.varchar "spisnya", limit: 50
    t.varchar "snkia", limit: 50
    t.varchar "kurangi", limit: 50
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
  end

  create_table "AsesmenGeriartriRajal", primary_key: "Id_Geriatri", id: :integer, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 50
    t.varchar "NoCm", limit: 10
    t.varchar "IdPegawai", limit: 50
    t.varchar "KdRuangan", limit: 3
    t.varchar "RangsangPembuanganTinja", limit: 5
    t.varchar "RangsangBerkemih", limit: 5
    t.varchar "MembersihkanDiri", limit: 5
    t.varchar "PenggunaanJamban", limit: 5
    t.varchar "Makan", limit: 5
    t.varchar "BerubahSikap", limit: 5
    t.varchar "Berpindah", limit: 5
    t.varchar "MemakaiBaju", limit: 5
    t.varchar "NaikTurunTangga", limit: 5
    t.varchar "Mandi", limit: 5
    t.integer "SkorA"
    t.varchar "PuasDenganKehidupan", limit: 5
    t.varchar "SeringMerasaBosan", limit: 5
    t.varchar "SeringMerasaTidakBerdaya", limit: 5
    t.varchar "LebihSenangDiRumah", limit: 5
    t.varchar "MerasaTidakBerharga", limit: 5
    t.integer "SkorB"
    t.varchar "Umur", limit: 5
    t.varchar "JamSekarang", limit: 5
    t.varchar "AlamatTempatTinggal", limit: 5
    t.varchar "TahunIni", limit: 5
    t.varchar "SaatIniBeradaDimana", limit: 5
    t.varchar "MengenaliOrangRS", limit: 5
    t.varchar "TahunKemerdekaanRI", limit: 5
    t.varchar "NamaPresidenRI", limit: 5
    t.varchar "TahunKelahiranPasien", limit: 5
    t.varchar "MenghitungTerbalik", limit: 5
    t.integer "SkorC"
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
  end

  create_table "AsesmenIgd", id: false, force: :cascade do |t|
    t.varchar_max "NoPendaftaran"
    t.varchar_max "NoCM"
    t.datetime "TanggalInput"
    t.varchar_max "JamDatang"
    t.varchar_max "IdUser"
    t.varchar_max "JamAsesmen"
    t.varchar_max "radioanamnesa"
    t.varchar_max "KeluhanUtama"
    t.varchar_max "RiwayatPenyakitSekarang"
    t.varchar_max "RiwayatPenyakitDahulu"
    t.varchar_max "RiwayatPenyakitKeluarga"
    t.varchar_max "RiwayatAlergi"
    t.varchar_max "ObatKonsumsi"
    t.varchar_max "MasalahKesehatan"
    t.varchar_max "TidakSeimbang"
    t.varchar_max "MenggunakanAlatBantu"
    t.varchar_max "MemegangKursi"
    t.varchar_max "StatusPerkawinan"
    t.varchar_max "Agama"
    t.varchar_max "ResikoJatuh"
    t.varchar_max "PendidikanTerakhir"
    t.varchar_max "Pekerjaan"
    t.varchar_max "StatusSosial"
    t.varchar_max "SkriningGiziDewasa"
    t.varchar_max "SkriningGiziDewasaMakanan"
    t.varchar_max "TotalSkorSkriningGiziDewasa"
    t.varchar_max "DiagnosisGiziDewasa"
    t.varchar_max "TampakKurus"
    t.varchar_max "PenurunanBB"
    t.varchar_max "AsupanMakan"
    t.varchar_max "PenyakitMalNutrisi"
    t.varchar_max "TotalScoreAnak"
    t.varchar_max "InterpretasiSkor"
    t.varchar_max "LaporTeamGizi"
    t.varchar_max "TanggalJamLaporteam"
    t.varchar_max "Face"
    t.varchar_max "Kaki"
    t.varchar_max "Activity"
    t.varchar_max "Cry"
    t.varchar_max "Consolability"
    t.varchar_max "Nyeri"
    t.varchar_max "airway"
    t.varchar_max "breathing"
    t.varchar_max "Nadi"
    t.varchar_max "crt"
    t.varchar_max "warnakulit"
    t.varchar_max "pendarahan"
    t.varchar_max "kesadaran"
    t.varchar_max "E"
    t.varchar_max "M"
    t.varchar_max "V"
    t.varchar_max "luka"
    t.varchar_max "isi_luka"
    t.varchar_max "Ptichiae"
    t.varchar_max "isi_ptichiae"
    t.varchar_max "lokasinyeri"
    t.varchar_max "skalanyeri"
    t.varchar_max "StatusPsikologi"
    t.varchar_max "masalahkeperawatan"
    t.varchar_max "mandiri"
    t.varchar_max "kolaborasi"
    t.varchar_max "DischargePlanning"
    t.varchar_max "Ruangan"
    t.varchar_max "ranap"
    t.varchar_max "rajal"
    t.varchar_max "pulangsendiri"
    t.varchar_max "JamMeninggal"
    t.varchar_max "Skor"
  end

  create_table "AsesmenKepSkorNyeri", primary_key: "id_skornyeri", id: :integer, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 50
    t.varchar "NoCm", limit: 10
    t.varchar "IdPegawai", limit: 50
    t.varchar "KdRuangan", limit: 3
    t.varchar "SkorNyeri", limit: 50
    t.varchar "SkalaNyeri", limit: 50
    t.varchar "Durasi", limit: 50
    t.varchar "PolaNyeri", limit: 50
    t.varchar "Lokasi", limit: 50
    t.varchar "Frekuensi", limit: 50
    t.varchar "NyeriHilangBila", limit: 50
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
  end

  create_table "AsesmenPoliGigi", primary_key: "id_gigi", id: :integer, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 50
    t.varchar "NoCm", limit: 10
    t.varchar "IdPegawai", limit: 50
    t.varchar "KdRuangan", limit: 3
    t.varchar "nomenklatursatu1", limit: 50
    t.varchar "nomenklatursatu2", limit: 50
    t.varchar "nomenklatursatu3", limit: 50
    t.varchar "nomenklatursatu4", limit: 50
    t.varchar "nomenklatursatu5", limit: 50
    t.varchar "nomenklatursatu6", limit: 50
    t.varchar "nomenklatursatu7", limit: 50
    t.varchar "nomenklatursatu8", limit: 50
    t.varchar "nomenklaturdua1", limit: 50
    t.varchar "nomenklaturdua2", limit: 50
    t.varchar "nomenklaturdua3", limit: 50
    t.varchar "nomenklaturdua4", limit: 50
    t.varchar "nomenklaturdua5", limit: 50
    t.varchar "nomenklaturdua6", limit: 50
    t.varchar "nomenklaturdua7", limit: 50
    t.varchar "nomenklaturdua8", limit: 50
    t.varchar "nomenklaturtiga1", limit: 50
    t.varchar "nomenklaturtiga2", limit: 50
    t.varchar "nomenklaturtiga3", limit: 50
    t.varchar "nomenklaturtiga4", limit: 50
    t.varchar "nomenklaturtiga5", limit: 50
    t.varchar "nomenklaturtiga6", limit: 50
    t.varchar "nomenklaturtiga7", limit: 50
    t.varchar "nomenklaturtiga8", limit: 50
    t.varchar "nomenklaturempat1", limit: 50
    t.varchar "nomenklaturempat2", limit: 50
    t.varchar "nomenklaturempat3", limit: 50
    t.varchar "nomenklaturempat4", limit: 50
    t.varchar "nomenklaturempat5", limit: 50
    t.varchar "nomenklaturempat6", limit: 50
    t.varchar "nomenklaturempat7", limit: 50
    t.varchar "nomenklaturempat8", limit: 50
    t.varchar "nomenklaturlimaI", limit: 50
    t.varchar "nomenklaturlimaII", limit: 50
    t.varchar "nomenklaturlimaIII", limit: 50
    t.varchar "nomenklaturlimaIV", limit: 50
    t.varchar "nomenklaturlimaV", limit: 50
    t.varchar "nomenklaturenamI", limit: 50
    t.varchar "nomenklaturenamII", limit: 50
    t.varchar "nomenklaturenamIII", limit: 50
    t.varchar "nomenklaturenamIV", limit: 50
    t.varchar "nomenklaturenamV", limit: 50
    t.varchar "nomenklaturtujuhI", limit: 50
    t.varchar "nomenklaturtujuhII", limit: 50
    t.varchar "nomenklaturtujuhIII", limit: 50
    t.varchar "nomenklaturtujuhIV", limit: 50
    t.varchar "nomenklaturtujuhV", limit: 50
    t.varchar "nomenklaturdelapanI", limit: 50
    t.varchar "nomenklaturdelapanII", limit: 50
    t.varchar "nomenklaturdelapanIII", limit: 50
    t.varchar "nomenklaturdelapanIV", limit: 50
    t.varchar "nomenklaturdelapanV", limit: 50
    t.varchar "hypertensi", limit: 50
    t.varchar "asma", limit: 50
    t.varchar "penyakitdarah", limit: 50
    t.varchar "hepatitis", limit: 50
    t.varchar "dm", limit: 50
    t.varchar "demamtypoid", limit: 50
    t.varchar "jantung", limit: 50
    t.varchar "alergi", limit: 50
    t.varchar "dentalfoto", limit: 50
    t.varchar "panoramic", limit: 50
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
  end

  create_table "AsuransiPasien", primary_key: "QAsuransiPasien", id: :integer, force: :cascade do |t|
    t.char "IdPenjamin", limit: 10, null: false
    t.varchar "IdAsuransi", limit: 15, null: false
    t.char "NoCM", limit: 15, null: false
    t.varchar "NamaPeserta", limit: 50, null: false
    t.varchar "IDPeserta", limit: 16
    t.char "KdGolongan", limit: 2
    t.datetime "TglLahir", null: false
    t.varchar "Alamat", limit: 100
    t.varchar "KdInstitusiAsal", limit: 4
    t.smalldatetime "TglBerlaku"
    t.index ["IdPenjamin", "NoCM"], name: "ClusteredIndex-20230808-223549"
  end

  create_table "BackupUpdatingBiayaPelayananOA", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.datetime "TglPelayanan", null: false
    t.datetime "TglUpdate", null: false
    t.varchar "Keterangan", limit: 150, null: false
    t.char "IdUser", limit: 10, null: false
  end

  create_table "BerkasPasienUpload", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.varchar "NamaFileKTP", limit: 2000, null: false
    t.varchar "NamaFileKK", limit: 2000, null: false
    t.char "status_Berkas", limit: 1
    t.varchar "BerkasKTP", limit: 2000
    t.varchar "BerkasKK", limit: 2000
  end

  create_table "BiayaBLUD", id: false, force: :cascade do |t|
    t.char "KdBiaya", limit: 10
    t.varchar "KodeBiaya", limit: 50
    t.varchar "Nama", limit: 1000
  end

  create_table "BiayaPelayanan", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "KdSubInstalasi", limit: 3, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "KdPelayananRS", limit: 6, null: false
    t.char "KdKelas", limit: 2, null: false
    t.char "StatusCITO", limit: 1, null: false
    t.integer "Tarif", null: false
    t.integer "TarifCito", null: false
    t.integer "JmlPelayanan", null: false
    t.datetime "TglPelayanan", null: false
    t.char "NoLab_Rad", limit: 10
    t.char "IdPegawai", limit: 10, null: false
    t.char "NoStruk", limit: 10
    t.char "StatusAPBD", limit: 2, null: false
    t.char "KdJenisTarif", limit: 2, null: false
    t.char "IdPegawai2", limit: 10
    t.char "IdUser", limit: 10, null: false
    t.char "IdPegawai3", limit: 10
    t.char "KdRuanganAsal", limit: 3
    t.integer "HariTglPelayanan"
    t.integer "BlnTglPelayanan"
    t.integer "ThnTglPelayanan"
  end

  create_table "BiayaPelayananAkuntansi", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10
    t.char "KdPelayananRS", limit: 15, null: false
    t.integer "JmlPelayanan", null: false
    t.money "Tarif", precision: 19, scale: 4, null: false
    t.money "TarifCito", precision: 19, scale: 4, null: false
    t.money "TotalBiaya", precision: 19, scale: 4, null: false
    t.datetime "TglPelayanan", null: false
    t.char "KdKelas", limit: 2, null: false
    t.char "NoStruk", limit: 10
    t.char "KdJenisTarif", limit: 2, null: false
    t.char "KdSubInstalasi", limit: 3, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.money "JmlHutangPenjamin", precision: 19, scale: 4, null: false
    t.money "JmlTanggunganRS", precision: 19, scale: 4, null: false
    t.money "JmlPembebasan", precision: 19, scale: 4, null: false
    t.money "JmlHarusDibayar", precision: 19, scale: 4, null: false
    t.char "KdRuanganAsal", limit: 3
    t.datetime "TglPosting", null: false
    t.char "KdRuanganKasir", limit: 3
    t.char "IdUserKasir", limit: 12
  end

  create_table "BiayaPelayananAkuntansi_History", id: false, force: :cascade do |t|
    t.char "NoStruk", limit: 10
    t.char "IdUserKasir", limit: 12
    t.varchar "Keterangan", limit: 50
  end

  create_table "BiayaPelayananNew", id: false, force: :cascade do |t|
    t.integer "ID_Tindakan"
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "KdSubInstalasi", limit: 3, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "KdPelayananRS", limit: 6, null: false
    t.char "KdKelas", limit: 2, null: false
    t.char "StatusCITO", limit: 1, null: false
    t.integer "Tarif", null: false
    t.integer "TarifCito", null: false
    t.integer "JmlPelayanan", null: false
    t.datetime "TglPelayanan", null: false
    t.char "NoLab_Rad", limit: 10
    t.char "IdPegawai", limit: 10, null: false
    t.char "NoStruk", limit: 10
    t.char "StatusAPBD", limit: 2, null: false
    t.char "KdJenisTarif", limit: 2, null: false
    t.char "IdPegawai2", limit: 10
    t.char "IdUser", limit: 10, null: false
    t.char "IdPegawai3", limit: 10
    t.char "KdRuanganAsal", limit: 3
    t.integer "HariTglPelayanan"
    t.integer "BlnTglPelayanan"
    t.integer "ThnTglPelayanan"
    t.index ["ID_Tindakan", "NoPendaftaran", "KdPelayananRS", "NoStruk"], name: "IX_BiayaPelayanan"
  end

  create_table "Booking_Online", id: false, force: :cascade do |t|
    t.varchar "NoBooking", limit: 10
    t.varchar "NoCM", limit: 10
    t.varchar "KdKelompokPasien", limit: 2
    t.varchar "IdDokter", limit: 10
    t.varchar "KdRuangan", limit: 3
    t.varchar "KdSubInstalasi", limit: 3
    t.varchar "JamMulai", limit: 5
    t.varchar "JamSelesai", limit: 5
    t.varchar "NoAntrian", limit: 3
    t.datetime "TglPerjanjian"
    t.varchar "UserPendaftar", limit: 10
    t.datetime "TglDaftar"
    t.varchar "UserValidasi", limit: 10
    t.datetime "TglCetakPendaftaran"
    t.varchar "StatusKehadiran", limit: 1
  end

  create_table "Booking_Online_2", id: false, force: :cascade do |t|
    t.char "NoBooking", limit: 15
    t.datetime "TglBooking"
    t.varchar "NoKartuPeserta", limit: 15
    t.char "KdRuanganPoli", limit: 3
    t.char "KdSubInstalasi", limit: 3
    t.varchar "JamMulai", limit: 5
    t.varchar "JamSelesai", limit: 5
    t.char "IdDokter", limit: 10
    t.char "StatusKehadiran", limit: 1
    t.char "NoAntrian", limit: 3
    t.char "NoCM", limit: 15
    t.datetime "TglDaftar"
    t.char "IdUserPendaftar", limit: 10
    t.char "IdUserKoja", limit: 10
    t.varchar "SumberDaftar", limit: 3
    t.varchar_max "nmJenisPeserta"
    t.varchar "kdKelas", limit: 2
    t.varchar "PKUkdProvider", limit: 50
    t.varchar_max "PKUnmProvider"
    t.varchar_max "noKunjungan"
    t.varchar "kdDiag", limit: 10
    t.varchar_max "nmDiag"
    t.date "TglKunjungan"
    t.varchar "NoHP", limit: 15
  end

  create_table "CPPT_IGD", primary_key: "IdCPPT", id: :integer, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10
    t.char "NoCM", limit: 8
    t.char "KdRuangan", limit: 3
    t.char "IdDokter", limit: 10
    t.varchar_max "Instruksi"
    t.varchar_max "Instruksi_Objektif"
    t.varchar_max "Instruksi_Assesment"
    t.varchar_max "Instruksi_Plan"
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
    t.char "StatusVerif", limit: 1
    t.varchar_max "Instruksi_interuksi"
    t.char "IdVerifikator", limit: 10
  end

  create_table "CPPT_Ranap", primary_key: "IdCPPT", id: :integer, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10
    t.char "NoCM", limit: 8
    t.char "KdRuangan", limit: 3
    t.char "IdDokter", limit: 10
    t.varchar_max "Instruksi"
    t.varchar_max "Instruksi_Objektif"
    t.varchar_max "Instruksi_Assesment"
    t.varchar_max "Instruksi_Plan"
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
    t.char "StatusVerif", limit: 1
    t.varchar "Instruksi_interuksi", limit: 5000
  end

  create_table "CUTI", id: false, force: :cascade do |t|
    t.varchar "KD_CUTI", limit: 6
    t.varchar "IDHADIR", limit: 6
    t.varchar "KODEHADIR", limit: 6
    t.varchar "NAMA", limit: 100
    t.integer "TAMBAHANMENIT"
    t.char "StatusEnabled", limit: 1
  end

  create_table "C_AssesmenAwalIGD", id: false, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 50
    t.varchar "NoCM", limit: 50
    t.varchar "NamaPasien", limit: 2000
    t.datetime "TanggalJamMedis"
    t.varchar "KeluhanUtamaAsesmen", limit: 2000
    t.varchar "radioanamnesa", limit: 2000
    t.varchar "Paliatif", limit: 2000
    t.varchar "RiwayatAlergi", limit: 2000
    t.varchar "RiwayatAlergi_lain", limit: 2000
    t.varchar "RiwayatAlergi_Obat", limit: 2000
    t.varchar "RiwayatAlergi_rokok", limit: 2000
    t.varchar "RiwayatAlergi_rokok_hari", limit: 2000
    t.varchar "RiwayatAlergi_alkohol", limit: 2000
    t.varchar "RiwayatAlergi_alkohol_banyak", limit: 2000
    t.varchar "Hipertensi", limit: 2000
    t.varchar "Hipertensi_obat", limit: 2000
    t.varchar "PenyakitJantung", limit: 2000
    t.varchar "PenyakitJantung_obat", limit: 2000
    t.varchar "Diabetes", limit: 2000
    t.varchar "Diabetes_obat", limit: 2000
    t.varchar "Asma", limit: 2000
    t.varchar "Kanker", limit: 2000
    t.varchar "Asma_obat", limit: 2000
    t.varchar "Kanker_obat", limit: 2000
    t.varchar "Lain_lainPenyakit", limit: 2000
    t.varchar "TekananDarah", limit: 2000
    t.varchar "nadi", limit: 2000
    t.varchar "Pernafasan", limit: 2000
    t.varchar "Suhu", limit: 2000
    t.varchar "SpO2", limit: 2000
    t.varchar "BB", limit: 2000
    t.varchar "TB", limit: 2000
    t.varchar "E", limit: 2000
    t.varchar "M", limit: 2000
    t.varchar "V", limit: 2000
    t.varchar "Kesadaran", limit: 2000
    t.varchar "KeadaanUmum", limit: 2000
    t.varchar "Nkepala", limit: 2000
    t.varchar "Dkepala", limit: 2000
    t.varchar "Nmata", limit: 2000
    t.varchar "Dmata", limit: 2000
    t.varchar "Ntht", limit: 2000
    t.varchar "Dtht", limit: 2000
    t.varchar "NLeher", limit: 2000
    t.varchar "DLeher", limit: 2000
    t.varchar "NJantung", limit: 2000
    t.varchar "DJantung", limit: 2000
    t.varchar "NParu", limit: 2000
    t.varchar "DParu", limit: 2000
    t.varchar "NDada", limit: 2000
    t.varchar "DDada", limit: 2000
    t.varchar "NPerut", limit: 2000
    t.varchar "DPerut", limit: 2000
    t.varchar "NPunggung", limit: 2000
    t.varchar "DPunggung", limit: 2000
    t.varchar "Nanus", limit: 2000
    t.varchar "Danus", limit: 2000
    t.varchar "Ngerak", limit: 2000
    t.varchar "Dgerak", limit: 2000
    t.varchar "Motorik", limit: 2000
    t.varchar "Fisiologis", limit: 2000
    t.varchar "Patologis", limit: 2000
    t.varchar "lainnya", limit: 2000
    t.varchar "Laboratorium", limit: 2000
    t.varchar "EKG", limit: 2000
    t.varchar "Radilogi", limit: 2000
    t.varchar "IdUser", limit: 2000
    t.datetime "CreateDate"
  end

  create_table "C_TriasePasienIGD", id: false, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 50
    t.varchar "NoCM", limit: 50
    t.varchar "NamaPasien", limit: 2000
    t.varchar "KeluhanUtama", limit: 2000
    t.varchar "caradatang", limit: 2000
    t.varchar "Konfirmasi", limit: 2000
    t.varchar "rujukan", limit: 2000
    t.varchar "AsalRujukan", limit: 2000
    t.varchar "PengantarPasien", limit: 2000
    t.varchar "ResponPasien", limit: 2000
    t.varchar "JalanNafas_emergency", limit: 2000
    t.varchar "JalanNafas_urgent", limit: 2000
    t.varchar "JalanNafas_Nonurgent", limit: 2000
    t.varchar "DOA", limit: 2000
    t.varchar "Pernafasan_Emergency", limit: 2000
    t.varchar "Pernafasan_Urgent", limit: 2000
    t.varchar "Pernafasan_NonUrgent", limit: 2000
    t.varchar "Sirkulas_Emergency", limit: 2000
    t.varchar "Sirkulasi_urgent", limit: 2000
    t.varchar "Sirkulasi_Nonurgent", limit: 2000
    t.varchar "Mental_Emergency", limit: 2000
    t.varchar "Mental_Urgent", limit: 2000
    t.varchar "Mental_NonUrgent", limit: 2000
    t.varchar "Kategori_emergency", limit: 2000
    t.varchar "Kategori_Urgent", limit: 2000
    t.varchar "Kategori_NonUrgent", limit: 2000
    t.varchar "Kategori_Meninggal", limit: 2000
    t.varchar "IdUser", limit: 2000
    t.datetime "CreateDate"
  end

  create_table "CaraBayar", primary_key: "KdCaraBayar", id: { type: :char, limit: 2 }, force: :cascade do |t|
    t.varchar "CaraBayar", limit: 30, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 30
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "CaraDatang_TP", id: false, force: :cascade do |t|
    t.varchar "KodeDatang", limit: 50
    t.varchar "KeteranganDatang", limit: 50
    t.integer "Enable"
  end

  create_table "CaraMasuk", primary_key: "KdCaraMasuk", id: { type: :char, limit: 2 }, force: :cascade do |t|
    t.varchar "CaraMasuk", limit: 30, null: false
    t.integer "QCaraMasuk", limit: 1, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 30
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "CatatanCovidPasien", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.datetime "TglDaftar", null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "KdStatusPasien", limit: 2, null: false
    t.varchar "KriteriaKegawatan", limit: 500
    t.varchar "Keterangan", limit: 500
    t.char "IdUser", limit: 10, null: false
    t.datetime "TglInput"
  end

  create_table "CatatanKeperawatan", primary_key: "IdCatatanKeperawatan", id: :integer, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10
    t.char "NoCM", limit: 8
    t.char "IdUser", limit: 10
    t.varchar_max "Keterangan"
    t.datetime "Createdate"
  end

  create_table "CatatanKlinisPasien", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.datetime "TglPeriksa", null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "KdSubInstalasi", limit: 3, null: false
    t.char "IdPegawai", limit: 10, null: false
    t.varchar "TekananDarah", limit: 20
    t.varchar "Nadi", limit: 20
    t.varchar "Pernapasan", limit: 20
    t.varchar "Suhu", limit: 20
    t.varchar "BeratTinggiBadan", limit: 20
    t.varchar "LingkarKepala", limit: 50
    t.char "KdKesadaran", limit: 2
    t.varchar "Keterangan", limit: 1000
    t.integer "GCSE", limit: 1
    t.integer "GCSF", limit: 1
    t.integer "GCSM", limit: 1
    t.char "IdUser", limit: 10, null: false
    t.char "IdParamedis", limit: 10
    t.varchar "SpO2", limit: 20
  end

  create_table "CatatanKlinisPasienNew", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.datetime "TglPeriksa", null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "KdSubInstalasi", limit: 3, null: false
    t.char "IdPegawai", limit: 10, null: false
    t.varchar "TekananDarah", limit: 20
    t.varchar "Nadi", limit: 20
    t.varchar "Pernapasan", limit: 20
    t.varchar "Saturasi", limit: 20
    t.varchar "Suhu", limit: 20
    t.varchar "BeratTinggiBadan", limit: 20
    t.varchar "LingkarKepala", limit: 50
    t.varchar "IMT", limit: 20
    t.char "KdKesadaran", limit: 2
    t.varchar "Keterangan", limit: 1000
    t.integer "GCSE", limit: 1
    t.integer "GCSF", limit: 1
    t.integer "GCSM", limit: 1
    t.char "IdUser", limit: 10
    t.char "IdParamedis", limit: 10
    t.varchar "SpO2", limit: 20
    t.varchar "LingkarPinggang", limit: 20
  end

  create_table "CatatanMedikPasien", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.datetime "TglPeriksa", null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "KdSubInstalasi", limit: 3, null: false
    t.char "IdDokter", limit: 10, null: false
    t.varchar "KeluhanUtama", limit: 1000
    t.char "KdTriase", limit: 2
    t.char "KdImunisasi", limit: 3
    t.varchar "Pengobatan", limit: 1000
    t.varchar "Keterangan", limit: 1000
    t.char "IdUser", limit: 10, null: false
  end

  create_table "CatatanMedikPasienRajal", id: false, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 10, null: false
    t.varchar "NoCM", limit: 15, null: false
    t.datetime "TglPeriksa", null: false
    t.varchar "KdRuangan", limit: 3, null: false
    t.varchar "KdSubInstalasi", limit: 3, null: false
    t.varchar "IdDokter", limit: 10, null: false
    t.varchar_max "KeluhanUtama"
    t.varchar_max "KeluhanTambahan"
    t.varchar_max "RiwayatPenyakitSekarang"
    t.varchar_max "RiwayatAlergi"
    t.varchar_max "RiwayatPenyakitDahulu"
    t.varchar "KdTriase", limit: 2
    t.varchar "KdImunisasi", limit: 3
    t.varchar_max "Pengobatan"
    t.varchar_max "Keterangan"
    t.varchar "IdUser", limit: 10, null: false
    t.varchar_max "statusfungsional"
    t.varchar_max "InformasiKesehatan"
    t.varchar_max "Edukasi"
  end

  create_table "CatatanMedikPasienRajalNew", id: false, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 10, null: false
    t.varchar "NoCM", limit: 15, null: false
    t.datetime "TglPeriksa", null: false
    t.varchar "KdRuangan", limit: 3, null: false
    t.varchar "KdSubInstalasi", limit: 3, null: false
    t.varchar "IdDokter", limit: 10, null: false
    t.varchar_max "KeluhanUtama"
    t.varchar_max "KeluhanTambahan"
    t.varchar_max "RiwayatPenyakitSekarang"
    t.varchar_max "RiwayatAlergi"
    t.varchar_max "RiwayatPenyakitDahulu"
    t.varchar "KdTriase", limit: 2
    t.varchar "KdImunisasi", limit: 3
    t.varchar_max "Pengobatan"
    t.varchar_max "Keterangan"
    t.varchar "IdUser", limit: 10, null: false
    t.varchar_max "statusfungsional"
    t.varchar_max "riwayatobatyangdiminum"
    t.varchar "StatusGizi", limit: 50
    t.varchar_max "Edukasi"
  end

  create_table "CatatanMedikPasienRanap", id: false, force: :cascade do |t|
    t.char "IdCatatanMedik", limit: 10, null: false
    t.varchar "NoPendaftaran", limit: 10, null: false
    t.varchar "NoCM", limit: 15, null: false
    t.datetime "TglPeriksa", null: false
    t.varchar "KdRuangan", limit: 3, null: false
    t.varchar "KdSubInstalasi", limit: 3, null: false
    t.varchar "IdDokter", limit: 10, null: false
    t.varchar_max "KeluhanUtama"
    t.varchar_max "KeluhanTambahan"
    t.varchar_max "RiwayatPenyakitSekarang"
    t.varchar_max "RiwayatAlergi"
    t.varchar_max "RiwayatPenyakitDahulu"
    t.varchar "KdTriase", limit: 2
    t.varchar "KdImunisasi", limit: 3
    t.varchar_max "Pengobatan"
    t.varchar_max "Keterangan"
    t.varchar "IdUser", limit: 10, null: false
    t.varchar_max "statusfungsional"
  end

  create_table "CatatanPengawasanKeperawatan", id: false, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 50
    t.varchar "NoCM", limit: 50
    t.datetime "Jam"
    t.varchar_max "Kes"
    t.varchar_max "Td"
    t.varchar_max "Nd"
    t.varchar_max "Hr"
    t.varchar_max "Rr"
    t.varchar_max "Sato2"
    t.varchar_max "Sh"
    t.datetime "Createdate"
  end

  create_table "CetakResep", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10
    t.char "NoResep", limit: 10
    t.datetime "TglCetak"
  end

  create_table "Closing", id: false, force: :cascade do |t|
    t.char "NoClosing", limit: 10, null: false
    t.datetime "TglClosing", null: false
    t.char "KdRuangan", limit: 3, null: false
    t.varchar "Keterangan", limit: 200
    t.char "IdUser", limit: 10, null: false
  end

  create_table "ConvertDetailPemeriksaanToNilaiNormalPK", id: false, force: :cascade do |t|
    t.varchar "kdDetailPeriksa", limit: 5, null: false
    t.char "kdNilaiNormal", limit: 4, null: false
    t.char "JK", limit: 1
  end

  create_table "ConvertJenisPemeriksaanToPelayanan", id: false, force: :cascade do |t|
    t.char "KdPelayananRS", limit: 6, null: false
    t.char "KdJenisPeriksa", limit: 3, null: false
    t.char "KdInstalasi", limit: 2, null: false
  end

  create_table "ConvertPaketAsuransiToPelayanan", id: false, force: :cascade do |t|
    t.varchar "KdPaket", limit: 3, null: false
    t.char "KdKelompokPasien", limit: 2, null: false
    t.char "IdPenjamin", limit: 10, null: false
    t.char "KdPelayananRS", limit: 6, null: false
  end

  create_table "ConvertPaketDetailObatEMR", id: false, force: :cascade do |t|
    t.varchar "KdBarang", limit: 9, null: false
    t.float "Jml3Hari"
    t.float "Jml7Hari"
    t.char "KdAturanPakai", limit: 4
    t.char "KdRuangan", limit: 3, null: false
    t.varchar "KdEksternal", limit: 200
  end

  create_table "ConvertPaketOAAsuransiToBarang", primary_key: ["KdPaket", "KdKelompokPasien", "IdPenjamin", "KdBarang"], force: :cascade do |t|
    t.varchar "KdPaket", limit: 3, null: false
    t.char "KdKelompokPasien", limit: 2, null: false
    t.char "IdPenjamin", limit: 10, null: false
    t.varchar "KdBarang", limit: 9, null: false
  end

  create_table "ConvertPaketPelayananOAToLPRSRuangan", id: false, force: :cascade do |t|
    t.integer "KdPaket", null: false
    t.char "KdRuangan", limit: 3, null: false
    t.varchar "KdBarang", limit: 9, null: false
    t.integer "resepke"
    t.char "KdRuanganTujuan", limit: 3
  end

  create_table "ConvertPaketPelayananTMToLPRSRuangan", id: false, force: :cascade do |t|
    t.varchar "KdPaket", limit: 3, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "KdPelayananRS", limit: 6, null: false
  end

  create_table "ConvertPelayananToJasaDokter", id: false, force: :cascade do |t|
    t.char "KdDetailJenisJasaPelayanan", limit: 2, null: false
    t.char "KdPelayananRS", limit: 6, null: false
    t.char "KdKomponen", limit: 2, null: false
  end

  create_table "DATAALAMAT", id: false, force: :cascade do |t|
    t.varchar "PIN", limit: 4
    t.varchar "NOREK", limit: 15
    t.varchar "NMPEGAWAI", limit: 200
    t.varchar "ALAMAT", limit: 250
    t.varchar "KODEWILAYAH", limit: 10
    t.varchar "KELURAHAN", limit: 200
    t.varchar "KECAMATAN", limit: 200
    t.varchar "KABUPATEN", limit: 200
    t.varchar "PROVINSI", limit: 200
    t.varchar "KODEPOS", limit: 10
  end

  create_table "DATAPEGAWAIGAJI", id: false, force: :cascade do |t|
    t.varchar "NOREK", limit: 20
    t.varchar "NMPEGAWAI", limit: 200
    t.varchar "PENDIDIKAN", limit: 50
    t.date "TMT"
    t.varchar "KAWIN", limit: 6
    t.varchar "JMLANAK", limit: 4
    t.varchar "KD_STATUS", limit: 20
    t.varchar "KD_RUANGAN", limit: 20
    t.varchar "KD_JABATAN", limit: 20
    t.datetime "PERIODE"
    t.varchar "BATCHPERIODE", limit: 6
    t.varchar "DATAPERIODE", limit: 20
    t.datetime "TGLUPDATE"
    t.varchar "PIN", limit: 4
    t.varchar "KD_JABATANREMUN", limit: 10
  end

  create_table "DATASDM", id: false, force: :cascade do |t|
    t.char "PIN", limit: 4
    t.varchar "NIP", limit: 20
    t.varchar "IDPEGAWAI", limit: 20
    t.varchar "NOREK", limit: 25
    t.varchar "NMPEGAWAI", limit: 100
    t.varchar "TEMPATLAHIR", limit: 50
    t.date "TGLLAHIR"
    t.varchar "JENISKELAMIN", limit: 1
    t.varchar "AGAMA", limit: 25
    t.varchar "KDKAWIN", limit: 25
    t.char "JUMLAHANAK", limit: 2
    t.varchar "ALAMAT", limit: 225
    t.varchar "IBU_KANDUNG", limit: 100
    t.varchar "NOTELP", limit: 50
    t.varchar "EMAIL", limit: 200
    t.varchar_max "NMREKBANK"
    t.date "TMTK1"
    t.date "TMTK2"
    t.date "TMTTETAP"
    t.varchar "MASA_KERJA", limit: 10
    t.date "PENSIUN"
  end

  create_table "DATASDMAKTIFITAS", primary_key: "KD_AKTIFITAS", id: :integer, force: :cascade do |t|
    t.varchar "PIN", limit: 6
    t.varchar "NIP", limit: 20
    t.varchar "NOREK", limit: 20
    t.varchar "IDPEGAWAI", limit: 25
    t.date "TGLINPUT"
    t.datetime "TGLAKTIFITAS"
    t.datetime "JAMMULAI"
    t.datetime "JAMSELESAI"
    t.varchar "AKTIFITAS", limit: 250
    t.float "WAKTUEFEKTIF"
    t.float "VOLUME"
    t.float "CAPAIAN"
    t.varchar "KETERANGAN", limit: 250
    t.varchar "STATUS", limit: 250
  end

  create_table "DATASDMDETAIL", id: false, force: :cascade do |t|
    t.varchar "PIN", limit: 6
    t.varchar "NIP", limit: 20
    t.varchar "IDPEGAWAI", limit: 30
    t.varchar "NOREK", limit: 30
    t.varchar "KTP", limit: 30
    t.varchar "NPWP", limit: 30
    t.varchar "BPJS", limit: 30
    t.varchar "KD_STATUS", limit: 6
    t.varchar "KD_RUANGAN", limit: 6
    t.varchar "KD_JABATAN", limit: 6
    t.varchar "KD_PEJABAT", limit: 6
    t.varchar "NIKNONPNS", limit: 200
    t.integer "KD_JABATANREMUN"
    t.varchar "NOKK", limit: 50
    t.varchar "BPJSTK", limit: 50
    t.varchar "NOKONTRAK", limit: 100
    t.datetime "TGLKONTRAK"
  end

  create_table "DATASDMKESEHATAN", id: false, force: :cascade do |t|
    t.varchar "PIN", limit: 6
    t.varchar "NIP", limit: 20
    t.varchar "IDPEGAWAI", limit: 30
    t.varchar "NOREK", limit: 30
    t.varchar "NO_SIP", limit: 200
    t.date "TERBIT_SIP"
    t.date "BERLAKU_SIP"
    t.varchar "NO_STR", limit: 200
    t.date "TERBIT_STR"
    t.date "BERLAKU_STR"
  end

  create_table "DATASDMPENDIDIKAN", id: false, force: :cascade do |t|
    t.varchar "PIN", limit: 6
    t.varchar "NIP", limit: 20
    t.varchar "IDPEGAWAI", limit: 30
    t.varchar "NOREK", limit: 30
    t.varchar "THN_LULUS", limit: 20
    t.varchar "NO_IJAZAH", limit: 100
    t.varchar "NAMA_SEKOLAH", limit: 100
    t.varchar "PENDIDIKAN", limit: 100
    t.varchar "STUDI", limit: 100
  end

  create_table "DATASDMPNS", id: false, force: :cascade do |t|
    t.varchar "PIN", limit: 6
    t.varchar "NIP", limit: 20
    t.varchar "IDPEGAWAI", limit: 30
    t.varchar "NOREK", limit: 30
    t.varchar "NIK", limit: 30
    t.varchar "NRK", limit: 10
    t.varchar "PANGKAT", limit: 50
    t.varchar "GOLONGANRUANG", limit: 6
    t.varchar "FUNGSIONAL", limit: 50
    t.varchar "NOKARPEG", limit: 50
    t.varchar "NOKARSU", limit: 50
    t.date "TMTPNS"
    t.date "TMTCPNS"
    t.varchar "GOLONGAN", limit: 6
    t.date "TMTPANGKAT"
    t.date "TGLPANGKATLANJUT"
    t.varchar "KD_JABATAN", limit: 6
  end

  create_table "DATASDMSTRUKTUR", id: false, force: :cascade do |t|
    t.varchar "PIN", limit: 6
    t.varchar "KD_ATASANLANGSUNG", limit: 20
    t.varchar "KD_ATASAN", limit: 20
    t.varchar "KD_JABATAN", limit: 20
    t.integer "KD_ATASAN2"
  end

  create_table "DATASHIFT", id: false, force: :cascade do |t|
    t.varchar "PIN", limit: 4
    t.varchar "NMPEGAWAI", limit: 250
    t.varchar "NOREK", limit: 12
    t.varchar "NMREKBANK", limit: 250
    t.varchar "KD_RUANGAN", limit: 10
    t.varchar "STATUSPEGAWAI", limit: 50
    t.float "JUMLAH"
    t.float "POTONGAN"
    t.float "PPH21"
    t.float "DITERIMA"
    t.varchar "BATCHPERIODE", limit: 6
    t.varchar "DATAPERIODE", limit: 50
    t.datetime "TGLUPDATE"
  end

  create_table "DATASTRUKTURPEGAWAI", id: false, force: :cascade do |t|
    t.varchar "PIN", limit: 4
    t.varchar "NMPEGAWAI", limit: 200
    t.varchar "KD_STATUS", limit: 20
    t.varchar "KD_RUANGAN", limit: 20
    t.varchar "KD_JABATAN", limit: 20
    t.datetime "PERIODE"
    t.varchar "BATCHPERIODE", limit: 6
  end

  create_table "DaftarPoli", id: false, force: :cascade do |t|
    t.char "KdRuangan", limit: 3
    t.varchar "NamaRuangan", limit: 50
    t.varchar "GambarPoli", limit: 50
    t.integer "StatusEnabled", limit: 1
    t.char "KdSubInstalasi", limit: 3
    t.varchar "KdPoliBPJS", limit: 6
    t.char "KdInstalasi", limit: 2
  end

  create_table "DaftarPoliJKN", id: false, force: :cascade do |t|
    t.char "KdRuangan", limit: 3
    t.varchar "NamaPoli", limit: 50
    t.varchar "NmSubSpesialis", limit: 50
    t.varchar "KdSubSpesialis", limit: 50
    t.varchar "kdpoli", limit: 50
  end

  create_table "DaftarPoliMappingBpjs", id: false, force: :cascade do |t|
    t.char "KdRuangan", limit: 3
    t.varchar "NamaRuangan", limit: 50
    t.varchar "GambarPoli", limit: 50
    t.integer "StatusEnabled", limit: 1
    t.char "KdSubInstalasi", limit: 3
    t.varchar "KdPoliBPJS", limit: 6
    t.char "KdInstalasi", limit: 2
  end

  create_table "DaftarTMNonTanggungan", id: false, force: :cascade do |t|
    t.char "KdKelompokPasien", limit: 2, null: false
    t.char "IdPenjamin", limit: 10, null: false
    t.char "KdPelayananRS", limit: 6, null: false
    t.real "PersenTanggunganTM", null: false
    t.real "PersenTanggunganRSTM", null: false
  end

  create_table "DaftrarRSMoU", id: false, force: :cascade do |t|
    t.varchar "KdRS", limit: 50
    t.varchar "NamaRS", limit: 50
  end

  create_table "DataCurrentPegawai", id: false, force: :cascade do |t|
    t.char "IdPegawai", limit: 10, null: false
    t.varchar "KdPangkat", limit: 2
    t.varchar "KdJabatan", limit: 5
    t.varchar "KdEselon", limit: 2
    t.varchar "NIP", limit: 30
    t.char "KdStatus", limit: 2, null: false
    t.varchar "KdKualifikasiJurusan", limit: 4
    t.char "KdStatusPerkawinan", limit: 2
    t.char "KdSuku", limit: 2
    t.char "KdAgama", limit: 2
    t.char "KdGolonganDarah", limit: 2
    t.char "StatusRhesus", limit: 1
    t.char "KdTypePegawai", limit: 2
    t.varchar "KdDetailKategoryPegawai", limit: 2
    t.integer "KdNegara", limit: 1
    t.varchar "Negara", limit: 50
    t.char "KdRuanganKerja", limit: 3
    t.char "KdPegawaiAtasan", limit: 10
    t.varchar "KdJenjang", limit: 5
    t.varchar "PathFoto", limit: 250
    t.char "NRK", limit: 6
    t.integer "StatusRangkapJabatan", limit: 1
    t.integer "KdJnsPolaKerja", limit: 1
  end

  create_table "DataPegawai", id: false, force: :cascade do |t|
    t.char "IdPegawai", limit: 10, null: false
    t.char "KdJenisPegawai", limit: 3, null: false
    t.varchar "KdTitle", limit: 20
    t.varchar "NamaLengkap", limit: 50, null: false
    t.varchar "NamaKeluarga", limit: 50
    t.varchar "NamaPanggilan", limit: 50
    t.char "JenisKelamin", limit: 1
    t.varchar "TempatLahir", limit: 50
    t.datetime "TglLahir"
    t.datetime "TglMasuk"
    t.datetime "TglKeluar"
    t.varchar "KodeExternal", limit: 50
    t.varchar_max "SIP"
    t.index ["IdPegawai", "KdJenisPegawai", "NamaLengkap"], name: "IX_DataPegawai"
  end

  create_table "DataStokBarangMedis", id: false, force: :cascade do |t|
    t.char "NoClosing", limit: 10, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.varchar "KdBarang", limit: 9, null: false
    t.char "KdAsal", limit: 2, null: false
    t.char "NoTerima", limit: 10, null: false
    t.float "JmlStokSystem"
    t.float "JmlStokReal"
    t.money "HargaNetto1", precision: 19, scale: 4, null: false
    t.money "HargaNetto2", precision: 19, scale: 4, null: false
    t.real "Discount", null: false
  end

  create_table "DataStokBarangMedisTemp2", primary_key: ["KdRuangan", "KdBarang", "KdAsal"], force: :cascade do |t|
    t.char "TglInput", limit: 9, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.varchar "KdBarang", limit: 9, null: false
    t.char "KdAsal", limit: 2, null: false
    t.char "NoTerima", limit: 10, null: false
    t.float "JmlStokSystem"
    t.float "JmlStokReal"
    t.money "HargaNetto1", precision: 19, scale: 4
    t.money "HargaNetto2", precision: 19, scale: 4
    t.real "Discount", null: false
    t.integer "Status"
    t.datetime "ExpiredDate"
  end

  create_table "DetailBackupUpdatingBiayaPelayananOA", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.datetime "TglPelayanan", null: false
    t.varchar "KdBarang", limit: 9, null: false
    t.char "KdAsal", limit: 2, null: false
    t.char "KdKomponen", limit: 2, null: false
    t.money "HargaSatuan", precision: 19, scale: 4, null: false
    t.money "JmlDiscount", precision: 19, scale: 4, null: false
  end

  create_table "DetailBarangKeluar", id: false, force: :cascade do |t|
    t.char "NoKirim", limit: 10, null: false
    t.varchar "KdBarang", limit: 9, null: false
    t.char "KdAsal", limit: 2, null: false
    t.decimal "JmlKirim", precision: 9, scale: 2, null: false
    t.money "HargaNetto", precision: 19, scale: 4, null: false
    t.money "HargaJual", precision: 19, scale: 4, null: false
    t.money "Discount", precision: 19, scale: 4, null: false
    t.money "Ppn", precision: 19, scale: 4, null: false
    t.char "NoClosing", limit: 10
    t.char "NoTerima", limit: 10, null: false
    t.float "JmlKeluar"
  end

  create_table "DetailBarangKeluarVerifikasi", primary_key: ["NoKirim", "KdBarang", "KdAsal", "NoTerima"], force: :cascade do |t|
    t.char "NoKirim", limit: 10, null: false
    t.varchar "KdBarang", limit: 9, null: false
    t.char "KdAsal", limit: 2, null: false
    t.decimal "JmlKirim", precision: 9, scale: 2, null: false
    t.money "HargaNetto", precision: 19, scale: 4, null: false
    t.money "HargaJual", precision: 19, scale: 4, null: false
    t.money "Discount", precision: 19, scale: 4, null: false
    t.money "Ppn", precision: 19, scale: 4, null: false
    t.char "NoClosing", limit: 10
    t.char "NoTerima", limit: 10, null: false
    t.float "JmlKeluar"
  end

  create_table "DetailBiayaPelayanan", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "KdSubInstalasi", limit: 3, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "KdPelayananRS", limit: 6, null: false
    t.char "KdKelas", limit: 2, null: false
    t.char "StatusCITO", limit: 1, null: false
    t.integer "Tarif", null: false
    t.integer "TarifCito", null: false
    t.integer "JmlPelayanan", null: false
    t.datetime "TglPelayanan", null: false
    t.char "NoLab_Rad", limit: 10
    t.char "IdPegawai", limit: 10, null: false
    t.char "NoStruk", limit: 10
    t.char "StatusAPBD", limit: 2, null: false
    t.char "KdJenisTarif", limit: 2, null: false
    t.char "IdPenjamin", limit: 10, null: false
    t.char "KdKelasPenjamin", limit: 2, null: false
    t.money "TarifKelasPenjamin", precision: 19, scale: 4, null: false
    t.money "JmlHutangPenjamin", precision: 19, scale: 4, null: false
    t.money "JmlTanggunganRS", precision: 19, scale: 4, null: false
    t.money "JmlPembebasan", precision: 19, scale: 4, null: false
    t.varchar "KdPaket", limit: 3
    t.char "IdPegawai2", limit: 10
    t.char "IdUser", limit: 10, null: false
    t.char "IdPegawai3", limit: 10
    t.char "KdRuanganAsal", limit: 3
    t.char "NoBKMClaim", limit: 10
    t.integer "BlnTglPelayanan"
    t.integer "ThnTglPelayanan"
    t.integer "JmlPasien", limit: 1
    t.integer "HariTglPelayanan"
    t.varchar "HariTglStruk", limit: 2
    t.varchar "BlnTglStruk", limit: 2
    t.varchar "ThnTglStruk", limit: 2
    t.money "JmlHarusDiBayar", precision: 19, scale: 4
    t.integer "TotalBiaya"
    t.money "TotalHutangPenjamin", precision: 19, scale: 4
    t.money "TotalTanggunganRS", precision: 19, scale: 4
    t.money "TotalPembebasan", precision: 19, scale: 4
    t.money "TotalHarusDiBayar", precision: 19, scale: 4
    t.char "NoBKMClaimTRS", limit: 10
    t.char "NoPostingJurnal", limit: 10
    t.index ["NoPendaftaran", "KdPelayananRS"], name: "IX_DetailBiayaPelayanan", order: { KdPelayananRS: :desc }
  end

  create_table "DetailBiayaPelayananNew", id: false, force: :cascade do |t|
    t.integer "ID_Tindakan"
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "KdSubInstalasi", limit: 3, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "KdPelayananRS", limit: 6, null: false
    t.char "KdKelas", limit: 2, null: false
    t.char "StatusCITO", limit: 1, null: false
    t.integer "Tarif", null: false
    t.integer "TarifCito", null: false
    t.integer "JmlPelayanan", null: false
    t.datetime "TglPelayanan", null: false
    t.char "NoLab_Rad", limit: 10
    t.char "IdPegawai", limit: 10, null: false
    t.char "NoStruk", limit: 10
    t.char "StatusAPBD", limit: 2, null: false
    t.char "KdJenisTarif", limit: 2, null: false
    t.char "IdPenjamin", limit: 10, null: false
    t.char "KdKelasPenjamin", limit: 2, null: false
    t.money "TarifKelasPenjamin", precision: 19, scale: 4, null: false
    t.money "JmlHutangPenjamin", precision: 19, scale: 4, null: false
    t.money "JmlTanggunganRS", precision: 19, scale: 4, null: false
    t.money "JmlPembebasan", precision: 19, scale: 4, null: false
    t.varchar "KdPaket", limit: 3
    t.char "IdPegawai2", limit: 10
    t.char "IdUser", limit: 10, null: false
    t.char "IdPegawai3", limit: 10
    t.char "KdRuanganAsal", limit: 3
    t.char "NoBKMClaim", limit: 10
    t.integer "BlnTglPelayanan"
    t.integer "ThnTglPelayanan"
    t.integer "JmlPasien", limit: 1
    t.integer "HariTglPelayanan"
    t.varchar "HariTglStruk", limit: 2
    t.varchar "BlnTglStruk", limit: 2
    t.varchar "ThnTglStruk", limit: 2
    t.money "JmlHarusDiBayar", precision: 19, scale: 4
    t.integer "TotalBiaya"
    t.money "TotalHutangPenjamin", precision: 19, scale: 4
    t.money "TotalTanggunganRS", precision: 19, scale: 4
    t.money "TotalPembebasan", precision: 19, scale: 4
    t.money "TotalHarusDiBayar", precision: 19, scale: 4
    t.char "NoBKMClaimTRS", limit: 10
    t.char "NoPostingJurnal", limit: 10
    t.index ["ID_Tindakan", "NoPendaftaran", "KdPelayananRS"], name: "IX_DetailBiayaPelayananNew"
    t.index ["ID_Tindakan", "NoPendaftaran"], name: "NonClusteredIndex-20230130-130951"
  end

  create_table "DetailEmergency", primary_key: "No", id: :integer, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10
    t.char "KdEmergency", limit: 2
    t.datetime "TglInput"
  end

  create_table "DetailGolonganBarang", primary_key: "KdDetailGolBarang", id: { type: :char, limit: 3 }, force: :cascade do |t|
    t.varchar "DetailGolBarang", limit: 50, null: false
    t.integer "QDetailGolBarang", limit: 1, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1
  end

  create_table "DetailHargaNettoBarang", id: false, force: :cascade do |t|
    t.varchar "KdBarang", limit: 9, null: false
    t.char "KdAsal", limit: 2, null: false
    t.char "Satuan", limit: 1, null: false
    t.money "HargaNetto1", precision: 19, scale: 4, null: false
    t.money "HargaNetto2", precision: 19, scale: 4, null: false
  end

  create_table "DetailHasilPemeriksaan", primary_key: "NoLab_Rad", id: { type: :char, limit: 10 }, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.varchar "BahanPemeriksaan", limit: 100, null: false
    t.datetime "TglPengambilan", null: false
    t.datetime "TglPemeriksaan", null: false
  end

  create_table "DetailHasilPeriksaLaboratoryPK", primary_key: ["NoLaboratorium", "KdPelayananRS", "KdDetailPeriksa"], force: :cascade do |t|
    t.char "NoLaboratorium", limit: 10, null: false
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.char "KdPelayananRS", limit: 6, null: false
    t.varchar "KdDetailPeriksa", limit: 5, null: false
    t.varchar "HasilPeriksa", limit: 200
    t.char "KdLaboratory", limit: 3
    t.char "kdNilaiNormal", limit: 4
    t.varchar "HasilPeriksaLuar", limit: 200
    t.char "IdUser", limit: 10
  end

  create_table "DetailHasilPeriksaLaboratoryPKTemp", id: false, force: :cascade do |t|
    t.char "NoLaboratorium", limit: 10, null: false
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.char "KdPelayananRS", limit: 6, null: false
    t.varchar "KdDetailPeriksa", limit: 5, null: false
    t.varchar "HasilPeriksa", limit: 200
    t.char "KdLaboratory", limit: 3
    t.char "kdNilaiNormal", limit: 4
    t.varchar "HasilPeriksaLuar", limit: 200
  end

  create_table "DetailHasilPeriksaRadiologyNew", id: false, force: :cascade do |t|
    t.char "KodeExternal", limit: 10, null: false
    t.char "NoRadiology", limit: 10, null: false
    t.char "NoPendaftaran", limit: 10
    t.char "KdPelayananRs", limit: 6, null: false
    t.varchar "HasilPeriksa"
  end

  create_table "DetailJenisBarang", primary_key: "QDetailJenisBarang", id: { type: :integer, limit: 2 }, force: :cascade do |t|
    t.varchar "KdDetailJenisBarang", limit: 5, null: false
    t.char "KdJenisBarang", limit: 2, null: false
    t.varchar "DetailJenisBarang", limit: 150, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 20
    t.integer "StatusEnabled", limit: 1, null: false
    t.char "KdJenisAset", limit: 2
  end

  create_table "DetailJenisJasaPelayanan", primary_key: "QDetailJenisJasaPelayanan", id: { type: :integer, limit: 1 }, force: :cascade do |t|
    t.char "KdJenisJasaPelayanan", limit: 2, null: false
    t.char "KdDetailJenisJasaPelayanan", limit: 2, null: false
    t.varchar "DetailJenisJasaPelayanan", limit: 50, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "DetailKategoryPegawai", id: false, force: :cascade do |t|
    t.varchar "KdDetailKategoryPegawai", limit: 2, null: false
    t.varchar "DetailKategoryPegawai", limit: 50, null: false
    t.varchar "ReportDisplay", limit: 50
    t.char "KdKategoryPegawai", limit: 1
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 30
    t.integer "StatusEnabled", limit: 1
  end

  create_table "DetailKelasPelayanan", id: false, force: :cascade do |t|
    t.char "KdDetailJenisJasaPelayanan", limit: 2, null: false
    t.char "KdKelas", limit: 2, null: false
  end

  create_table "DetailOrderPelayananEMRLab", id: false, force: :cascade do |t|
    t.char "NoOrder", limit: 10, null: false
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.char "KdPelayananRS", limit: 6, null: false
    t.integer "JmlPelayanan", null: false
    t.char "IdDokterOrder", limit: 10, null: false
    t.char "StatusCito", limit: 1, null: false
    t.char "NoLab_Rad", limit: 10
    t.char "KdRuanganOrder", limit: 3, null: false
    t.datetime "TglOrder", null: false
    t.char "KdRuanganTujuan", limit: 3, null: false
    t.varchar "KeteranganLainnya", limit: 200
    t.char "IdUser", limit: 10, null: false
  end

  create_table "DetailOrderPelayananOA", primary_key: ["NoOrder", "NoPendaftaran", "KdBarang", "ResepKe", "KdJenisObat"], force: :cascade do |t|
    t.char "NoOrder", limit: 10, null: false
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.char "NoPakai", limit: 10
    t.char "IdDokterOrder", limit: 10
    t.char "StatusCito", limit: 1, null: false
    t.varchar "KdBarang", limit: 9, null: false
    t.real "JmlBarang", default: 0.0, null: false
    t.varchar "NoResep", limit: 15, null: false
    t.integer "ResepKe", limit: 1, null: false
    t.varchar "AturanPakai", limit: 500
    t.varchar "KeteranganPakai", limit: 50
    t.datetime "TglResep"
    t.char "NoRiwayat", limit: 10
    t.char "NoRetur", limit: 10
    t.integer "JmlRetur", default: 0, null: false
    t.char "KdPelayananRSUsed", limit: 6
    t.varchar "KeteranganLainnya", limit: 200
    t.char "KdJenisObat", limit: 2, null: false
    t.varchar "KeteranganLainnya2", limit: 200
    t.varchar "StatusOrder", limit: 20
    t.money "harga", precision: 19, scale: 4
    t.varchar_max "keteranganracik"
    t.varchar_max "dosis"
    t.varchar "obatpulang", limit: 50
  end

  create_table "DetailOrderPelayananOANew", primary_key: ["NoOrder", "NoPendaftaran", "KdBarang", "ResepKe", "KdJenisObat"], force: :cascade do |t|
    t.char "NoOrder", limit: 10, null: false
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.char "NoPakai", limit: 10
    t.char "IdDokterOrder", limit: 10
    t.char "StatusCito", limit: 1, null: false
    t.varchar "KdBarang", limit: 9, null: false
    t.real "JmlBarang", null: false
    t.varchar "NoResep", limit: 15, null: false
    t.integer "ResepKe", limit: 1, null: false
    t.varchar "AturanPakai", limit: 500
    t.varchar "KeteranganPakai", limit: 50
    t.datetime "TglResep"
    t.char "NoRiwayat", limit: 10
    t.char "NoRetur", limit: 10
    t.integer "JmlRetur", null: false
    t.char "KdPelayananRSUsed", limit: 6
    t.varchar "KeteranganLainnya", limit: 200
    t.char "KdJenisObat", limit: 2, null: false
    t.varchar "KeteranganLainnya2", limit: 200
    t.varchar "StatusOrder", limit: 20
    t.money "harga", precision: 19, scale: 4
    t.varchar_max "keteranganracik"
    t.varchar_max "dosis"
    t.varchar "obatpulang", limit: 50
    t.integer "ID_Resep"
    t.index ["ID_Resep"], name: "index_idresep", unique: true
    t.index ["NoOrder", "NoPendaftaran"], name: "NonClusteredIndex-20230130-123201"
    t.index ["NoOrder", "NoPendaftaran"], name: "NonClusteredIndex-20231013-101328"
    t.index ["TglResep"], name: "NonClusteredIndex-20231013-111735", order: :desc
  end

  create_table "DetailOrderPelayananOANew_2308240220", id: false, force: :cascade do |t|
    t.char "NoOrder", limit: 10, null: false
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.char "NoPakai", limit: 10
    t.char "IdDokterOrder", limit: 10
    t.char "StatusCito", limit: 1, null: false
    t.varchar "KdBarang", limit: 9, null: false
    t.real "JmlBarang", null: false
    t.varchar "NoResep", limit: 15, null: false
    t.integer "ResepKe", limit: 1, null: false
    t.varchar "AturanPakai", limit: 500
    t.varchar "KeteranganPakai", limit: 50
    t.datetime "TglResep"
    t.char "NoRiwayat", limit: 10
    t.char "NoRetur", limit: 10
    t.integer "JmlRetur", null: false
    t.char "KdPelayananRSUsed", limit: 6
    t.varchar "KeteranganLainnya", limit: 200
    t.char "KdJenisObat", limit: 2, null: false
    t.varchar "KeteranganLainnya2", limit: 200
    t.varchar "StatusOrder", limit: 20
    t.money "harga", precision: 19, scale: 4
    t.varchar_max "keteranganracik"
    t.varchar_max "dosis"
    t.varchar "obatpulang", limit: 50
    t.integer "ID_Resep"
  end

  create_table "DetailOrderPelayananOAracikan", id: false, force: :cascade do |t|
    t.char "NoOrder", limit: 10, null: false
    t.char "NoPendaftaran", limit: 10, null: false
    t.real "KebutuhanML", null: false
    t.real "KebutuhanTB"
    t.char "KdJenisObat", limit: 2, null: false
    t.integer "ResepKe", limit: 1, null: false
    t.varchar "kdBarang", limit: 9, null: false
    t.real "JumlahReal", null: false
    t.integer "Jumlah"
    t.integer "qtyRacikan", null: false
  end

  create_table "DetailOrderRuangan", id: false, force: :cascade do |t|
    t.char "NoOrder", limit: 10, null: false
    t.varchar "KdBarang", limit: 9, null: false
    t.float "JmlOrder", null: false
    t.char "NoKirim", limit: 10
    t.varchar "idstatus", limit: 1
  end

  create_table "DetailOrderRuanganTemp", id: false, force: :cascade do |t|
    t.char "NoOrder", limit: 10
    t.varchar_max "NamaBarang"
    t.varchar "KdBarang", limit: 9, null: false
    t.float "JmlOrder", null: false
    t.char "NoKirim", limit: 10
    t.varchar "idstatus", limit: 1
    t.varchar "IdUser", limit: 10, null: false
    t.char "KdRuanganOrder", limit: 3, null: false
    t.char "StokRuanganOrder", limit: 7
    t.char "StokRuanganTujuan", limit: 7
    t.varchar_max "NamaPemesan"
    t.varchar "StatusVerif", limit: 50
  end

  create_table "DetailPasien", id: false, force: :cascade do |t|
    t.char "NoCM", limit: 15, null: false
    t.varchar "NamaKeluarga", limit: 50
    t.varchar "Warganegara", limit: 50
    t.varchar "GolDarah", limit: 2
    t.char "Rhesus", limit: 1
    t.varchar "StatusNikah", limit: 10
    t.varchar "Pekerjaan", limit: 30
    t.varchar "Agama", limit: 100
    t.varchar "Suku", limit: 20
    t.varchar "Pendidikan", limit: 25
    t.varchar "NamaAyah", limit: 30
    t.varchar "NamaIbu", limit: 30
    t.varchar "NamaSuamiIstri", limit: 30
    t.varchar "BahasaDigunakan", limit: 500
    t.varchar "BbLahir", limit: 50
    t.varchar "PanjangLahir", limit: 50
  end

  create_table "DetailPegawai", id: false, force: :cascade do |t|
    t.char "IdPegawai", limit: 10, null: false
    t.varchar "Hobby", limit: 100
    t.varchar "TinggiBadan", limit: 20
    t.varchar "BeratBadan", limit: 20
    t.varchar "JenisRambut", limit: 20
    t.varchar "BentukMuka", limit: 50
    t.varchar "WarnaKulit", limit: 50
    t.varchar "CiriCiriKhas", limit: 300
    t.varchar "CacatTubuh", limit: 200
  end

  create_table "DetailPelayananLaboratororium", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "KdPelayananRS", limit: 6, null: false
    t.datetime "TglPelayanan", null: false
    t.char "NoLab_Rad", limit: 10, null: false
    t.char "KdLaboratory", limit: 3, null: false
  end

  create_table "DetailPelayananMedik", id: false, force: :cascade do |t|
    t.char "KdJnsPelayanan", limit: 3, null: false
    t.char "KdRuangan", limit: 3, null: false
  end

  create_table "DetailPemakaianAlkes", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "KdSubInstalasi", limit: 3, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "KdKelas", limit: 2, default: "00", null: false
    t.varchar "KdBarang", limit: 9, null: false
    t.char "KdAsal", limit: 2, default: "01", null: false
    t.decimal "JmlBarang", precision: 9, scale: 2, null: false
    t.money "HargaSatuan", precision: 19, scale: 4, null: false
    t.datetime "TglPelayanan", null: false
    t.char "NoLab_Rad", limit: 10
    t.char "NoStruk", limit: 10
    t.char "IdPegawai", limit: 10, null: false
    t.char "SatuanJml", limit: 1, null: false
    t.char "IdPenjamin", limit: 10, null: false
    t.char "KdKelasPenjamin", limit: 2, null: false
    t.money "TarifKelasPenjamin", precision: 19, scale: 4, null: false
    t.money "JmlHutangPenjamin", precision: 19, scale: 4, null: false
    t.money "JmlTanggunganRS", precision: 19, scale: 4, null: false
    t.money "JmlPembebasan", precision: 19, scale: 4, null: false
    t.money "HargaBeli", precision: 19, scale: 4, default: 0.0, null: false
    t.char "IdPegawai2", limit: 10
    t.char "IdUser", limit: 10, null: false
    t.varchar "KdPaket", limit: 3
    t.char "KdJenisObat", limit: 2
    t.integer "JmlService", default: 0, null: false
    t.money "TarifService", precision: 19, scale: 4, default: 0.0, null: false
    t.varchar "NoResep", limit: 15
    t.money "BiayaAdministrasi", precision: 19, scale: 4, default: 0.0, null: false
    t.char "StatusStok", limit: 1
    t.char "KdRuanganAsal", limit: 3
    t.char "KdPelayananRS", limit: 6
    t.char "NoBKMClaim", limit: 10
    t.integer "HariTglPelayanan"
    t.integer "BlnTglPelayanan"
    t.integer "ThnTglPelayanan"
    t.varchar "HariTglStruk", limit: 2
    t.varchar "BlnTglStruk", limit: 2
    t.varchar "ThnTglStruk", limit: 2
    t.decimal "JmlHarusDiBayar", precision: 36, scale: 6
    t.decimal "TotalBiaya", precision: 31, scale: 6
    t.float "TotalHutangPenjamin"
    t.float "TotalTanggunganRS"
    t.float "TotalPembebasan"
    t.char "NoTerima", limit: 10, null: false
    t.char "NoPostingJurnal", limit: 10
    t.integer "StatusTanggung", limit: 1
  end

  create_table "DetailPemakaianAlkesDeleted", primary_key: ["NoPendaftaran", "KdRuangan", "KdBarang", "KdAsal", "TglPelayanan", "SatuanJml", "NoTerima"], force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "KdSubInstalasi", limit: 3, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "KdKelas", limit: 2, null: false
    t.varchar "KdBarang", limit: 9, null: false
    t.char "KdAsal", limit: 2, null: false
    t.decimal "JmlBarang", precision: 9, scale: 2, null: false
    t.money "HargaSatuan", precision: 19, scale: 4, null: false
    t.datetime "TglPelayanan", null: false
    t.char "NoLab_Rad", limit: 10
    t.char "NoStruk", limit: 10
    t.char "IdPegawai", limit: 10, null: false
    t.char "SatuanJml", limit: 1, null: false
    t.char "IdPenjamin", limit: 10, null: false
    t.char "KdKelasPenjamin", limit: 2, null: false
    t.money "TarifKelasPenjamin", precision: 19, scale: 4, null: false
    t.money "JmlHutangPenjamin", precision: 19, scale: 4, null: false
    t.money "JmlTanggunganRS", precision: 19, scale: 4, null: false
    t.money "JmlPembebasan", precision: 19, scale: 4, null: false
    t.money "HargaBeli", precision: 19, scale: 4, null: false
    t.char "IdPegawai2", limit: 10
    t.char "IdUser", limit: 10, null: false
    t.varchar "KdPaket", limit: 3
    t.char "KdJenisObat", limit: 2
    t.integer "JmlService", null: false
    t.money "TarifService", precision: 19, scale: 4, null: false
    t.varchar "NoResep", limit: 15
    t.money "BiayaAdministrasi", precision: 19, scale: 4, null: false
    t.char "StatusStok", limit: 1
    t.char "KdRuanganAsal", limit: 3
    t.char "KdPelayananRS", limit: 6
    t.char "NoBKMClaim", limit: 10
    t.integer "HariTglPelayanan"
    t.integer "BlnTglPelayanan"
    t.integer "ThnTglPelayanan"
    t.varchar "HariTglStruk", limit: 2
    t.varchar "BlnTglStruk", limit: 2
    t.varchar "ThnTglStruk", limit: 2
    t.money "JmlHarusDiBayar", precision: 19, scale: 4
    t.decimal "TotalBiaya", precision: 31, scale: 6
    t.float "TotalHutangPenjamin"
    t.float "TotalTanggunganRS"
    t.float "TotalPembebasan"
    t.char "NoTerima", limit: 10, null: false
    t.char "NoPostingJurnal", limit: 10
    t.char "NoPostingJurnalDel", limit: 10
  end

  create_table "DetailPemakaianAlkesNew", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "KdSubInstalasi", limit: 3
    t.char "KdRuangan", limit: 3
    t.char "KdKelas", limit: 2
    t.varchar "KdBarang", limit: 9
    t.char "KdAsal", limit: 2
    t.decimal "JmlBarang", precision: 9, scale: 2
    t.money "HargaSatuan", precision: 19, scale: 4
    t.datetime "TglPelayanan"
    t.char "NoLab_Rad", limit: 10
    t.char "NoStruk", limit: 10
    t.char "IdPegawai", limit: 10
    t.char "SatuanJml", limit: 1
    t.char "IdPenjamin", limit: 10
    t.char "KdKelasPenjamin", limit: 2
    t.money "TarifKelasPenjamin", precision: 19, scale: 4
    t.money "JmlHutangPenjamin", precision: 19, scale: 4
    t.money "JmlTanggunganRS", precision: 19, scale: 4
    t.money "JmlPembebasan", precision: 19, scale: 4
    t.money "HargaBeli", precision: 19, scale: 4
    t.char "IdPegawai2", limit: 10
    t.char "IdUser", limit: 10
    t.varchar "KdPaket", limit: 3
    t.char "KdJenisObat", limit: 2
    t.integer "JmlService"
    t.money "TarifService", precision: 19, scale: 4
    t.varchar "NoResep", limit: 15
    t.money "BiayaAdministrasi", precision: 19, scale: 4
    t.char "StatusStok", limit: 1
    t.char "KdRuanganAsal", limit: 3
    t.char "KdPelayananRS", limit: 6
    t.char "NoBKMClaim", limit: 10
    t.integer "HariTglPelayanan"
    t.integer "BlnTglPelayanan"
    t.integer "ThnTglPelayanan"
    t.varchar "HariTglStruk", limit: 2
    t.varchar "BlnTglStruk", limit: 2
    t.varchar "ThnTglStruk", limit: 2
    t.decimal "JmlHarusDiBayar", precision: 36, scale: 6
    t.decimal "TotalBiaya", precision: 31, scale: 6
    t.float "TotalHutangPenjamin"
    t.float "TotalTanggunganRS"
    t.float "TotalPembebasan"
    t.char "NoTerima", limit: 10
    t.char "NoPostingJurnal", limit: 10
    t.integer "StatusTanggung", limit: 1
    t.integer "ID_Resep"
    t.index ["NoPendaftaran", "KdSubInstalasi", "KdRuangan", "KdKelas", "KdBarang", "KdAsal", "NoStruk", "KdKelasPenjamin", "NoResep", "KdRuanganAsal", "KdPelayananRS", "NoBKMClaim", "NoTerima", "ID_Resep"], name: "IX-DetailPemakaianAlkesNew"
  end

  create_table "DetailPemakaianAlkesNew_2308240220", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "KdSubInstalasi", limit: 3
    t.char "KdRuangan", limit: 3
    t.char "KdKelas", limit: 2
    t.varchar "KdBarang", limit: 9
    t.char "KdAsal", limit: 2
    t.decimal "JmlBarang", precision: 9, scale: 2
    t.money "HargaSatuan", precision: 19, scale: 4
    t.datetime "TglPelayanan"
    t.char "NoLab_Rad", limit: 10
    t.char "NoStruk", limit: 10
    t.char "IdPegawai", limit: 10
    t.char "SatuanJml", limit: 1
    t.char "IdPenjamin", limit: 10
    t.char "KdKelasPenjamin", limit: 2
    t.money "TarifKelasPenjamin", precision: 19, scale: 4
    t.money "JmlHutangPenjamin", precision: 19, scale: 4
    t.money "JmlTanggunganRS", precision: 19, scale: 4
    t.money "JmlPembebasan", precision: 19, scale: 4
    t.money "HargaBeli", precision: 19, scale: 4
    t.char "IdPegawai2", limit: 10
    t.char "IdUser", limit: 10
    t.varchar "KdPaket", limit: 3
    t.char "KdJenisObat", limit: 2
    t.integer "JmlService"
    t.money "TarifService", precision: 19, scale: 4
    t.varchar "NoResep", limit: 15
    t.money "BiayaAdministrasi", precision: 19, scale: 4
    t.char "StatusStok", limit: 1
    t.char "KdRuanganAsal", limit: 3
    t.char "KdPelayananRS", limit: 6
    t.char "NoBKMClaim", limit: 10
    t.integer "HariTglPelayanan"
    t.integer "BlnTglPelayanan"
    t.integer "ThnTglPelayanan"
    t.varchar "HariTglStruk", limit: 2
    t.varchar "BlnTglStruk", limit: 2
    t.varchar "ThnTglStruk", limit: 2
    t.decimal "JmlHarusDiBayar", precision: 36, scale: 6
    t.decimal "TotalBiaya", precision: 31, scale: 6
    t.float "TotalHutangPenjamin"
    t.float "TotalTanggunganRS"
    t.float "TotalPembebasan"
    t.char "NoTerima", limit: 10
    t.char "NoPostingJurnal", limit: 10
    t.integer "StatusTanggung", limit: 1
    t.integer "ID_Resep"
  end

  create_table "DetailPemakaianAlkesObatKronis", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "KdSubInstalasi", limit: 3, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "KdKelas", limit: 2, null: false
    t.varchar "KdBarang", limit: 9, null: false
    t.char "KdAsal", limit: 2, null: false
    t.decimal "JmlBarang", precision: 9, scale: 2, null: false
    t.money "HargaSatuan", precision: 19, scale: 4, null: false
    t.datetime "TglPelayanan", null: false
    t.char "NoLab_Rad", limit: 10
    t.char "NoStruk", limit: 10
    t.char "IdPegawai", limit: 10, null: false
    t.char "SatuanJml", limit: 1, null: false
    t.char "IdPenjamin", limit: 10, null: false
    t.char "KdKelasPenjamin", limit: 2, null: false
    t.money "TarifKelasPenjamin", precision: 19, scale: 4, null: false
    t.money "JmlHutangPenjamin", precision: 19, scale: 4, null: false
    t.money "JmlTanggunganRS", precision: 19, scale: 4, null: false
    t.money "JmlPembebasan", precision: 19, scale: 4, null: false
    t.money "HargaBeli", precision: 19, scale: 4, null: false
    t.char "IdPegawai2", limit: 10
    t.char "IdUser", limit: 10, null: false
    t.varchar "KdPaket", limit: 3
    t.char "KdJenisObat", limit: 2
    t.integer "JmlService", null: false
    t.money "TarifService", precision: 19, scale: 4, null: false
    t.varchar "NoResep", limit: 15
    t.money "BiayaAdministrasi", precision: 19, scale: 4, null: false
    t.char "StatusStok", limit: 1
    t.char "KdRuanganAsal", limit: 3
    t.char "KdPelayananRS", limit: 6
    t.char "NoBKMClaim", limit: 10
    t.integer "HariTglPelayanan"
    t.integer "BlnTglPelayanan"
    t.integer "ThnTglPelayanan"
    t.varchar "HariTglStruk", limit: 2
    t.varchar "BlnTglStruk", limit: 2
    t.varchar "ThnTglStruk", limit: 2
    t.decimal "JmlHarusDiBayar", precision: 36, scale: 6
    t.decimal "TotalBiaya", precision: 31, scale: 6
    t.float "TotalHutangPenjamin"
    t.float "TotalTanggunganRS"
    t.float "TotalPembebasan"
    t.char "NoTerima", limit: 10, null: false
    t.char "NoPostingJurnal", limit: 10
    t.integer "StatusTanggung", limit: 1
  end

  create_table "DetailPemakaianAlkesUpdated", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "KdSubInstalasi", limit: 3, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "KdKelas", limit: 2, null: false
    t.varchar "KdBarang", limit: 9, null: false
    t.char "KdAsal", limit: 2, null: false
    t.decimal "JmlBarangLama", precision: 9, scale: 2, null: false
    t.decimal "JmlBarangBaru", precision: 9, scale: 2, null: false
    t.decimal "JmlBarangTambah", precision: 9, scale: 2, null: false
    t.money "HargaSatuan", precision: 19, scale: 4, null: false
    t.datetime "TglPelayanan", null: false
    t.char "NoStruk", limit: 10
    t.char "SatuanJml", limit: 1, null: false
    t.char "IdUser", limit: 10, null: false
    t.varchar "NoResep", limit: 15
    t.integer "resepKe"
  end

  create_table "DetailPemakaianAlkesUpdatedNew", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "KdSubInstalasi", limit: 3, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "KdKelas", limit: 2, null: false
    t.varchar "KdBarang", limit: 9, null: false
    t.char "KdAsal", limit: 2, null: false
    t.decimal "JmlBarangLama", precision: 9, scale: 2, null: false
    t.decimal "JmlBarangBaru", precision: 9, scale: 2, null: false
    t.decimal "JmlBarangTambah", precision: 9, scale: 2, null: false
    t.money "HargaSatuan", precision: 19, scale: 4, null: false
    t.datetime "TglPelayanan", null: false
    t.char "NoStruk", limit: 10
    t.char "SatuanJml", limit: 1, null: false
    t.char "IdUser", limit: 10, null: false
    t.varchar "NoResep", limit: 15
    t.integer "resepKe"
    t.integer "ID_Resep"
  end

  create_table "DetailPemeriksaan", id: false, force: :cascade do |t|
    t.varchar "KdDetailPeriksa", limit: 5, null: false
    t.varchar_max "NamaDetailPeriksa", null: false
    t.char "KdPelayananRS", limit: 6
    t.varchar "MemoHasilPeriksa", limit: 500
    t.char "KdSatuanHasil", limit: 2
    t.char "KdInstalasi", limit: 2, null: false
    t.integer "NoUrut", limit: 1
    t.integer "NoUrutDetail", limit: 1
    t.char "KdListPemeriksaan", limit: 3
  end

  create_table "DetailPemesananBarang", primary_key: ["NoOrder", "KdBarang", "KdAsal"], force: :cascade do |t|
    t.char "NoOrder", limit: 10, null: false
    t.varchar "KdBarang", limit: 9, null: false
    t.char "KdAsal", limit: 2, null: false
    t.float "JmlBarang", null: false
    t.char "KdSatuanJmlB", limit: 2, null: false
    t.real "Discount", null: false
    t.char "NoTerima", limit: 10
    t.money "HargaPO", precision: 19, scale: 4
    t.char "NoOrderSupp", limit: 10
    t.char "KdSupplier", limit: 10
    t.char "KdPabrik", limit: 10
    t.char "ID_KOMODITAS", limit: 10
    t.integer "jenis_katalog", limit: 1
    t.float "VolumePPTK"
    t.varchar_max "KET"
  end

  create_table "DetailPersenTarifCito", id: false, force: :cascade do |t|
    t.char "KdJnsPelayanan", limit: 3, null: false
    t.char "KdKomponen", limit: 2, null: false
    t.real "PersenTarifCito", null: false
  end

  create_table "DetailResepObat", id: false, force: :cascade do |t|
    t.varchar "NoResep", limit: 15, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.varchar "KdBarang", limit: 9, null: false
    t.char "KdAsal", limit: 2, null: false
    t.char "SatuanJml", limit: 1, null: false
    t.datetime "TglPelayanan", null: false
    t.integer "ResepKe", limit: 1, null: false
    t.char "NoTerima", limit: 10, null: false
  end

  create_table "DetailReturStrukApotik", primary_key: ["NoRetur", "NoStruk", "KdBarang", "KdAsal", "KdRuangan", "NoTerima"], force: :cascade do |t|
    t.char "NoRetur", limit: 10, null: false
    t.char "NoStruk", limit: 10, null: false
    t.varchar "KdBarang", limit: 9, null: false
    t.char "KdAsal", limit: 2, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.float "JmlBarangRetur", null: false
    t.char "NoTerima", limit: 10, null: false
  end

  create_table "DetailReturStrukBarangKeluar", id: false, force: :cascade do |t|
    t.char "NoRetur", limit: 10, null: false
    t.char "NoKirim", limit: 10, null: false
    t.varchar "KdBarang", limit: 9, null: false
    t.char "KdAsal", limit: 2, null: false
    t.decimal "JmlRetur", precision: 9, scale: 2, null: false
  end

  create_table "DetailReturStrukBarangKeluarTemp", id: false, force: :cascade do |t|
    t.char "NoRetur", limit: 10
    t.char "NoKirim", limit: 10, null: false
    t.varchar "KdBarang", limit: 9, null: false
    t.char "KdAsal", limit: 2, null: false
    t.float "JmlRetur", null: false
    t.varchar "IdUser", limit: 10, null: false
    t.varchar "StatusVerif", limit: 1, null: false
    t.varchar "KdRuanganPengirim", limit: 3, null: false
    t.varchar "KdRuanganPenerima", limit: 3, null: false
    t.float "HargaSatuan"
    t.float "TotalRetur"
  end

  create_table "DetailReturStrukTerimaBarang", primary_key: ["NoRetur", "NoTerima", "KdBarang", "KdAsal"], force: :cascade do |t|
    t.char "NoRetur", limit: 10, null: false
    t.char "NoTerima", limit: 10, null: false
    t.varchar "KdBarang", limit: 9, null: false
    t.char "KdAsal", limit: 2, null: false
    t.float "JmlRetur", null: false
  end

  create_table "DetailRujukanAsal", primary_key: "KdDetailRujukanAsal", id: { type: :char, limit: 8 }, force: :cascade do |t|
    t.varchar "DetailRujukanAsal", limit: 100, null: false
    t.char "KdRujukanAsal", limit: 2, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 100
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "DetailStatusIGDSubInstalasi", primary_key: "No", id: :integer, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "KdSubinstalasi", limit: 3
    t.datetime "TglInput"
  end

  create_table "DetailTerimaBarang", primary_key: ["NoTerima", "KdBarang", "KdAsal"], force: :cascade do |t|
    t.char "NoTerima", limit: 10, null: false
    t.varchar "KdBarang", limit: 9, null: false
    t.char "KdAsal", limit: 2, null: false
    t.float "JmlTerima", null: false
    t.float "HargaNetto", null: false
    t.money "Discount", precision: 19, scale: 4, null: false
    t.float "Ppn"
    t.datetime "TglKadaluarsa"
    t.money "DiscountGive", precision: 19, scale: 4, null: false
    t.money "DiscountSave", precision: 19, scale: 4, null: false
    t.char "NoClosing", limit: 10
    t.float "JmlKeluar"
  end

  create_table "DetailTerimaBarangSequence", primary_key: "NoInput", id: :integer, force: :cascade do |t|
    t.char "NoTerima", limit: 10, null: false
    t.varchar "KdBarang", limit: 9, null: false
    t.char "KdAsal", limit: 2, null: false
    t.float "JmlTerima", null: false
    t.float "HargaNetto", null: false
    t.money "Discount", precision: 19, scale: 4, null: false
    t.float "Ppn", null: false
    t.datetime "TglKadaluarsa"
    t.datetime "tglinput", null: false
    t.money "DiscountSave", precision: 19, scale: 4, null: false
    t.char "NoClosing", limit: 10
    t.float "JmlKeluar"
  end

  create_table "DetailTerimaBarangTemp", id: false, force: :cascade do |t|
    t.char "NoTerima", limit: 10, null: false
    t.datetime "TglTerima", null: false
    t.char "KdSupplier", limit: 4
    t.char "NoOrder", limit: 10
    t.varchar "NoFaktur", limit: 28
    t.varchar_max "NamaPemesan"
    t.datetime "TglFaktur"
    t.datetime "TglJatuhTempo"
    t.datetime "TglHarusDibayar"
    t.char "KdRuangan", limit: 3, null: false
    t.varchar "KdBarang", limit: 9, null: false
    t.char "KdAsal", limit: 2, null: false
    t.float "JmlTerima", null: false
    t.float "HargaNetto1", null: false
    t.float "HargaNetto2", null: false
    t.money "Discount", precision: 19, scale: 4, null: false
    t.float "Ppn", null: false
    t.datetime "TglKadaluarsa"
    t.money "DiscountGive", precision: 19, scale: 4, null: false
    t.money "DiscountSave", precision: 19, scale: 4, null: false
    t.char "NoClosing", limit: 10
    t.float "JmlKeluar"
    t.float "Total"
    t.varchar "StatusData", limit: 10
    t.varchar "BatchPeriod", limit: 500
  end

  create_table "DetailTriage", primary_key: "No", id: :integer, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10
    t.char "KdTriage", limit: 2
    t.datetime "TglInput"
    t.varchar "JalanNafas", limit: 50
    t.varchar "Pernafasan", limit: 50
    t.varchar "Sirkulasi", limit: 50
    t.varchar "MentalStatus", limit: 50
  end

  create_table "Diagnosa", primary_key: "KdDiagnosa", id: { type: :varchar, limit: 7 }, force: :cascade do |t|
    t.varchar "NoDTD", limit: 7
    t.varchar "NamaDiagnosa", limit: 100, null: false
    t.varchar "kodeDiagnosa"
    t.integer "QDiagnosa", null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 100
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "DiagnosaAskep", primary_key: "KdDiagnosaAskep", id: { type: :varchar, limit: 10 }, force: :cascade do |t|
    t.varchar "DiagnosaAskep", limit: 250, null: false
    t.varchar "Deskripsi", limit: 1000
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 75
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "DiagnosaAsuhanKeperawatan", id: false, force: :cascade do |t|
    t.varchar "KdAsuhan", limit: 7, null: false
    t.varchar "NamaAsuhanKeperawatan", limit: 1000, null: false
    t.varchar "Kategori", limit: 500, null: false
    t.varchar "SubKategori", limit: 1000
    t.varchar "Definisi", limit: 1000
    t.varchar "Tujuan", limit: 1000
    t.varchar "NamaExternal", limit: 500
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "DiagnosaINACBG", id: false, force: :cascade do |t|
    t.varchar "KdDiagnosa", limit: 7, null: false
    t.varchar "NoDTD", limit: 7
    t.varchar "NamaDiagnosa", limit: 500, null: false
    t.varchar "kodeDiagnosa"
    t.integer "QDiagnosa", null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 500
    t.integer "StatusEnabled", limit: 1, null: false
    t.index ["KdDiagnosa", "NoDTD", "NamaDiagnosa", "kodeDiagnosa", "QDiagnosa", "KodeExternal", "NamaExternal"], name: "IX_DiagnosaINACBG"
  end

  create_table "DiagnosaManualResume", primary_key: "Id", id: :integer, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 50
    t.varchar "NoCM", limit: 50
    t.varchar "KdRuangan", limit: 3
    t.varchar "Diagnosa", limit: 150
    t.datetime "CreateDate"
  end

  create_table "DiagnosaTindakan", id: false, force: :cascade do |t|
    t.varchar "KdDiagnosaTindakan", limit: 5, null: false
    t.varchar "DiagnosaTindakan", limit: 300, null: false
    t.varchar "KodeDiagnosaT"
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 300
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "DischargePlanning", primary_key: "IdDischargePlanning", id: :integer, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10
    t.char "NoCM", limit: 8
    t.varchar "aktivitas1", limit: 500
    t.varchar "aktivitas2", limit: 500
    t.varchar "aktivitas3", limit: 500
    t.varchar "EdukasiKesehatan1", limit: 500
    t.varchar "EdukasiKesehatan2", limit: 500
    t.varchar "EdukasiKesehatan3", limit: 500
    t.varchar "EdukasiKesehatan4", limit: 500
    t.varchar "EdukasiKesehatan5", limit: 500
    t.varchar "PerawatanDirumah1", limit: 500
    t.varchar "PerawatanDirumah2", limit: 500
    t.varchar "Diet1", limit: 500
    t.varchar "Diet2", limit: 500
    t.varchar "SpritualPsikologis1", limit: 500
    t.varchar "SpritualPsikologis2", limit: 500
    t.varchar "SpritualPsikologis3", limit: 500
    t.varchar "SpritualPsikologis4", limit: 500
    t.varchar "SpritualPsikologis5", limit: 500
    t.varchar "SpritualPsikologis6", limit: 500
    t.varchar "RincianPemulangan1", limit: 500
    t.varchar "RincianPemulangan2", limit: 500
    t.varchar "RincianPemulangan3", limit: 500
    t.datetime "Createdate"
    t.char "IdUser", limit: 10
  end

  create_table "EdukasiKeperawatan", id: false, force: :cascade do |t|
    t.varchar "KdDiagnosa", limit: 7, null: false
    t.varchar "SubIntervensi", limit: 255
    t.varchar "KdEdukasi", limit: 7, null: false
    t.varchar "NamaEdukasi", limit: 1000, null: false
    t.varchar "NamaExternal", limit: 500
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "Emergency", primary_key: "KdEmergency", id: { type: :char, limit: 2 }, force: :cascade do |t|
    t.varchar "StatusEmergency", limit: 25
    t.char "StatusEnabled", limit: 1
  end

  create_table "Eselon", primary_key: "KdEselon", id: { type: :varchar, limit: 2 }, force: :cascade do |t|
    t.varchar "NamaEselon", limit: 20, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 20
    t.integer "StatusEnabled", limit: 1
  end

  create_table "EtiketResep", primary_key: ["NoResep", "KdRuangan", "KdBarang", "KdAsal", "TglPelayanan"], force: :cascade do |t|
    t.varchar "NoResep", limit: 15, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.varchar "KdBarang", limit: 9, null: false
    t.char "KdAsal", limit: 2, null: false
    t.datetime "TglPelayanan", null: false
    t.char "KdJenisObat", limit: 2, null: false
    t.varchar "Signa", limit: 7
    t.char "KdSatuanEtiket", limit: 4
    t.char "KdWaktuEtiket", limit: 2
    t.integer "ResepKe", limit: 1, null: false
  end

  create_table "Fungsional", primary_key: "id_fungsional", id: :integer, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 50
    t.varchar "NoCm", limit: 10
    t.varchar "IdPegawai", limit: 50
    t.varchar "KdRuangan", limit: 3
    t.varchar "AlatBantu", limit: 50
    t.varchar "Prothesa", limit: 50
    t.varchar "CacatTubuh", limit: 250
    t.varchar "ADL", limit: 50
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
  end

  create_table "GolonganAsuransi", primary_key: "KdGolongan", id: { type: :char, limit: 2 }, force: :cascade do |t|
    t.varchar "NamaGolongan", limit: 50, null: false
    t.integer "QGolongan", limit: 1, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "GolonganBarang", primary_key: "KdGolBarang", id: { type: :char, limit: 2 }, force: :cascade do |t|
    t.varchar "GolonganBarang", limit: 15, null: false
    t.integer "QGolBarang", limit: 1, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 15
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "GolonganDarah", id: false, force: :cascade do |t|
    t.char "KdGolonganDarah", limit: 2, null: false
    t.varchar "GolonganDarah", limit: 2, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 2
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "GolonganPegawai", primary_key: "QGolongan", id: { type: :integer, limit: 1 }, force: :cascade do |t|
    t.varchar "KdGolongan", limit: 2, null: false
    t.varchar "NamaGolongan", limit: 20, null: false
    t.char "NoUrut", limit: 2
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 30
    t.integer "StatusEnabled", limit: 1, null: false
    t.real "PPHJasa"
  end

  create_table "HISTORY_CUTI", primary_key: "KD_AJUAN", id: :integer, force: :cascade do |t|
    t.varchar "PIN", limit: 6
    t.varchar "KD_CUTI", limit: 6
    t.date "TGL_AJUAN"
    t.date "TGL_AWAL"
    t.date "TGL_AKHIR"
    t.varchar "CUTI_TAHUN", limit: 10
    t.float "LAMA_CUTI"
    t.varchar "STATUS_CUTI", limit: 50
    t.varchar "VALIDASIATASAN", limit: 60
    t.float "TAMBAHANMENIT"
    t.float "TOTALWAKTUEFEKTIF"
  end

  create_table "HargaKomponen", id: false, force: :cascade do |t|
    t.char "KdKomponen", limit: 2, null: false
    t.char "KdKelas", limit: 2, null: false
    t.char "KdPelayananRS", limit: 6, null: false
    t.money "Harga", precision: 19, scale: 4, null: false
    t.char "KdJenisTarif", limit: 2, null: false
    t.char "Status", limit: 1, null: false
    t.index ["KdKomponen", "KdKelas", "KdPelayananRS", "KdJenisTarif"], name: "IX-HargaKomponen"
  end

  create_table "HargaNettoBarang", id: false, force: :cascade do |t|
    t.char "KdBarang", limit: 9, null: false
    t.char "KdAsal", limit: 2, null: false
    t.money "HargaNetto1", precision: 19, scale: 4, null: false
    t.money "HargaNetto2", precision: 19, scale: 4, null: false
    t.real "Discount", null: false
    t.datetime "TglKadaluarsa"
  end

  create_table "HargaNettoBarangFIFO", id: false, force: :cascade do |t|
    t.varchar "KdBarang", limit: 9, null: false
    t.char "KdAsal", limit: 2, null: false
    t.char "NoTerima", limit: 10, null: false
    t.money "HargaNetto1", precision: 19, scale: 4, null: false
    t.money "HargaNetto2", precision: 19, scale: 4, null: false
    t.real "Discount", null: false
    t.datetime "TglKadaluarsa"
  end

  create_table "HargaNettoBarangNonMedis", primary_key: ["KdBarang", "KdAsal"], force: :cascade do |t|
    t.varchar "KdBarang", limit: 9, null: false
    t.char "KdAsal", limit: 2, null: false
    t.money "HargaNetto", precision: 19, scale: 4, null: false
    t.money "HargaJual", precision: 19, scale: 4, null: false
    t.real "Discount", null: false
  end

  create_table "HasilPemeriksaan", id: false, force: :cascade do |t|
    t.char "NoLab_Rad", limit: 10, null: false
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.char "KdKelompokUmur", limit: 2
    t.char "JenisKelamin", limit: 1, null: false
    t.datetime "TglHasil", null: false
    t.char "IdUser", limit: 10, null: false
    t.varchar "Catatan", limit: 200
  end

  create_table "HasilPeriksaRadiology", primary_key: "kodeExternal", id: { type: :char, limit: 10 }, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoRadiology", limit: 10, null: false
    t.char "NoCM", limit: 8, null: false
    t.datetime "TglPeriksa", null: false
    t.varchar "NamaExternal"
    t.varchar "KdPelayananRs"
    t.varchar "HasilPeriksa"
  end

  create_table "HistoryPasienPulang", id: false, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 20
    t.varchar "NoCM", limit: 20
    t.varchar "IdUser", limit: 20
    t.datetime "CreateDate"
  end

  create_table "HistoryPelayananTindakan", primary_key: "Urutan", id: :integer, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10
    t.datetime "TglPelayanan"
    t.char "KdRuangan", limit: 3
    t.char "KdPelayananRs", limit: 6
    t.integer "Qty"
    t.money "Harga", precision: 19, scale: 4
    t.char "IdUser", limit: 10
    t.char "IdPegawai", limit: 10
    t.datetime "TglModif"
    t.char "Status", limit: 1
  end

  create_table "HistoryRiwayatObatAlkes", primary_key: ["NoRiwayat", "TanggalRiwayat"], force: :cascade do |t|
    t.char "NoRiwayat", limit: 10, null: false
    t.datetime "TanggalRiwayat", null: false
    t.char "IdUser", limit: 10, null: false
    t.varchar "NoBukti", limit: 50
  end

  create_table "HistoryStokBarangRuangan", primary_key: "NoUrut", id: :integer, force: :cascade do |t|
    t.datetime "TanggalTransaksi", null: false
    t.varchar "KdBarang", limit: 9, null: false
    t.char "KdAsal", limit: 2, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "NoRiwayat", limit: 10
    t.nchar "JmlStokAwal", limit: 10
    t.float "JmlTransaksi"
  end

  create_table "HubunganKeluarga", primary_key: "Hubungan", id: { type: :char, limit: 2 }, force: :cascade do |t|
    t.varchar "NamaHubungan", limit: 50, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "HubunganPesertaAsuransi", primary_key: "QHubungan", id: { type: :integer, limit: 1 }, force: :cascade do |t|
    t.char "KdHubungan", limit: 2, null: false
    t.varchar "NamaHubungan", limit: 50, null: false
    t.varchar "Singkatan", limit: 2
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "InsidenInternal_Akibat", id: false, force: :cascade do |t|
    t.char "IdAkibatInsiden", limit: 2, null: false
    t.char "AkibatInsiden", limit: 255, null: false
    t.char "StatusEnabled", limit: 1, null: false
  end

  create_table "InsidenInternal_Input", primary_key: "KdInsiden", id: :integer, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 10, null: false
    t.varchar "NoCM", limit: 10, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.datetime "TanggalKejadian"
    t.varchar "Insiden", limit: 255
    t.varchar "KronologisInsiden", limit: 255
    t.char "IdJenisInsiden", limit: 2
    t.char "Pelapor", limit: 50
    t.char "TerjadiPada", limit: 50
    t.char "IdInsidenMenyangkut", limit: 2
    t.char "IdLokasiInsiden", limit: 2
    t.char "UnitKerjaInsiden", limit: 50
    t.char "IdAkibatInsiden", limit: 2
    t.varchar "PenangananInsiden", limit: 255
    t.varchar "TindakanDilakukanOleh", limit: 50
    t.varchar "GradingInsiden", limit: 10
    t.varchar "KejadianSama", limit: 50
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
    t.varchar "IdUser", limit: 10
  end

  create_table "InsidenInternal_Jenis", id: false, force: :cascade do |t|
    t.char "IdJenisInsiden", limit: 2, null: false
    t.char "JenisInsiden", limit: 255, null: false
    t.char "StatusEnabled", limit: 1, null: false
  end

  create_table "InsidenInternal_Lokasi", id: false, force: :cascade do |t|
    t.char "IdLokasiInsiden", limit: 2, null: false
    t.char "LokasiInsiden", limit: 255, null: false
    t.char "StatusEnabled", limit: 1, null: false
  end

  create_table "InsidenInternal_Menyangkut", id: false, force: :cascade do |t|
    t.char "IdInsidenMenyangkut", limit: 2, null: false
    t.char "InsidenMenyangkut", limit: 255, null: false
    t.char "StatusEnabled", limit: 1, null: false
  end

  create_table "Instalasi", primary_key: "QInstalasi", id: { type: :integer, limit: 1 }, force: :cascade do |t|
    t.char "KdInstalasi", limit: 2, null: false
    t.varchar "NamaInstalasi", limit: 50, null: false
    t.varchar "Fungsi", limit: 50, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "InstitusiAsalPasien", primary_key: "QInstitusiAsal", id: { type: :integer, limit: 2 }, force: :cascade do |t|
    t.varchar "KdInstitusiAsal", limit: 4, null: false
    t.varchar "InstitusiAsal", limit: 50, null: false
    t.char "IdPenjamin", limit: 10
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "IntruksiDokter", id: false, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 50
    t.varchar "NoCM", limit: 50
    t.varchar "KdRuangan", limit: 50
    t.varchar "IdDokter", limit: 50
    t.varchar_max "Keterangan"
    t.datetime "CreateDate"
  end

  create_table "IntruksiDokterNew", primary_key: "IdIntruksi", id: :integer, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 50
    t.varchar "NoCM", limit: 50
    t.varchar "KdRuangan", limit: 50
    t.varchar "IdDokter", limit: 50
    t.varchar_max "Keterangan"
    t.datetime "CreateDate"
  end

  create_table "Jabatan", id: false, force: :cascade do |t|
    t.varchar "KdJabatan", limit: 5, null: false
    t.varchar "NamaJabatan", limit: 50, null: false
    t.varchar "KdJenisJabatan", limit: 2, null: false
    t.char "NoUrut", limit: 2
    t.integer "QJabatan", limit: 1
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 30
    t.integer "StatusEnabled", limit: 1, null: false
    t.real "Score"
    t.varchar "KdTipePekerjaan", limit: 2
  end

  create_table "JadwalPegawaiPeriksa", id: false, force: :cascade do |t|
    t.char "IdPegawai", limit: 10, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "StatusPegawai", limit: 1, null: false
    t.char "StatusEnabled", limit: 1
  end

  create_table "JadwalPraktekDokter", id: false, force: :cascade do |t|
    t.char "KdPraktek", limit: 10, null: false
    t.varchar "Hari", limit: 7
    t.datetime "Tgl"
    t.varchar "IdDokter", limit: 10
    t.char "KdRuangan", limit: 3
    t.varchar "JamMulai", limit: 5
    t.varchar "JamSelesai", limit: 5
    t.varchar "Keterangan", limit: 50
    t.varchar "Kuota", limit: 50
  end

  create_table "JadwalPraktekDokterEMR", id: false, force: :cascade do |t|
    t.char "KdPraktek", limit: 10, null: false
    t.varchar "Hari", limit: 7
    t.datetime "Tgl"
    t.varchar "IdDokter", limit: 10
    t.char "KdRuangan", limit: 3
    t.varchar "JamMulai", limit: 5
    t.varchar "JamSelesai", limit: 5
    t.varchar "Keterangan", limit: 50
    t.varchar "Kuota", limit: 50
  end

  create_table "JalanNafas_TP", id: false, force: :cascade do |t|
    t.varchar "KdJalanNafas", limit: 50
    t.varchar "NamaJalanNafas", limit: 50
    t.integer "Enable"
  end

  create_table "JenisBarang", primary_key: "QJenisBarang", id: { type: :integer, limit: 2 }, force: :cascade do |t|
    t.char "KdJenisBarang", limit: 2, null: false
    t.varchar "JenisBarang", limit: 150, null: false
    t.char "KdKelompokBarang", limit: 2, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 25
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "JenisBelanjaBLUD", id: false, force: :cascade do |t|
    t.char "KdJenisBelanja", limit: 10
    t.varchar "JenisBelanja", limit: 100
  end

  create_table "JenisBiayaBLUD", id: false, force: :cascade do |t|
    t.char "KdJenisBiaya", limit: 10
    t.varchar "JenisBiaya", limit: 100
  end

  create_table "JenisDiagnosa", primary_key: "QJenisDiagnosa", id: { type: :integer, limit: 1 }, force: :cascade do |t|
    t.char "KdJenisDiagnosa", limit: 2, null: false
    t.varchar "JenisDiagnosa", limit: 30, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 30
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "JenisJabatan", id: false, force: :cascade do |t|
    t.varchar "KdJenisJabatan", limit: 2, null: false
    t.varchar "JenisJabatan", limit: 30, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 30
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "JenisJasaPelayanan", id: false, force: :cascade do |t|
    t.char "KdJenisJasaPelayanan", limit: 2, null: false
    t.varchar "JenisJasaPelayanan", limit: 50, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 30
    t.integer "StatusEnabled", limit: 1
  end

  create_table "JenisKartu", primary_key: "KdJenisKartu", id: { type: :char, limit: 2 }, force: :cascade do |t|
    t.varchar "JenisKartu", limit: 50, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "JenisKelamin", id: false, force: :cascade do |t|
    t.char "KdJenisKelamin", limit: 2, null: false
    t.varchar "JenisKelamin", limit: 20, null: false
    t.char "Singkatan", limit: 1, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 20
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "JenisMenuDiet", primary_key: "KdJenisMenuDiet", id: { type: :char, limit: 3 }, force: :cascade do |t|
    t.varchar "JenisMenudiet", limit: 100
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 100
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "JenisMenuDietTambahan", id: false, force: :cascade do |t|
    t.char "KdJenisDietTambahan", limit: 3, null: false
    t.varchar "JenisDietTambahan", limit: 100, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 100
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "JenisObat", id: false, force: :cascade do |t|
    t.char "KdJenisObat", limit: 2, null: false
    t.varchar "JenisObat", limit: 20, null: false
    t.money "TarifService", precision: 19, scale: 4, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 20
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "JenisOperasi", primary_key: "KdJenisOperasi", id: { type: :char, limit: 2 }, force: :cascade do |t|
    t.varchar "JenisOperasi", limit: 50, null: false
    t.varchar "Singkatan", limit: 5
    t.integer "QJenisOperasi", limit: 1, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "JenisPegawai", id: false, force: :cascade do |t|
    t.char "KdJenisPegawai", limit: 3, null: false
    t.varchar "JenisPegawai", limit: 50, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 30
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "JenisPelayanan", primary_key: "KdJnsPelayanan", id: { type: :char, limit: 3 }, force: :cascade do |t|
    t.varchar "Deskripsi", limit: 75, null: false
    t.real "PersenTarifCito"
    t.integer "QJnsPelayanan", limit: 2, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 30
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "JenisPemeriksaan", primary_key: "KdJenisPeriksa", id: { type: :char, limit: 3 }, force: :cascade do |t|
    t.varchar "JenisPeriksa", limit: 50, null: false
    t.char "KdInstalasi", limit: 2, null: false
    t.integer "NoUrut", limit: 1
    t.integer "QJenisPeriksa", limit: 2, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "JenisPendidikan", primary_key: "KdJenisPendidikan", id: { type: :varchar, limit: 3 }, force: :cascade do |t|
    t.varchar "JenisPendidikan", limit: 50, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "JenisPeserta", id: false, force: :cascade do |t|
    t.varchar "KdJenisPeserta", limit: 2
    t.varchar_max "JenisPeserta"
    t.varchar "StatusEnabled", limit: 1
  end

  create_table "JenisTarif", primary_key: "KdJenisTarif", id: { type: :char, limit: 2 }, force: :cascade do |t|
    t.varchar "JenisTarif", limit: 50, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1
  end

  create_table "JenisTindakanMaternal", id: false, force: :cascade do |t|
    t.char "KdTindakan", limit: 3, null: false
    t.text_basic "TindakanPersalinan", null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 30
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "JenisWaktu", id: false, force: :cascade do |t|
    t.char "KdJenisWaktu", limit: 3, null: false
    t.varchar "JenisWaktu", limit: 12, null: false
    t.integer "JamAwal", null: false
    t.integer "JamAkhir", null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 12
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "JudulKriteriaAsuhanKeperawatan", id: false, force: :cascade do |t|
    t.varchar "KdJudulKriteria", limit: 7, null: false
    t.varchar "Judul1", limit: 255, null: false
    t.varchar "Judul2", limit: 255, null: false
    t.varchar "Judul3", limit: 255, null: false
    t.varchar "Judul4", limit: 255, null: false
    t.varchar "Judul5", limit: 255, null: false
    t.varchar "NamaExternal", limit: 500
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "KajianFungsional", primary_key: "IdFungsional", id: :integer, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10
    t.char "NoCM", limit: 8
    t.integer "AlatBantu"
    t.integer "CacatTubuh"
    t.varchar "CacatTubuh2", limit: 50
    t.datetime "Createdate"
    t.char "IdUser", limit: 10
  end

  create_table "KamarOperasi", id: false, force: :cascade do |t|
    t.char "KdKamarOperasi", limit: 10, null: false
    t.char "KamarOperasi", limit: 50, null: false
    t.char "StatusEnabled", limit: 1, null: false
  end

  create_table "KartuStokBarang", id: false, force: :cascade do |t|
    t.varchar "NoBukti", limit: 10
    t.varchar "NoReferensi", limit: 25
    t.varchar "NoReferensi2", limit: 10
    t.integer "resepKe", limit: 1
    t.varchar "UraianReferensi", limit: 75
    t.datetime "TglTransaksi", null: false
    t.char "kdRuangan", limit: 3, null: false
    t.char "kdRuanganTujuanOrKdSupplier", limit: 4
    t.char "kdBarang", limit: 9, null: false
    t.char "kdAsal", limit: 2, null: false
    t.float "saldoAwal"
    t.float "jmlBarang", null: false
    t.float "saldoAkhir"
    t.char "JenisTransaksi", limit: 2, null: false
    t.char "statusTransaksi", limit: 1, null: false
    t.datetime "TglInput", null: false
    t.varchar "idUser", limit: 10
    t.index ["NoBukti", "NoReferensi", "NoReferensi2"], name: "IX_ClusteredIndex-20220819-152934"
  end

  create_table "KategoriMasterTemplateDokterPhp", id: :integer, force: :cascade do |t|
    t.varchar "Modul", limit: 50
    t.datetime "CreateDate"
  end

  create_table "KategoryBarang", primary_key: "KdKategoryBarang", id: { type: :char, limit: 2 }, force: :cascade do |t|
    t.varchar "KategoryBarang", limit: 50, null: false
    t.integer "QKategoryBarang", limit: 2, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "KategoryDiet", primary_key: "KdKategoryDiet", id: { type: :varchar, limit: 3 }, force: :cascade do |t|
    t.varchar "KategoryDiet", limit: 50, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "KdSatpel", id: false, force: :cascade do |t|
    t.bigint "KdSatpel", null: false
    t.varchar_max "Ket"
    t.integer "StatusEnabled", limit: 1
    t.char "IdPegawai", limit: 50
  end

  create_table "Kecamatan", primary_key: "KdKecamatan", id: { type: :varchar, limit: 6 }, force: :cascade do |t|
    t.char "KdPropinsi", limit: 2, null: false
    t.varchar "KdKotaKabupaten", limit: 4, null: false
    t.varchar "NamaKecamatan", limit: 50, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "KelasPelayanan", primary_key: "KdKelas", id: { type: :char, limit: 2 }, force: :cascade do |t|
    t.varchar "DeskKelas", limit: 50, null: false
    t.varchar "Singkatan", limit: 50
    t.integer "QKelas", limit: 1, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 30
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "KelasRuangan", id: false, force: :cascade do |t|
    t.char "KdRuangan", limit: 3, null: false
    t.char "KdKelas", limit: 2, null: false
  end

  create_table "KelasTanggunganPenjamin", primary_key: ["KdKelompokPasien", "IdPenjamin", "KdKelas"], force: :cascade do |t|
    t.char "KdKelompokPasien", limit: 2, null: false
    t.char "IdPenjamin", limit: 10, null: false
    t.char "KdKelas", limit: 2, null: false
  end

  create_table "KelompokBarang", primary_key: "KdKelompokBarang", id: { type: :char, limit: 2 }, force: :cascade do |t|
    t.varchar "KelompokBarang", limit: 15, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 15
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "KelompokPasien", primary_key: "QKelompokPasien", id: { type: :integer, limit: 1 }, force: :cascade do |t|
    t.char "KdKelompokPasien", limit: 2, null: false
    t.varchar "JenisPasien", limit: 30, null: false
    t.varchar "TipePembayaran", limit: 30, null: false
    t.char "KdJenisTarif", limit: 2
    t.varchar "Singkatan", limit: 30
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 30
    t.integer "StatusEnabled", limit: 1, null: false
    t.index ["KdKelompokPasien", "JenisPasien"], name: "IX_KelompokPasien10132022"
  end

  create_table "KelompokUmur", id: false, force: :cascade do |t|
    t.char "KdKelompokUmur", limit: 3, null: false
    t.varchar "KelompokUmur", limit: 30
    t.varchar "RangeUmur", limit: 50
    t.integer "UmurMin"
    t.integer "UmurMax"
    t.varchar "Keterangan", limit: 50
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 30
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "Keluhan", primary_key: "KdKeluhan", id: { type: :char, limit: 3 }, force: :cascade do |t|
    t.char "KdRuangan", limit: 3, null: false
    t.varchar "Keluhan", limit: 50, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 30
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "Kelurahan", id: false, force: :cascade do |t|
    t.char "KdPropinsi", limit: 2, null: false
    t.varchar "KdKotaKabupaten", limit: 4, null: false
    t.varchar "KdKecamatan", limit: 6, null: false
    t.varchar "KdKelurahan", limit: 9, null: false
    t.varchar "KodePos", limit: 10
    t.varchar "NamaKelurahan", limit: 50, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "Kesadaran", primary_key: "KdKesadaran", id: { type: :char, limit: 2 }, force: :cascade do |t|
    t.varchar "NamaKesadaran", limit: 50, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "KlaimDiagnosa", id: false, force: :cascade do |t|
    t.varchar "KdDiagnosa", limit: 50
    t.varchar_max "Diagnosa"
    t.varchar_max "AspekMedisKlaim"
  end

  create_table "KodeBelanjaBLUD", id: false, force: :cascade do |t|
    t.integer "Kode"
    t.char "KodeBelanja", limit: 10
    t.varchar_max "Nama"
    t.integer "StatusEnabled", limit: 1
    t.char "KodeOperasional", limit: 10
    t.char "KdSatuanjmlK", limit: 10
    t.char "KdRekening", limit: 10
    t.char "KdSatuanJmlK2", limit: 10
    t.char "KdSatpel", limit: 10
    t.char "KdBiaya", limit: 10
    t.char "KdJenisBiaya", limit: 10
    t.char "KdJenisBelanja", limit: 10
    t.char "KdDetailJenisBelanja", limit: 10
    t.char "KdKabag", limit: 10
    t.char "KdPptkPelaksana", limit: 10
    t.char "Tahun", limit: 10
  end

  create_table "KodeJadwal", id: false, force: :cascade do |t|
    t.varchar "KD_CUTI", limit: 4, null: false
    t.varchar_max "NAMASHIFT"
    t.varchar "JAM_MASUK", limit: 100
    t.varchar "JAM_KELUAR", limit: 100, null: false
    t.char "StatusEnabled", limit: 1
  end

  create_table "KolaborasiKeperawatan", id: false, force: :cascade do |t|
    t.varchar "KdDiagnosa", limit: 7, null: false
    t.varchar "SubIntervensi", limit: 255
    t.varchar "KdKolaborasi", limit: 7, null: false
    t.varchar "NamaKolaborasi", limit: 1000, null: false
    t.varchar "NamaExternal", limit: 500
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "KomponenTarif", primary_key: "KdKomponen", id: { type: :char, limit: 2 }, force: :cascade do |t|
    t.varchar "NamaKomponen", limit: 50, null: false
    t.varchar "Keterangan", limit: 50
    t.varchar "Singkatan", limit: 5
    t.char "NoUrut", limit: 3
    t.integer "QKomponen", limit: 1, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1
  end

  create_table "KomponenUnitLaporan", primary_key: "KdKomponenUnit", id: { type: :char, limit: 2 }, force: :cascade do |t|
    t.varchar "KomponenUnit", limit: 50, null: false
    t.varchar "Singkatan", limit: 5
    t.integer "QKomponenUnit", limit: 1, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "KondisiKeluar", primary_key: "KdKondisiKeluar", id: { type: :char, limit: 2 }, force: :cascade do |t|
    t.varchar "KondisiKeluar", limit: 50, null: false
    t.integer "QKondisiKeluar", limit: 1, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "KondisiKlinisAsuhanKeperawatan", id: false, force: :cascade do |t|
    t.varchar "KdAsuhan", limit: 7, null: false
    t.varchar "KdKondisi", limit: 7, null: false
    t.varchar "KondisiKlinis", limit: 1000, null: false
    t.varchar "NamaExternal", limit: 500
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "KondisiPulang", primary_key: "KdKondisiPulang", id: { type: :char, limit: 2 }, force: :cascade do |t|
    t.varchar "KondisiPulang", limit: 50, null: false
    t.varchar "Singkatan", limit: 5
    t.integer "QKondisiPulang", limit: 1, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "KondisiRujukanAGD", id: false, force: :cascade do |t|
    t.char "KdKondisiRujukan", limit: 2, null: false
    t.varchar_max "KondisiPasien", null: false
  end

  create_table "Konfirmasi_TP", id: false, force: :cascade do |t|
    t.varchar "KodeKonfirmasi", limit: 50
    t.varchar "KeteranganKonfirmasi", limit: 50
    t.integer "Enable"
  end

  create_table "KotaKabupaten", id: false, force: :cascade do |t|
    t.char "KdPropinsi", limit: 2, null: false
    t.varchar "KdKotaKabupaten", limit: 4, null: false
    t.varchar "NamaKotaKabupaten", limit: 50, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "KriteriaAsuhanKeperawatan", id: false, force: :cascade do |t|
    t.varchar "KdAsuhan", limit: 7, null: false
    t.varchar "KdKelompok", limit: 7
    t.varchar "Kdkriteria", limit: 7, null: false
    t.varchar "NamaKriteria", limit: 1000, null: false
    t.varchar "NamaExternal", limit: 500
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "KualifikasiJurusan", id: false, force: :cascade do |t|
    t.text "KdKualifikasiJurusan"
    t.varchar "KualifikasiJurusan", limit: 200
    t.string "KdPendidikan", limit: 255
    t.text "KodeExternal"
    t.text "NamaExternal"
    t.float "StatusEnabled"
    t.string "KdDetailKelompokPegawai", limit: 255
    t.integer "KdKetenagaan", limit: 1
  end

  create_table "KunjunganPasien", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoKunjungan", limit: 10, null: false
    t.char "KdKelompokPasien", limit: 2, null: false
  end

  create_table "LIS_HASIL_DATA", id: false, force: :cascade do |t|
    t.varchar "reg_no", limit: 15
    t.varchar "order_lab", limit: 15
    t.varchar "pasied_id", limit: 10
    t.varchar "lis_sampel_no", limit: 100
    t.varchar "acc_by", limit: 100
    t.datetime "acc_date"
    t.varchar "reserve1", limit: 100
    t.varchar "reserve2", limit: 100
    t.varchar "reserve3", limit: 100
    t.varchar "reserve4", limit: 100
    t.varchar "test_id", limit: 100
    t.varchar "nama_test", limit: 200
    t.varchar "jenis_hasil", limit: 100
    t.varchar "hasil", limit: 2000
    t.varchar "satuan", limit: 100
    t.varchar "nilai_normal", limit: 100
    t.varchar "flag", limit: 6
    t.varchar "kode_paket", limit: 100
    t.datetime "created_date", default: -> { "getdate()" }
    t.varchar "active", limit: 1, default: "1"
    t.index ["reg_no", "order_lab", "pasied_id", "lis_sampel_no", "test_id", "kode_paket", "active"], name: "LIS_HASIL_DATA_reg_no_IDX"
  end

  create_table "LIS_ORDER_HD", id: false, force: :cascade do |t|
    t.varchar "pmrn", limit: 15
    t.varchar "pname", limit: 100
    t.varchar "sex", limit: 1
    t.date "birth_dt"
    t.varchar "address", limit: 100
    t.varchar "No_tlp", limit: 25
    t.varchar "No_hp", limit: 25
    t.varchar "email", limit: 25
    t.varchar "nik", limit: 20
    t.varchar "order_kontrol", limit: 2
    t.varchar "ptype", limit: 2
    t.varchar "reg_no", limit: 15
    t.varchar "order_lab", limit: 15
    t.varchar "provider_id", limit: 15
    t.varchar "provider_name", limit: 50
    t.datetime "order_date"
    t.varchar "clinician_id", limit: 15
    t.varchar "clinician_name", limit: 100
    t.varchar "bangsal_id", limit: 15
    t.varchar "bangsal_name", limit: 100
    t.varchar "bed_id", limit: 10
    t.varchar "bed_name", limit: 20
    t.varchar "class_id", limit: 10
    t.varchar "class_name", limit: 15
    t.varchar "cito", limit: 1
    t.varchar "med_legal", limit: 1
    t.varchar "user_id", limit: 100
    t.varchar "reserve1", limit: 100
    t.varchar "reserve2", limit: 200
    t.varchar "reserve3", limit: 300
    t.varchar "reserve4", limit: 300
    t.datetime "create_date"
    t.varchar "status_transfer", limit: 1, default: "0"
  end

  create_table "LIS_ORDER_IT", id: false, force: :cascade do |t|
    t.varchar "reg_no", limit: 15
    t.varchar "order_lab", limit: 50
    t.varchar "pasied_id", limit: 15
    t.varchar "kode_tes", limit: 25
    t.varchar "nama_tes", limit: 500
  end

  create_table "LIS_RESULT_DETAIL", primary_key: ["ID", "ONO", "TEST_CD"], force: :cascade do |t|
    t.varchar "ID", limit: 30, null: false
    t.varchar "ONO", limit: 20, null: false
    t.varchar "TEST_CD", limit: 20, null: false
    t.varchar "TEST_NM", limit: 50
    t.varchar "DATA_TYP", limit: 5
    t.varchar "RESULT_VALUE", limit: 40
    t.text_basic "RESULT_FT"
    t.varchar "UNIT", limit: 15
    t.varchar "REF_RANGE", limit: 30
    t.varchar "STATUS", limit: 1
    t.varchar "TEST_COMMENT", limit: 2000
    t.varchar "VALIDATE_BY", limit: 60
    t.varchar "VALIDATE_ON", limit: 14
    t.varchar "DISP_SEQ", limit: 15
    t.varchar "FLAG", limit: 4
    t.varchar "ORDER_TESTID", limit: 20
    t.varchar "ORDER_TESTNM", limit: 50
    t.varchar "TEST_GROUP", limit: 20
    t.varchar "ITEM_PARENT", limit: 6
  end

  create_table "LabelEtiket", primary_key: ["NoPendaftaran", "NoResep", "ResepKe", "KdBarang", "TglPelayanan", "KdAsal", "KdRuangan"], force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoResep", limit: 10, null: false
    t.integer "ResepKe", null: false
    t.char "KdBarang", limit: 9, null: false
    t.float "qty", null: false
    t.datetime "TglPelayanan", null: false
    t.char "KdSatuanEtiket", limit: 2
    t.char "KdWaktuEtiket", limit: 2
    t.char "KdJnsObat", limit: 2
    t.char "KdAsal", limit: 2, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.money "HargaSatuan", precision: 19, scale: 4
    t.char "KdsubInstalasi", limit: 3
    t.char "Satuan", limit: 1
    t.char "KdKelas", limit: 2
    t.money "TarifService", precision: 19, scale: 4
    t.char "IdPegawai", limit: 10
  end

  create_table "LabelEtiket2", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10
    t.char "NoResep", limit: 10
    t.integer "ResepKe"
    t.char "KdBarang", limit: 9
    t.float "qty"
    t.datetime "TglPelayanan"
    t.varchar "KdSatuanEtiket", limit: 500
    t.char "KdWaktuEtiket", limit: 2
    t.char "KdJnsObat", limit: 2
    t.char "KdAsal", limit: 2
    t.char "KdRuangan", limit: 3
    t.money "HargaSatuan", precision: 19, scale: 4
    t.char "KdsubInstalasi", limit: 3
    t.char "Satuan", limit: 1
    t.char "KdKelas", limit: 2
    t.money "TarifService", precision: 19, scale: 4
    t.char "IdPegawai", limit: 10
    t.varchar "Keterangan", limit: 500
  end

  create_table "LaundryTerima", id: false, force: :cascade do |t|
    t.char "NoTerima", limit: 10, null: false
    t.datetime "TglTerima", null: false
    t.char "KdSupplier", limit: 4
    t.char "NoOrder", limit: 10
    t.varchar "NoFaktur", limit: 25
    t.datetime "TglFaktur"
    t.datetime "TglJatuhTempo"
    t.datetime "TglHarusDibayar"
    t.real "TotalBeratKg"
    t.char "KdRuangan", limit: 3, null: false
    t.char "IdUser", limit: 10, null: false
    t.char "NoPosting", limit: 10
    t.integer "NoUrutJurnal", limit: 2
    t.varchar "NoSurat", limit: 50
    t.integer "StatusVerifikasi", limit: 1
  end

  create_table "LimitTanggunganPenjamin", id: false, force: :cascade do |t|
    t.char "KdKelompokPasien", limit: 2, null: false
    t.char "IdPenjamin", limit: 10, null: false
    t.char "KdKelas", limit: 2, null: false
    t.char "KdHubungan", limit: 2, null: false
    t.real "PersenDiTanggungP_TM", null: false
    t.real "PersenDiTanggungRS_TM", null: false
    t.real "PersenDiTanggungP_OA", null: false
    t.real "PersenDiTanggungRS_OA", null: false
    t.money "MaxDiTanggungan_TM", precision: 19, scale: 4, null: false
    t.money "MaxDiTanggungan_OA", precision: 19, scale: 4, null: false
  end

  create_table "ListPelayananRS", primary_key: "QPelayananRS", id: { type: :integer, limit: 2 }, force: :cascade do |t|
    t.char "KdPelayananRS", limit: 6, null: false
    t.varchar "NamaPelayanan", limit: 1000, null: false
    t.char "KdJnsPelayanan", limit: 3, null: false
    t.char "KdKomponenUnit", limit: 2
    t.char "StatusAktif", limit: 1
    t.char "KdJenisPeriksa", limit: 3
    t.integer "KdLevelProduk", limit: 1
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 30
    t.integer "StatusEnabled", limit: 1, null: false
    t.index ["KdPelayananRS", "KdJnsPelayanan"], name: "IX_ListPelayananRS"
  end

  create_table "ListPelayananRSLab", id: false, force: :cascade do |t|
    t.char "KdPelayananRS", limit: 6, null: false
    t.varchar "NamaPelayanan", limit: 1000, null: false
    t.varchar "KdLoinc", limit: 500
    t.varchar "NamaLoinc", limit: 500
    t.varchar "KdSpesimen", limit: 500
    t.varchar "JenisSpesimen", limit: 500
    t.varchar "KdMetode", limit: 500
    t.varchar "NamaMetode", limit: 500
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "List_Pelayanan_Eklaim", primary_key: "ID_Pelayanan_Eklaim", id: :integer, force: :cascade do |t|
    t.varchar "KdPelayananRS", limit: 50
    t.varchar "KdJnsPelayanan", limit: 50
    t.varchar "KdJnsEklaim", limit: 50
    t.varchar "Kd_icd9", limit: 50
    t.varchar "ProcedureIcd9", limit: 500
    t.varchar "StatusEnabled", limit: 10
  end

  create_table "LoginAkuntansi", id: false, force: :cascade do |t|
    t.integer "PIN"
    t.varchar "NamaPegawai", limit: 100
    t.varchar "Pegawai", limit: 50
    t.varchar "IdPegawai", limit: 50
    t.varchar "Username", limit: 50
    t.varchar "UserPassword", limit: 50
    t.varchar "KdRuangan", limit: 50
    t.varchar "Gambar", limit: 50
    t.varchar "PermissionsAllowed", limit: 50
    t.varchar "UserSession", limit: 120
  end

  create_table "LoginAplikasi", id: false, force: :cascade do |t|
    t.varchar "PIN", limit: 50
    t.varchar "NamaPegawai", limit: 100
    t.varchar "Pegawai", limit: 50
    t.varchar "IdPegawai", limit: 50
    t.varchar "Username", limit: 50
    t.varchar "UserPassword", limit: 50
    t.varchar "KdRuangan", limit: 50
    t.varchar "KdRuanganMandiri", limit: 50
    t.varchar "Gambar", limit: 50
    t.varchar "PermissionsAllowed", limit: 50
    t.varchar "UserSession", limit: 120
  end

  create_table "LoginMonitoringKlaim", id: false, force: :cascade do |t|
    t.integer "PIN"
    t.varchar "NamaPegawai", limit: 100
    t.varchar "Pegawai", limit: 50
    t.varchar "IdPegawai", limit: 50
    t.varchar "Username", limit: 50
    t.varchar "UserPassword", limit: 50
    t.varchar "KdRuangan", limit: 50
    t.varchar "Gambar", limit: 50
    t.varchar "PermissionsAllowed", limit: 50
    t.varchar "UserSession", limit: 120
  end

  create_table "LoginPerawatEMR", id: false, force: :cascade do |t|
    t.integer "PIN"
    t.varchar "NamaPegawai", limit: 100
    t.varchar "Pegawai", limit: 50
    t.varchar "IdPegawai", limit: 50
    t.varchar "Username", limit: 50
    t.varchar "UserPassword", limit: 50
    t.varchar "KdRuangan", limit: 50
    t.varchar "KdRuanganMandiri", limit: 50
    t.varchar "Gambar", limit: 50
    t.varchar "PermissionsAllowed", limit: 50
    t.varchar "UserSession", limit: 120
  end

  create_table "LoginRuangan", id: false, force: :cascade do |t|
    t.char "IdPegawai", limit: 10, null: false
    t.char "KdRuangan", limit: 3, null: false
  end

  create_table "MAPAPOTEKERIHS", id: false, force: :cascade do |t|
    t.varchar "IdPegawai", limit: 50
    t.varchar "id_practitioner", limit: 50
    t.varchar "DisplayName", limit: 50
  end

  create_table "MAPDOKTERIHS", id: false, force: :cascade do |t|
    t.varchar "IdDokter", limit: 50
    t.varchar "id_practitioner", limit: 50
    t.varchar "DisplayName", limit: 50
  end

  create_table "MAPPOLI", id: false, force: :cascade do |t|
    t.varchar_max "kdpoli"
    t.varchar_max "nmpoli"
    t.varchar_max "kdpolirs"
    t.varchar_max "nmpolirs"
  end

  create_table "MAPRUANGANIHS", id: false, force: :cascade do |t|
    t.varchar "KdRuangan", limit: 50
    t.varchar "DisplayName", limit: 50
    t.varchar "id_location", limit: 100
  end

  create_table "MapingRuanganToDepo", id: false, force: :cascade do |t|
    t.char "KdRuanganDepo", limit: 3, null: false
    t.char "KdRuanganPelayanan", limit: 3, null: false
    t.char "Status", limit: 2, null: false
  end

  create_table "MappingDokterBPJS", id: false, force: :cascade do |t|
    t.varchar "IdDokter", limit: 50
    t.varchar "kdDokter", limit: 50
    t.varchar "nmDokter", limit: 50
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "MasalahKeperawatan", primary_key: "IdMP", id: :integer, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10
    t.char "NoCM", limit: 8
    t.char "IdUser", limit: 10
    t.varchar_max "Keterangan"
    t.datetime "Createdate"
  end

  create_table "MasterBarang", primary_key: "QBarang", id: :integer, force: :cascade do |t|
    t.char "KdBarang", limit: 9, null: false
    t.varchar "KdDetailJenisBarang", limit: 5
    t.char "KdKategoryBarang", limit: 2
    t.char "KdGolBarang", limit: 2
    t.char "KdSatuanJmlB", limit: 2
    t.char "KdSatuanJmlK", limit: 2
    t.varchar "NamaBarang", limit: 100, null: false
    t.integer "JmlTerkecil"
    t.real "JmlJualTerkecil"
    t.varchar "KeKuatan", limit: 12
    t.integer "JmlKemasan"
    t.varchar "KdPabrik", limit: 4
    t.char "KdStatusBarang", limit: 2
    t.varchar "KdGenerikBarang", limit: 9
    t.char "KdDetailGolBarang", limit: 3
    t.char "StatusAktif", limit: 1
    t.integer "StatusEnabled", limit: 1, null: false
    t.varchar "KodeExternal", limit: 50
    t.varchar "NamaExternal", limit: 50
    t.index ["KdBarang", "KdDetailJenisBarang", "KdKategoryBarang", "KdGolBarang", "KdSatuanJmlB", "KdSatuanJmlK", "JmlTerkecil", "JmlJualTerkecil", "JmlKemasan", "KdPabrik", "KdGenerikBarang", "KdDetailGolBarang"], name: "IX-MasterBarang"
  end

  create_table "MasterBarangDetail", primary_key: ["KdBarang", "tglEdit"], force: :cascade do |t|
    t.char "KdBarang", limit: 9, null: false
    t.integer "IsiKecil"
    t.varchar "Ket", limit: 100
    t.varchar "KodeExternal", limit: 100
    t.varchar "NamaExternal", limit: 100
    t.datetime "tglEdit", null: false
    t.char "StatusEnabled", limit: 1
  end

  create_table "MasterBarangIHS", id: false, force: :cascade do |t|
    t.varchar "KdBarang", limit: 20, null: false
    t.varchar "NamaBarang", limit: 1000
    t.varchar "JenisBarang", limit: 1000
    t.varchar "NamaPabrik", limit: 3000
    t.varchar "KdBarangZatAktif", limit: 2000
    t.varchar "KdKFA", limit: 2000
    t.varchar "KdPOAPabrik", limit: 2000
    t.varchar "NamaBarangIHS", limit: 2000
    t.varchar "IHSID", limit: 2000
    t.varchar "Keterangan", limit: 2000
    t.varchar "StatusEnabled", limit: 1
  end

  create_table "MasterBarangNonMedis", primary_key: "KdBarang", id: { type: :varchar, limit: 9 }, force: :cascade do |t|
    t.varchar "NamaBarang", limit: 500, null: false
    t.varchar "KdDetailJenisBarang", limit: 5, null: false
    t.char "KdSatuanJmlB", limit: 2
    t.varchar "KdBahanBarang", limit: 3
    t.varchar "KdType", limit: 3
    t.integer "JmlKemasan", null: false
    t.integer "JmlTerkecil"
    t.real "JmlJualTerkecil"
    t.varchar "KdWarnaBarang", limit: 2
    t.integer "KdGolonganLinen", limit: 1
    t.datetime "TglProduksi"
    t.integer "StatusEnabled", limit: 1, null: false
    t.varchar "KodeExternal", limit: 50
    t.varchar "NamaExternal", limit: 50
  end

  create_table "MasterDataPendukung", primary_key: "TglBerlaku", id: :datetime, force: :cascade do |t|
    t.integer "JmlPembulatanHarga", null: false
    t.integer "LamaToleransi", limit: 1, null: false
    t.char "StatusPersenCito", limit: 1, null: false
    t.varchar "MessageToDay", limit: 150, null: false
    t.char "KdKomponenHargaNetto", limit: 2, null: false
    t.integer "LamaBerlakuNoRujukan", null: false
    t.char "KdKomponenTarifServisResep", limit: 2, null: false
    t.char "KdKomponenProfit", limit: 2, null: false
    t.char "KdKomponenTarifTotalTM", limit: 2, null: false
    t.char "KdKomponenTarifTotalOA", limit: 2, null: false
    t.char "KdKomponenTarifCito", limit: 2, null: false
    t.varchar "NamaPrinterBarcode", limit: 50
    t.char "KdPelayananRSOA", limit: 6, null: false
    t.char "KdKomponenTarifLabLuar", limit: 2, null: false
    t.char "KdKomponenAdm", limit: 2, null: false
    t.char "KdKomponenDiscount", limit: 2, null: false
    t.char "KdPelayananRSAdmin", limit: 6
    t.integer "JumlahBAdminOAPerBaris"
    t.integer "JumlahBAdminTMPerHari"
  end

  create_table "MasterJadwalPraktekDokter", id: false, force: :cascade do |t|
    t.char "KdPraktek", limit: 10, null: false
    t.varchar "Hari", limit: 7
    t.varchar "IdDokter", limit: 10
    t.char "KdRuangan", limit: 3
    t.varchar "JamMulai", limit: 5
    t.varchar "JamSelesai", limit: 5
    t.varchar "Keterangan", limit: 50
    t.char "StatusEnabled", limit: 1
    t.varchar "Kuota", limit: 50
    t.char "StatusDokter", limit: 1
  end

  create_table "MasterJam", id: false, force: :cascade do |t|
    t.char "KdJam", limit: 2, null: false
    t.varchar "JamMulai", limit: 5
    t.varchar "JamSelesai", limit: 5
  end

  create_table "MasterRiwayatDahuluEMR", id: :integer, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 50
    t.varchar "NoCM", limit: 50
    t.varchar "Riwayat", limit: 150
    t.datetime "CreateDate"
  end

  create_table "MasterShift", id: false, force: :cascade do |t|
    t.char "Kd", limit: 2, null: false
    t.varchar "Kdshift", limit: 3, null: false
    t.varchar "Uraian", limit: 900
    t.varchar "JamMulai", limit: 5
    t.varchar "JamSelesai", limit: 5
    t.char "StatusEnabled", limit: 1
  end

  create_table "MasterTemplateDokterPhp", id: :integer, force: :cascade do |t|
    t.integer "idkategori"
    t.varchar "IdDokter", limit: 50
    t.varchar "KdRuangan", limit: 50
    t.varchar_max "NamaTemplate"
    t.varchar "NamaDokter", limit: 150
    t.varchar_max "IsiTemplate"
    t.datetime "CreateDate"
  end

  create_table "MasterUraianBLUD", id: false, force: :cascade do |t|
    t.integer "Kd"
    t.integer "Kode"
    t.varchar "Uraian"
    t.integer "StatusEnabled", limit: 1
    t.char "KdSatuanJmlK", limit: 10
    t.char "KdSatuanJmlK2", limit: 10
    t.integer "KdRekening", limit: 1
  end

  create_table "Master_Cara_Datang_IGD", id: false, force: :cascade do |t|
    t.char "KdCaraDatang", limit: 2, null: false
    t.varchar "NamaExternal", limit: 30
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "Master_Geriatri_Rajal", id: false, force: :cascade do |t|
    t.varchar "KdGeriatri", limit: 3, null: false
    t.varchar "NamaExternal", limit: 250, null: false
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "Master_Informasi_Rujukan_IGD", id: false, force: :cascade do |t|
    t.char "KdInfoRujukan", limit: 2, null: false
    t.varchar "NamaExternal", limit: 30
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "Master_Konfirmasi_IGD", id: false, force: :cascade do |t|
    t.char "KdKonfirmasi", limit: 2, null: false
    t.varchar "NamaExternal", limit: 30
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "MayorObjektifKeperawatan", id: false, force: :cascade do |t|
    t.varchar "KdAsuhan", limit: 7, null: false
    t.varchar "SubGejala", limit: 100
    t.varchar "KdMayorObjektif", limit: 7, null: false
    t.varchar "NamaMayorObjektif", limit: 1000, null: false
    t.varchar "NamaExternal", limit: 500
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "MayorSubjektifKeperawatan", id: false, force: :cascade do |t|
    t.varchar "KdAsuhan", limit: 7, null: false
    t.varchar "SubGejala", limit: 100
    t.varchar "KdMayorSubjektif", limit: 7, null: false
    t.varchar "NamaMayorSubjektif", limit: 1000, null: false
    t.varchar "NamaExternal", limit: 500
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "MedisFixStok", id: false, force: :cascade do |t|
    t.varchar "KdBarang", limit: 10
    t.float "SisaStok"
  end

  create_table "MedisKartuStok", id: false, force: :cascade do |t|
    t.datetime "Tanggalinput"
    t.datetime "Tanggalkom"
    t.varchar "JenisTransaksi", limit: 12
    t.varchar "NoBukti", limit: 10
    t.varchar "KdBarang", limit: 9
    t.float "JumlahMasuk"
    t.float "JumlahKeluar"
    t.float "SisaStok"
    t.char "KdRuanganAwal", limit: 3
    t.char "KdRuanganAhir", limit: 3
  end

  create_table "MentalStatus_TP", id: false, force: :cascade do |t|
    t.varchar "KodeMentalStatus", limit: 50
    t.varchar "NamaMentalStatus", limit: 50
    t.integer "Enable"
  end

  create_table "MetodePemeriksaanLab", primary_key: "KdMetode", id: { type: :char, limit: 2 }, force: :cascade do |t|
    t.varchar "NamaMetode", limit: 100
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 30
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "MinorObjektifKeperawatan", id: false, force: :cascade do |t|
    t.varchar "KdAsuhan", limit: 7, null: false
    t.varchar "SubGejala", limit: 100
    t.varchar "KdMinorObjektif", limit: 7, null: false
    t.varchar "NamaMinorObjektif", limit: 1000, null: false
    t.varchar "NamaExternal", limit: 500
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "MinorSubjektifKeperawatan", id: false, force: :cascade do |t|
    t.varchar "KdAsuhan", limit: 7, null: false
    t.varchar "SubGejala", limit: 100
    t.varchar "KdMinorSubjektif", limit: 7, null: false
    t.varchar "NamaMinorSubjektif", limit: 1000, null: false
    t.varchar "NamaExternal", limit: 500
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "NamaGenerikBarang", primary_key: "KdGenerikBarang", id: { type: :varchar, limit: 9 }, force: :cascade do |t|
    t.varchar "NamaGenerikBarang", limit: 500, null: false
    t.integer "QGenerikBarang", null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "NamaOperasiPasien", id: false, force: :cascade do |t|
    t.char "IdNamaOperasi", limit: 3, null: false
    t.varchar "NamaOperasi", limit: 100, null: false
    t.char "NamaExternal", limit: 100
    t.char "StatusEnabled", limit: 1, null: false
  end

  create_table "Nama_Jenis_Eklaim", primary_key: "ID_JnsEklaim", id: :integer, force: :cascade do |t|
    t.varchar "KdJnsEklaim", limit: 50
    t.varchar "NamaJenisEklaim", limit: 50
    t.varchar "StatusEnabled", limit: 10
  end

  create_table "Negara", id: false, force: :cascade do |t|
    t.integer "KdNegara", limit: 1, null: false
    t.varchar "NamaNegara", limit: 50, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "NilaiNormalPemeriksaanPK", id: false, force: :cascade do |t|
    t.char "kdNilaiNormal", limit: 4, null: false
    t.char "kdMetode", limit: 2, null: false
    t.char "gender", limit: 1
    t.char "kdKelompokUmur", limit: 3
    t.varchar "rangeMin", limit: 100
    t.varchar "rangeMax", limit: 100
    t.varchar "rangeStandar", limit: 100
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "NoKamar", id: false, force: :cascade do |t|
    t.char "KdKamar", limit: 4, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "KdKelas", limit: 2, null: false
    t.integer "JlhBed", limit: 1, null: false
    t.varchar "NamaKamar", limit: 50, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 30
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "NonMedisFixStok", id: false, force: :cascade do |t|
    t.varchar "KdBarang", limit: 10
    t.float "SisaStok"
  end

  create_table "ObservasiCairan", id: false, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 50
    t.varchar "NoCM", limit: 50
    t.datetime "tanggaldanjam"
    t.varchar "JenisCairan", limit: 50
    t.varchar "NoBotol", limit: 50
    t.varchar_max "Obat"
    t.varchar_max "Dosis"
    t.varchar_max "Rute"
    t.varchar "TtdKeluarga", limit: 50
    t.varchar_max "Oral"
    t.varchar_max "Drain"
    t.varchar_max "NGT"
    t.varchar_max "Urine"
    t.varchar_max "Bab"
    t.datetime "CreateDate"
  end

  create_table "ObservasiKeperawatan", primary_key: "IdOK", id: :integer, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10
    t.char "NoCM", limit: 8
    t.char "IdUser", limit: 10
    t.varchar_max "Keterangan"
    t.datetime "CreateDate"
  end

  create_table "ObservasiKeperawatanRanap", id: false, force: :cascade do |t|
    t.varchar "KdDiagnosa", limit: 7, null: false
    t.varchar "SubIntervensi", limit: 1000
    t.varchar "KdObservasi", limit: 7
    t.varchar "NamaObservasi", limit: 1000
    t.varchar "NamaExternal", limit: 500
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "OrderPemesananObatEMR", id: false, force: :cascade do |t|
    t.char "NoOrder", limit: 10, null: false
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCm", limit: 8, null: false
    t.char "kdRuangan", limit: 3
    t.char "IdUser", limit: 10
    t.datetime "TglOrder"
    t.char "KdRuanganTujuan", limit: 3, null: false
    t.varchar "NoResep", limit: 10
    t.varchar "Status", limit: 3
    t.index ["NoOrder"], name: "index_NoOrder", unique: true
  end

  create_table "PENGAJUAN_CUTI", primary_key: "KD_AJUAN", id: :integer, force: :cascade do |t|
    t.varchar "PIN", limit: 6
    t.varchar "KD_CUTI", limit: 6
    t.date "TGL_AJUAN"
    t.date "TGL_AWAL"
    t.date "TGL_AKHIR"
    t.varchar "CUTI_TAHUN", limit: 10
    t.float "LAMA_CUTI"
    t.varchar "STATUS_CUTI", limit: 50
    t.varchar "SATUAN_ORGANISASI", limit: 100
    t.varchar "ALAMAT", limit: 100
    t.varchar "DELEGASI", limit: 200
    t.varchar "KETERANGAN", limit: 200
    t.varchar "NMPEJABAT", limit: 100
    t.varchar "VALIDASIATASAN", limit: 60
    t.float "TAMBAHANMENIT"
    t.float "TOTALWAKTUEFEKTIF"
  end

  create_table "Pabrik", primary_key: "KdPabrik", id: { type: :varchar, limit: 4 }, force: :cascade do |t|
    t.varchar "NamaPabrik", limit: 50, null: false
    t.varchar "AlamatPabrik", limit: 100
    t.varchar "KotaPabrik", limit: 50
    t.varchar "ProvinsiPabrik", limit: 50
    t.varchar "KodePosPabrik", limit: 50
    t.varchar "TelponPabrik", limit: 50
    t.varchar "FaxPabrik", limit: 50
    t.varchar "emailPabrik", limit: 50
    t.varchar "NegaraPabrik", limit: 50
    t.varchar "Website", limit: 200
    t.integer "QPabrik", limit: 2, null: false
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "PaketAsuransi", id: false, force: :cascade do |t|
    t.varchar "KdPaket", limit: 3, null: false
    t.varchar "NamaPaket", limit: 50, null: false
    t.varchar "Deskripsi", limit: 50
    t.char "StatusTanggungan", limit: 2
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "PaketLayanan", id: false, force: :cascade do |t|
    t.char "KdPelayananRS", limit: 6, null: false
    t.varchar "KdBarang", limit: 9, null: false
    t.char "KdAsal", limit: 2, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.integer "JmlBarang", limit: 1, null: false
    t.char "SatuanJml", limit: 1, null: false
  end

  create_table "PaketPelayananOA", primary_key: "KdPaket", id: :integer, force: :cascade do |t|
    t.varchar "NamaPaket", limit: 50, null: false
    t.varchar "Deskripsi", limit: 50
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "PaketPelayananTM", primary_key: "KdPaket", id: { type: :varchar, limit: 3 }, force: :cascade do |t|
    t.varchar "NamaPaket", limit: 50, null: false
    t.varchar "Deskripsi", limit: 50
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "Pangkat", primary_key: "KdPangkat", id: { type: :varchar, limit: 2 }, force: :cascade do |t|
    t.varchar "NamaPangkat", limit: 50, null: false
    t.char "NoUrut", limit: 2
    t.varchar "KdGolongan", limit: 2
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 30
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "Pasien", primary_key: "QPasien", id: :integer, force: :cascade do |t|
    t.char "NoCM", limit: 15, null: false
    t.varchar "NoIdentitas", limit: 20
    t.datetime "TglDaftarMembership", null: false
    t.varchar "Title", limit: 4, null: false
    t.varchar "NamaLengkap", limit: 50, null: false
    t.varchar "TempatLahir", limit: 25
    t.datetime "TglLahir", null: false
    t.char "JenisKelamin", limit: 1, null: false
    t.varchar "Alamat", limit: 100
    t.varchar "Telepon", limit: 15
    t.varchar "Propinsi", limit: 50
    t.varchar "Kota", limit: 25
    t.varchar "Kecamatan", limit: 25
    t.varchar "Kelurahan", limit: 25
    t.varchar "RTRW", limit: 5
    t.char "KodePos", limit: 5
    t.varchar "NoRM", limit: 12
    t.index ["NoCM", "NamaLengkap", "NoRM"], name: "IX_Pasien10132022"
  end

  create_table "PasienBatalDirawat", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.char "KdSubInstalasi", limit: 3, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.datetime "TglBatal", null: false
    t.varchar "Keterangan", limit: 100
    t.char "IdPegawai", limit: 10, null: false
  end

  create_table "PasienBatalPeriksa", primary_key: "NoPendaftaran", id: { type: :char, limit: 10 }, force: :cascade do |t|
    t.char "NoCM", limit: 15, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.datetime "TglMasuk", null: false
    t.datetime "TglBatalPeriksa", null: false
  end

  create_table "PasienBatalPulang", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.datetime "TglBatal", null: false
    t.char "IdPegawai", limit: 10, null: false
  end

  create_table "PasienBelumBayar", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
  end

  create_table "PasienDaftar", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.char "KdKelompokPasien", limit: 2, null: false
    t.datetime "TglPendaftaran", null: false
    t.char "KdDetailJenisJasaPelayanan", limit: 2, null: false
    t.char "StatusPasien", limit: 4, null: false
    t.char "KdRuanganAkhir", limit: 3, null: false
    t.datetime "TglPulang"
    t.varchar "KdPaket", limit: 3
    t.char "KdKelasAkhir", limit: 2, null: false
    t.char "IdPenjamin", limit: 10, null: false
    t.char "StatusLimitBPOA", limit: 1, null: false
    t.char "StatusLimitBPTM", limit: 1, null: false
    t.integer "HariTglMasuk"
    t.integer "BlnTglMasuk"
    t.integer "ThnTglMasuk"
    t.index ["NoPendaftaran", "NoCM"], name: "IX_PasienDaftar"
    t.index ["NoPendaftaran", "NoCM"], name: "NonClusteredIndex-20230130-143802"
  end

  create_table "PasienDaftarKoja", primary_key: "NoPendaftaranAsli", id: { type: :char, limit: 12 }, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
  end

  create_table "PasienIGDKeRi", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.datetime "TglKeluar", null: false
    t.char "KdStatusKeluar", limit: 2, null: false
    t.char "KdKondisiPulang", limit: 2, null: false
    t.char "IdUser", limit: 10, null: false
    t.integer "LamaDirawat", null: false
    t.char "DeskripsiLamaDirawat", limit: 8, null: false
    t.varchar "TempatTujuan", limit: 100
    t.char "KdRuangan", limit: 3
    t.integer "HariTglKeluar"
    t.integer "BlnTglKeluar"
    t.integer "ThnTglKeluar"
    t.char "KdKamar", limit: 4, null: false
    t.char "NoBed", limit: 2, null: false
    t.char "StatusMasuk", limit: 1
  end

  create_table "PasienIGDKeluar", primary_key: "NoPendaftaran", id: { type: :char, limit: 10 }, force: :cascade do |t|
    t.char "NoCM", limit: 15, null: false
    t.datetime "TglKeluar", null: false
    t.char "KdStatusKeluar", limit: 2, null: false
    t.char "KdKondisiPulang", limit: 2, null: false
    t.char "IdUser", limit: 10, null: false
    t.integer "LamaDirawat", null: false
    t.char "DeskripsiLamaDirawat", limit: 8, null: false
    t.varchar "TempatTujuan", limit: 100
    t.char "KdRuangan", limit: 3
    t.integer "HariTglKeluar"
    t.integer "BlnTglKeluar"
    t.integer "ThnTglKeluar"
  end

  create_table "PasienJKN", primary_key: "idPasienJKN", id: :integer, force: :cascade do |t|
    t.varchar "nomorKartuBPJS", limit: 50
    t.varchar "nik", limit: 50
    t.varchar "nomorKK", limit: 50
    t.varchar "nama", limit: 50
    t.varchar "jenisKelamin", limit: 4
    t.datetime "tanggalLahir"
    t.varchar "noHP", limit: 20
    t.varchar_max "alamat"
    t.varchar "kodeProvinsi", limit: 10
    t.varchar "namaProvinsi", limit: 50
    t.varchar "kodeKabKota", limit: 10
    t.varchar "namaKabKota", limit: 50
    t.varchar "kodeKec", limit: 10
    t.varchar "namaKec", limit: 50
    t.varchar "kodeKel", limit: 10
    t.varchar "namaKel", limit: 50
    t.varchar "rw", limit: 10
    t.varchar "rt", limit: 10
    t.datetime "createdate"
  end

  create_table "PasienKDRT", id: false, force: :cascade do |t|
    t.varchar "NoUrut", limit: 7, null: false
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 8, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "StatusPasien", limit: 4
    t.char "KetVisum", limit: 1
    t.datetime "TglInput", null: false
    t.datetime "TglUpdate"
    t.char "KdDirujukKe", limit: 3
    t.char "KdAsalRujukan", limit: 2
    t.char "KdStatusPasien", limit: 2
    t.char "IdDokter", limit: 10
    t.varchar "Keterangan", limit: 100
    t.char "IdUser", limit: 10, null: false
  end

  create_table "PasienKeluarIgd", id: false, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 50
    t.varchar "NoCM", limit: 50
    t.varchar_max "PesanSQL"
    t.varchar_max "PesanError"
    t.datetime "CreateDate"
  end

  create_table "PasienKeluarKamar", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.char "NoPakai", limit: 10, null: false
    t.datetime "TglKeluar", null: false
    t.integer "LamaDirawat", limit: 1, null: false
    t.char "KdStatusKeluar", limit: 2, null: false
    t.char "KdKondisiKeluar", limit: 2
    t.char "IdUser", limit: 10, null: false
    t.char "KdRuangan", limit: 3
    t.integer "HariTglKeluar"
    t.integer "BlnTglKeluar"
    t.integer "ThnTglKeluar"
  end

  create_table "PasienKeluarmeninggal", id: false, force: :cascade do |t|
    t.char "NoUrut", limit: 7, null: false
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.datetime "TglKeluar", null: false
    t.char "KdStatusKeluar", limit: 2, null: false
    t.char "KdKondisiPulang", limit: 2
    t.char "IdUser", limit: 10, null: false
    t.char "KdRuangan", limit: 3
    t.integer "HariTglKeluar"
    t.integer "BlnTglKeluar"
    t.integer "ThnTglKeluar"
    t.varchar "NoKematian", limit: 200
  end

  create_table "PasienMPP", id: false, force: :cascade do |t|
    t.integer "IdMPP", null: false
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 8, null: false
    t.char "KdDiagnosa", limit: 5, null: false
    t.char "Terminasi", limit: 2, null: false
    t.char "Justifikasi", limit: 255, null: false
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
    t.varchar "IdUser", limit: 10
    t.float "Simulasi"
  end

  create_table "PasienMPP_HasilPelayanan", id: false, force: :cascade do |t|
    t.char "IdHP", limit: 2, null: false
    t.char "NamaHP", limit: 255, null: false
    t.char "StatusEnabled", limit: 1, null: false
  end

  create_table "PasienMPP_Input_Identifikasi", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.char "Identifikasi", limit: 1000, null: false
    t.datetime "CreateDate"
    t.varchar "IdUser", limit: 10
  end

  create_table "PasienMPP_Input_Penilaian", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.char "IdHP", limit: 2, null: false
    t.varchar "IdUser", limit: 10
  end

  create_table "PasienMPP_Input_Skrining", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.char "IdSkrining", limit: 3, null: false
    t.varchar "IdUser", limit: 10
  end

  create_table "PasienMPP_Skrining", id: false, force: :cascade do |t|
    t.char "IdSkrining", limit: 3, null: false
    t.char "NamaSkrining", limit: 255, null: false
    t.char "StatusEnabled", limit: 1, null: false
  end

  create_table "PasienMPP_SkriningAwal", id: false, force: :cascade do |t|
    t.char "IdSkriningAwal", limit: 10, null: false
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 8, null: false
    t.char "JmlDokter", limit: 1, null: false
    t.char "TindakanBeresiko", limit: 1, null: false
    t.char "LOS", limit: 1, null: false
    t.char "PotensialKomplain", limit: 1, null: false
    t.char "KemungkinanCacat", limit: 1, null: false
    t.char "PotensialBiaya", limit: 1, null: false
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
    t.varchar "IdUser", limit: 10
    t.varchar "Diagnosa", limit: 255
  end

  create_table "PasienMasukRumahSakit", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.char "KdSubInstalasi", limit: 3, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.datetime "TglMasuk", null: false
    t.char "KdKelas", limit: 2, null: false
    t.char "NoAntrian", limit: 3, null: false
    t.char "StatusPasien", limit: 4, null: false
    t.char "StatusPeriksa", limit: 1, null: false
    t.char "IdUser", limit: 10, null: false
  end

  create_table "PasienMeninggal", primary_key: "NoPendaftaran", id: { type: :char, limit: 10 }, force: :cascade do |t|
    t.char "NoCM", limit: 15, null: false
    t.datetime "TglMeninggal", null: false
    t.integer "KdPenyebab", limit: 1
    t.char "IdDokter", limit: 10, null: false
    t.char "IdPegawai", limit: 10, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "KdSubInstalasi", limit: 3, null: false
  end

  create_table "PasienPMK", id: false, force: :cascade do |t|
    t.datetime "SysUpdate"
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.char "NoPakai", limit: 15, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "KdKelas", limit: 2, null: false
    t.char "KdKamar", limit: 4, null: false
    t.char "NoBed", limit: 2, null: false
    t.datetime "TglMasuk", null: false
    t.char "StatusPasien", limit: 1, null: false
    t.varchar "PIN", limit: 10
  end

  create_table "PasienPenyelidikanEpidemiologiCovid", primary_key: "NoUrut", id: :integer, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10
    t.char "NoCM", limit: 8
    t.datetime "TglGejala"
    t.varchar "Demam", limit: 10
    t.varchar "Batuk", limit: 1
    t.varchar "Pilek", limit: 1
    t.varchar "SakitTenggorokan", limit: 1
    t.varchar "SesakNapas", limit: 1
    t.varchar "Menggigil", limit: 1
    t.varchar "SakitKepala", limit: 1
    t.varchar "Lemah", limit: 1
    t.varchar "NyeriOtot", limit: 1
    t.varchar "Mual", limit: 1
    t.varchar "NyeriAbdomen", limit: 1
    t.varchar "Diare", limit: 1
    t.varchar "LainnyaKlinis", limit: 50
    t.varchar "Hamil", limit: 1
    t.varchar "Diabetes", limit: 1
    t.varchar "PenyakitJantung", limit: 1
    t.varchar "Hipertensi", limit: 1
    t.varchar "Keganasan", limit: 1
    t.varchar "GangguanImunologi", limit: 1
    t.varchar "GagalGinjalKronis", limit: 1
    t.varchar "GagalHatiKronis", limit: 1
    t.varchar "PPOK", limit: 1
    t.varchar "LainnyaPenyerta", limit: 50
    t.varchar "PernahDirawat", limit: 1
    t.datetime "TglDirawat"
    t.varchar "NamaRS", limit: 50
    t.varchar "Ruangan", limit: 50
    t.varchar "DirawatICU", limit: 1
    t.varchar "DirawatIntubasi", limit: 1
    t.varchar "DirawatEMCO", limit: 1
    t.varchar "StatusTerakhir", limit: 2
    t.varchar "Pneumonia", limit: 1
    t.varchar "ARDS", limit: 1
    t.varchar "DiagnosaLainnya", limit: 100
    t.varchar "DiagnosaEtiologi", limit: 1
    t.varchar "DiagnosaEtiologiLainnya", limit: 100
    t.varchar "KontakSuspek", limit: 1
    t.varchar "KontakPositif", limit: 1
    t.varchar "KontakPasarHewan", limit: 1
    t.varchar "LokasiKontakHewan", limit: 50
    t.varchar "PetugasKesehatan", limit: 1
    t.varchar "RiwayatBerkunjung", limit: 1
    t.varchar "LokasiBerkunjung", limit: 50
    t.varchar "ClusterISPA", limit: 1
    t.varchar "ClusterLainnya", limit: 50
    t.char "KdRujukanAsal", limit: 3
    t.datetime "TglInput"
    t.datetime "TglUpdate"
    t.char "IdUser", limit: 10
    t.varchar "XRayParu", limit: 1
    t.varchar "HasilXRay", limit: 50
    t.varchar "HitungLekosit", limit: 1
    t.varchar "Lekosit", limit: 50
    t.varchar "Limposit", limit: 50
    t.varchar "Trombosit", limit: 50
    t.varchar "Ventilator", limit: 1
    t.varchar "Serologis", limit: 1
    t.datetime "SerologisTanggal"
    t.varchar "UsapNasofaring", limit: 1
    t.datetime "UsapNasofaringTanggal"
    t.varchar "UsapOrofaring", limit: 1
    t.datetime "UsapOrofaringTanggal"
    t.varchar "Sputum", limit: 1
    t.datetime "SputumTanggal"
    t.varchar "SampelLainnya", limit: 50
    t.datetime "SampelLainnyaTanggal"
  end

  create_table "PasienPenyelidikanEpidemiologiCovid_Hubungan", primary_key: "Id", id: :integer, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10
    t.char "NoCM", limit: 8
    t.varchar "Nama", limit: 100
    t.varchar "Umur", limit: 20
    t.varchar "JenisKelamin", limit: 1
    t.varchar "Hubungan", limit: 100
    t.varchar "Alamat", limit: 100
    t.varchar "NoHP", limit: 20
    t.datetime "TglKontakAwal"
    t.datetime "TglKontakAkhir"
    t.datetime "TglInput"
    t.char "IdUser", limit: 10
  end

  create_table "PasienPenyelidikanEpidemiologiCovid_PemeriksaanPenunjang", primary_key: "Id", id: :integer, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10
    t.char "NoCM", limit: 8
    t.varchar "NamaPemeriksaan", limit: 100
    t.varchar "NamaPemeriksaanLainnya", limit: 100
    t.datetime "TglPengambilan"
    t.varchar "TempatPemeriksaan", limit: 50
    t.varchar "Hasil", limit: 100
    t.datetime "TglInput"
    t.char "IdUser", limit: 10
  end

  create_table "PasienPenyelidikanEpidemiologiCovid_Riwayat", primary_key: "Id", id: :integer, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10
    t.char "NoCM", limit: 8
    t.varchar "LokasiKunjungan", limit: 10
    t.datetime "TglPerjalanan"
    t.datetime "TglTiba"
    t.datetime "TglInput"
    t.char "IdUser", limit: 10
  end

  create_table "PasienPenyelidikanEpidemiologiCovid_RiwayatSuspek", primary_key: "Id", id: :integer, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10
    t.char "NoCM", limit: 8
    t.varchar "LokasiDirawat", limit: 10
    t.datetime "TglKunjungan"
    t.datetime "TglInput"
    t.char "IdUser", limit: 10
  end

  create_table "PasienPerjanjian", primary_key: "No", force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCm", limit: 8, null: false
    t.varchar "NamaPenjamin", limit: 50
    t.char "JK", limit: 1
    t.datetime "TglLahir"
    t.varchar "NIP", limit: 50
    t.char "KdHubungan", limit: 2
    t.varchar "Alamat", limit: 50
    t.varchar "RTRW", limit: 10
    t.varchar "Telepon", limit: 30
    t.char "KdPropinsi", limit: 2
    t.char "KdKotaKab", limit: 4
    t.char "KdKec", limit: 6
    t.char "KdKel", limit: 8
    t.varchar "KodePos", limit: 10
    t.char "Status", limit: 1
    t.datetime "TanggalPerjanjian"
    t.float "TotalBiayaPerjanjian"
    t.float "JmldepositPerjanjian"
    t.varchar "Pembayaranke", limit: 50
    t.char "NoStruk", limit: 10
    t.char "NoBkm", limit: 10
  end

  create_table "PasienPrioritas", primary_key: "IdPasienPrioritas", id: :integer, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10
    t.varchar_max "Keterangan"
    t.datetime "Createdate"
  end

  create_table "PasienPulang", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.datetime "TglPulang", null: false
    t.integer "LamaDirawat", limit: 1, null: false
    t.varchar "NamaPenerima", limit: 30
    t.char "KdStatusPulang", limit: 2, null: false
    t.char "KdKondisiPulang", limit: 2, null: false
    t.char "IdPegawai", limit: 10, null: false
    t.datetime "TglDMKKembali"
    t.char "KdRuangan", limit: 3
    t.integer "HariTglPulang"
    t.integer "BlnTglPulang"
    t.integer "ThnTglPulang"
    t.index ["NoPendaftaran", "NoCM", "IdPegawai"], name: "IX_PasienPulang10132022"
  end

  create_table "PasienPulangPaksa", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.char "KdStatusKeluar", limit: 3, null: false
    t.varchar "Keterangan", limit: 255, null: false
    t.datetime "TglKeluar", null: false
    t.char "IdDokter", limit: 10, null: false
    t.char "IdPegawai", limit: 10, null: false
    t.char "KdRuangan", limit: 3, null: false
  end

  create_table "PasienRajalBokingRanap", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15
    t.varchar "NamaLengkap", limit: 500
    t.char "KdRujukanPerujuk", limit: 3
    t.varchar "Boking", limit: 100
    t.datetime "TglDirujuk"
    t.varchar "Diagnosa", limit: 500
    t.char "IdUser", limit: 10
  end

  create_table "PasienRujukan", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.char "KdRuanganAsal", limit: 3, null: false
    t.char "KdRuanganTujuan", limit: 3, null: false
    t.datetime "TglDirujuk", null: false
    t.char "IdDokterPerujuk", limit: 10, null: false
    t.char "StatusPeriksa", limit: 1, null: false
    t.char "NoAntrian", limit: 3
    t.char "KdSubInstalasi", limit: 3, null: false
    t.char "IdUser", limit: 10
  end

  create_table "PasienRujukanAGD", id: false, force: :cascade do |t|
    t.varchar "NoUrut", limit: 7, null: false
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "KdRujukanAsal", limit: 3
    t.char "Datang", limit: 1
    t.char "Pulang", limit: 1
    t.datetime "TglInput", null: false
    t.datetime "TglUpdate"
    t.char "KdDirujukKe", limit: 3
    t.char "IdDokter", limit: 10
    t.char "KdKondisiRujukan", limit: 2
    t.char "KdKondisiPasien", limit: 2
    t.varchar "Keterangan", limit: 100
    t.char "IdUser", limit: 10, null: false
  end

  create_table "PasienRujukanEMR", primary_key: "NoRujukan", id: :integer, force: :cascade do |t|
    t.varchar "KdDiagnosa", limit: 50
    t.varchar "NoPendaftaran", limit: 50
    t.varchar "NoCM", limit: 50
    t.varchar_max "NamaPasien"
    t.varchar_max "StatusRujuk"
    t.varchar "KdRuanganPerujuk", limit: 3
    t.varchar "KdRuanganPenerima", limit: 3
    t.varchar_max "RuanganPerujuk"
    t.varchar_max "RuanganPenerima"
    t.datetime "TglDirujuk"
    t.datetime "TglInput"
    t.varchar "IdDokterPerujuk", limit: 50
    t.varchar "IdDokterPenerima", limit: 50
    t.varchar "StatusDibaca", limit: 2
    t.varchar_max "Keterangan"
    t.varchar "StatusKonsul", limit: 10
    t.varchar "IdUser", limit: 50
    t.varchar "IdDokterBalas", limit: 10
    t.varchar "KdDiagnosaBalas", limit: 50
    t.varchar_max "KeteranganBalas"
    t.datetime "TglBalas"
    t.varchar "adm_nopendaftaran", limit: 15
    t.varchar "adm_iddokter", limit: 15
    t.datetime "adm_createdate"
  end

  create_table "PasienRujukanSPGDT", id: false, force: :cascade do |t|
    t.varchar "NoUrut", limit: 7, null: false
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "KdRujukanAsal", limit: 3
    t.char "Datang", limit: 1
    t.char "Pulang", limit: 1
    t.datetime "TglInput", null: false
    t.datetime "TglUpdate"
    t.char "KdDirujukKe", limit: 3
    t.char "IdDokter", limit: 10
    t.char "KdKondisiRujukan", limit: 2
    t.char "KdKondisiPasien", limit: 2
    t.varchar "Keterangan", limit: 255
    t.char "IdUser", limit: 10, null: false
    t.char "IdPetugas", limit: 10
  end

  create_table "PasienRujukanbedah", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.char "KdRuanganAsalawal", limit: 3, null: false
    t.char "KdRuanganAsalahir", limit: 3
    t.char "KdRuanganTujuan", limit: 3, null: false
    t.datetime "TglDirujuk", null: false
    t.char "IdDokterPerujuk", limit: 10, null: false
    t.char "StatusPeriksa", limit: 1, null: false
    t.char "NoAntrian", limit: 3
    t.char "KdSubInstalasi", limit: 3, null: false
    t.char "IdUser", limit: 10
  end

  create_table "PasienSelesaiFarmasi", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.char "NoPakai", limit: 10
    t.datetime "TglValidasi", null: false
    t.char "KdRuanganRawatInap", limit: 3
    t.char "KdRuanganFarmasi", limit: 3
    t.char "IdUser", limit: 10, null: false
    t.char "StatusSelesai", limit: 1, null: false
  end

  create_table "PasienSelesaiInput", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10
    t.char "NoCM", limit: 8
    t.char "IdUser", limit: 10
    t.datetime "TglSelesaiInput"
    t.char "KdRuanganInput", limit: 3
    t.index ["NoPendaftaran", "NoCM"], name: "IX_PasienSelesaiInput"
  end

  create_table "PasienSudahBayar", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.char "NoStruk", limit: 10, null: false
  end

  create_table "PasienTemporary", id: false, force: :cascade do |t|
    t.char "NoCM", limit: 15, null: false
    t.char "StatusAktif", limit: 1, null: false
    t.datetime "TglPembatalan", null: false
    t.char "IdUser", limit: 10, null: false
  end

  create_table "PasienYgMendapatPelayananOA", primary_key: ["NoPendaftaran", "KdRuangan", "KdRuanganAsal"], force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "KdRuanganAsal", limit: 3, null: false
  end

  create_table "Pekerjaan", primary_key: "KdPekerjaan", id: { type: :char, limit: 2 }, force: :cascade do |t|
    t.varchar "Pekerjaan", limit: 30, null: false
    t.integer "QPekerjaan", limit: 1, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 30
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "PelayananRuangan", id: false, force: :cascade do |t|
    t.char "KdRuangan", limit: 3, null: false
    t.char "KdPelayananRS", limit: 6, null: false
    t.char "Status", limit: 2, null: false
  end

  create_table "PemakaianAlkes", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "KdSubInstalasi", limit: 3, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "KdKelas", limit: 2, null: false
    t.varchar "KdBarang", limit: 9, null: false
    t.char "KdAsal", limit: 2, null: false
    t.decimal "JmlBarang", precision: 9, scale: 2, null: false
    t.money "HargaSatuan", precision: 19, scale: 4, null: false
    t.datetime "TglPelayanan", null: false
    t.char "NoLab_Rad", limit: 10
    t.char "NoStruk", limit: 10
    t.char "IdPegawai", limit: 10, null: false
    t.char "SatuanJml", limit: 1, null: false
    t.money "HargaBeli", precision: 19, scale: 4, null: false
    t.char "IdPegawai2", limit: 10
    t.char "IdUser", limit: 10, null: false
    t.char "KdJenisObat", limit: 2
    t.integer "JmlService", null: false
    t.money "TarifService", precision: 19, scale: 4, null: false
    t.varchar "NoResep", limit: 15
    t.money "BiayaAdministrasi", precision: 19, scale: 4, null: false
    t.char "StatusStok", limit: 1
    t.char "KdRuanganAsal", limit: 3
    t.char "KdPelayananRSUsed", limit: 6
    t.char "KdStatusHasil", limit: 2
    t.integer "JmlExpose"
    t.char "KdStatusKontras", limit: 2
    t.char "IdPenanggungJawab", limit: 10
    t.varchar "Keterangan", limit: 500
    t.integer "HariTglPelayanan"
    t.integer "BlnTglPelayanan"
    t.integer "ThnTglPelayanan"
    t.char "NoTerima", limit: 10, null: false
  end

  create_table "PemakaianAlkesNew", id: false, force: :cascade do |t|
    t.integer "ID_Resep"
    t.char "NoPendaftaran", limit: 10
    t.char "KdSubInstalasi", limit: 3
    t.char "KdRuangan", limit: 3
    t.char "KdKelas", limit: 2
    t.varchar "KdBarang", limit: 9
    t.char "KdAsal", limit: 2
    t.decimal "JmlBarang", precision: 9, scale: 2
    t.money "HargaSatuan", precision: 19, scale: 4
    t.datetime "TglPelayanan"
    t.char "NoLab_Rad", limit: 10
    t.char "NoStruk", limit: 10
    t.char "IdPegawai", limit: 10
    t.char "SatuanJml", limit: 1
    t.money "HargaBeli", precision: 19, scale: 4
    t.char "IdPegawai2", limit: 10
    t.char "IdUser", limit: 10
    t.char "KdJenisObat", limit: 2
    t.integer "JmlService"
    t.money "TarifService", precision: 19, scale: 4
    t.varchar "NoResep", limit: 15
    t.money "BiayaAdministrasi", precision: 19, scale: 4
    t.char "StatusStok", limit: 1
    t.char "KdRuanganAsal", limit: 3
    t.char "KdPelayananRSUsed", limit: 6
    t.char "KdStatusHasil", limit: 2
    t.integer "JmlExpose"
    t.char "KdStatusKontras", limit: 2
    t.char "IdPenanggungJawab", limit: 10
    t.varchar "Keterangan", limit: 500
    t.integer "HariTglPelayanan"
    t.integer "BlnTglPelayanan"
    t.integer "ThnTglPelayanan"
    t.char "NoTerima", limit: 10
    t.index ["ID_Resep", "NoPendaftaran", "KdSubInstalasi", "KdRuangan", "KdKelas", "KdBarang", "KdAsal", "NoStruk", "KdJenisObat", "KdRuanganAsal"], name: "IX-PemakaianAlkesNew"
  end

  create_table "PemakaianAlkesObatKronis", primary_key: ["NoPendaftaran", "KdRuangan", "KdBarang", "KdAsal", "TglPelayanan", "SatuanJml", "NoTerima"], force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "KdSubInstalasi", limit: 3, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "KdKelas", limit: 2, null: false
    t.varchar "KdBarang", limit: 9, null: false
    t.char "KdAsal", limit: 2, null: false
    t.decimal "JmlBarang", precision: 9, scale: 2, null: false
    t.money "HargaSatuan", precision: 19, scale: 4, null: false
    t.datetime "TglPelayanan", null: false
    t.char "NoLab_Rad", limit: 10
    t.char "NoStruk", limit: 10
    t.char "IdPegawai", limit: 10, null: false
    t.char "SatuanJml", limit: 1, null: false
    t.money "HargaBeli", precision: 19, scale: 4, null: false
    t.char "IdPegawai2", limit: 10
    t.char "IdUser", limit: 10, null: false
    t.char "KdJenisObat", limit: 2
    t.integer "JmlService", null: false
    t.money "TarifService", precision: 19, scale: 4, null: false
    t.varchar "NoResep", limit: 15
    t.money "BiayaAdministrasi", precision: 19, scale: 4, null: false
    t.char "StatusStok", limit: 1
    t.char "KdRuanganAsal", limit: 3
    t.char "KdPelayananRSUsed", limit: 6
    t.char "KdStatusHasil", limit: 2
    t.integer "JmlExpose"
    t.char "KdStatusKontras", limit: 2
    t.char "IdPenanggungJawab", limit: 10
    t.varchar "Keterangan", limit: 500
    t.integer "HariTglPelayanan"
    t.integer "BlnTglPelayanan"
    t.integer "ThnTglPelayanan"
    t.char "NoTerima", limit: 10, null: false
    t.varchar "Ket", limit: 50
  end

  create_table "PemakaianAlkesRacikan", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.real "KebutuhanML", null: false
    t.real "KebutuhanTB"
    t.char "KdJenisObat", limit: 2, null: false
    t.integer "ResepKe", limit: 1, null: false
    t.varchar "kdBarang", limit: 9, null: false
    t.real "JumlahReal", null: false
    t.integer "Jumlah"
    t.integer "qtyRacikan", null: false
    t.varchar "AsalBarang", limit: 20
    t.char "kdAsal", limit: 2
    t.char "Satuan", limit: 1
    t.varchar "AturanPakai", limit: 50
    t.varchar "KeteranganPakai", limit: 50
    t.varchar "KeteranganLainnya", limit: 200
    t.varchar "KeteranganLainnya2", limit: 200
  end

  create_table "PemakaianAsuransi", id: false, force: :cascade do |t|
    t.char "IdPenjamin", limit: 10, null: false
    t.varchar "IdAsuransi", limit: 15, null: false
    t.char "NoCM", limit: 15, null: false
    t.varchar "NoPendaftaran", limit: 15, null: false
    t.char "KdHubungan", limit: 2, null: false
    t.varchar "NoSJP", limit: 30, null: false
    t.datetime "TglSJP", null: false
    t.varchar "NoBP", limit: 3
    t.integer "KunjunganKe"
    t.varchar "UnitBagian", limit: 50
    t.integer "AnakKe"
    t.char "KdKelasDiTanggung", limit: 2
    t.varchar "PayLoad", limit: 250
  end

  create_table "PemakaianBahanAlat", id: false, force: :cascade do |t|
    t.char "NoRiwayat", limit: 10, null: false
    t.char "KdPelayananRS", limit: 6, null: false
    t.char "KdBarang", limit: 9, null: false
    t.char "KdAsal", limit: 2, null: false
    t.datetime "TglPemakaian", null: false
    t.varchar "SatuanJml", limit: 10, null: false
    t.real "JmlBarang", null: false
    t.money "HargaSatuan", precision: 19, scale: 4, null: false
    t.varchar "Keperluan", limit: 200
    t.money "HargaBeli", precision: 19, scale: 4
    t.char "KdRuangan", limit: 3, null: false
    t.char "NoTerima", limit: 10, null: false
    t.char "NoPostingJurnal", limit: 10
    t.char "KdKelas", limit: 2
    t.integer "KdRincianBelanja", limit: 2
  end

  create_table "PemakaianBahanAlatTemp", id: false, force: :cascade do |t|
    t.char "NoRiwayat", limit: 10
    t.char "KdPelayananRS", limit: 6
    t.char "KdBarang", limit: 9, null: false
    t.varchar "NamaBarang", limit: 500
    t.char "KdAsal", limit: 2, null: false
    t.datetime "TglPemakaian", null: false
    t.varchar "SatuanJml", limit: 20, null: false
    t.real "JmlBarang", null: false
    t.money "HargaSatuan", precision: 19, scale: 4, null: false
    t.varchar "Keperluan", limit: 200
    t.money "HargaBeli", precision: 19, scale: 4
    t.char "KdRuangan", limit: 3, null: false
    t.char "NoTerima", limit: 10
    t.char "NoPostingJurnal", limit: 10
    t.char "KdKelas", limit: 2
    t.integer "KdRincianBelanja", limit: 2
    t.char "StatusVerif", limit: 1
    t.varchar "iduser", limit: 50
  end

  create_table "PemakaianKamar", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.char "NoPakai", limit: 10, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "KdKelas", limit: 2, null: false
    t.char "KdKamar", limit: 4, null: false
    t.char "NoBed", limit: 2, null: false
    t.datetime "TglMasuk", null: false
    t.char "StatusKeluar", limit: 1, null: false
    t.char "StatusPasien", limit: 4, null: false
    t.char "KdCaraMasuk", limit: 2
    t.char "KdRuanganAsal", limit: 3
    t.char "IdUser", limit: 10, null: false
    t.char "KdKelasPel", limit: 2, null: false
    t.char "StatusRawat", limit: 2, null: false
    t.integer "HariTglMasuk"
    t.integer "BlnTglMasuk"
    t.integer "ThnTglMasuk"
  end

  create_table "PemakaianMedisRuangan", id: false, force: :cascade do |t|
    t.char "NoRiwayat", limit: 10, null: false
    t.varchar "Kdbarang", limit: 9, null: false
    t.char "KdAsal", limit: 2
    t.datetime "TglPemakaian"
    t.datetime "TglKom"
    t.real "JmlBarang"
    t.money "HargaSatuan", precision: 19, scale: 4
    t.varchar "Keperluan", limit: 200
    t.money "HargaBeli", precision: 19, scale: 4
    t.char "KdRuangan", limit: 3, null: false
    t.char "NoPostingJurnal", limit: 10
    t.char "IdUser", limit: 10, null: false
  end

  create_table "PemakaianaAsuransiMultiple", id: false, force: :cascade do |t|
    t.char "IdPenjamin", limit: 10, null: false
    t.varchar "IdAsuransi", limit: 15, null: false
    t.char "NoCM", limit: 15, null: false
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "KdHubungan", limit: 2, null: false
    t.varchar "NoSJP", limit: 30, null: false
    t.datetime "TglSJP", null: false
    t.varchar "NoBP", limit: 3
    t.integer "KunjunganKe"
    t.varchar "UnitBagian", limit: 50
    t.integer "AnakKe"
    t.char "KdKelasDiTanggung", limit: 2
    t.char "KdInstalasi", limit: 2
    t.char "KdRuangan", limit: 3
    t.varchar "PayLoad", limit: 250
  end

  create_table "PembayaranDepositBiayaPerawatan", id: false, force: :cascade do |t|
    t.char "NoBKM", limit: 10, null: false
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.money "JmlBayarSebelumnya", precision: 19, scale: 4, null: false
    t.money "JmlSudahDibayar", precision: 19, scale: 4, null: false
    t.integer "PembayaranKe", limit: 1, null: false
    t.money "SisaDeposit", precision: 19, scale: 4, null: false
    t.char "NoStruk", limit: 10
  end

  create_table "PembayaranTagihanPasien", primary_key: "NoBKM", id: { type: :char, limit: 10 }, force: :cascade do |t|
    t.char "NoStruk", limit: 10, null: false
    t.money "JmlBayarSebelumnya", precision: 19, scale: 4, null: false
    t.money "JmlSudahDibayar", precision: 19, scale: 4, null: false
    t.money "JmlPembebasan", precision: 19, scale: 4, null: false
    t.money "SisaTagihan", precision: 19, scale: 4, null: false
    t.integer "PembayaranKe", limit: 1, null: false
    t.char "StatusPiutang", limit: 2
    t.money "SisaHutangPenjamin", precision: 19, scale: 4, null: false
  end

  create_table "PenanggungJawab", primary_key: "NoPendaftaran", id: { type: :char, limit: 10 }, force: :cascade do |t|
    t.char "NoCM", limit: 15, null: false
    t.varchar "NamaPJ", limit: 20, null: false
    t.varchar "AlamatPJ", limit: 50, null: false
    t.varchar "TeleponPJ", limit: 20
    t.char "Hubungan", limit: 2
    t.varchar "Propinsi", limit: 25
    t.varchar "Kota", limit: 25
    t.varchar "Kecamatan", limit: 25
    t.varchar "Kelurahan", limit: 25
    t.varchar "RTRW", limit: 5
    t.char "KodePos", limit: 5
    t.varchar "Pekerjaan", limit: 30
    t.varchar "NoIdentitas", limit: 30
  end

  create_table "PenanggungJawabPasien", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.varchar_max "NamaPJ", null: false
    t.varchar_max "AlamatPJ", null: false
    t.varchar "TeleponPJ", limit: 20
    t.char "Hubungan", limit: 2
    t.varchar_max "Propinsi"
    t.varchar_max "Kota"
    t.varchar_max "Kecamatan"
    t.varchar_max "Kelurahan"
    t.varchar "RTRW", limit: 10
    t.char "KodePos", limit: 10
    t.varchar_max "Pekerjaan"
    t.varchar_max "NoIdentitas"
  end

  create_table "PenanggungJawabTtd", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.varchar "KdRuangan", limit: 3, null: false
    t.datetime "TglTTD", null: false
    t.char "IdIdentitas", limit: 20
    t.varchar_max "TTDInformasiIGD"
    t.varchar_max "TTDInformasi"
    t.varchar_max "TTDPersetujuan"
    t.varchar_max "TTDHakKelas"
    t.varchar_max "TTDPasienUmum"
    t.varchar_max "TTDPasienFreOp"
    t.varchar_max "TTDPasienNaikKelas"
    t.varchar_max "TTDPenolakan"
    t.varchar_max "TTDPengkajianRanap"
  end

  create_table "Pendaftaran", primary_key: ["TglPendaftaran", "NoPendaftaran", "KdRuangan"], force: :cascade do |t|
    t.char "KdSubInstalasi", limit: 3, null: false
    t.datetime "TglPendaftaran", null: false
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "KdJenisRegistrasi", limit: 2, null: false
    t.char "IdPegawai", limit: 10, null: false
    t.char "KdRuangan", limit: 3, null: false
  end

  create_table "Pendidikan", primary_key: "QPendidikan", id: { type: :integer, limit: 1 }, force: :cascade do |t|
    t.char "KdPendidikan", limit: 2, null: false
    t.varchar "Pendidikan", limit: 25, null: false
    t.char "NoUrut", limit: 2
    t.varchar "KdJenisPendidikan", limit: 3
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 25
    t.integer "StatusEnabled", limit: 1, null: false
    t.real "Score"
  end

  create_table "Penjamin", primary_key: "QPenjamin", id: { type: :integer, limit: 2 }, force: :cascade do |t|
    t.char "IdPenjamin", limit: 10, null: false
    t.varchar "NamaPenjamin", limit: 50, null: false
    t.varchar "AlamatPenjamin", limit: 100
    t.varchar "TelpPenjamin", limit: 15
    t.char "NoAgreement", limit: 15
    t.varchar "DeskAgreement", limit: 125
    t.varchar "PropinsiPenjamin", limit: 25
    t.varchar "KotaPenjamin", limit: 25
    t.char "ZIPPenjamin", limit: 5
    t.datetime "TglAwalAgreement"
    t.datetime "TglAkhirAgreement"
    t.integer "KdAccount"
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1, null: false
    t.index ["IdPenjamin", "NamaPenjamin"], name: "IX_Penjamin"
  end

  create_table "PenjaminKelompokPasien", id: false, force: :cascade do |t|
    t.char "IdPenjamin", limit: 10, null: false
    t.char "KdKelompokPasien", limit: 2, null: false
  end

  create_table "PenjaminSisaTagihanPasien", primary_key: "NoStruk", id: { type: :char, limit: 10 }, force: :cascade do |t|
    t.varchar "NamaLengkap", limit: 50, null: false
    t.datetime "TglLahir", null: false
    t.char "JenisKelamin", limit: 1, null: false
    t.varchar "NoIdentitas", limit: 50, null: false
    t.varchar "Hubungan", limit: 50, null: false
    t.varchar "Alamat", limit: 100, null: false
    t.varchar "Telepon", limit: 15
    t.varchar "Propinsi", limit: 25, null: false
    t.varchar "Kota", limit: 25, null: false
    t.varchar "Kecamatan", limit: 25, null: false
    t.varchar "Kelurahan", limit: 25, null: false
    t.varchar "RTRW", limit: 7
    t.char "KodePos", limit: 5
  end

  create_table "PenyebabAsuhanKeperawatan", id: false, force: :cascade do |t|
    t.varchar "KdAsuhan", limit: 7, null: false
    t.varchar "KdSubPenyebab", limit: 20
    t.varchar "KdPenyebab", limit: 7, null: false
    t.varchar "NamaPenyebab", limit: 1000, null: false
    t.varchar "NamaExternal", limit: 500
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "PenyebabKematian", primary_key: "KdPenyebabKematian", id: { type: :integer, limit: 1, default: nil }, force: :cascade do |t|
    t.varchar "PenyebabKematian", limit: 50, null: false
    t.varchar "ReportDisplay", limit: 50
    t.integer "QPenyebabKematian", limit: 1, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "PeriksaDiagnosa", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.varchar "KdDiagnosa", limit: 7, null: false
    t.datetime "TglPeriksa", null: false
    t.char "KdJenisDiagnosa", limit: 2, null: false
    t.char "KdSubInstalasi", limit: 3, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "IdPegawai", limit: 10, null: false
    t.char "StatusKasus", limit: 4, null: false
    t.integer "KdPenyebabDiagnosa", limit: 2
    t.varchar "KdInfeksiNosokomial", limit: 2
    t.integer "KdPenyebabIN", limit: 2
    t.integer "KdMorfologiNeoplasma", limit: 1
    t.integer "KdKetunaanKelainan", limit: 1
    t.datetime "TglMasuk"
    t.integer "JmlPasienKel0", limit: 1, null: false
    t.integer "JmlPasienKel1", limit: 1, null: false
    t.integer "JmlPasienKel2", limit: 1, null: false
    t.integer "JmlPasienKel3", limit: 1, null: false
    t.integer "JmlPasienKel4", limit: 1, null: false
    t.integer "JmlPasienKel5", limit: 1, null: false
    t.integer "JmlPasienKel6", limit: 1, null: false
    t.integer "JmlPasienKel7", limit: 1, null: false
    t.integer "JmlPasienKel8", limit: 1, null: false
    t.integer "JmlPasienOutPria", limit: 1, null: false
    t.integer "JmlPasienOutWanita", limit: 1, null: false
    t.integer "JmlPasienOutHidup", limit: 1
    t.integer "JmlPasienOutMati", limit: 1, null: false
    t.integer "JmlKunjungan", limit: 1, null: false
    t.integer "HariTglPeriksa"
    t.integer "BlnTglPeriksa"
    t.integer "ThnTglPeriksa"
    t.index ["NoPendaftaran", "NoCM"], name: "ClusteredIndex-20230814-082550"
  end

  create_table "PeriksaDiagnosaKeperawatan", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.varchar "KdDiagnosa", limit: 10, null: false
    t.datetime "TglPeriksa", null: false
    t.char "KdJenisDiagnosa", limit: 2, null: false
    t.char "KdSubInstalasi", limit: 3, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "IdPegawai", limit: 10, null: false
    t.char "StatusKasus", limit: 4, null: false
    t.integer "KdPenyebabDiagnosa", limit: 2
    t.varchar "KdInfeksiNosokomial", limit: 2
    t.integer "KdPenyebabIN", limit: 2
    t.integer "KdMorfologiNeoplasma", limit: 1
    t.integer "KdKetunaanKelainan", limit: 1
    t.datetime "TglMasuk"
    t.integer "JmlPasienKel0", limit: 1, null: false
    t.integer "JmlPasienKel1", limit: 1, null: false
    t.integer "JmlPasienKel2", limit: 1, null: false
    t.integer "JmlPasienKel3", limit: 1, null: false
    t.integer "JmlPasienKel4", limit: 1, null: false
    t.integer "JmlPasienKel5", limit: 1, null: false
    t.integer "JmlPasienKel6", limit: 1, null: false
    t.integer "JmlPasienKel7", limit: 1, null: false
    t.integer "JmlPasienKel8", limit: 1, null: false
    t.integer "JmlPasienOutPria", limit: 1, null: false
    t.integer "JmlPasienOutWanita", limit: 1, null: false
    t.integer "JmlPasienOutHidup", limit: 1
    t.integer "JmlPasienOutMati", limit: 1, null: false
    t.integer "JmlKunjungan", limit: 1, null: false
    t.integer "HariTglPeriksa"
    t.integer "BlnTglPeriksa"
    t.integer "ThnTglPeriksa"
  end

  create_table "PeriksaDiagnosaKlaim", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.varchar "KdDiagnosa", limit: 7, null: false
    t.datetime "TglPeriksa", null: false
    t.char "KdJenisDiagnosa", limit: 2, null: false
    t.char "KdSubInstalasi", limit: 3, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "IdPegawai", limit: 10, null: false
    t.char "StatusKasus", limit: 4, null: false
    t.integer "KdPenyebabDiagnosa", limit: 2
    t.varchar "KdInfeksiNosokomial", limit: 2
    t.integer "KdPenyebabIN", limit: 2
    t.integer "KdMorfologiNeoplasma", limit: 1
    t.integer "KdKetunaanKelainan", limit: 1
    t.datetime "TglMasuk"
    t.integer "JmlPasienKel0", limit: 1, null: false
    t.integer "JmlPasienKel1", limit: 1, null: false
    t.integer "JmlPasienKel2", limit: 1, null: false
    t.integer "JmlPasienKel3", limit: 1, null: false
    t.integer "JmlPasienKel4", limit: 1, null: false
    t.integer "JmlPasienKel5", limit: 1, null: false
    t.integer "JmlPasienKel6", limit: 1, null: false
    t.integer "JmlPasienKel7", limit: 1, null: false
    t.integer "JmlPasienKel8", limit: 1, null: false
    t.integer "JmlPasienOutPria", limit: 1, null: false
    t.integer "JmlPasienOutWanita", limit: 1, null: false
    t.integer "JmlPasienOutHidup", limit: 1
    t.integer "JmlPasienOutMati", limit: 1, null: false
    t.integer "JmlKunjungan", limit: 1, null: false
    t.integer "HariTglPeriksa"
    t.integer "BlnTglPeriksa"
    t.integer "ThnTglPeriksa"
  end

  create_table "PeriksaDiagnosaKlaimTindakan", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.varchar "KdDiagnosa", limit: 7, null: false
    t.datetime "TglPeriksa", null: false
    t.char "KdJenisDiagnosa", limit: 2, null: false
    t.char "KdSubInstalasi", limit: 3, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "IdPegawai", limit: 10, null: false
    t.char "StatusKasus", limit: 4, null: false
    t.integer "KdPenyebabDiagnosa", limit: 2
    t.varchar "KdInfeksiNosokomial", limit: 2
    t.integer "KdPenyebabIN", limit: 2
    t.integer "KdMorfologiNeoplasma", limit: 1
    t.integer "KdKetunaanKelainan", limit: 1
    t.datetime "TglMasuk"
    t.integer "JmlPasienKel0", limit: 1, null: false
    t.integer "JmlPasienKel1", limit: 1, null: false
    t.integer "JmlPasienKel2", limit: 1, null: false
    t.integer "JmlPasienKel3", limit: 1, null: false
    t.integer "JmlPasienKel4", limit: 1, null: false
    t.integer "JmlPasienKel5", limit: 1, null: false
    t.integer "JmlPasienKel6", limit: 1, null: false
    t.integer "JmlPasienKel7", limit: 1, null: false
    t.integer "JmlPasienKel8", limit: 1, null: false
    t.integer "JmlPasienOutPria", limit: 1, null: false
    t.integer "JmlPasienOutWanita", limit: 1, null: false
    t.integer "JmlPasienOutHidup", limit: 1
    t.integer "JmlPasienOutMati", limit: 1, null: false
    t.integer "JmlKunjungan", limit: 1, null: false
    t.integer "HariTglPeriksa"
    t.integer "BlnTglPeriksa"
    t.integer "ThnTglPeriksa"
  end

  create_table "PeriksaDiagnosaTindakan", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.varchar "KdDiagnosa", limit: 7, null: false
    t.datetime "TglPeriksa", null: false
    t.char "KdJenisDiagnosa", limit: 2, null: false
    t.char "KdSubInstalasi", limit: 3, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "IdPegawai", limit: 10, null: false
    t.char "StatusKasus", limit: 4, null: false
    t.integer "KdPenyebabDiagnosa", limit: 2
    t.varchar "KdInfeksiNosokomial", limit: 2
    t.integer "KdPenyebabIN", limit: 2
    t.integer "KdMorfologiNeoplasma", limit: 1
    t.integer "KdKetunaanKelainan", limit: 1
    t.datetime "TglMasuk"
    t.integer "JmlPasienKel0", limit: 1, null: false
    t.integer "JmlPasienKel1", limit: 1, null: false
    t.integer "JmlPasienKel2", limit: 1, null: false
    t.integer "JmlPasienKel3", limit: 1, null: false
    t.integer "JmlPasienKel4", limit: 1, null: false
    t.integer "JmlPasienKel5", limit: 1, null: false
    t.integer "JmlPasienKel6", limit: 1, null: false
    t.integer "JmlPasienKel7", limit: 1, null: false
    t.integer "JmlPasienKel8", limit: 1, null: false
    t.integer "JmlPasienOutPria", limit: 1, null: false
    t.integer "JmlPasienOutWanita", limit: 1, null: false
    t.integer "JmlPasienOutHidup", limit: 1, default: 0
    t.integer "JmlPasienOutMati", limit: 1, null: false
    t.integer "JmlKunjungan", limit: 1, null: false
    t.integer "HariTglPeriksa"
    t.integer "BlnTglPeriksa"
    t.integer "ThnTglPeriksa"
  end

  create_table "PeriksaDiagnosa_PHP", id: false, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 10
    t.varchar "NoCM", limit: 15
    t.varchar "KdDiagnosa", limit: 7
    t.datetime "TglPeriksa"
    t.varchar "KdJenisDiagnosa", limit: 2
    t.varchar "KdSubInstalasi", limit: 3
    t.varchar "KdRuangan", limit: 3
    t.varchar "IdPegawai", limit: 10
    t.varchar "JenisDiagnosa", limit: 30
  end

  create_table "Pernafasan_TP", id: false, force: :cascade do |t|
    t.varchar "KdPernafasan", limit: 50
    t.varchar "NamaPernafasan", limit: 50
    t.integer "Enable"
  end

  create_table "PersentaseTPBPOA", primary_key: ["KdKelompokPasien", "IdPenjamin", "KdAsal"], force: :cascade do |t|
    t.char "KdKelompokPasien", limit: 2, null: false
    t.char "IdPenjamin", limit: 10, null: false
    t.char "KdAsal", limit: 2, null: false
    t.real "PersenTanggunganOA", null: false
    t.real "PersenTanggunganRSOA", null: false
    t.varchar "Keterangan", limit: 200
    t.char "StatusPaket", limit: 1, null: false
  end

  create_table "PersentaseTPBPTM", id: false, force: :cascade do |t|
    t.char "KdKelompokPasien", limit: 2, null: false
    t.char "IdPenjamin", limit: 10, null: false
    t.real "PersenTanggunganTM", null: false
    t.real "PersenTanggunganRSTM", null: false
    t.varchar "Keterangan", limit: 200
    t.char "StatusPaket", limit: 1, null: false
  end

  create_table "PersentaseUpTarifBarangOut", primary_key: ["KdInstalasi", "KdAsal"], force: :cascade do |t|
    t.char "KdInstalasi", limit: 2, null: false
    t.char "KdAsal", limit: 2, null: false
    t.real "PersentaseUp", null: false
    t.char "JenisHargaNetto", limit: 1, null: false
  end

  create_table "PersentaseUpTarifOA", id: false, force: :cascade do |t|
    t.char "KdKelompokPasien", limit: 2, null: false
    t.char "IdPenjamin", limit: 10, null: false
    t.char "KdAsal", limit: 2, null: false
    t.real "PersentaseUpOA", null: false
    t.char "JenisHargaNetto", limit: 1, null: false
  end

  create_table "PersentaseUpTarifTM", id: false, force: :cascade do |t|
    t.char "KdKelompokPasien", limit: 2, null: false
    t.char "IdPenjamin", limit: 10, null: false
    t.char "KdKelas", limit: 2, null: false
    t.char "KdKelasTarif", limit: 2, null: false
    t.real "PersentaseUpTM", null: false
    t.varchar "Keterangan", limit: 200
  end

  create_table "Perusahaan", primary_key: "KdPerusahaan", id: :integer, force: :cascade do |t|
    t.varchar "NamaPerusahaan", limit: 100
    t.varchar "Alamat", limit: 100
    t.varchar "NoTelp", limit: 100
    t.char "StatusEnabled", limit: 1
  end

  create_table "PetugasPemeriksaPasienOA", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.datetime "TglPelayanan", null: false
    t.varchar "KdBarang", limit: 9, null: false
    t.char "KdAsal", limit: 2, null: false
    t.char "SatuanJml", limit: 1, null: false
    t.char "IdPegawai", limit: 10, null: false
    t.char "IdUser", limit: 10, null: false
  end

  create_table "PindahKamarPerawatanRI", primary_key: "NoPakai", id: { type: :char, limit: 10 }, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.datetime "TglPindah", null: false
    t.char "KdRuanganTujuan", limit: 3, null: false
    t.char "StatusMasukKamar", limit: 1, null: false
    t.char "IdUser", limit: 10, null: false
    t.integer "HariTglPindah"
    t.integer "BlnTglPindah"
    t.integer "ThnTglPindah"
  end

  create_table "PlafonBlud", id: false, force: :cascade do |t|
    t.integer "Id"
    t.char "Kode", limit: 10
    t.char "Tahun", limit: 4
    t.money "Plafon", precision: 19, scale: 4
    t.integer "StatusEnabled", limit: 1
    t.money "PlafonSesudah", precision: 19, scale: 4
    t.money "PlafonUbah", precision: 19, scale: 4
  end

  create_table "PlanningBlud", primary_key: "Kd", id: :integer, force: :cascade do |t|
    t.integer "Kode"
    t.varchar "KodeBarang", limit: 50
    t.integer "VolumeKoef1"
    t.integer "VolumeKoef2"
    t.integer "VolumeSatuan"
    t.money "Harga", precision: 19, scale: 4
    t.money "HargaPPN", precision: 19, scale: 4
    t.money "Netto", precision: 19, scale: 4
    t.varchar "Tahun", limit: 50
    t.integer "StatusEnabled", limit: 1
    t.char "KdRekening", limit: 10
    t.char "IdPegawai", limit: 10
    t.datetime "TglInput"
    t.datetime "TglHapus"
    t.char "IdKabag", limit: 10
    t.datetime "TglVerif"
    t.integer "VolumeKabag"
    t.char "IdKsp", limit: 10
    t.integer "VolumeKsp"
    t.datetime "TglVerifKsp"
    t.integer "VolumeKoef1Ubah"
    t.integer "VolumeKoef2Ubah"
    t.integer "VolumeUbah"
    t.money "HargaUbah", precision: 19, scale: 4
    t.money "HargaPPNUbah", precision: 19, scale: 4
    t.money "NettoUbah", precision: 19, scale: 4
    t.char "IdPegawaiUbah", limit: 10
    t.datetime "TglUbah"
    t.char "IdKabagUbah", limit: 10
    t.datetime "TglVerifKabagUbah"
    t.integer "JenisAnggaran", limit: 1
    t.datetime "TglVerifKabagKeu"
    t.char "idKabagKeu", limit: 10
    t.varchar_max "AlasanUbah"
  end

  create_table "PlanningBludHistory", primary_key: "Id", id: :integer, force: :cascade do |t|
    t.integer "Kode"
    t.varchar "KodeBarang", limit: 50
    t.integer "VolumeKoef1"
    t.integer "VolumeKoef2"
    t.integer "VolumeSatuan"
    t.money "Harga", precision: 19, scale: 4
    t.money "HargaPPN", precision: 19, scale: 4
    t.money "Netto", precision: 19, scale: 4
    t.varchar "Tahun", limit: 50
    t.integer "StatusEnabled", limit: 1
    t.char "KdRekening", limit: 10
    t.char "IdPegawai", limit: 10
    t.datetime "TglInput"
    t.integer "Kd"
    t.varchar_max "AlasanUbah"
    t.integer "JenisDpa", limit: 1
  end

  create_table "Posting", primary_key: "NoPosting", id: { type: :char, limit: 10 }, force: :cascade do |t|
    t.datetime "TglPosting", null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "IdUser", limit: 10, null: false
    t.varchar "Deskripsi", limit: 100
  end

  create_table "PostingDataKasirPenerimaan", primary_key: ["NoPosting", "NoBKM", "JenisTransaksi"], force: :cascade do |t|
    t.char "NoPosting", limit: 10, null: false
    t.char "NoBKM", limit: 10, null: false
    t.char "JenisTransaksi", limit: 2, null: false
    t.char "NoStruk", limit: 10, null: false
  end

  create_table "ProfileBarang", id: false, force: :cascade do |t|
    t.char "KdBarang", limit: 10
    t.integer "LamaHari"
    t.integer "LatTime"
    t.integer "kirahari"
    t.integer "totalpemakaian"
    t.integer "ratarata"
    t.integer "pt"
    t.integer "pb"
    t.integer "StatusEnabled", limit: 1
    t.varchar "Ket", limit: 100
    t.char "ID_KOMODITAS", limit: 10
    t.integer "jenis_katalog", limit: 1
    t.char "KdSupplier", limit: 10
    t.char "KdPabrik", limit: 10
    t.integer "JmlMinimum"
    t.char "IdSediaan", limit: 10
    t.char "IdZatAktif", limit: 10
    t.char "KdKelasTerapi", limit: 10
  end

  create_table "ProgramOperasiRS", primary_key: "KdProgram", id: { type: :char, limit: 10 }, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCm", limit: 8
    t.char "KdRuanganAsal", limit: 3, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.varchar "Diagnosa", limit: 100
    t.varchar "Tindakan", limit: 100
    t.datetime "TglRencana"
    t.datetime "TglMulai"
    t.datetime "TglSelesai"
    t.char "IdDokter", limit: 10
    t.char "KdKamar", limit: 3
    t.varchar "Ket", limit: 10
    t.char "Status", limit: 1, null: false
    t.char "IdDokter2", limit: 10
    t.varchar "Ass", limit: 100
    t.varchar "Ins", limit: 100
    t.varchar "OL", limit: 100
    t.varchar "Keterangan", limit: 500
    t.integer "StatusKeluar", limit: 1
    t.varchar "KeteranganS", limit: 500
  end

  create_table "ProgramOperasiRS2", primary_key: "KdProgram", id: { type: :char, limit: 10 }, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCm", limit: 8
    t.char "KdRuanganAsal", limit: 3, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.varchar "Diagnosa", limit: 100
    t.varchar "Tindakan", limit: 100
    t.datetime "TglRencana"
    t.datetime "TglMulai"
    t.datetime "TglSelesai"
    t.char "IdDokter", limit: 10
    t.varchar "KdKamar", limit: 100
    t.varchar "Ket", limit: 100
    t.char "Status", limit: 1, null: false
    t.char "IdDokter2", limit: 10
    t.varchar "Ass", limit: 100
    t.varchar "StatusJenisOperasi", limit: 100
    t.varchar "Telepon", limit: 15
    t.varchar "UmurPasien", limit: 100
    t.varchar "Keterangan", limit: 500
    t.integer "StatusKeluar", limit: 1
    t.varchar "KriteriaLamaOperasi", limit: 500
    t.datetime "PulangRawat"
    t.varchar "KeadaanPulang", limit: 50
    t.integer "UrutanKe"
    t.varchar "Statusjadwal", limit: 50
    t.char "noibs", limit: 10
  end

  create_table "Propinsi", primary_key: "KdPropinsi", id: { type: :char, limit: 2 }, force: :cascade do |t|
    t.varchar "NamaPropinsi", limit: 30, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 30
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "PsikologisSpiritual", primary_key: "IdPS", id: :integer, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10
    t.char "NoCM", limit: 8
    t.integer "Psikologis"
    t.integer "MasalahPrilaku"
    t.varchar "KeteranganPrilaku", limit: 50
    t.varchar "NilaiNilaiKepercayaan", limit: 50
    t.integer "Agama"
    t.datetime "CreateDate"
    t.char "IdUser", limit: 10
  end

  create_table "RegistrasiApotik", id: false, force: :cascade do |t|
    t.char "NoApotik", limit: 10, null: false
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.datetime "TglDirujuk", null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "IdDokter", limit: 10, null: false
    t.char "KdKelas", limit: 2
    t.integer "HariTglMasuk"
    t.integer "BlnTglMasuk"
    t.integer "ThnTglMasuk"
  end

  create_table "RegistrasiIBS", primary_key: "NoIBS", id: { type: :char, limit: 10 }, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.datetime "TglPendaftaran", null: false
    t.char "KdRujukanAsal", limit: 2, null: false
    t.varchar "NamaRujukanAsal", limit: 50, null: false
    t.varchar "NamaPerujuk", limit: 50, null: false
    t.datetime "TglDirujuk", null: false
    t.char "KdSubInstalasi", limit: 3, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "KdJenisOperasi", limit: 2, null: false
    t.char "StatusPasien", limit: 4, null: false
    t.char "StatusOperasi", limit: 1, null: false
    t.char "KdKelas", limit: 2, null: false
    t.char "NoAntrian", limit: 3, null: false
    t.char "KdRuanganPerujuk", limit: 3, null: false
    t.integer "HariTglMasuk"
    t.integer "BlnTglMasuk"
    t.integer "ThnTglMasuk"
  end

  create_table "RegistrasiIEDTA", primary_key: "NoIEDTA", id: { type: :char, limit: 10 }, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.datetime "TglPendaftaran", null: false
    t.char "KdRujukanAsal", limit: 2, null: false
    t.varchar "NamaRujukanAsal", limit: 50, null: false
    t.varchar "NamaPerujuk", limit: 50
    t.datetime "TglDirujuk", null: false
    t.char "KdSubInstalasi", limit: 3, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "IdDokter", limit: 10, null: false
    t.char "StatusPasien", limit: 4, null: false
    t.char "KdKelas", limit: 2, null: false
    t.char "NoAntrian", limit: 3, null: false
    t.char "KdRuanganPerujuk", limit: 3, null: false
    t.integer "HariTglMasuk"
    t.integer "BlnTglMasuk"
    t.integer "ThnTglMasuk"
  end

  create_table "RegistrasiIGD", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.char "KdSubInstalasi", limit: 3, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "KdRujukanAsal", limit: 2, null: false
    t.char "KdTransportasi", limit: 2, null: false
    t.char "KdKeadaanUmum", limit: 2, null: false
    t.datetime "TglMasuk", null: false
    t.char "IdDokter", limit: 10
    t.char "StatusPulang", limit: 1, null: false
    t.char "KdKelas", limit: 2, null: false
    t.char "StatusPasien", limit: 4, null: false
    t.integer "HariTglMasuk"
    t.integer "BlnTglMasuk"
    t.integer "ThnTglMasuk"
    t.index ["NoPendaftaran", "NoCM", "KdRuangan", "IdDokter"], name: "IX_RegistrasiIGD"
  end

  create_table "RegistrasiLaboratorium", id: false, force: :cascade do |t|
    t.char "NoLaboratorium", limit: 10, null: false
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.datetime "TglPendaftaran", null: false
    t.char "KdRujukanAsal", limit: 2, null: false
    t.varchar "NamaRujukanAsal", limit: 50, null: false
    t.varchar "NamaPerujuk", limit: 50
    t.datetime "TglDirujuk", null: false
    t.char "KdSubInstalasi", limit: 3, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "IdDokter", limit: 10, null: false
    t.char "StatusPasien", limit: 4, null: false
    t.char "KdKelas", limit: 2, null: false
    t.char "NoAntrian", limit: 3, null: false
    t.char "KdRuanganPerujuk", limit: 3, null: false
    t.integer "HariTglMasuk"
    t.integer "BlnTglMasuk"
    t.integer "ThnTglMasuk"
  end

  create_table "RegistrasiLaboratoriumSequence", id: false, force: :cascade do |t|
    t.char "NoPendaftaranSeq", limit: 12, null: false
    t.nchar "NoLaboratorium", limit: 10, null: false
    t.char "NoPendaftaran", limit: 10
  end

  create_table "RegistrasiPenunjang", id: false, force: :cascade do |t|
    t.char "NoPenunjang", limit: 10, null: false
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.datetime "TglPendaftaran", null: false
    t.char "KdRujukanAsal", limit: 2, null: false
    t.varchar "NamaRujukanAsal", limit: 50, null: false
    t.varchar "NamaPerujuk", limit: 50
    t.datetime "TglDirujuk", null: false
    t.char "KdSubInstalasi", limit: 3, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "IdDokter", limit: 10
    t.char "KdJenisOperasi", limit: 2
    t.char "StatusPasien", limit: 4, null: false
    t.char "KdKelas", limit: 2, null: false
    t.char "NoAntrian", limit: 3, null: false
    t.char "KdRuanganPerujuk", limit: 3, null: false
  end

  create_table "RegistrasiRI", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.char "KdSubInstalasi", limit: 3, null: false
    t.char "KdKelas", limit: 2, null: false
    t.datetime "TglMasuk", null: false
    t.char "StatusPulang", limit: 1, null: false
    t.char "IdDokter", limit: 10
    t.char "KdCaraMasuk", limit: 2, null: false
    t.char "KdRujukanAsal", limit: 2, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.index ["NoPendaftaran", "NoCM", "IdDokter"], name: "IX_RegistrasiRI10132022"
  end

  create_table "RegistrasiRJ", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.char "KdSubInstalasi", limit: 3, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "IdDokter", limit: 10
    t.datetime "TglMasuk", null: false
    t.char "KdKelas", limit: 2, null: false
    t.char "NoAntrian", limit: 3, null: false
    t.char "StatusPasien", limit: 4, null: false
    t.char "KdRujukanAsal", limit: 2, null: false
    t.integer "HariTglMasuk"
    t.integer "BlnTglMasuk"
    t.integer "ThnTglMasuk"
    t.index ["NoPendaftaran", "NoCM"], name: "IX_RegistrasiRJ"
  end

  create_table "RegistrasiRJJam", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.char "KdSubInstalasi", limit: 3, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "IdDokter", limit: 10
    t.datetime "TglMasuk", null: false
    t.char "KdKelas", limit: 2, null: false
    t.char "NoAntrian", limit: 3, null: false
    t.char "StatusPasien", limit: 4, null: false
    t.char "KdRujukanAsal", limit: 2, null: false
    t.integer "HariTglMasuk"
    t.integer "BlnTglMasuk"
    t.integer "ThnTglMasuk"
    t.varchar "JamMulai", limit: 20, null: false
    t.varchar "JamSelesai", limit: 20, null: false
  end

  create_table "RegistrasiRadiology", primary_key: "NoRadiology", id: { type: :char, limit: 10 }, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.datetime "TglPendaftaran", null: false
    t.char "KdRujukanAsal", limit: 2, null: false
    t.varchar "NamaRujukanAsal", limit: 50, null: false
    t.varchar "NamaPerujuk", limit: 50
    t.datetime "TglDirujuk", null: false
    t.char "KdSubInstalasi", limit: 3, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "IdDokter", limit: 10, null: false
    t.char "StatusPasien", limit: 4, null: false
    t.char "KdKelas", limit: 2, null: false
    t.char "NoAntrian", limit: 3, null: false
    t.char "KdRuanganPerujuk", limit: 3, null: false
    t.integer "HariTglMasuk"
    t.integer "BlnTglMasuk"
    t.integer "ThnTglMasuk"
  end

  create_table "RekapKomponenBiayaPelayananOA", primary_key: ["NoBKM", "NoPendaftaran", "KdRuangan", "KdKomponen", "KdBarang", "KdAsal", "TglPelayanan", "NoStruk", "SatuanJml", "NoTerima"], force: :cascade do |t|
    t.char "NoBKM", limit: 10, null: false
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "KdKelas", limit: 2, null: false
    t.char "KdKomponen", limit: 2, null: false
    t.varchar "KdBarang", limit: 9, null: false
    t.char "KdAsal", limit: 2, null: false
    t.decimal "JmlBarang", precision: 9, scale: 2, null: false
    t.money "HargaSatuan", precision: 19, scale: 4, null: false
    t.datetime "TglPelayanan", null: false
    t.char "NoStruk", limit: 10, null: false
    t.char "NoLab_Rad", limit: 10
    t.char "IdPegawai", limit: 10
    t.char "SatuanJml", limit: 1, null: false
    t.money "JmlBayar", precision: 19, scale: 4, null: false
    t.money "JmlHutangPenjamin", precision: 19, scale: 4, null: false
    t.money "JmlTanggunganRS", precision: 19, scale: 4, null: false
    t.money "JmlPembebasan", precision: 19, scale: 4, null: false
    t.money "SisaTagihan", precision: 19, scale: 4, null: false
    t.char "KdDetailJenisJasaPelayanan", limit: 2, null: false
    t.varchar "KdPaket", limit: 3
    t.varchar "NoResep", limit: 15
    t.char "KdPelayananRS", limit: 6, null: false
    t.char "KdSubInstalasi", limit: 3, null: false
    t.integer "HariTglPelayanan"
    t.integer "BlnTglPelayanan"
    t.integer "ThnTglPelayanan"
    t.varchar "HariTglBKM", limit: 2
    t.varchar "BlnTglBKM", limit: 2
    t.varchar "ThnTglBKM", limit: 2
    t.char "KdRuanganAsal", limit: 3
    t.char "NoPosting", limit: 10
    t.integer "NoUrutJurnal", limit: 2
    t.char "NoTerima", limit: 10, null: false
  end

  create_table "RekapKomponenBiayaPelayananTM", primary_key: ["NoBKM", "NoPendaftaran", "KdRuangan", "KdPelayananRS", "KdKomponen", "TglPelayanan", "NoStruk"], force: :cascade do |t|
    t.char "NoBKM", limit: 10, null: false
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "KdPelayananRS", limit: 6, null: false
    t.char "KdKomponen", limit: 2, null: false
    t.char "KdKelas", limit: 2, null: false
    t.integer "JmlPelayanan", null: false
    t.datetime "TglPelayanan", null: false
    t.money "Tarif", precision: 19, scale: 4, null: false
    t.char "NoLab_Rad", limit: 10
    t.char "IdPegawai", limit: 10, null: false
    t.char "NoStruk", limit: 10, null: false
    t.char "KdDetailJenisJasaPelayanan", limit: 2, null: false
    t.money "JmlBayar", precision: 19, scale: 4, null: false
    t.money "JmlHutangPenjamin", precision: 19, scale: 4, null: false
    t.money "JmlTanggunganRS", precision: 19, scale: 4, null: false
    t.money "JmlPembebasan", precision: 19, scale: 4, null: false
    t.money "SisaTagihan", precision: 19, scale: 4, null: false
    t.varchar "KdPaket", limit: 3
    t.char "KdSubInstalasi", limit: 3, null: false
    t.integer "HariTglPelayanan"
    t.integer "BlnTglPelayanan"
    t.integer "ThnTglPelayanan"
    t.varchar "HariTglBKM", limit: 2
    t.varchar "BlnTglBKM", limit: 2
    t.varchar "ThnTglBKM", limit: 2
    t.char "KdRuanganAsal", limit: 3
    t.char "NoPosting", limit: 10
    t.integer "NoUrutJurnal", limit: 2
  end

  create_table "RekapitulasiKamarRawatInap", primary_key: ["TglHitung", "KdRuangan", "KdKelas", "KdKamar"], force: :cascade do |t|
    t.datetime "TglHitung", null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "KdKelas", limit: 2, null: false
    t.char "KdKamar", limit: 4, null: false
    t.integer "JmlBedTerisi", null: false
    t.integer "JmlBedKosong", null: false
  end

  create_table "RencanaPulang", id: false, force: :cascade do |t|
    t.datetime "SysUpdate"
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.char "NoPakai", limit: 15, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "KdKelas", limit: 2, null: false
    t.char "KdKamar", limit: 4, null: false
    t.char "NoBed", limit: 2, null: false
    t.datetime "TglMasuk", null: false
    t.char "StatusRencana", limit: 1, null: false
    t.varchar "PIN", limit: 10
  end

  create_table "RencanaPulang_Keterangan", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.varchar "Keterangan", limit: 50, null: false
  end

  create_table "RencanaTindakanKeperawatan", primary_key: "IdRT", id: :integer, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10
    t.char "NoCM", limit: 8
    t.char "IdUser", limit: 10
    t.varchar_max "Keterangan"
    t.datetime "CreateDate"
  end

  create_table "ResepObat", id: false, force: :cascade do |t|
    t.varchar "NoResep", limit: 15
    t.datetime "TglResep"
    t.char "IdDokter", limit: 10
    t.char "KdRuanganAsal", limit: 3
    t.char "IdUser", limit: 10, null: false
    t.char "ResepBebas", limit: 1
  end

  create_table "ResepObatDeleted", id: false, force: :cascade do |t|
    t.varchar "NoResep", limit: 15, null: false
    t.datetime "TglResep", null: false
    t.char "IdDokter", limit: 10
    t.char "KdRuanganAsal", limit: 3
    t.char "IdUser", limit: 10, null: false
    t.char "ResepBebas", limit: 1
    t.char "STATUS", limit: 1
  end

  create_table "ResepPengobatan_new", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 12, null: false
    t.char "NoCM", limit: 15, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "IdDokter", limit: 10, null: false
    t.varchar_max "Keterangan"
    t.datetime "TglEntry", null: false
    t.char "Status", limit: 1
  end

  create_table "ResikoCedera", primary_key: "id_resiko", id: :integer, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 50
    t.varchar "NoCm", limit: 10
    t.varchar "IdPegawai", limit: 50
    t.varchar "KdRuangan", limit: 3
    t.varchar "CaraBerjalanPasien", limit: 50
    t.varchar "PasienMenggunakanPenopang", limit: 50
    t.varchar "Hasil", limit: 50
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
  end

  create_table "ResponTime", primary_key: "NoUrut", id: :integer, force: :cascade do |t|
    t.char "NoStruk", limit: 10, null: false
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.char "KdKelompokPasien", limit: 2, null: false
    t.char "IdPenjamin", limit: 10, null: false
    t.datetime "TglMulai"
    t.datetime "TglSelesai"
    t.varchar "Keterangan", limit: 100
  end

  create_table "Retur", id: false, force: :cascade do |t|
    t.char "NoRetur", limit: 10, null: false
    t.datetime "TglRetur", null: false
    t.char "KdRuangan", limit: 3, null: false
    t.varchar "Keterangan", limit: 50
    t.char "NoBKK", limit: 10
    t.char "IdUser", limit: 10, null: false
  end

  create_table "ReturBiayaPelayanan", primary_key: ["NoRetur", "NoPendaftaran", "KdRuangan", "KdPelayananRS", "KdKelas", "TglPelayanan"], force: :cascade do |t|
    t.char "NoRetur", limit: 10, null: false
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "KdRuangan", limit: 10, null: false
    t.char "KdPelayananRS", limit: 6, null: false
    t.char "KdKelas", limit: 2, null: false
    t.datetime "TglPelayanan", null: false
    t.integer "JmlRetur", null: false
    t.varchar "NoVerifikasi", limit: 10
  end

  create_table "ReturnPemakaianAlkes", id: false, force: :cascade do |t|
    t.char "NoRetur", limit: 10, null: false
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.varchar "KdBarang", limit: 9, null: false
    t.char "KdAsal", limit: 2, null: false
    t.datetime "TglPelayanan", null: false
    t.char "SatuanJml", limit: 1, null: false
    t.decimal "JmlRetur", precision: 9, scale: 2, null: false
    t.char "NoTerima", limit: 10, null: false
  end

  create_table "Riwayat", primary_key: "NoRiwayat", id: { type: :char, limit: 10 }, force: :cascade do |t|
    t.datetime "TglRiwayat", null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "IdUser", limit: 10, null: false
  end

  create_table "RiwayatEntryUserIGD", primary_key: "nourut", id: :integer, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.datetime "TglPelayanan", null: false
    t.char "IdPegawai", limit: 10
    t.char "IdUser", limit: 10, null: false
  end

  create_table "RiwayatInputTransaksiMundur", id: false, force: :cascade do |t|
    t.varchar "NoBuktiTransaksi", limit: 20
    t.datetime "Tgltransaksi"
    t.datetime "TglInput"
    t.varchar "KdBarang", limit: 9
    t.char "KdAsal", limit: 2
    t.char "Kdruangan", limit: 3
    t.char "IdUser", limit: 10
    t.float "JmlBrg"
    t.char "StatusTransaksi", limit: 2
  end

  create_table "RiwayatJenisPasien", primary_key: ["NoPendaftaran", "KdRuangan", "TglMasuk"], force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.datetime "TglMasuk", null: false
    t.char "KdKelompokPasien", limit: 2, null: false
  end

  create_table "RiwayatMultiPenjamin", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.integer "NoPakaiKe", limit: 2, null: false
    t.date "TglPakai", null: false
    t.char "KdKelompokPasien", limit: 2, null: false
    t.nchar "IdPenjamin", limit: 10, null: false
    t.char "NoBKM", limit: 10
  end

  create_table "RiwayatTransaksiObatAlkes", id: false, force: :cascade do |t|
    t.char "Noriwayat", limit: 10, null: false
    t.varchar "NoBukti", limit: 50, null: false
    t.datetime "TglTransaksi", null: false
    t.char "KdRuangan", limit: 3, null: false
    t.varchar "KdBarang", limit: 9, null: false
    t.char "KdAsal", limit: 2, null: false
    t.integer "Jml"
    t.char "NoClosing", limit: 10
    t.char "Status", limit: 2
    t.varchar "Deskripsi", limit: 100
    t.integer "StokAwal"
  end

  create_table "RuangKerja", primary_key: "KdRuangKerja", id: { type: :char, limit: 3 }, force: :cascade do |t|
    t.varchar "RuangKerja", limit: 50, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "Ruangan", primary_key: "QRuangan", id: { type: :integer, limit: 2 }, force: :cascade do |t|
    t.char "KdRuangan", limit: 3, null: false
    t.varchar "NamaRuangan", limit: 50, null: false
    t.varchar "NoRuangan", limit: 4
    t.varchar "LokasiRuangan", limit: 50
    t.char "KdInstalasi", limit: 2, null: false
    t.char "KdDetailJenisJasaPelayanan", limit: 2
    t.integer "NoCounter"
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1, null: false
    t.varchar "singkatan", limit: 30
  end

  create_table "Rujukan", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.varchar "NoRujukan", limit: 30
    t.char "KdRujukanAsal", limit: 2, null: false
    t.varchar "SubRujukanAsal", limit: 100
    t.varchar "NamaPerujuk", limit: 50
    t.datetime "TglDirujuk", null: false
    t.varchar "DiagnosaRujukan", limit: 100
  end

  create_table "RujukanAsal", primary_key: "KdRujukanAsal", id: { type: :char, limit: 2 }, force: :cascade do |t|
    t.varchar "RujukanAsal", limit: 30, null: false
    t.varchar "Singkatan", limit: 5
    t.integer "QRujukanAsal", limit: 1, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 30
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "Rujukan_TP", id: false, force: :cascade do |t|
    t.varchar "KodeRujukan", limit: 50
    t.varchar "KeteranganRujukan", limit: 50
    t.integer "Enable"
  end

  create_table "SALDO_CUTI", id: false, force: :cascade do |t|
    t.varchar "PIN", limit: 20
    t.varchar "IdPegawai", limit: 10
    t.integer "KD_CUTI"
    t.varchar "CUTI_TAHUN", limit: 10
    t.integer "SALDO"
    t.integer "SALDO_DIAMBIL"
    t.integer "SALDO_TAMBAHAN"
    t.varchar "STATUS", limit: 20
  end

  create_table "SatuanEtiketResep", primary_key: "KdSatuanEtiket", id: { type: :varchar, limit: 4 }, force: :cascade do |t|
    t.varchar "SatuanEtiket", limit: 5000, null: false
    t.varchar "Singkatan", limit: 15
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 30
    t.integer "StatusEnabled", limit: 1
    t.integer "Urutan"
  end

  create_table "SatuanHasil", primary_key: "KdSatuanHasil", id: { type: :char, limit: 2 }, force: :cascade do |t|
    t.varchar "SatuanHasil", limit: 30, null: false
    t.char "KdInstalasi", limit: 2, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 30
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "SatuanJumlahB", primary_key: "KdSatuanJmlB", id: { type: :char, limit: 2 }, force: :cascade do |t|
    t.varchar "SatuanJmlB", limit: 20, null: false
    t.integer "QSatuanJmlB", limit: 1, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 20
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "SatuanJumlahK", primary_key: "KdSatuanJmlK", id: { type: :char, limit: 2 }, force: :cascade do |t|
    t.varchar "SatuanJmlK", limit: 20, null: false
    t.integer "QSatuanJmlK", limit: 1, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 20
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "SatuanJumlahKD", id: false, force: :cascade do |t|
    t.char "KdSatuanKD", limit: 2, null: false
    t.varchar "SatuanKD", limit: 20, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 20
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "SettingBiayaAdministrasi", id: false, force: :cascade do |t|
    t.money "TarifAdminOA", precision: 19, scale: 4, null: false
    t.integer "JmlBarisOAPerTarifAdminOA", null: false
    t.integer "JmlPelayananAdminTMPerHari", null: false
  end

  create_table "SettingDataUmum", id: false, force: :cascade do |t|
    t.varchar "MessageToDay", limit: 150, null: false
    t.integer "LamaJamToleransiRI", limit: 1, null: false
    t.integer "LamaBerlakuNoRujukan", limit: 1, null: false
    t.char "StatusPersenCito", limit: 1, null: false
    t.varchar "NamaPrinterBarcode", limit: 50
    t.integer "StatusAntrian", limit: 1
    t.integer "StatusFIFO", limit: 1
  end

  create_table "SettingGlobal", id: false, force: :cascade do |t|
    t.varchar "Prefix", limit: 50, null: false
    t.varchar "Value", limit: 150, null: false
    t.text_basic "Keterangan", null: false
  end

  create_table "SettingKomponenFixed", id: false, force: :cascade do |t|
    t.char "KdKomponenHargaNetto", limit: 2, null: false
    t.char "KdKomponenProfit", limit: 2, null: false
    t.char "KdKomponenServiceResep", limit: 2, null: false
    t.char "KdKomponenTarifTotalTM", limit: 2, null: false
    t.char "KdKomponenTarifTotalOA", limit: 2, null: false
    t.char "KdKomponenTarifCito", limit: 2, null: false
    t.char "KdKomponenTarifLabLuar", limit: 2, null: false
    t.char "KdKomponenTarifAdmin", limit: 2, null: false
    t.char "KdKomponenDiscount", limit: 2, null: false
  end

  create_table "SettingPelayananFixed", id: false, force: :cascade do |t|
    t.char "KdPelayananRSOA", limit: 6, null: false
    t.char "KdPelayananRSAP", limit: 6, null: false
    t.char "KdPelayananRSAdmin", limit: 6, null: false
  end

  create_table "SettingPeriodeAkuntansi", primary_key: "KdPeriode", id: { type: :integer, limit: 1, default: nil }, force: :cascade do |t|
    t.varchar "DeskripsiPeriode", limit: 75, null: false
    t.datetime "TglAwalPeriode", null: false
    t.datetime "TglAkhirPeriode", null: false
    t.char "NoPosting", limit: 10, null: false
    t.char "NoClosing", limit: 10
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1
    t.integer "StatusAccount", limit: 1
    t.integer "KdMetodePenyusutan", limit: 1
    t.integer "KdAccountEkuitas"
    t.integer "KdAccountEkuitasThnLalu"
  end

  create_table "Sirkulasi_TP", id: false, force: :cascade do |t|
    t.varchar "KodeSirkulasi", limit: 50
    t.varchar "NamaSirkulasi", limit: 50
    t.integer "Enable"
  end

  create_table "SkriningGizi", primary_key: "IdSkriningGizi", id: :integer, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10
    t.char "NoCM", limit: 8
    t.varchar "PenurunanBB", limit: 50
    t.integer "PenurunanNafsuMakan"
    t.varchar "ResultSkrining", limit: 50
    t.varchar "DiagnosisKhusus", limit: 50
    t.datetime "Createdate"
    t.char "IdUser", limit: 10
  end

  create_table "SkriningNyeri", primary_key: "IdSkriningNyeri", id: :integer, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 12
    t.char "NoCM", limit: 10
    t.integer "Wajah"
    t.integer "Ekstermitas"
    t.integer "Menangis"
    t.integer "KemampuanDitenangkan"
    t.integer "ResultSkrining"
    t.datetime "Createdate"
    t.nchar "IdUser", limit: 10
  end

  create_table "SkriningNyeriAnak", primary_key: "IdSkriningNyeri", id: :integer, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 12
    t.char "NoCM", limit: 10
    t.integer "EkspresiWajah"
    t.integer "Ekstermitas"
    t.integer "Toleransi"
    t.integer "ResultSkrining"
    t.datetime "Createdate"
    t.varchar "IdUser", limit: 50
  end

  create_table "SkriningNyeriNew", primary_key: "IdNyeri", id: :integer, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 50
    t.varchar "NoCM", limit: 50
    t.varchar_max "Penyebab"
    t.varchar_max "Kualitas"
    t.varchar "Penyebaran", limit: 50
    t.varchar "Skala", limit: 50
    t.varchar "Waktu", limit: 50
    t.varchar "JenisNyeri", limit: 50
    t.varchar "IdUser", limit: 50
    t.datetime "CreateDate"
    t.varchar "parameter1", limit: 50
    t.varchar "parameter2", limit: 50
    t.varchar "parameter3", limit: 50
    t.varchar "parameter4", limit: 50
  end

  create_table "SkriningPTM", primary_key: "id_ptm", id: :integer, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 50
    t.varchar "NoCm", limit: 10
    t.varchar "IdPegawai", limit: 50
    t.varchar "KdRuangan", limit: 3
    t.varchar "TTDiabetesMelitus", limit: 50
    t.varchar "TTHipertensi", limit: 50
    t.varchar "TTPenyakitJantung", limit: 50
    t.varchar "TTStroke", limit: 50
    t.varchar "TTAsma", limit: 50
    t.varchar "TTKanker", limit: 50
    t.varchar "TTKolesterolTinggi", limit: 50
    t.varchar "TTBenjolan", limit: 50
    t.varchar "Perokok", limit: 50
    t.varchar "Alkohol", limit: 50
    t.varchar "AktivitasFisik", limit: 50
    t.varchar "KonsumsiMakanBerat", limit: 50
    t.varchar "DSDiabetesMelitus", limit: 50
    t.varchar "DSHipertensi", limit: 50
    t.varchar "DSPenyakitJantung", limit: 50
    t.varchar "DSStroke", limit: 50
    t.varchar "DSAsma", limit: 50
    t.varchar "DSKanker", limit: 50
    t.varchar "DSHiperKolesterol", limit: 50
    t.varchar "DSBenjolan", limit: 50
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
  end

  create_table "SkriningResikoJatuh", primary_key: "IdSRK", id: :integer, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10
    t.char "NoCM", limit: 8, null: false
    t.integer "Keseimbangan"
    t.integer "AlatBantu"
    t.integer "Menopang"
    t.integer "Hasil"
    t.integer "Tindakan"
    t.datetime "Createdate"
    t.char "IdUser", limit: 10
  end

  create_table "SosialEkonomi", primary_key: "IdSP", id: :integer, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10
    t.char "NoCM", limit: 10
    t.integer "StatusPernikahan"
    t.integer "HubunganKeluarga"
    t.integer "TempatTinggal"
    t.varchar "Pekerjaan", limit: 50
    t.datetime "Createdate"
    t.char "IdUser", limit: 10
  end

  create_table "SpsBlud", id: false, force: :cascade do |t|
    t.integer "Id"
    t.char "Kode", limit: 10
    t.char "Tahun", limit: 4
    t.char "Bulan", limit: 2
    t.money "Sps", precision: 19, scale: 4
    t.integer "StatusEnabled", limit: 1
  end

  create_table "StatusBarang", primary_key: "KdStatusBarang", id: { type: :char, limit: 2 }, force: :cascade do |t|
    t.varchar "StatusBarang", limit: 20, null: false
    t.varchar "Keterangan", limit: 100
    t.integer "QStatusBarang", limit: 1, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 20
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "StatusBed", id: false, force: :cascade do |t|
    t.char "KdKamar", limit: 4, null: false
    t.char "NoBed", limit: 2, null: false
    t.char "StatusBed", limit: 1, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 30
    t.char "JenisBed", limit: 1
    t.char "KategoryBed", limit: 2
    t.integer "StatusEnabled", limit: 1
  end

  create_table "StatusCovidPasien", id: false, force: :cascade do |t|
    t.char "KdStatus", limit: 2, null: false
    t.varchar "Kriteria", limit: 500
    t.varchar_max "Keterangan"
    t.char "StatusEnabel", limit: 1, null: false
  end

  create_table "StatusKeluarIGD", primary_key: "No", id: :integer, force: :cascade do |t|
    t.varchar "StatusKeluar", limit: 100
    t.char "Status", limit: 1
    t.varchar "Keterangan", limit: 100
  end

  create_table "StatusKeluarKamar", primary_key: "QStatusKeluar", id: { type: :integer, limit: 1 }, force: :cascade do |t|
    t.char "KdStatusKeluar", limit: 2, null: false
    t.varchar "StatusKeluar", limit: 50, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "StatusLabel", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 8, null: false
    t.char "kdpaket", limit: 8, null: false
  end

  create_table "StatusOrder", primary_key: "KdStatusOrder", id: { type: :integer, limit: 1, default: nil }, force: :cascade do |t|
    t.varchar "StatusOrder", limit: 50, null: false
    t.varchar "ReportDisplay", limit: 50
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "StatusPasienIGD", primary_key: "IdStatusIGD", id: :integer, force: :cascade do |t|
    t.char "NoCM", limit: 10
    t.char "NoPendaftaran", limit: 10
    t.varchar "StatusIGD", limit: 50
    t.datetime "Createdate"
    t.char "IdUser", limit: 10
  end

  create_table "StatusPegawai", primary_key: "QStatus", id: { type: :integer, limit: 1 }, force: :cascade do |t|
    t.char "KdStatus", limit: 2, null: false
    t.varchar "Status", limit: 30, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 30
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "StatusPeriksaPasien", primary_key: "KdStatusPeriksa", id: { type: :char, limit: 2 }, force: :cascade do |t|
    t.varchar "StatusPeriksa", limit: 50, null: false
    t.char "Singkatan", limit: 1
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "StatusPerkawinan", primary_key: "QStatusPerkawinan", id: { type: :integer, limit: 1 }, force: :cascade do |t|
    t.char "KdStatusPerkawinan", limit: 2, null: false
    t.varchar "StatusPerkawinan", limit: 50
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 10
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "StatusPsikologiPasien", primary_key: "id_statuspsikologi", id: :integer, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 50
    t.varchar "NoCM", limit: 10
    t.varchar "IdPegawai", limit: 50
    t.varchar "KdRuangan", limit: 3
    t.varchar "StatusPsikologi", limit: 50
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
    t.varchar "IdPegawai2", limit: 50
  end

  create_table "StatusPulang", primary_key: "KdStatusPulang", id: { type: :char, limit: 2 }, force: :cascade do |t|
    t.varchar "StatusPulang", limit: 50, null: false
    t.integer "QStatusPulang", limit: 1, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "Status_Pulang_Eklaim", primary_key: "ID_StatusPlg", id: :integer, default: nil, force: :cascade do |t|
    t.varchar "KdStatusPlg", limit: 50
    t.varchar "StatusPlgEklaim", limit: 50
    t.varchar "StatusEnabled", limit: 10
  end

  create_table "StokBarangNonMedis", primary_key: ["KdBarang", "KdAsal", "KdRuangan"], force: :cascade do |t|
    t.varchar "KdBarang", limit: 9, null: false
    t.char "KdAsal", limit: 2, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.real "JmlMinimum", null: false
    t.float "JmlStok", null: false
    t.varchar "Lokasi", limit: 12
    t.real "JmlStokCuci"
    t.real "JmlStokPengeringan"
    t.real "JmlStokPerbaikan"
    t.real "JmlStokSetrika"
    t.real "JmlStokKeluar"
  end

  create_table "StokRuangan", id: false, force: :cascade do |t|
    t.varchar "KdBarang", limit: 50, null: false
    t.char "KdAsal", limit: 2, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.integer "JmlMinimum", null: false
    t.float "JmlStok", null: false
    t.varchar "Lokasi", limit: 15
    t.float "JmlMaximal"
  end

  create_table "StokRuanganFIFO", primary_key: ["KdBarang", "KdAsal", "KdRuangan", "NoTerima"], force: :cascade do |t|
    t.varchar "KdBarang", limit: 9, default: "01", null: false
    t.char "KdAsal", limit: 2, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "NoTerima", limit: 10, null: false
    t.integer "JmlMinimum", default: 0, null: false
    t.float "JmlStok", default: 0.0, null: false
    t.varchar "Lokasi", limit: 12
  end

  create_table "StrukBuktiKasMasuk", id: false, force: :cascade do |t|
    t.char "NoBKM", limit: 10, null: false
    t.datetime "TglBKM", null: false
    t.char "KdCaraBayar", limit: 2, null: false
    t.char "KdJenisKartu", limit: 2
    t.varchar "NamaBank", limit: 100
    t.varchar "NoKartu", limit: 50
    t.varchar "AtasNama", limit: 50
    t.money "JmlBayar", precision: 19, scale: 4, null: false
    t.money "Administrasi", precision: 19, scale: 4, null: false
    t.varchar "Keterangan", limit: 100
    t.char "KdRuangan", limit: 3, null: false
    t.char "IdUser", limit: 10, null: false
    t.char "NoUrut", limit: 10
    t.char "NoValidasi", limit: 12
    t.char "NoRiwayat", limit: 10
    t.char "NoPosting", limit: 10
    t.integer "NoUrutJurnal", limit: 2
    t.index ["NoBKM", "NoUrut"], name: "IX_strukbuktikasmasuk"
  end

  create_table "StrukBuktiKasMasukGD", id: false, force: :cascade do |t|
    t.char "NoBKM", limit: 12, null: false
    t.datetime "TglBKM", null: false
    t.char "KdCaraBayar", limit: 2, null: false
    t.char "KdJenisKartu", limit: 2
    t.varchar "NamaBank", limit: 100
    t.varchar "NoKartu", limit: 50
    t.varchar "AtasNama", limit: 50
    t.money "JmlBayar", precision: 19, scale: 4, null: false
    t.money "Administrasi", precision: 19, scale: 4, null: false
    t.varchar "Keterangan", limit: 100
    t.char "KdRuangan", limit: 3, null: false
    t.char "IdUser", limit: 10, null: false
    t.char "NoUrut", limit: 10
    t.char "NoValidasi", limit: 12
    t.char "NoRiwayat", limit: 10
    t.char "NoPosting", limit: 10
    t.integer "NoUrutJurnal", limit: 2
    t.char "NoBKMAsli", limit: 10, null: false
  end

  create_table "StrukBuktiKasMasukRI", id: false, force: :cascade do |t|
    t.char "NoBKM", limit: 12, null: false
    t.datetime "TglBKM", null: false
    t.char "KdCaraBayar", limit: 2, null: false
    t.char "KdJenisKartu", limit: 2
    t.varchar "NamaBank", limit: 100
    t.varchar "NoKartu", limit: 50
    t.varchar "AtasNama", limit: 50
    t.money "JmlBayar", precision: 19, scale: 4, null: false
    t.money "Administrasi", precision: 19, scale: 4, null: false
    t.varchar "Keterangan", limit: 100
    t.char "KdRuangan", limit: 3, null: false
    t.char "IdUser", limit: 10, null: false
    t.char "NoUrut", limit: 10
    t.char "NoValidasi", limit: 12
    t.char "NoRiwayat", limit: 10
    t.char "NoPosting", limit: 10
    t.integer "NoUrutJurnal", limit: 2
    t.char "NoBKMAsli", limit: 10, null: false
  end

  create_table "StrukBuktiKasMasukRJ", id: false, force: :cascade do |t|
    t.char "NoBKM", limit: 12, null: false
    t.datetime "TglBKM", null: false
    t.char "KdCaraBayar", limit: 2, null: false
    t.char "KdJenisKartu", limit: 2
    t.varchar "NamaBank", limit: 100
    t.varchar "NoKartu", limit: 50
    t.varchar "AtasNama", limit: 50
    t.money "JmlBayar", precision: 19, scale: 4, null: false
    t.money "Administrasi", precision: 19, scale: 4, null: false
    t.varchar "Keterangan", limit: 100
    t.char "KdRuangan", limit: 3, null: false
    t.char "IdUser", limit: 10, null: false
    t.char "NoUrut", limit: 10
    t.char "NoValidasi", limit: 12
    t.char "NoRiwayat", limit: 10
    t.char "NoPosting", limit: 10
    t.integer "NoUrutJurnal", limit: 2
    t.char "NoBKMAsli", limit: 10, null: false
  end

  create_table "StrukKirim", primary_key: "NoKirim", id: { type: :char, limit: 10 }, force: :cascade do |t|
    t.datetime "TglKirim", null: false
    t.char "NoOrder", limit: 10
    t.char "KdRuangan", limit: 3, null: false
    t.char "KdRuanganTujuan", limit: 3, null: false
    t.char "IdUserPenerima", limit: 10
    t.char "IdUser", limit: 10, null: false
    t.char "NoPosting", limit: 10
    t.integer "NoUrutJurnal", limit: 2
    t.integer "StatusVerifikasi", limit: 1
  end

  create_table "StrukOrder", primary_key: "NoOrder", id: { type: :char, limit: 10 }, force: :cascade do |t|
    t.datetime "TglOrder", null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "KdRuanganTujuan", limit: 3
    t.char "KdSupplier", limit: 4
    t.char "IdUser", limit: 10, null: false
    t.index ["NoOrder"], name: "NonClusteredIndex-20230130-123336"
    t.index ["NoOrder"], name: "NonClusteredIndex-20231013-100847", unique: true
  end

  create_table "StrukPelayananPasien", id: false, force: :cascade do |t|
    t.char "NoStruk", limit: 10, null: false
    t.datetime "TglStruk", null: false
    t.char "NoPendaftaran", limit: 10
    t.char "NoCM", limit: 15
    t.char "KdKelompokPasien", limit: 2, null: false
    t.char "IdPenjamin", limit: 10, null: false
    t.money "TotalBiaya", precision: 19, scale: 4, null: false
    t.money "TotalPpn", precision: 19, scale: 4, null: false
    t.money "TotalDiscount", precision: 19, scale: 4, null: false
    t.money "JmlHutangPenjamin", precision: 19, scale: 4, null: false
    t.money "JmlTanggunganRS", precision: 19, scale: 4, null: false
    t.money "JmlPembebasan", precision: 19, scale: 4, null: false
    t.money "JmlHarusDibayar", precision: 19, scale: 4, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "IdUser", limit: 10, null: false
    t.varchar "NoResep", limit: 15
    t.varchar "NoSJP", limit: 30
    t.char "NoRiwayat", limit: 10
    t.char "KdRuanganTerakhir", limit: 3
    t.integer "HariTglStruk"
    t.integer "BlnTglStruk"
    t.integer "ThnTglStruk"
    t.char "KdKelasTerakhir", limit: 2
    t.smalldatetime "TglMasukTerakhir"
    t.smalldatetime "TglKeluarTerakhir"
    t.index ["NoStruk", "NoPendaftaran"], name: "IX_StrukPelayananPasien"
  end

  create_table "StrukTerima", id: false, force: :cascade do |t|
    t.char "NoTerima", limit: 10, null: false
    t.datetime "TglTerima", null: false
    t.char "KdSupplier", limit: 4
    t.char "NoOrder", limit: 10
    t.varchar "NoFaktur", limit: 28
    t.datetime "TglFaktur"
    t.datetime "TglJatuhTempo"
    t.datetime "TglHarusDibayar"
    t.real "TotalBeratKg"
    t.char "KdRuangan", limit: 3, null: false
    t.char "IdUser", limit: 10, null: false
    t.char "NoPosting", limit: 10
    t.integer "NoUrutJurnal", limit: 2
    t.varchar "NoSurat", limit: 50
    t.integer "StatusVerifikasi", limit: 1
  end

  create_table "StrukTerimaVerifikasi", primary_key: ["NoVerifikasi", "NoTerima"], force: :cascade do |t|
    t.nchar "NoVerifikasi", limit: 10, null: false
    t.char "NoTerima", limit: 10, null: false
    t.datetime "TglTerima", null: false
    t.char "KdSupplier", limit: 4
    t.datetime "TglVerifikasi"
    t.char "iduser", limit: 10
    t.integer "StatusVerifikasi", limit: 1
  end

  create_table "SubInstalasi", primary_key: "QSubInstalasi", id: { type: :integer, limit: 1 }, force: :cascade do |t|
    t.char "KdSubInstalasi", limit: 3, null: false
    t.varchar "NamaSubInstalasi", limit: 50, null: false
    t.varchar "Singkatan", limit: 5
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1, null: false
    t.index ["KdSubInstalasi", "NamaSubInstalasi"], name: "IX_SubInstalasi10132022"
  end

  create_table "SubInstalasiRuangan", id: false, force: :cascade do |t|
    t.char "KdRuangan", limit: 3, null: false
    t.char "KdSubInstalasi", limit: 3, null: false
  end

  create_table "SubRuangKerja", id: false, force: :cascade do |t|
    t.char "KdSubRuangKerja", limit: 3, null: false
    t.varchar "SubRuangKerja", limit: 50, null: false
    t.char "KdRuangKerja", limit: 3, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "SubRujukanAsal", id: false, force: :cascade do |t|
    t.varchar "KdSubRujukanAsal", limit: 3
    t.varchar_max "SubRujukanAsal"
    t.varchar "StatusEnabled", limit: 1
  end

  create_table "Suku", primary_key: "QSuku", id: { type: :integer, limit: 1 }, force: :cascade do |t|
    t.char "KdSuku", limit: 2, null: false
    t.varchar "Suku", limit: 20, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 20
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "Supplier", primary_key: "KdSupplier", id: { type: :char, limit: 4 }, force: :cascade do |t|
    t.varchar "NamaSupplier", limit: 50, null: false
    t.varchar "AlamatSupplier", limit: 100
    t.varchar "KotaSupplier", limit: 30
    t.varchar "ProvinsiSupplier", limit: 30
    t.varchar "KodePosSupplier", limit: 8
    t.varchar "TelponSupplier", limit: 20
    t.varchar "FaxSupplier", limit: 15
    t.varchar "emailSupplier", limit: 50
    t.char "KdInstalasi", limit: 2
    t.integer "KdAccount"
    t.integer "QSupplier", limit: 2, null: false
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "SupplierPabrik", primary_key: ["KdPabrik", "KdSupplier"], force: :cascade do |t|
    t.varchar "KdPabrik", limit: 4, null: false
    t.char "KdSupplier", limit: 4, null: false
  end

  create_table "T_AdditionalPrecaution", id: false, force: :cascade do |t|
    t.char "KdAdditionalPrecaution", limit: 2, null: false
    t.varchar "AdditionalPrecaution", limit: 50, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "T_AntenatalCare", primary_key: "IdAntenatalCare", id: :integer, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10
    t.char "NoCM", limit: 8
    t.datetime "TglPeriksa"
    t.varchar "UmurKehamilan", limit: 100
    t.varchar "Anamnesa", limit: 100
    t.varchar "BeratBadan", limit: 100
    t.varchar "TD", limit: 100
    t.varchar "LILA", limit: 100
    t.varchar "EDEMA", limit: 100
    t.varchar "StatusGizi", limit: 100
    t.varchar "TFU", limit: 100
    t.varchar "RelekPatella", limit: 100
    t.varchar "DetakJantungJanin", limit: 100
    t.varchar "Kepala", limit: 100
    t.varchar "Presentasi", limit: 100
    t.varchar "StatusImunisasi", limit: 100
    t.varchar "Injeksi", limit: 100
    t.varchar "Rujuk", limit: 255
    t.varchar "Terapi", limit: 255
    t.varchar "Edukasi", limit: 255
    t.datetime "TanggalKembali"
    t.datetime "TglUpdate"
    t.char "IdUser", limit: 10
  end

  create_table "T_AsuhanKeperawatan", id: false, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 10
    t.varchar "NoCM", limit: 8
    t.varchar "KdRuangan", limit: 3
    t.varchar "KdDiagnosa", limit: 10
    t.varchar "Penyebab", limit: 100
    t.varchar "DeskripsiPenyebab1", limit: 255
    t.varchar "DeskripsiPenyebab2", limit: 255
    t.varchar "DeskripsiPenyebab3", limit: 255
    t.varchar "KondisiKlinis", limit: 100
    t.varchar "DeskripsiKondisiKlinis", limit: 255
    t.varchar "GejalaMayorSubyektif", limit: 100
    t.varchar "DeskripsiMayorSubyektif", limit: 255
    t.varchar "GejalaMayorObyektif", limit: 100
    t.varchar "DeskripsiMayorObyektif", limit: 255
    t.varchar "GejalaMinorSubyektif", limit: 100
    t.varchar "DeskripsiMinorSubyektif", limit: 255
    t.varchar "GejalaMinorObyektif", limit: 100
    t.varchar "DeskripsiMinorObyektif", limit: 255
    t.varchar "Observasi", limit: 100
    t.varchar "DeskripsiObservasi1", limit: 255
    t.varchar "DeskripsiObservasi2", limit: 255
    t.varchar "DeskripsiObservasi3", limit: 255
    t.varchar "DeskripsiObservasi4", limit: 255
    t.varchar "DeskripsiObservasi5", limit: 255
    t.varchar "Terapeutik", limit: 100
    t.varchar "DeskripsiTerapeutik1", limit: 255
    t.varchar "DeskripsiTerapeutik2", limit: 255
    t.varchar "DeskripsiTerapeutik3", limit: 255
    t.varchar "DeskripsiTerapeutik4", limit: 255
    t.varchar "DeskripsiTerapeutik5", limit: 255
    t.varchar "Edukasi", limit: 100
    t.varchar "DeskripsiEdukasi1", limit: 255
    t.varchar "DeskripsiEdukasi2", limit: 255
    t.varchar "DeskripsiEdukasi3", limit: 255
    t.varchar "DeskripsiEdukasi4", limit: 255
    t.varchar "DeskripsiEdukasi5", limit: 255
    t.varchar "Kolaborasi", limit: 100
    t.varchar "DeskripsiKolaborasi1", limit: 255
    t.varchar "DeskripsiKolaborasi2", limit: 255
    t.varchar "DeskripsiKolaborasi3", limit: 255
    t.varchar "DeskripsiKolaborasi4", limit: 255
    t.varchar "DeskripsiKolaborasi5", limit: 255
    t.varchar "IdUserPJ", limit: 10
    t.varchar "IdUser", limit: 10
    t.varchar "PIN", limit: 10
    t.varchar "StatusDiagnosa", limit: 1
    t.datetime "CreateDate", null: false
    t.datetime "UpdateDate", null: false
  end

  create_table "T_CaraKB", id: false, force: :cascade do |t|
    t.char "KdCaraKB", limit: 2, null: false
    t.varchar "CaraKB", limit: 100
    t.char "StatusEnabled", limit: 1
  end

  create_table "T_Defekasi", id: false, force: :cascade do |t|
    t.char "KdDefekasi", limit: 2, null: false
    t.varchar "Defekasi", limit: 50, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "T_DischargePlanning", primary_key: "ID_dp", id: :integer, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 10
    t.varchar "NoCM", limit: 8
    t.varchar "KdRuangan", limit: 3
    t.varchar "DischargePlanning", limit: 255
    t.varchar "IdUser", limit: 10
    t.varchar "PIN", limit: 10
    t.datetime "CreateDate", null: false
    t.datetime "UpdateDate", null: false
  end

  create_table "T_DokterRencanaOperasi", id: false, force: :cascade do |t|
    t.char "IdDokter", limit: 10, null: false
    t.varchar "NamaDokter", limit: 100, null: false
    t.char "StatusEnabled", limit: 1, null: false
  end

  create_table "T_HambatanBelajar", id: false, force: :cascade do |t|
    t.char "KdHambatanBelajar", limit: 2, null: false
    t.varchar "HambatanBelajar", limit: 50, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "T_Imunisasi", id: false, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 50
    t.char "NoCM", limit: 8
    t.varchar "BeratLahir", limit: 50
    t.varchar "PanjangLahir", limit: 50
    t.varchar_max "StatusGizi"
    t.varchar_max "JenisVaksin"
    t.varchar_max "KIE"
    t.varchar_max "Keterangan"
    t.datetime "TglInput"
    t.datetime "TglUpdate"
    t.char "IdUser", limit: 10
    t.varchar_max "Suhu"
  end

  create_table "T_KelahiranBayi", primary_key: "IdKelahiran", id: :integer, force: :cascade do |t|
    t.char "NoSurat", limit: 50
    t.char "NoPendaftaran", limit: 10
    t.char "NoCM", limit: 8
    t.varchar "NamaIbu", limit: 100
    t.varchar "NIK_Ibu", limit: 20
    t.varchar "AlamatIbu", limit: 100
    t.varchar "PekerjaanIbu", limit: 100
    t.varchar "NamaAyah", limit: 100
    t.varchar "NIK_Ayah", limit: 20
    t.varchar "AlamatAyah", limit: 100
    t.varchar "PekerjaanAyah", limit: 100
    t.varchar "NoPendaftaranIbu", limit: 20
    t.datetime "TglKelahiran"
    t.varchar "KelahiranKeBerapa", limit: 2
    t.varchar "KembarBerapa", limit: 2
    t.varchar "PanjangBadan", limit: 20
    t.varchar "BeratBadan", limit: 20
    t.varchar "KdDiagnosaTindakan", limit: 5
    t.varchar "KdKelainanBawaan", limit: 5
    t.datetime "TglInput"
    t.datetime "TglUpdate"
    t.char "IdUser", limit: 10
    t.varchar "IdBidanPenolong", limit: 10
  end

  create_table "T_KesulitanBernafas", id: false, force: :cascade do |t|
    t.char "KdKesulitanBernafas", limit: 2, null: false
    t.varchar "NamaKesulitanBernafas", limit: 50, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "T_KriteriaHasilKeperawatan", id: false, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 10
    t.varchar "NoCM", limit: 8
    t.varchar "KdRuangan", limit: 3
    t.varchar "KdDiagnosa", limit: 10
    t.integer "Hasil1"
    t.integer "Hasil2"
    t.integer "Hasil3"
    t.integer "Hasil4"
    t.integer "Hasil5"
    t.integer "Hasil6"
    t.integer "Hasil7"
    t.integer "Hasil8"
    t.integer "Hasil9"
    t.integer "Hasil10"
    t.integer "Hasil11"
    t.integer "Hasil12"
    t.integer "Hasil13"
    t.integer "Hasil14"
    t.integer "Hasil15"
    t.integer "Hasil16"
    t.integer "Hasil17"
    t.integer "Hasil18"
    t.integer "Hasil19"
    t.integer "Hasil20"
    t.integer "Hasil21"
    t.integer "Hasil22"
    t.integer "Hasil23"
    t.integer "Hasil24"
    t.integer "Hasil25"
    t.integer "Hasil26"
    t.integer "Hasil27"
    t.integer "Hasil28"
    t.integer "Hasil29"
    t.integer "Hasil30"
    t.integer "Hasil31"
    t.integer "Hasil32"
    t.integer "Hasil33"
    t.integer "Hasil34"
    t.integer "Hasil35"
    t.varchar "IdUser", limit: 10
    t.varchar "PIN", limit: 10
    t.datetime "CreateDate", null: false
    t.datetime "UpdateDate", null: false
  end

  create_table "T_MasalahDitemukan", id: false, force: :cascade do |t|
    t.char "KdMasalahDitemukan", limit: 2, null: false
    t.varchar "MasalahDitemukan", limit: 50, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "T_MedicalResume", primary_key: "MRId", id: { type: :varchar, limit: 7 }, force: :cascade do |t|
    t.char "NoCM", limit: 8, null: false
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.varchar_max "IndikasiPasien"
    t.varchar_max "RingkasanRiwayat"
    t.varchar "KeadaanUmum", limit: 255
    t.varchar "Kesadaran", limit: 255
    t.varchar "Tensi", limit: 20
    t.varchar "Nadi", limit: 20
    t.varchar "Suhu", limit: 20
    t.varchar "Pernapasan", limit: 20
    t.varchar "PemeriksaanFisik", limit: 255
    t.varchar "PemeriksaanDiagnostik", limit: 255
    t.varchar "KdDiagnosa", limit: 7
    t.varchar "DiagnosaLainnya", limit: 20
    t.varchar "ProsedurTerapi", limit: 255
    t.char "InstruksiAnjuran", limit: 1
    t.char "StatusPulang", limit: 20
    t.char "StatusPulangLainnya", limit: 20
    t.char "InstruksiTindakLanjut", limit: 255
    t.char "InstruksiTindakLanjutLainnya", limit: 255
    t.datetime "TanggalKontrol"
    t.varchar "UserId", limit: 20
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
  end

  create_table "T_PA_AlergiReaksi", primary_key: "ID_AlergiReaksi", id: :integer, force: :cascade do |t|
    t.char "NoCM", limit: 8
    t.char "NoPendaftaran", limit: 10
    t.char "KdRuangan", limit: 3
    t.varchar "TidakAdaAlergi", limit: 1
    t.varchar "AlergiObat", limit: 1
    t.varchar "SebutkanAlergiObat", limit: 50
    t.varchar "SebutkanReaksiObat", limit: 50
    t.varchar "AlergiMakanan", limit: 1
    t.varchar "SebutkanAlergiMakanan", limit: 50
    t.varchar "SebutkanReaksiMakanan", limit: 50
    t.varchar "AlergiLainnya", limit: 1
    t.varchar "SebutkanAlergiLainnya", limit: 50
    t.varchar "ReaksiAlergiLainnya", limit: 50
    t.varchar "KancingTandaAlergi", limit: 1
    t.varchar "Tidakdiketahui", limit: 1
    t.varchar "KeluhanUtama", limit: 250
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
    t.varchar "UserId", limit: 10
  end

  create_table "T_PA_Behavioral_Pain_Scale", primary_key: "ID_bps", id: :integer, force: :cascade do |t|
    t.char "NoCM", limit: 8
    t.char "NoPendaftaran", limit: 10
    t.char "KdRuangan", limit: 3
    t.varchar "EkspresiWajah", limit: 25
    t.varchar "PergerakanPosisi", limit: 25
    t.varchar "ToleransiVentilasi", limit: 200
    t.varchar "TotalScore", limit: 25
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
    t.varchar "UserId", limit: 15
  end

  create_table "T_PA_FLACCS", primary_key: "ID_bps", id: :integer, force: :cascade do |t|
    t.char "NoCM", limit: 8
    t.char "NoPendaftaran", limit: 10
    t.char "KdRuangan", limit: 3
    t.varchar "Wajah", limit: 25
    t.varchar "Ekstremitas", limit: 25
    t.varchar "Menangis", limit: 25
    t.varchar "Kemampuan", limit: 25
    t.varchar "TotalScore", limit: 25
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
    t.varchar "UserId", limit: 15
  end

  create_table "T_PA_Identitas", id: false, force: :cascade do |t|
    t.char "NoCM", limit: 8
    t.char "NoPendaftaran", limit: 10
    t.char "KdRuangan", limit: 3
    t.varchar "Jenis", limit: 50
    t.varchar "Agama", limit: 50
    t.varchar "Pendidikan", limit: 50
    t.varchar "Pekerjaan", limit: 50
    t.varchar "Warganegara", limit: 50
    t.varchar_max "KeluhanUtama"
    t.varchar_max "KeluhanSekarang"
    t.varchar "RiwayatMasukRS", limit: 1
    t.varchar "RiwayatLamaRawat", limit: 50
    t.varchar "RiwayatAlasanRawat", limit: 50
    t.varchar "RiwayatOperasi", limit: 1
    t.varchar "RiwayatJenisOperasi", limit: 50
    t.varchar "RiwayatPenyakit", limit: 255
    t.varchar "RiwayatPenyakitLainnya", limit: 255
    t.varchar "RiwayatAlergi", limit: 1
    t.varchar "JenisAlergiObat", limit: 50
    t.varchar "JenisAlergiMakanan", limit: 50
    t.varchar "JenisAlergiLain", limit: 50
    t.varchar "JenisAlergiLainnya", limit: 100
    t.varchar "TipeReaksi", limit: 100
    t.varchar "ObatDikonsumsi", limit: 1
    t.varchar "ObatDikonsumsiSebutkan", limit: 100
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
    t.varchar "UserId", limit: 10
  end

  create_table "T_PA_Intervensi_JatuhRendah_Anak", primary_key: "ID_Rjrendahanak", id: :integer, force: :cascade do |t|
    t.char "NoCM", limit: 8
    t.char "NoPendaftaran", limit: 10
    t.char "KdRuangan", limit: 3
    t.varchar "OrientasikanPasien", limit: 25
    t.varchar "TempatkanBelPanggilanDalamJangkauan", limit: 25
    t.varchar "AnjurkanPasien", limit: 25
    t.varchar "KajiKebutuhanEliminasi", limit: 25
    t.varchar "HindarkanBarang", limit: 25
    t.varchar "AjakKeluarga", limit: 25
    t.varchar "PeneranganCukup", limit: 25
    t.varchar "Dokumentasikan", limit: 25
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
    t.varchar "UserId", limit: 15
  end

  create_table "T_PA_Intervensi_JatuhRendah_Dewasa", primary_key: "ID_Rjd", id: :integer, force: :cascade do |t|
    t.char "NoCM", limit: 8
    t.char "NoPendaftaran", limit: 10
    t.char "KdRuangan", limit: 3
    t.varchar "ResikoJatuhRendah", limit: 25
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
    t.varchar "UserId", limit: 15
  end

  create_table "T_PA_Intervensi_JatuhRendah_Geriartri", primary_key: "ID_RJsedangGeriartri", id: :integer, force: :cascade do |t|
    t.char "NoCM", limit: 8
    t.char "NoPendaftaran", limit: 10
    t.char "KdRuangan", limit: 3
    t.varchar "ResikoJatuhRendah", limit: 25
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
    t.varchar "UserId", limit: 15
  end

  create_table "T_PA_Intervensi_JatuhSedang_Dewasa", primary_key: "ID_Rjd", id: :integer, force: :cascade do |t|
    t.char "NoCM", limit: 8
    t.char "NoPendaftaran", limit: 10
    t.char "KdRuangan", limit: 3
    t.varchar "ObservasiBantuan", limit: 25
    t.varchar "ObservasiTempatkanBel", limit: 25
    t.varchar "ObservasiBendaMilikPasien", limit: 25
    t.varchar "ObservasiPosisiTempatTidur", limit: 25
    t.varchar "ObservasiPakaian", limit: 25
    t.varchar "ObservasiPasienAmbulasi", limit: 25
    t.varchar "ObservasiPasangPengaman", limit: 25
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
    t.varchar "UserId", limit: 15
  end

  create_table "T_PA_Intervensi_JatuhSedang_Geriartri", primary_key: "ID_RJsedangGeriartri", id: :integer, force: :cascade do |t|
    t.char "NoCM", limit: 8
    t.char "NoPendaftaran", limit: 10
    t.char "KdRuangan", limit: 3
    t.varchar "SarankanMintaBantuan", limit: 25
    t.varchar "TempatkanBel", limit: 25
    t.varchar "TempatkanBendaMilikPasien", limit: 25
    t.varchar "TempatTidurPosisiRendah", limit: 25
    t.varchar "PastikanCelanaPanjang", limit: 25
    t.varchar "BantuPasienAmbulasi", limit: 25
    t.varchar "PasangkanPengamanSisiTempatTidur", limit: 25
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
    t.varchar "UserId", limit: 15
  end

  create_table "T_PA_Intervensi_JatuhTinggi_Anak", primary_key: "ID_Rjtinggianak", id: :integer, force: :cascade do |t|
    t.char "NoCM", limit: 8
    t.char "NoPendaftaran", limit: 10
    t.char "KdRuangan", limit: 3
    t.varchar "SarankanMintaBantuan", limit: 25
    t.varchar "TempatkanBelPanggilan", limit: 25
    t.varchar "TempatkanBendaMilikPasien", limit: 25
    t.varchar "PastikanTempatTidur", limit: 25
    t.varchar "PastikanPakaianPasien", limit: 25
    t.varchar "BantuPasienSaatAmbulasi", limit: 25
    t.varchar "PasangkanPengaman", limit: 25
    t.varchar "PastikanStiker", limit: 25
    t.varchar "PastikanLabel", limit: 25
    t.varchar "BantuPasienKeKamarKecil", limit: 25
    t.varchar "PasangkanRestrain", limit: 25
    t.varchar "BeritahukanEfekDariObat", limit: 25
    t.varchar "BerikanOrientasiRuangan", limit: 25
    t.varchar "BeriPenjelasanUlang", limit: 25
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
    t.varchar "UserId", limit: 15
  end

  create_table "T_PA_Intervensi_JatuhTinggi_Dewasa", primary_key: "ID_Rjd", id: :integer, force: :cascade do |t|
    t.char "NoCM", limit: 8
    t.char "NoPendaftaran", limit: 10
    t.char "KdRuangan", limit: 3
    t.varchar "SarankanMemintaBantuan", limit: 25
    t.varchar "TempelkanBelPasien", limit: 25
    t.varchar "TempatkanBendaMilikPasien", limit: 25
    t.varchar "TempatTidurPosisiRendah", limit: 25
    t.varchar "PakaianDiatasMataKaki", limit: 25
    t.varchar "TransferAmbulasi", limit: 25
    t.varchar "PengamanSisiTempatTidur", limit: 25
    t.varchar "StikerResikoPasienJatuh", limit: 25
    t.varchar "LabelResikoJatuh", limit: 25
    t.varchar "BantuPasienKeKamarKecil", limit: 25
    t.varchar "PasangkanRestrain", limit: 25
    t.varchar "BeritahukanEfekDariObat", limit: 25
    t.varchar "BerikanOrientasiRuangan", limit: 25
    t.varchar "BeriPenjelasanUlang", limit: 25
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
    t.varchar "UserId", limit: 15
  end

  create_table "T_PA_Intervensi_JatuhTinggi_Geriartri", primary_key: "ID_RjtinggiGeriartri", id: :integer, force: :cascade do |t|
    t.char "NoCM", limit: 8
    t.char "NoPendaftaran", limit: 10
    t.char "KdRuangan", limit: 3
    t.varchar "SarankanMintaBantuan", limit: 25
    t.varchar "TempatkanBelPasien", limit: 25
    t.varchar "TempatkanBendaMilikPasien", limit: 25
    t.varchar "TempatTidurPosisiRendah", limit: 25
    t.varchar "PastikanCelanaPanjang", limit: 25
    t.varchar "BantuPasienAmbulasi", limit: 25
    t.varchar "PengamanSisiTempatTidur", limit: 25
    t.varchar "StikerRJ", limit: 25
    t.varchar "LabelRJ", limit: 25
    t.varchar "BantuPasienkeKamarKecil", limit: 25
    t.varchar "PasangkanTaliPengaman", limit: 25
    t.varchar "BeritahukanEfekObat", limit: 25
    t.varchar "BerikanOrientasiRuangan", limit: 25
    t.varchar "BeriPenjelasanUlangRJ", limit: 25
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
    t.varchar "UserId", limit: 15
  end

  create_table "T_PA_KeadaanUmum", id: false, force: :cascade do |t|
    t.char "NoCM", limit: 8
    t.char "NoPendaftaran", limit: 10
    t.varchar "JenisKesadaran", limit: 100
    t.varchar "AnamnesisV", limit: 50
    t.varchar "AnamnesisE", limit: 50
    t.varchar "AnamnesisM", limit: 50
    t.varchar "AnamnesisGCS", limit: 50
    t.varchar "AnamnesisSpO2", limit: 50
    t.varchar "AnamnesisPernapasan", limit: 50
    t.varchar "AnamnesisNadi", limit: 50
    t.varchar "AnamnesisTekananDarah", limit: 50
    t.varchar "AnamnesisSuhu", limit: 50
    t.varchar "AnamnesisCRT", limit: 50
    t.varchar "KesulitanBernafas", limit: 1
    t.varchar "MemakaiO2", limit: 100
    t.varchar "KesulitanBernafasJenis", limit: 100
    t.varchar "PernafasanCuping", limit: 1
    t.varchar "Batuk", limit: 100
    t.varchar "JenisBatuk", limit: 100
    t.varchar "ArteriRadialis", limit: 100
    t.varchar "JugularisVenaPressure", limit: 100
    t.varchar "Oedem", limit: 100
    t.varchar "RiwayatInfeksi", limit: 100
    t.varchar "RiwayatInfeksiJenis", limit: 100
    t.varchar "AdditionalPrecaution", limit: 100
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
    t.varchar "UserId", limit: 10
  end

  create_table "T_PA_KeadaanUmumNew", primary_key: "ID_KeadaanUmum", id: :integer, force: :cascade do |t|
    t.char "NoCM", limit: 8
    t.char "NoPendaftaran", limit: 10
    t.char "KdRuangan", limit: 3
    t.varchar "JenisKesadaran", limit: 100
    t.varchar "GSCV", limit: 50
    t.varchar "GSCE", limit: 50
    t.varchar "GSCM", limit: 50
    t.varchar "PATekananDarah", limit: 50
    t.varchar "PAFrekuensiNadi", limit: 50
    t.varchar "PASuhu", limit: 50
    t.varchar "PAFrekuensiNafas", limit: 50
    t.varchar "PABeratBadan", limit: 50
    t.varchar "PATinggiBadan", limit: 50
    t.varchar "PASaturasi", limit: 50
    t.varchar "PFPernapasan", limit: 50
    t.varchar "PFPenglihatan", limit: 50
    t.varchar "PFPendengaran", limit: 50
    t.varchar "PFMulut", limit: 50
    t.varchar "PFReflekMenelan", limit: 50
    t.varchar "PFBicara", limit: 50
    t.varchar "PFLuka", limit: 50
    t.varchar "PFLokasiLuka", limit: 50
    t.varchar "PFDefakasi", limit: 50
    t.varchar "PFMiksi", limit: 50
    t.varchar "PFGastrointetinal", limit: 50
    t.varchar "PFPolaTidur", limit: 50
    t.varchar "PFPolaTidurSelainNormal", limit: 50
    t.varchar "PFKulit", limit: 50
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
    t.varchar "UserId", limit: 10
  end

  create_table "T_PA_KomunikasiPengajaran", id: false, force: :cascade do |t|
    t.char "NoCM", limit: 8
    t.char "NoPendaftaran", limit: 10
    t.varchar "Bicara", limit: 100
    t.varchar "KapanGangguan", limit: 100
    t.varchar "BahasaSehari", limit: 100
    t.varchar "BahasaSehariLainnya", limit: 100
    t.varchar "Penerjemah", limit: 1
    t.varchar "PenerjemahBahasa", limit: 100
    t.varchar "BahasaIsyarat", limit: 1
    t.varchar "HambatanBelajar", limit: 100
    t.varchar "CaraBelajar", limit: 100
    t.varchar "CaraBelajarVisual", limit: 100
    t.varchar "HubunganKeluarga", limit: 100
    t.varchar "StatusFungsional", limit: 100
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
    t.varchar "UserId", limit: 10
  end

  create_table "T_PA_MasalahKeperawatan", id: false, force: :cascade do |t|
    t.char "NoCM", limit: 8
    t.char "NoPendaftaran", limit: 10
    t.varchar "Perkemihan", limit: 1
    t.varchar "JenisKemih", limit: 100
    t.varchar "Defekasi", limit: 1
    t.varchar "JenisDefekasi", limit: 100
    t.varchar "MasalahDitemukan", limit: 100
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
    t.varchar "UserId", limit: 10
  end

  create_table "T_PA_NumericRatingScale", primary_key: "ID_Numeric", id: :integer, force: :cascade do |t|
    t.char "NoCM", limit: 8
    t.char "NoPendaftaran", limit: 10
    t.char "KdRuangan", limit: 3
    t.varchar "NumericRatingScale", limit: 200
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
    t.varchar "UserId", limit: 15
  end

  create_table "T_PA_NyeriLuka", id: false, force: :cascade do |t|
    t.char "NoCM", limit: 8
    t.char "NoPendaftaran", limit: 10
    t.varchar "ApakahNyeri", limit: 1
    t.varchar "LokasiNyeri", limit: 100
    t.varchar "JenisNyeri", limit: 100
    t.varchar "SkalaNyeri", limit: 2
    t.varchar "IntegritasKulit", limit: 100
    t.varchar "ResikoDekubitus", limit: 1
    t.varchar "BradenScore", limit: 10
    t.varchar "Luka", limit: 1
    t.varchar "LokasiLuka", limit: 50
    t.varchar "FormatPerawatanLuka", limit: 1
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
    t.varchar "UserId", limit: 10
  end

  create_table "T_PA_Pengkajian_Awal_Nyeri_Ranap", primary_key: "ID_Pengkajian", id: :integer, force: :cascade do |t|
    t.char "NoCM", limit: 8
    t.char "NoPendaftaran", limit: 10
    t.char "KdRuangan", limit: 3
    t.varchar "Penyebab", limit: 200
    t.varchar "Kualitas", limit: 25
    t.varchar "Penyebaran", limit: 25
    t.varchar "Skala", limit: 200
    t.varchar "Waktu", limit: 25
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
    t.varchar "UserId", limit: 10
  end

  create_table "T_PA_ProsedurInvasif", id: false, force: :cascade do |t|
    t.char "NoCM", limit: 8
    t.char "NoPendaftaran", limit: 10
    t.varchar "InfusIntravena", limit: 1
    t.varchar "InfusIntravenaDipasang", limit: 100
    t.datetime "InfusIntravenaTanggal"
    t.varchar "CentralLine", limit: 1
    t.varchar "CentralLineDipasang", limit: 100
    t.datetime "CentralLineTanggal"
    t.varchar "DowerChateter", limit: 1
    t.varchar "DowerChateterDipasang", limit: 100
    t.datetime "DowerChateterTanggal"
    t.varchar "SelangNGT", limit: 1
    t.varchar "SelangNGTDipasang", limit: 100
    t.datetime "SelangNGTTanggal"
    t.varchar "CytostomyChat", limit: 1
    t.varchar "CytostomyChatDipasang", limit: 50
    t.datetime "CytostomyChatTanggal"
    t.varchar "Tracheostomy", limit: 1
    t.varchar "TracheostomyDipasang", limit: 100
    t.datetime "TracheostomyTanggal"
    t.varchar "InvasifLain", limit: 1
    t.varchar "InvasifLainDipasang", limit: 100
    t.datetime "InvasifLainTanggal"
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
    t.varchar "UserId", limit: 10
  end

  create_table "T_PA_PsikososialEkonomi", id: false, force: :cascade do |t|
    t.char "NoCM", limit: 8
    t.char "NoPendaftaran", limit: 10
    t.varchar "StatusPerkawinan", limit: 50
    t.varchar "TinggalBersamaKeluarga", limit: 1
    t.varchar "TinggalBersamaKeluargaJelaskan", limit: 100
    t.varchar "EkspresiEmosi", limit: 100
    t.varchar "RiwayatKebiasaan", limit: 100
    t.varchar "RiwayatKebiasaanLainnya", limit: 100
    t.varchar "RiwayatKebiasaanJumlah", limit: 100
    t.varchar "ResikoMencederai", limit: 1
    t.varchar "PenghasilanPerBulan", limit: 100
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
    t.varchar "UserId", limit: 10
  end

  create_table "T_PA_RiwayatKesehatan", primary_key: "ID_RiwayatKesehatan", id: :integer, force: :cascade do |t|
    t.char "NoCM", limit: 8
    t.char "NoPendaftaran", limit: 10
    t.char "KdRuangan", limit: 3
    t.varchar "Pernahdirawat", limit: 50
    t.varchar "TerakhirDiRawat", limit: 50
    t.varchar "DimanaTerakhirDiRawat", limit: 50
    t.varchar "AlatImplanTerpasang", limit: 50
    t.varchar "RiwayatDalamKeluarga", limit: 50
    t.varchar "PenyakitMayor", limit: 50
    t.varchar "StatusPsikologis", limit: 50
    t.varchar "LainLainPsikologis", limit: 50
    t.varchar "StatusMental", limit: 50
    t.varchar "LainLainStatusMental", limit: 50
    t.varchar "LainLainPerilakuKekerasanFisik", limit: 50
    t.varchar "HubunganPasienDanAnggotaKeluarga", limit: 50
    t.varchar "PekerjaanPasien", limit: 50
    t.varchar "CaraPembayaran", limit: 50
    t.varchar "TinggalDenganSiapa", limit: 50
    t.varchar "PenggunaanAlatBantuDiri", limit: 50
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
    t.varchar "UserId", limit: 10
  end

  create_table "T_PA_SkriningFaktorRisikoPasienPulang", primary_key: "ID_FaktorRisikoPasienPulang", id: :integer, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 10
    t.varchar "NoCM", limit: 8
    t.varchar "KdRuangan", limit: 3
    t.varchar "KeluargaMengetahuiRencanaPulang", limit: 10
    t.varchar "UmurDiatasEnamLima", limit: 10
    t.varchar "KeteranganUmurDiatasEnamLima", limit: 255
    t.varchar "KeterbatasanMobilitas", limit: 10
    t.varchar "KeteranganKeterbatasanMobilitas", limit: 255
    t.varchar "Perawatan", limit: 10
    t.varchar "KeteranganPerawatan", limit: 255
    t.varchar "Batasan", limit: 10
    t.varchar "KeteranganBatasan", limit: 255
    t.varchar "TinggalSendiri", limit: 10
    t.varchar "KeteranganTinggalSendiri", limit: 255
    t.varchar "Khawatir", limit: 10
    t.varchar "KeteranganKhawatir", limit: 255
    t.varchar "AdaYangMerawat", limit: 10
    t.varchar "KeteranganAdaYangMerawat", limit: 255
    t.varchar "AdaTangga", limit: 10
    t.varchar "KeteranganAdaTangga", limit: 255
    t.varchar "TanggungJawab", limit: 10
    t.varchar "KeteranganTanggungJawab", limit: 255
    t.varchar "PerawatanLanjutan", limit: 10
    t.varchar "KeteranganPerawatanLanjutan", limit: 255
    t.varchar "PulangDenganJumlahObatLebih", limit: 10
    t.varchar "KeteranganPulangDenganJumlahObatLebih", limit: 255
    t.varchar "PasienMengajukanPermohonan", limit: 10
    t.varchar "KeteranganPasienMengajukanPermohonan", limit: 255
    t.varchar "TransportasiPasienUntukPulang", limit: 10
    t.varchar "KeteranganTransportasiPasienUntukPulang", limit: 255
    t.varchar "JenisTempatTinggalPasien", limit: 10
    t.varchar "IdUser", limit: 10
    t.varchar "PIN", limit: 10
    t.datetime "CreateDate", null: false
    t.datetime "UpdateDate", null: false
  end

  create_table "T_PA_SkriningGizi", primary_key: "ID_SkriningGizi", id: :integer, force: :cascade do |t|
    t.char "NoCM", limit: 8
    t.char "NoPendaftaran", limit: 10
    t.char "KdRuangan", limit: 3
    t.varchar "PenurunanBeratBadan", limit: 25
    t.varchar "AsupanMakananBerkurang", limit: 25
    t.varchar "TindakanKhusus", limit: 25
    t.varchar "SebutkanTindakanKhusus", limit: 25
    t.varchar "RisikoMalnutrisi", limit: 25
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
    t.varchar "UserId", limit: 10
  end

  create_table "T_PA_SkriningStatusFungsional", primary_key: "ID_StatusFungsional", id: :integer, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 10
    t.varchar "NoCM", limit: 8
    t.varchar "KdRuangan", limit: 3
    t.integer "MengendalikanBAB"
    t.integer "MengendalikanBAK"
    t.integer "MembersihkanDiri"
    t.integer "PenggunaanToilet"
    t.integer "Makan"
    t.integer "BerubahSikapDariBaringKeDuduk"
    t.integer "BerpindahAtauBerjalan"
    t.integer "MemakaiBaju"
    t.integer "NaikTurunTangga"
    t.integer "Mandi"
    t.integer "Sum_SkorStatusFungsional"
    t.datetime "TanggalInput"
    t.varchar "Mandiri", limit: 2
    t.varchar "PerluBantuanRingan", limit: 2
    t.varchar "KetergantunganSedang", limit: 2
    t.varchar "KetergantunganBerat", limit: 2
    t.varchar "SebutkanKetergantunganBerat", limit: 255
    t.varchar "KetergantunganTotal", limit: 2
    t.varchar "DiLaporkanKeDokter", limit: 255
    t.varchar "TidakSemuanya", limit: 255
    t.varchar "IdUser", limit: 10
    t.datetime "CreateDate", null: false
    t.datetime "UpdateDate", null: false
  end

  create_table "T_PA_StatusNutrisi", id: false, force: :cascade do |t|
    t.char "NoCM", limit: 8
    t.char "NoPendaftaran", limit: 10
    t.varchar "PenurunanBB", limit: 1
    t.varchar "AsupanMakanBerkurang", limit: 1
    t.varchar "TindakanKhusus", limit: 1
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
    t.varchar "UserId", limit: 10
  end

  create_table "T_PA_StatusTiba", primary_key: "ID_StatusTiba", id: :integer, force: :cascade do |t|
    t.char "NoCM", limit: 8
    t.char "NoPendaftaran", limit: 10
    t.char "KdRuangan", limit: 3
    t.varchar "TibaDenganCara", limit: 1
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
    t.varchar "UserId", limit: 10
  end

  create_table "T_PA_WongBakerPainScale", primary_key: "ID_WongBacker", id: :integer, force: :cascade do |t|
    t.char "NoCM", limit: 8
    t.char "NoPendaftaran", limit: 10
    t.char "KdRuangan", limit: 3
    t.varchar "WongBakerPainScale", limit: 200
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
    t.varchar "UserId", limit: 15
  end

  create_table "T_PaketObat", primary_key: "IdPaket", id: :integer, force: :cascade do |t|
    t.char "NamaPaket", limit: 50
    t.varchar "KdRuangan", limit: 3
    t.varchar "KdBarang", limit: 50
    t.varchar_max "NamaBarang"
    t.varchar "JenisBarang", limit: 50
    t.varchar "IdUser", limit: 10
  end

  create_table "T_PaketTindakan", primary_key: "IdPaket", id: :integer, force: :cascade do |t|
    t.char "NamaPaket", limit: 50
    t.varchar "KdRuangan", limit: 3
    t.varchar "KdPelayananRS", limit: 50
    t.varchar_max "NamaPelayanan"
    t.varchar "JenisPelayanan", limit: 50
    t.varchar "IdUser", limit: 10
    t.varchar "KdKelas", limit: 2
  end

  create_table "T_Partograf", primary_key: "IdPartograf", id: :integer, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10
    t.char "NoCM", limit: 8
    t.varchar "G", limit: 20
    t.varchar "P", limit: 20
    t.varchar "A", limit: 20
    t.datetime "TanggalMasukVK"
    t.datetime "WaktuKetubanPecah"
    t.datetime "WaktuMules"
    t.varchar "DenyutJantungJanin", limit: 20
    t.varchar "AirKetuban1", limit: 20
    t.varchar "AirKetuban2", limit: 20
    t.varchar "PembukaanServik", limit: 20
    t.varchar "TurunKepala", limit: 20
    t.datetime "Waktu_Jam"
    t.varchar "Kontraksi", limit: 20
    t.varchar "KontraksiDetik", limit: 20
    t.varchar "Oksitosin1", limit: 20
    t.varchar "Oksitosin2", limit: 20
    t.varchar "ObatCairan", limit: 1000
    t.varchar "Nadi", limit: 20
    t.varchar "TekananDarah", limit: 20
    t.varchar "Temperatur", limit: 20
    t.varchar "Urin_Protein", limit: 20
    t.varchar "Urin_Aseten", limit: 20
    t.varchar "Urin_Volume", limit: 20
    t.datetime "TglInput"
    t.datetime "TglUpdate"
    t.char "IdUser", limit: 10
    t.varchar "TekananDarahPer", limit: 20
  end

  create_table "T_PelaksanaanRisikoJatuhAnak", id: false, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 10
    t.varchar "NoCM", limit: 8
    t.varchar "KdRuangan", limit: 3
    t.varchar "OrientasiRuangan", limit: 1
    t.varchar "MerendahkanTTdanRem", limit: 1
    t.varchar "PengamanSampingTT", limit: 1
    t.varchar "MenggunakanAlasKaki", limit: 1
    t.varchar "MenilaiKebutuhan", limit: 1
    t.varchar "MemberiAkses", limit: 1
    t.varchar "MemastikanLingkungan", limit: 1
    t.varchar "MemasangLampu", limit: 1
    t.varchar "MenyediakanLeaflet", limit: 1
    t.varchar "MendokumentasikanPemantauan", limit: 1
    t.varchar "MemberiTandaPeringatan", limit: 1
    t.varchar "ProtokolPencegahan", limit: 1
    t.varchar "ObservasiDuaJam", limit: 1
    t.varchar "MendampingiPasien", limit: 1
    t.varchar "MenyesuaikanTT", limit: 1
    t.varchar "PenempatanPasien", limit: 1
    t.varchar "MelibatkanPasien", limit: 1
    t.varchar "EvaluasiTerapi", limit: 1
    t.varchar "PencegahanJatuh", limit: 1
    t.varchar "MembukaPintu", limit: 1
    t.varchar "MerendahkanTT", limit: 1
    t.varchar "DokumentasiKegiatan", limit: 1
    t.varchar "IdUser", limit: 10
    t.varchar "PIN", limit: 10
    t.datetime "CreateDate", null: false
    t.datetime "UpdateDate", null: false
  end

  create_table "T_PelaksanaanRisikoJatuhDewasa", id: false, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 10
    t.varchar "NoCM", limit: 8
    t.varchar "KdRuangan", limit: 3
    t.varchar "ObservasiBantuan", limit: 1
    t.varchar "KeselamatanLingkungan", limit: 1
    t.varchar "MonitorKebutuhan", limit: 1
    t.varchar "MemberiEdukasi", limit: 1
    t.varchar "MenggunakanAlatBantu", limit: 1
    t.varchar "AnjuranAlasKaki", limit: 1
    t.varchar "MemakaikanGelang", limit: 1
    t.varchar "AnjuranAlasKaki2", limit: 1
    t.varchar "IntervensiStandar", limit: 1
    t.varchar "PencegahanDetail", limit: 1
    t.varchar "DekatNurseStation", limit: 1
    t.varchar "MemasangHandtrail", limit: 1
    t.varchar "MenyiapkanKomod", limit: 1
    t.varchar "MenjagaLantai", limit: 1
    t.varchar "DampingiPasien", limit: 1
    t.varchar "IdUser", limit: 10
    t.varchar "PIN", limit: 10
    t.datetime "CreateDate", null: false
    t.datetime "UpdateDate", null: false
  end

  create_table "T_PelaksanaanRisikoJatuhGeriatrik", id: false, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 10
    t.varchar "NoCM", limit: 8
    t.varchar "KdRuangan", limit: 3
    t.varchar "ObservasiBantuan", limit: 1
    t.varchar "KeselamatanLingkungan", limit: 1
    t.varchar "MonitoringKebutuhan", limit: 1
    t.varchar "MemberEdukasi", limit: 1
    t.varchar "MenggunakanAlatBantu", limit: 1
    t.varchar "MenganjurkanAlasKaki", limit: 1
    t.varchar "MemakaikanGelang", limit: 1
    t.varchar "AnjuranAlasKaki", limit: 1
    t.varchar "IntervensiJatuh", limit: 1
    t.varchar "MelakukanPencegahanJatuh", limit: 1
    t.varchar "DekatNurseStation", limit: 1
    t.varchar "MemasangHandrail", limit: 1
    t.varchar "MenyiapkanKomod", limit: 1
    t.varchar "MenjagaLantai", limit: 1
    t.varchar "DampingiPasien", limit: 1
    t.varchar "IdUser", limit: 10
    t.varchar "PIN", limit: 10
    t.datetime "CreateDate", null: false
    t.datetime "UpdateDate", null: false
  end

  create_table "T_PemberianObat", id: false, force: :cascade do |t|
    t.char "KdPemberian", limit: 2, null: false
    t.varchar "NamaPemberian", limit: 50
    t.char "StatusEnabled", limit: 1
  end

  create_table "T_PengkajianAwalDokter", primary_key: "ID_pad", id: :integer, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 10
    t.varchar "NoCM", limit: 8
    t.varchar "KdRuangan", limit: 3
    t.datetime "TglInput"
    t.varchar_max "KeluhanUtamaDokter"
    t.varchar_max "RiwayatPenyakitSekarang"
    t.varchar_max "PFKeadaanUmum"
    t.varchar "PFKesadaran", limit: 20
    t.varchar "PFKepala", limit: 20
    t.varchar_max "KetPFKepala"
    t.varchar "PFMata", limit: 20
    t.varchar_max "KetPFMata"
    t.varchar "PFHidung", limit: 20
    t.varchar_max "KetPFHidung"
    t.varchar "PFGigidanMulut", limit: 20
    t.varchar_max "KetPFGigidanMulut"
    t.varchar "PFTenggorokan", limit: 20
    t.varchar_max "KetPFTenggorokan"
    t.varchar "PFTelinga", limit: 20
    t.varchar_max "KetPFTelinga"
    t.varchar "PFLeher", limit: 20
    t.varchar_max "KetPFLeher"
    t.varchar "PFThoraks", limit: 20
    t.varchar_max "KetPFThoraks"
    t.varchar "PFJantung", limit: 20
    t.varchar_max "KetPFJantung"
    t.varchar "PFParu", limit: 20
    t.varchar_max "KetPFParu"
    t.varchar "PFAbdomen", limit: 20
    t.varchar_max "KetPFAbdomen"
    t.varchar "PFGenitaliadanAnus", limit: 20
    t.varchar_max "KetPFGenitaliadanAnus"
    t.varchar "PFEkstremitas", limit: 20
    t.varchar_max "KetPFEkstremitas"
    t.varchar "PFKulit", limit: 20
    t.varchar_max "KetPFKulit"
    t.varchar_max "DiagnosisAtauAssesment"
    t.varchar_max "TataLaksana"
    t.varchar_max "PerencanaanEdukasi"
    t.varchar_max "PerencanaanDiagnostik"
    t.varchar "SudahDiBerikanRekonsiliasi", limit: 20
    t.varchar_max "ObatYangSedangDikonsumsiSaatIni"
    t.varchar_max "PerencanaanTerapi"
    t.varchar_max "PerencanaanKonsul"
    t.varchar "IdUser", limit: 10
    t.varchar "PIN", limit: 10
    t.datetime "CreateDate", null: false
    t.datetime "UpdateDate", null: false
  end

  create_table "T_PenilaianRisikoJatuhAnak", id: false, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 10
    t.varchar "NoCM", limit: 8
    t.varchar "KdRuangan", limit: 3
    t.integer "Umur"
    t.integer "JenisKelamin"
    t.integer "Diagnosa"
    t.integer "GangguanKognitif"
    t.integer "FaktorLingkungan"
    t.integer "Pembedahan"
    t.integer "PenggunaanMedikaMentosa"
    t.integer "TotalSkor"
    t.varchar "IdUser", limit: 10
    t.varchar "PIN", limit: 10
    t.datetime "CreateDate", null: false
    t.datetime "UpdateDate", null: false
  end

  create_table "T_PenilaianRisikoJatuhAnakOld", id: false, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 10
    t.varchar "NoCM", limit: 8
    t.varchar "KdRuangan", limit: 3
    t.integer "Umur"
    t.integer "JenisKelamin"
    t.integer "Diagnosa"
    t.integer "GangguanKognitif"
    t.integer "FaktorLingkungan"
    t.integer "Respon"
    t.integer "PenggunaanObat"
    t.varchar "IdUser", limit: 10
    t.varchar "PIN", limit: 10
    t.datetime "CreateDate", null: false
    t.datetime "UpdateDate", null: false
  end

  create_table "T_PenilaianRisikoJatuhDewasa", id: false, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 10
    t.varchar "NoCM", limit: 8
    t.varchar "KdRuangan", limit: 3
    t.integer "RiwayatJatuh"
    t.integer "DiagnosisSekunder"
    t.integer "AlatBantuJalan"
    t.integer "MenggunakanInfus"
    t.integer "CaraBerjalan"
    t.integer "StatusMental"
    t.varchar "IdUser", limit: 10
    t.varchar "PIN", limit: 10
    t.datetime "CreateDate", null: false
    t.datetime "UpdateDate", null: false
  end

  create_table "T_PenilaianRisikoJatuhGeriartri", primary_key: "ID_rjgeriartri", id: :integer, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 10
    t.varchar "NoCM", limit: 8
    t.varchar "KdRuangan", limit: 3
    t.integer "GangguanBerjalan"
    t.integer "PusingPingsan"
    t.integer "KebingunganSesaat"
    t.integer "Nokturia"
    t.integer "Intermiten"
    t.integer "KelemahanUmum"
    t.integer "ObatBeresikoTinggi"
    t.integer "RiwayatJatuh"
    t.integer "Osteoporosis"
    t.integer "GangguanPendengaran"
    t.integer "Usiadiatastujuhpuluh"
    t.integer "TotalSkor"
    t.varchar "IdUser", limit: 10
    t.varchar "PIN", limit: 10
    t.datetime "CreateDate", null: false
    t.datetime "UpdateDate", null: false
  end

  create_table "T_PenilaianRisikoJatuhGeriatrik", id: false, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 10
    t.varchar "NoCM", limit: 8
    t.varchar "KdRuangan", limit: 3
    t.integer "PasienJatuh"
    t.integer "JatuhDuaBulan"
    t.integer "Delirium"
    t.integer "Disorientasi"
    t.integer "Agitasi"
    t.integer "MemakaiKacamata"
    t.integer "PenglihatanBuram"
    t.integer "Glaukoma"
    t.integer "PerilakuBerkemih"
    t.integer "Transfer"
    t.integer "Mobilitas"
    t.integer "TotalSkor"
    t.varchar "IdUser", limit: 10
    t.varchar "PIN", limit: 10
    t.datetime "CreateDate", null: false
    t.datetime "UpdateDate", null: false
  end

  create_table "T_PerawatanNifas", primary_key: "IdPerawatanNifas", id: :integer, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10
    t.char "NoCM", limit: 8
    t.varchar "KU", limit: 500
    t.varchar "Keluhan", limit: 500
    t.varchar "TD", limit: 500
    t.varchar "LAC", limit: 500
    t.varchar "FU", limit: 500
    t.varchar "Kont", limit: 500
    t.varchar "Perinium", limit: 500
    t.varchar "Loc", limit: 500
    t.varchar "BAK", limit: 500
    t.varchar "BAB", limit: 500
    t.varchar "BAK_Bayi", limit: 500
    t.varchar "BAB_Bayi", limit: 500
    t.varchar "KU_Bayi", limit: 500
    t.varchar "ASI_Saja", limit: 500
    t.varchar "Therapi", limit: 500
    t.datetime "TglInput"
    t.datetime "TglUpdate"
    t.char "IdUser", limit: 10
  end

  create_table "T_Perkemihan", id: false, force: :cascade do |t|
    t.char "KdPerkemihan", limit: 2, null: false
    t.varchar "Perkemihan", limit: 50, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "T_ResumePasien", primary_key: "IdResume", id: :integer, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10
    t.char "NoCM", limit: 8
    t.varchar "NamaSuami", limit: 100
    t.datetime "TglLahirSuami"
    t.varchar "AgamaSuami", limit: 20
    t.varchar "PendidikanSuami", limit: 50
    t.varchar "PekerjaanSuami", limit: 50
    t.varchar "PerkawinanSuamiKe", limit: 10
    t.varchar "LamaPerkawinanSuami", limit: 10
    t.varchar "PerkawinanIstriKe", limit: 10
    t.varchar "LamaPerkawinanIstri", limit: 10
    t.varchar "Gravida", limit: 100
    t.varchar "Partus", limit: 100
    t.varchar "Abortus", limit: 100
    t.datetime "TglPeriksa"
    t.datetime "TglHPHT"
    t.datetime "TglTP"
    t.varchar "BB_SebelumHamil", limit: 20
    t.varchar "TinggiBadan", limit: 20
    t.varchar "AlergiObat", limit: 100
    t.varchar "AlergiMakanan", limit: 100
    t.varchar "KelainanJantung", limit: 1
    t.varchar "KelainanParu", limit: 1
    t.varchar "KelainanGinjal", limit: 1
    t.varchar "KelainanHati", limit: 1
    t.varchar "Asma", limit: 1
    t.varchar "DM", limit: 1
    t.varchar "Hipertensi", limit: 1
    t.varchar "Operasi", limit: 1
    t.varchar "Gemelli", limit: 1
    t.varchar "Thalasemia", limit: 1
    t.varchar "HipertensiKeluarga", limit: 1
    t.varchar "DMKeluarga", limit: 1
    t.varchar "AsmaKeluarga", limit: 1
    t.varchar "Lainnya", limit: 50
    t.datetime "TglInput"
    t.datetime "TglUpdate"
    t.char "IdUser", limit: 10
  end

  create_table "T_ResumePasienTMP", primary_key: "IdResume", id: :integer, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10
    t.char "NoCM", limit: 8
    t.varchar "NamaSuami", limit: 100
    t.datetime "TglLahirSuami"
    t.varchar "AgamaSuami", limit: 20
    t.varchar "PendidikanSuami", limit: 50
    t.varchar "PekerjaanSuami", limit: 50
    t.varchar "PerkawinanSuamiKe", limit: 10
    t.varchar "LamaPerkawinanSuami", limit: 10
    t.varchar "PerkawinanIstriKe", limit: 10
    t.varchar "LamaPerkawinanIstri", limit: 10
    t.varchar "Gravida", limit: 100
    t.varchar "Partus", limit: 100
    t.varchar "Abortus", limit: 100
    t.datetime "TglPeriksa"
    t.datetime "TglHPHT"
    t.datetime "TglTP"
    t.varchar "BB_SebelumHamil", limit: 20
    t.varchar "TinggiBadan", limit: 20
    t.varchar "AlergiObat", limit: 100
    t.varchar "AlergiMakanan", limit: 100
    t.varchar "KelainanJantung", limit: 1
    t.varchar "KelainanParu", limit: 1
    t.varchar "KelainanGinjal", limit: 1
    t.varchar "KelainanHati", limit: 1
    t.varchar "Asma", limit: 1
    t.varchar "DM", limit: 1
    t.varchar "Hipertensi", limit: 1
    t.varchar "Operasi", limit: 1
    t.varchar "PenyakitJantung", limit: 1
    t.varchar "Stroke", limit: 1
    t.varchar "HipertensiKeluarga", limit: 1
    t.varchar "DMKeluarga", limit: 1
    t.varchar "AsmaKeluarga", limit: 1
    t.varchar "Kanker", limit: 1
    t.varchar "KolesterolTinggi", limit: 1
    t.varchar "Benjolan", limit: 1
    t.varchar "Lainnya", limit: 50
    t.datetime "TglInput"
    t.datetime "TglUpdate"
    t.char "IdUser", limit: 10
    t.varchar "Perokok", limit: 1
    t.varchar "KonsumsiAlkohol", limit: 1
    t.varchar "AktivitasFisik", limit: 1
    t.varchar "MakananBerserat", limit: 1
    t.varchar "DiabetesMellitus", limit: 1
    t.varchar "HipertensiDiriSendiri", limit: 1
    t.varchar "PenyakitJantungDiriSendiri", limit: 1
    t.varchar "StrokeDiriSendiri", limit: 1
    t.varchar "AsmaDiriSendiri", limit: 1
    t.varchar "KankerDiriSendiri", limit: 1
    t.varchar "Hiperkolesterol", limit: 1
    t.varchar "BenjolanPayudara", limit: 1
  end

  create_table "T_RiwayatInfeksi", id: false, force: :cascade do |t|
    t.char "KdRiwayatInfeksi", limit: 2, null: false
    t.varchar "RiwayatInfeksi", limit: 50, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "T_RiwayatKB", primary_key: "IdRiwayatKB", id: :integer, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10
    t.char "NoCM", limit: 8
    t.varchar "JmlAnakLakiHidup", limit: 20
    t.varchar "JmlAnakPerempuanHidup", limit: 20
    t.varchar "UmurAnakTerakhirHidup", limit: 100
    t.varchar "StatusPesertaKB", limit: 100
    t.varchar "CaraKBTerakhir", limit: 100
    t.datetime "TglHaidTerakhir"
    t.varchar "SedangHamil", limit: 1
    t.varchar "Gravida", limit: 100
    t.varchar "Partus", limit: 100
    t.varchar "Abortus", limit: 100
    t.varchar "Menyusui", limit: 1
    t.varchar "SakitKuning", limit: 1
    t.varchar "PendarahanPervaginum", limit: 1
    t.varchar "Keputihan", limit: 1
    t.varchar "TumorPayudara", limit: 1
    t.varchar "TumorRahim", limit: 1
    t.varchar "TumorIndungTelur", limit: 1
    t.varchar "TandaRadang", limit: 1
    t.varchar "KeganasanGinekologi", limit: 1
    t.varchar "PosisiRahim", limit: 20
    t.datetime "TglDicabut"
    t.datetime "TglInput"
    t.datetime "TglUpdate"
    t.char "IdUser", limit: 10
    t.datetime "TglLahirSuamiIstri"
    t.varchar "PendidikanSuamiIstri", limit: 50
    t.varchar "PekerjaanSuamiIstri", limit: 50
    t.datetime "TglKembali"
  end

  create_table "T_RiwayatKehamilan", primary_key: "IdRiwayat", id: :integer, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10
    t.char "NoCM", limit: 8
    t.varchar "Umur", limit: 100
    t.varchar "RiwayatLH", limit: 20
    t.varchar "RiwayatLM", limit: 20
    t.varchar "RiwayatAB", limit: 20
    t.varchar "JK", limit: 1
    t.varchar "BeratPanjangBadan", limit: 50
    t.varchar "KdPersalinan", limit: 2
    t.varchar "Penolong", limit: 50
    t.varchar "Tempat", limit: 50
    t.varchar "KeadaanAnak", limit: 100
    t.varchar "Komplikasi", limit: 100
    t.varchar "LamaMenyusui", limit: 20
    t.varchar "RKB", limit: 20
    t.varchar "TT", limit: 20
    t.datetime "TglInput"
    t.datetime "TglUpdate"
    t.char "IdUser", limit: 10
  end

  create_table "T_StatusPesertaKB", id: false, force: :cascade do |t|
    t.char "KdStatusPesertaKB", limit: 2, null: false
    t.varchar "NamaStatus", limit: 100
    t.char "StatusEnabled", limit: 1
  end

  create_table "T_Usg", id: false, force: :cascade do |t|
    t.varchar "NoCM", limit: 8
    t.varchar "NoPendaftaran", limit: 10
    t.varchar_max "Identitas"
    t.varchar_max "NamaPasien"
    t.varchar "Umur", limit: 20
    t.varchar_max "DJA"
    t.varchar_max "Keluhan"
    t.varchar_max "Janinkepala"
    t.varchar_max "JaninTunggal"
    t.varchar_max "JaninIntra"
    t.varchar "JenisKelamin", limit: 3
    t.varchar_max "BPD"
    t.varchar_max "FL"
    t.varchar_max "CRL"
    t.varchar_max "kelainan"
    t.varchar_max "kehamilan"
    t.varchar_max "Plasenta"
    t.varchar_max "Anjuran"
    t.datetime "TglInput"
    t.varchar "IdUser", limit: 10
    t.varchar_max "ICA"
    t.varchar_max "HC"
    t.varchar_max "AC"
    t.varchar_max "GS"
    t.varchar_max "TBJ"
    t.varchar_max "TinggiBadan"
    t.varchar_max "BeratBadan"
    t.varchar_max "IUFD"
    t.varchar_max "EFW"
    t.varchar_max "Abnormal"
    t.varchar_max "lokasiimplan"
    t.varchar_max "Kesan"
    t.varchar_max "Sonografisa"
    t.varchar_max "Saran"
    t.varchar_max "TP"
  end

  create_table "TagihanSupplier", id: false, force: :cascade do |t|
    t.char "NoTerima", limit: 10, null: false
    t.char "KdAsal", limit: 2, null: false
    t.money "TotalBiaya", precision: 19, scale: 4, null: false
    t.money "TotalPpn", precision: 19, scale: 4, null: false
    t.money "TotalDiscount", precision: 19, scale: 4, null: false
    t.money "TotalMaterai", precision: 19, scale: 4, null: false
    t.money "TotalDiscountGive", precision: 19, scale: 4, null: false
    t.money "TotalDiscountSave", precision: 19, scale: 4, null: false
  end

  create_table "TandaTanganPasien", id: false, force: :cascade do |t|
    t.char "NoCM", limit: 15
    t.varchar "SignaturesName", limit: 50
    t.datetime "Createdate"
  end

  create_table "TanggunganAsuransiNonPaket", id: false, force: :cascade do |t|
    t.varchar "KdPaket", limit: 3, null: false
    t.char "KdKelompokPasien", limit: 2, null: false
    t.char "IdPenjamin", limit: 10, null: false
    t.char "KdPelayananRS", limit: 6, null: false
    t.char "KdKelas", limit: 2, null: false
    t.money "JmlTanggungan", precision: 19, scale: 4, null: false
    t.real "PersenTRSfromSelisih", null: false
  end

  create_table "TanggunganLuarPaketOAAsuransi", primary_key: ["KdPaket", "KdKelompokPasien", "IdPenjamin", "KdBarang"], force: :cascade do |t|
    t.varchar "KdPaket", limit: 3, null: false
    t.char "KdKelompokPasien", limit: 2, null: false
    t.char "IdPenjamin", limit: 10, null: false
    t.varchar "KdBarang", limit: 9, null: false
    t.money "JmlTanggungan", precision: 19, scale: 4, null: false
    t.real "PersenTRSfromSelisih", null: false
  end

  create_table "TanggunganPaketAsuransi", id: false, force: :cascade do |t|
    t.varchar "KdPaket", limit: 3, null: false
    t.char "KdKelompokPasien", limit: 2, null: false
    t.char "IdPenjamin", limit: 10, null: false
    t.char "KdKelas", limit: 2, null: false
    t.money "JmlTanggungan", precision: 19, scale: 4, null: false
    t.real "PersenTRSfromSelisih", null: false
  end

  create_table "TanggunganPaketAsuransiPerTindakan", id: false, force: :cascade do |t|
    t.varchar "KdPaket", limit: 3, null: false
    t.char "KdKelompokPasien", limit: 2, null: false
    t.char "IdPenjamin", limit: 10, null: false
    t.char "KdPelayananRS", limit: 6, null: false
    t.char "KdKelas", limit: 2, null: false
    t.money "JmlTanggungan", precision: 19, scale: 4, null: false
    t.real "PersenTRSfromSelisih", null: false
  end

  create_table "TanggunganPaketOAAsuransi", primary_key: ["KdPaket", "KdKelompokPasien", "IdPenjamin"], force: :cascade do |t|
    t.varchar "KdPaket", limit: 3, null: false
    t.char "KdKelompokPasien", limit: 2, null: false
    t.char "IdPenjamin", limit: 10, null: false
    t.money "JmlTanggungan", precision: 19, scale: 4, null: false
    t.real "PersenTRSfromSelisih", null: false
  end

  create_table "TarifPelayanan", id: false, force: :cascade do |t|
    t.char "KdPelayananRS", limit: 6, null: false
    t.char "KdKelas", limit: 2, null: false
    t.money "Total", precision: 19, scale: 4, null: false
    t.char "KdJenisTarif", limit: 2, null: false
    t.index ["KdPelayananRS", "KdKelas", "KdJenisTarif"], name: "IX-TarifPelayanan"
  end

  create_table "TblAktifitas", primary_key: "kodeaktifitas", id: :integer, force: :cascade do |t|
    t.varchar "aktifitas", limit: 250
    t.integer "waktuefektif"
    t.varchar "bidang", limit: 100
  end

  create_table "TblBidang", id: false, force: :cascade do |t|
    t.string "KodeBidang", limit: 6, null: false
    t.string "Bidang", limit: 50
  end

  create_table "TblHKDloadData", id: false, force: :cascade do |t|
    t.integer "IDLog", null: false
    t.string "PIN", limit: 11
    t.datetime "TglWaktu"
    t.string "TA", limit: 2
    t.string "HKAddr", limit: 2
    t.string "IPAddr", limit: 20
    t.string "Keterangan", limit: 15
    t.varchar "flag", limit: 1
  end

  create_table "TblHKUser", primary_key: ["NIP", "PIN"], force: :cascade do |t|
    t.string "NIP", limit: 18, null: false
    t.string "PIN", limit: 10, null: false
    t.string "Temp0", limit: 3
    t.string "Temp1", limit: 3
    t.string "Temp2", limit: 3
    t.string "Temp3", limit: 3
    t.string "Temp4", limit: 3
    t.string "Temp5", limit: 3
    t.string "Temp6", limit: 3
    t.string "Temp7", limit: 3
    t.string "Temp8", limit: 3
    t.string "Authority", limit: 1
    t.string "RThreshold", limit: 3
    t.string "TimeZone", limit: 2
  end

  create_table "TblJabatan", id: false, force: :cascade do |t|
    t.string "KodeJabatan", limit: 5, null: false
    t.string "Jabatan", limit: 200
    t.varchar "Bidang", limit: 200
    t.varchar "Bidang2", limit: 200
    t.varchar "Bidang3", limit: 200
  end

  create_table "TblJabatanProfesi", id: false, force: :cascade do |t|
    t.varchar "id", limit: 25
    t.varchar "jenisprofesi", limit: 200
    t.varchar "kelompok", limit: 50
  end

  create_table "TblJabatanRemun", id: false, force: :cascade do |t|
    t.string "KodeJabatan", limit: 5, null: false
    t.string "Jabatan", limit: 200
    t.varchar "Bidang", limit: 200
    t.float "CV"
    t.float "Level"
  end

  create_table "TblKawin", id: false, force: :cascade do |t|
    t.varchar "KodeKawin", limit: 6
    t.varchar "Nama", limit: 50
  end

  create_table "TblPejabat", id: false, force: :cascade do |t|
    t.integer "KodePejabat"
    t.varchar "PIN", limit: 6
    t.varchar "IDPEGAWAI", limit: 30
    t.varchar "NIP", limit: 30
    t.varchar "NamaPejabat", limit: 200
    t.varchar "KodeJabatan", limit: 6
    t.varchar "Kelompok", limit: 50
  end

  create_table "TblRealisasi", id: false, force: :cascade do |t|
    t.string "TipeShift", limit: 1
    t.string "KodeShift", limit: 18
    t.string "Uraian", limit: 50
    t.datetime "TglAwal"
    t.datetime "TglAkhir"
    t.string "H01", limit: 2
    t.string "H02", limit: 2
    t.string "H03", limit: 2
    t.string "H04", limit: 2
    t.string "H05", limit: 2
    t.string "H06", limit: 2
    t.string "H07", limit: 2
    t.string "H08", limit: 2
    t.string "H09", limit: 2
    t.string "H10", limit: 2
    t.string "H11", limit: 2
    t.string "H12", limit: 2
    t.string "H13", limit: 2
    t.string "H14", limit: 2
    t.string "H15", limit: 2
    t.string "H16", limit: 2
    t.string "H17", limit: 2
    t.string "H18", limit: 2
    t.string "H19", limit: 2
    t.string "H20", limit: 2
    t.string "H21", limit: 2
    t.string "H22", limit: 2
    t.string "H23", limit: 2
    t.string "H24", limit: 2
    t.string "H25", limit: 2
    t.string "H26", limit: 2
    t.string "H27", limit: 2
    t.string "H28", limit: 2
    t.string "H29", limit: 2
    t.string "H30", limit: 2
    t.string "H31", limit: 2
  end

  create_table "TblSeksi", id: false, force: :cascade do |t|
    t.varchar "kd_seksi", limit: 12
    t.varchar "seksi", limit: 200
    t.varchar "BlokName", limit: 10
    t.varchar "FloorName", limit: 10
  end

  create_table "TblShift", id: false, force: :cascade do |t|
    t.string "TipeShift", limit: 1
    t.string "KodeShift", limit: 18
    t.string "Uraian", limit: 50
    t.datetime "TglAwal"
    t.datetime "TglAkhir"
    t.string "H01", limit: 2
    t.string "H02", limit: 2
    t.string "H03", limit: 2
    t.string "H04", limit: 2
    t.string "H05", limit: 2
    t.string "H06", limit: 2
    t.string "H07", limit: 2
    t.string "H08", limit: 2
    t.string "H09", limit: 2
    t.string "H10", limit: 2
    t.string "H11", limit: 2
    t.string "H12", limit: 2
    t.string "H13", limit: 2
    t.string "H14", limit: 2
    t.string "H15", limit: 2
    t.string "H16", limit: 2
    t.string "H17", limit: 2
    t.string "H18", limit: 2
    t.string "H19", limit: 2
    t.string "H20", limit: 2
    t.string "H21", limit: 2
    t.string "H22", limit: 2
    t.string "H23", limit: 2
    t.string "H24", limit: 2
    t.string "H25", limit: 2
    t.string "H26", limit: 2
    t.string "H27", limit: 2
    t.string "H28", limit: 2
    t.string "H29", limit: 2
    t.string "H30", limit: 2
    t.string "H31", limit: 2
  end

  create_table "TblStudi", id: false, force: :cascade do |t|
    t.varchar "KodeStudi", limit: 30
    t.varchar "studi", limit: 100
  end

  create_table "TblTransaksi", id: false, force: :cascade do |t|
    t.string "NIP", limit: 18
    t.string "KodeGolongan", limit: 5
    t.string "KodeEselon", limit: 5
    t.string "KodeJabatan", limit: 5
    t.string "KodeFungsional", limit: 5
    t.string "KodeDeputi", limit: 6
    t.string "KodeBiro", limit: 6
    t.string "KodeBidang", limit: 6
    t.string "KodeSeksi", limit: 6
    t.string "KodeLokasi", limit: 5
    t.string "KodeAgama", limit: 5
    t.string "Tahun", limit: 4
    t.string "Bulan", limit: 2
    t.datetime "TglBuku"
    t.string "KodeWaktuKerja", limit: 2
    t.datetime "JadwalMasuk"
    t.datetime "TolMasuk"
    t.datetime "JadwalKeluar"
    t.datetime "TolKeluar"
    t.datetime "BatasWaktu"
    t.datetime "TglWaktuMasuk"
    t.datetime "TglWaktuKeluar"
    t.string "KA1", limit: 5
    t.string "KA2", limit: 5
    t.string "KA3", limit: 5
    t.integer "xTimes"
    t.integer "xVoucher"
    t.string "Opr", limit: 3
    t.boolean "Nerus", null: false
  end

  create_table "TekananBed", id: false, force: :cascade do |t|
    t.char "KdRuangan", limit: 3, null: false
    t.char "KdKamar", limit: 4, null: false
    t.char "Keterangan", limit: 500, null: false
    t.char "StatusTekanan", limit: 1, null: false
    t.datetime "CreateDate"
    t.datetime "CreateUpdate"
    t.varchar "PIN", limit: 10
  end

  create_table "TempDetailApotikJual", id: false, force: :cascade do |t|
    t.char "NoTemporary", limit: 5, null: false
    t.varchar "KdBarang", limit: 9, null: false
    t.char "KdAsal", limit: 2, null: false
    t.money "HargaSatuan", precision: 19, scale: 4, null: false
    t.money "JmlHutangPenjamin", precision: 19, scale: 4, null: false
    t.money "JmlTanggunganRS", precision: 19, scale: 4, null: false
    t.varchar "KdPaket", limit: 3
  end

  create_table "TempDetailApotikJualStok", id: false, force: :cascade do |t|
    t.char "NoTemporary", limit: 5, null: false
    t.varchar "KdBarang", limit: 9, null: false
    t.char "KdAsal", limit: 2, null: false
    t.money "HargaSatuan", precision: 19, scale: 4, null: false
    t.money "JmlHutangPenjamin", precision: 19, scale: 4, null: false
    t.money "JmlTanggunganRS", precision: 19, scale: 4, null: false
    t.varchar "KdPaket", limit: 3
    t.char "KdRuangan", limit: 3, null: false
    t.integer "JmlBarang", null: false
  end

  create_table "TempHargaKomponen", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.datetime "TglPelayanan", null: false
    t.char "KdPelayananRS", limit: 6, null: false
    t.char "KdKelas", limit: 2, null: false
    t.char "KdKomponen", limit: 2, null: false
    t.char "KdJenisTarif", limit: 2, null: false
    t.money "Harga", precision: 19, scale: 4, null: false
    t.integer "JmlPelayanan", null: false
    t.char "NoStruk", limit: 10
    t.char "IdPegawai", limit: 10
    t.money "JmlHutangPenjamin", precision: 19, scale: 4, null: false
    t.money "JmlTanggunganRS", precision: 19, scale: 4, null: false
    t.money "JmlPembebasan", precision: 19, scale: 4, null: false
    t.char "NoClosing", limit: 10
    t.varchar "KdPaket", limit: 3
    t.char "KdSubInstalasi", limit: 3, null: false
    t.char "NoLab_Rad", limit: 10
    t.char "NoBKM", limit: 10
    t.integer "HariTglPelayanan"
    t.integer "BlnTglPelayanan"
    t.integer "ThnTglPelayanan"
    t.money "JmlHarusDiBayar", precision: 19, scale: 4
    t.char "KdRuanganAsal", limit: 3
  end

  create_table "TempHargaKomponenObatAlkes", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.datetime "TglPelayanan", null: false
    t.varchar "KdBarang", limit: 9, null: false
    t.char "KdAsal", limit: 2, null: false
    t.char "SatuanJml", limit: 1, null: false
    t.char "KdKomponen", limit: 2, null: false
    t.money "HargaSatuan", precision: 19, scale: 4, null: false
    t.decimal "JmlBarang", precision: 9, scale: 2, null: false
    t.char "NoStruk", limit: 10
    t.char "KdJenisObat", limit: 2
    t.varchar "NoResep", limit: 15
    t.money "JmlHutangPenjamin", precision: 19, scale: 4, null: false
    t.money "JmlTanggunganRS", precision: 19, scale: 4, null: false
    t.money "JmlPembebasan", precision: 19, scale: 4, null: false
    t.char "NoClosing", limit: 10
    t.char "KdKelas", limit: 2
    t.char "KdSubInstalasi", limit: 3, null: false
    t.varchar "KdPaket", limit: 3
    t.char "NoLab_Rad", limit: 10
    t.char "NoBKM", limit: 10
    t.integer "HariTglPelayanan"
    t.integer "BlnTglPelayanan"
    t.integer "ThnTglPelayanan"
    t.money "JmlHarusDiBayar", precision: 19, scale: 4
    t.char "KdRuanganAsal", limit: 3
    t.char "NoTerima", limit: 10, null: false
    t.integer "StatusTanggung", limit: 1
  end

  create_table "TempTransaksi", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "KdPelayananRS", limit: 10, null: false
    t.money "Tarif", precision: 19, scale: 4, null: false
    t.integer "JmlPelayanan", null: false
    t.datetime "TglPelayanan", null: false
    t.varchar "IdPegawai", limit: 50, null: false
    t.char "NoStruk", limit: 10
    t.money "JmlHutangPenjamin", precision: 19, scale: 4, null: false
    t.money "JmlTanggunganRS", precision: 19, scale: 4, null: false
    t.money "JmlPembebasan", precision: 19, scale: 4, null: false
    t.money "JmlHarusDibayar", precision: 19, scale: 4, null: false
    t.integer "jenis_ID"
  end

  create_table "TempTransaksiOld", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "KdRuangan", limit: 3, null: false
    t.char "KdPelayananRS", limit: 10, null: false
    t.money "Tarif", precision: 19, scale: 4, null: false
    t.integer "JmlPelayanan", null: false
    t.datetime "TglPelayanan", null: false
    t.varchar "IdPegawai", limit: 50, null: false
    t.char "NoStruk", limit: 10
    t.money "JmlHutangPenjamin", precision: 19, scale: 4, null: false
    t.money "JmlTanggunganRS", precision: 19, scale: 4, null: false
    t.money "JmlPembebasan", precision: 19, scale: 4, null: false
    t.money "JmlHarusDibayar", precision: 19, scale: 4, null: false
  end

  create_table "Temp_PeriksaDiagnosa", id: false, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 50
    t.varchar "NoCm", limit: 50
    t.varchar "KdDiagnosa", limit: 50
    t.varchar_max "NamaDiagnosa"
    t.varchar "KdSubInstalasi", limit: 50
    t.varchar "KdRuangan", limit: 50
    t.varchar "IdDokter", limit: 50
    t.datetime "CreateDate"
    t.varchar "KdJenisDiagnosa", limit: 50
    t.varchar "IdUser", limit: 50
    t.varchar "StatusVerif", limit: 50
    t.varchar_max "KeteranganDiagnosa"
  end

  create_table "Temp_PeriksaDiagnosaKeperawatan", id: false, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 50
    t.varchar "NoCm", limit: 50
    t.varchar "KdDiagnosa", limit: 50
    t.varchar_max "NamaDiagnosa"
    t.varchar "KdSubInstalasi", limit: 50
    t.varchar "KdRuangan", limit: 50
    t.varchar "IdDokter", limit: 50
    t.datetime "CreateDate"
    t.varchar "KdJenisDiagnosa", limit: 50
    t.varchar "IdUser", limit: 50
    t.varchar "StatusVerif", limit: 50
    t.varchar_max "KeteranganDiagnosa"
  end

  create_table "Temp_PeriksaDiagnosaKlaim", id: false, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 50
    t.varchar "NoCm", limit: 50
    t.varchar "KdDiagnosa", limit: 50
    t.varchar_max "NamaDiagnosa"
    t.varchar "KdSubInstalasi", limit: 50
    t.varchar "KdRuangan", limit: 50
    t.varchar "IdDokter", limit: 50
    t.datetime "CreateDate"
    t.varchar "KdJenisDiagnosa", limit: 50
    t.varchar "IdUser", limit: 50
    t.varchar "StatusVerif", limit: 50
  end

  create_table "Temp_PeriksaDiagnosaKlaimTindakan", id: false, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 50
    t.varchar "NoCm", limit: 50
    t.varchar "KdDiagnosa", limit: 50
    t.varchar_max "NamaDiagnosa"
    t.varchar "KdSubInstalasi", limit: 50
    t.varchar "KdRuangan", limit: 50
    t.varchar "IdDokter", limit: 50
    t.datetime "CreateDate"
    t.varchar "KdJenisDiagnosa", limit: 50
    t.varchar "IdUser", limit: 50
    t.varchar "StatusVerif", limit: 50
  end

  create_table "Temp_PeriksaDiagnosaTindakan", id: false, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 50
    t.varchar "NoCm", limit: 50
    t.varchar "KdDiagnosa", limit: 50
    t.varchar_max "NamaDiagnosa"
    t.varchar "KdSubInstalasi", limit: 50
    t.varchar "KdRuangan", limit: 50
    t.varchar "IdDokter", limit: 50
    t.datetime "CreateDate"
    t.varchar "KdJenisDiagnosa", limit: 50
    t.varchar "IdUser", limit: 50
    t.varchar "StatusVerif", limit: 50
  end

  create_table "Temp_TindakanDokter", id: false, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 50
    t.varchar "NoCM", limit: 50
    t.varchar_max "JenisPelayanan"
    t.varchar_max "NamaPelayanan"
    t.varchar "KdSubInstalasi", limit: 50
    t.varchar "KdRuangan", limit: 10
    t.varchar "KdKelas", limit: 50
    t.nchar "StatusAPBD", limit: 10
    t.varchar "KdJenisTarif", limit: 50
    t.varchar "TarifCito", limit: 50
    t.varchar "StatusCITO", limit: 50
    t.varchar "KdPelayananRS", limit: 50
    t.varchar "Tarif", limit: 50
    t.datetime "CreateDate"
    t.varchar "IdUser", limit: 50
    t.varchar "JmlPelayanan", limit: 50
    t.varchar "StatusVerif", limit: 5
    t.varchar "StatusSP", limit: 50
    t.varchar_max "KeteranganTindakan"
    t.datetime "UpdateTanggal"
  end

  create_table "Temp_TindakanDokterNew", primary_key: "ID_Tindakan", id: :integer, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 50
    t.varchar "NoCM", limit: 50
    t.varchar_max "JenisPelayanan"
    t.varchar_max "NamaPelayanan"
    t.varchar "KdSubInstalasi", limit: 50
    t.varchar "KdRuangan", limit: 10
    t.varchar "KdKelas", limit: 50
    t.nchar "StatusAPBD", limit: 10
    t.varchar "KdJenisTarif", limit: 50
    t.varchar "TarifCito", limit: 50
    t.varchar "StatusCITO", limit: 50
    t.varchar "KdPelayananRS", limit: 50
    t.varchar "Tarif", limit: 50
    t.datetime "CreateDate"
    t.varchar "IdUser", limit: 50
    t.varchar "JmlPelayanan", limit: 50
    t.varchar "StatusVerif", limit: 5
    t.varchar "StatusSP", limit: 50
    t.varchar_max "KeteranganTindakan"
    t.datetime "UpdateTanggal"
    t.index ["ID_Tindakan", "NoPendaftaran"], name: "ClusteredIndex-20230130-130742"
    t.index ["ID_Tindakan", "NoPendaftaran"], name: "NonClusteredIndex-20230130-130826"
  end

  create_table "Temp_TindakanDokter_old", id: false, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 50
    t.varchar "NoCM", limit: 50
    t.varchar_max "JenisPelayanan"
    t.varchar_max "NamaPelayanan"
    t.varchar "KdSubInstalasi", limit: 50
    t.varchar "KdRuangan", limit: 10
    t.varchar "KdKelas", limit: 50
    t.nchar "StatusAPBD", limit: 10
    t.varchar "KdJenisTarif", limit: 50
    t.varchar "TarifCito", limit: 50
    t.varchar "StatusCITO", limit: 50
    t.varchar "KdPelayananRS", limit: 50
    t.varchar "Tarif", limit: 50
    t.datetime "CreateDate"
    t.varchar "IdUser", limit: 50
    t.varchar "JmlPelayanan", limit: 50
    t.varchar "StatusVerif", limit: 5
    t.varchar "StatusSP", limit: 50
    t.varchar_max "KeteranganTindakan"
  end

  create_table "Temp_TindakanLab", id: false, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 50
    t.varchar "NoLaboratorium", limit: 50
    t.varchar "NoCM", limit: 50
    t.varchar_max "JenisPelayanan"
    t.varchar_max "NamaPelayanan"
    t.varchar "KdSubInstalasi", limit: 50
    t.varchar "KdRuangan", limit: 10
    t.varchar "KdKelas", limit: 50
    t.nchar "StatusAPBD", limit: 10
    t.varchar "KdJenisTarif", limit: 50
    t.varchar "TarifCito", limit: 50
    t.varchar "StatusCITO", limit: 50
    t.varchar "KdPelayananRS", limit: 50
    t.varchar "Tarif", limit: 50
    t.datetime "CreateDate"
    t.varchar "IdDokter", limit: 50
    t.varchar "IdUser", limit: 50
    t.varchar "JmlPelayanan", limit: 50
    t.varchar "StatusVerif", limit: 5
    t.varchar "StatusSP", limit: 50
    t.varchar_max "KeteranganTindakan"
  end

  create_table "Temp_TindakanLabNew", primary_key: "ID_Tindakan", id: :integer, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 50
    t.varchar "NoLaboratorium", limit: 50
    t.varchar "NoCM", limit: 50
    t.varchar_max "JenisPelayanan"
    t.varchar_max "NamaPelayanan"
    t.varchar "KdSubInstalasi", limit: 50
    t.varchar "KdRuangan", limit: 10
    t.varchar "KdKelas", limit: 50
    t.nchar "StatusAPBD", limit: 10
    t.varchar "KdJenisTarif", limit: 50
    t.varchar "TarifCito", limit: 50
    t.varchar "StatusCITO", limit: 50
    t.varchar "KdPelayananRS", limit: 50
    t.varchar "Tarif", limit: 50
    t.datetime "CreateDate"
    t.varchar "IdDokter", limit: 50
    t.varchar "IdUser", limit: 50
    t.varchar "JmlPelayanan", limit: 50
    t.varchar "StatusVerif", limit: 5
    t.varchar "StatusSP", limit: 50
    t.varchar_max "KeteranganTindakan"
  end

  create_table "Temp_TindakanRad", id: false, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 50
    t.varchar "NoRadiologi", limit: 50
    t.varchar "NoCM", limit: 50
    t.varchar_max "JenisPelayanan"
    t.varchar_max "NamaPelayanan"
    t.varchar "KdSubInstalasi", limit: 50
    t.varchar "KdRuangan", limit: 10
    t.varchar "KdKelas", limit: 50
    t.nchar "StatusAPBD", limit: 10
    t.varchar "KdJenisTarif", limit: 50
    t.varchar "TarifCito", limit: 50
    t.varchar "StatusCITO", limit: 50
    t.varchar "KdPelayananRS", limit: 50
    t.varchar "Tarif", limit: 50
    t.datetime "CreateDate"
    t.varchar "IdDokter", limit: 50
    t.varchar "IdUser", limit: 50
    t.varchar "JmlPelayanan", limit: 50
    t.varchar "StatusVerif", limit: 5
    t.varchar "StatusSP", limit: 50
    t.varchar_max "KeteranganTindakan"
  end

  create_table "Temp_Tindakandokter_Backup", id: false, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 50
    t.varchar "NoCM", limit: 50
    t.varchar_max "JenisPelayanan"
    t.varchar_max "NamaPelayanan"
    t.varchar "KdSubInstalasi", limit: 50
    t.varchar "KdRuangan", limit: 10
    t.varchar "KdKelas", limit: 50
    t.nchar "StatusAPBD", limit: 10
    t.varchar "KdJenisTarif", limit: 50
    t.varchar "TarifCito", limit: 50
    t.varchar "StatusCITO", limit: 50
    t.varchar "KdPelayananRS", limit: 50
    t.varchar "Tarif", limit: 50
    t.datetime "CreateDate"
    t.varchar "IdUser", limit: 50
    t.varchar "JmlPelayanan", limit: 50
    t.varchar "StatusVerif", limit: 5
    t.varchar "StatusSP", limit: 50
    t.varchar_max "KeteranganTindakan"
  end

  create_table "Temp_resep", id: false, force: :cascade do |t|
    t.varchar "KdBarang", limit: 50
    t.varchar "NoPendaftaran", limit: 50
    t.varchar "NoCm", limit: 10
    t.varchar "JmlBarang", limit: 50
    t.varchar "StatusJmlBarang", limit: 2
    t.varchar "Keterangan", limit: 250
    t.varchar "AturanPakai", limit: 500
    t.datetime "CreateDate"
    t.varchar "IdPegawai", limit: 50
    t.varchar_max "NamaBarang"
    t.varchar "JenisBarang", limit: 50
    t.varchar "StatusVerif", limit: 50
    t.varchar "JmlStok", limit: 50
    t.varchar "JenisObat", limit: 2
    t.varchar "PemberianObat", limit: 2
    t.varchar "KdRuangan", limit: 3
    t.money "Harga", precision: 19, scale: 4
    t.varchar_max "keteranganracik"
    t.varchar_max "dosis"
    t.varchar "KeteranganLain", limit: 500
    t.varchar "obatpulang", limit: 50
  end

  create_table "Temp_resepNew", primary_key: "ID_Resep", id: :integer, force: :cascade do |t|
    t.varchar "KdBarang", limit: 50
    t.varchar "NoPendaftaran", limit: 50
    t.varchar "NoCm", limit: 10
    t.varchar "JmlBarang", limit: 50
    t.varchar "StatusJmlBarang", limit: 2
    t.varchar "Keterangan", limit: 250
    t.varchar "AturanPakai", limit: 500
    t.datetime "CreateDate"
    t.varchar "IdPegawai", limit: 50
    t.varchar_max "NamaBarang"
    t.varchar "JenisBarang", limit: 50
    t.varchar "StatusVerif", limit: 50
    t.varchar "JmlStok", limit: 50
    t.varchar "JenisObat", limit: 2
    t.varchar "PemberianObat", limit: 2
    t.varchar "KdRuangan", limit: 3
    t.money "Harga", precision: 19, scale: 4
    t.varchar_max "keteranganracik"
    t.varchar_max "dosis"
    t.varchar "KeteranganLain", limit: 500
    t.varchar "obatpulang", limit: 50
    t.index ["ID_Resep", "KdBarang", "NoPendaftaran", "NoCm", "JmlBarang", "StatusJmlBarang", "IdPegawai", "JmlStok", "JenisObat", "KdRuangan"], name: "ClusteredIndexTemp_resepNew-20230127-185455"
    t.index ["ID_Resep", "KdBarang", "NoPendaftaran"], name: "NonClusteredIndex-20230130-130612"
  end

  create_table "Temp_resepNew_backup", primary_key: "ID_Resep", id: :integer, force: :cascade do |t|
    t.varchar "KdBarang", limit: 50
    t.varchar "NoPendaftaran", limit: 50
    t.varchar "NoCm", limit: 10
    t.varchar "JmlBarang", limit: 50
    t.varchar "StatusJmlBarang", limit: 2
    t.varchar "Keterangan", limit: 250
    t.varchar "AturanPakai", limit: 500
    t.datetime "CreateDate"
    t.varchar "IdPegawai", limit: 50
    t.varchar_max "NamaBarang"
    t.varchar "JenisBarang", limit: 50
    t.varchar "StatusVerif", limit: 50
    t.varchar "JmlStok", limit: 50
    t.varchar "JenisObat", limit: 2
    t.varchar "PemberianObat", limit: 2
    t.varchar "KdRuangan", limit: 3
    t.money "Harga", precision: 19, scale: 4
    t.varchar_max "keteranganracik"
    t.varchar_max "dosis"
    t.varchar "KeteranganLain", limit: 500
    t.varchar "obatpulang", limit: 50
  end

  create_table "TerapeutikKeperawatan", id: false, force: :cascade do |t|
    t.varchar "KdDiagnosa", limit: 7, null: false
    t.varchar "SubIntervensi", limit: 255
    t.varchar "KdTerapeutik", limit: 7, null: false
    t.varchar "NamaTerapeutik", limit: 1000, null: false
    t.varchar "NamaExternal", limit: 500
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "TindakanKeperawatan", id: false, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 50
    t.varchar "NoCM", limit: 50
    t.varchar_max "TanggalKeperawatan"
    t.varchar_max "Tindakankeperawatan"
    t.varchar_max "NamaJelas"
    t.datetime "CreateDate"
  end

  create_table "TindakanPatologi", id: false, force: :cascade do |t|
    t.char "NoLab", limit: 10, null: false
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCm", limit: 8
    t.char "KdRuangan", limit: 3, null: false
    t.varchar "DiagnosaKlinis", limit: 500
    t.varchar "DiagnosaPB", limit: 500
    t.datetime "TglMulai"
    t.datetime "TglSelesai"
    t.text_basic "KeteranganKlinik"
    t.text_basic "KeteranganPB"
    t.text_basic "Makroskopik"
    t.text_basic "Mikroskopik"
    t.text_basic "Kesimpulan"
    t.text_basic "Anjuran"
    t.text_basic "Topologi"
    t.char "Iduser", limit: 10
    t.char "No_Lab_New", limit: 20
    t.text_basic "Lokasi"
    t.char "Id_Dokter", limit: 10
  end

  create_table "TindakanPemeriksaanPHP", id: :integer, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 50
    t.varchar "NoCM", limit: 50
    t.varchar "KdRuangan", limit: 50
    t.varchar_max "interna"
    t.varchar_max "ditemukan"
    t.datetime "CreateDate"
    t.datetime "CreateUpdate"
    t.varchar "IdDokter", limit: 50
    t.varchar "NamaDokter", limit: 150
  end

  create_table "Tindakan_Keperawatan_Ranap", primary_key: "IdTindakan", id: :integer, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10
    t.char "NoCM", limit: 8
    t.char "KdRuangan", limit: 3
    t.char "IdDokter", limit: 10
    t.varchar_max "TindakanKeperawatan"
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
    t.char "StatusVerif", limit: 1
  end

  create_table "Title", id: false, force: :cascade do |t|
    t.varchar "KdTitle", limit: 2, null: false
    t.varchar "NamaTitle", limit: 4, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 20
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "TotalBiayaPelayananOA", primary_key: "NoStruk", id: { type: :char, limit: 10 }, force: :cascade do |t|
    t.money "TotalBiaya", precision: 19, scale: 4, null: false
    t.money "JmlHutangPenjamin", precision: 19, scale: 4, null: false
    t.money "JmlTanggunganRS", precision: 19, scale: 4, null: false
    t.money "JmlPembebasan", precision: 19, scale: 4, null: false
    t.money "JmlHarusDibayar", precision: 19, scale: 4, null: false
    t.money "JmlDiscount", precision: 19, scale: 4, null: false
    t.money "JmlSudahDibayar", precision: 19, scale: 4, null: false
    t.money "SisaTagihan", precision: 19, scale: 4, null: false
    t.money "SisaHutangPenjamin", precision: 19, scale: 4, null: false
  end

  create_table "TotalBiayaPelayananOAHistory", primary_key: "NoStruk", id: { type: :char, limit: 10 }, force: :cascade do |t|
    t.money "TotalBiaya", precision: 19, scale: 4, null: false
    t.money "JmlHutangPenjamin", precision: 19, scale: 4, null: false
    t.money "JmlTanggunganRS", precision: 19, scale: 4, null: false
    t.money "JmlPembebasan", precision: 19, scale: 4, null: false
    t.money "JmlHarusDibayar", precision: 19, scale: 4, null: false
    t.money "JmlDiscount", precision: 19, scale: 4, null: false
    t.money "JmlSudahDibayar", precision: 19, scale: 4, null: false
    t.money "SisaTagihan", precision: 19, scale: 4, null: false
  end

  create_table "TotalBiayaPelayananTM", primary_key: "NoStruk", id: { type: :char, limit: 10 }, force: :cascade do |t|
    t.money "TotalBiaya", precision: 19, scale: 4, null: false
    t.money "JmlHutangPenjamin", precision: 19, scale: 4, null: false
    t.money "JmlTanggunganRS", precision: 19, scale: 4, null: false
    t.money "JmlPembebasan", precision: 19, scale: 4, null: false
    t.money "JmlHarusDibayar", precision: 19, scale: 4, null: false
    t.money "JmlDiscount", precision: 19, scale: 4, null: false
    t.money "JmlSudahDibayar", precision: 19, scale: 4, null: false
    t.money "SisaTagihan", precision: 19, scale: 4, null: false
    t.money "SisaHutangPenjamin", precision: 19, scale: 4, null: false
  end

  create_table "TotalBiayaPelayananTMHistory", primary_key: "NoStruk", id: { type: :char, limit: 10 }, force: :cascade do |t|
    t.money "TotalBiaya", precision: 19, scale: 4, null: false
    t.money "JmlHutangPenjamin", precision: 19, scale: 4, null: false
    t.money "JmlTanggunganRS", precision: 19, scale: 4, null: false
    t.money "JmlPembebasan", precision: 19, scale: 4, null: false
    t.money "JmlHarusDibayar", precision: 19, scale: 4, null: false
    t.money "JmlDiscount", precision: 19, scale: 4, null: false
    t.money "JmlSudahDibayar", precision: 19, scale: 4, null: false
    t.money "SisaTagihan", precision: 19, scale: 4, null: false
  end

  create_table "Triage_TP", id: false, force: :cascade do |t|
    t.varchar "KdTriage", limit: 50
    t.varchar "NamaTriage", limit: 50
    t.integer "Enable"
  end

  create_table "Triase", primary_key: "KdTriase", id: { type: :char, limit: 2 }, force: :cascade do |t|
    t.varchar "NamaTriase", limit: 50, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "TriasePasienIGD", primary_key: "IdTriaseIGD", id: :integer, force: :cascade do |t|
    t.char "NoCM", limit: 10
    t.char "NoPendaftaran", limit: 10
    t.char "CaraDatang", limit: 8
    t.char "Konfirmasi", limit: 10
    t.char "Rujukan", limit: 5
    t.varchar "AsalRujukan", limit: 50
    t.varchar "PengantarPasien", limit: 50
    t.varchar "ResponPasien", limit: 50
    t.datetime "CreateDate"
    t.char "IdUser", limit: 10
  end

  create_table "TypePegawai", id: false, force: :cascade do |t|
    t.char "KdTypePegawai", limit: 2, null: false
    t.varchar "TypePegawai", limit: 30, null: false
    t.varchar "ReportDisplay", limit: 30
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 30
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "UANGSHIFTRUANGAN", id: false, force: :cascade do |t|
    t.varchar "PIN", limit: 4
    t.varchar "NOREK", limit: 20
    t.varchar "PERIODE", limit: 20
    t.integer "S7L"
    t.integer "S5L"
    t.integer "S4L"
    t.integer "S3L"
    t.integer "S2L"
    t.integer "S1L"
    t.integer "S7"
    t.integer "S5"
    t.integer "S4"
    t.integer "S3"
    t.integer "S2"
    t.integer "S7HB"
    t.integer "S5HB"
    t.integer "S4HB"
    t.integer "S3HB"
    t.integer "S2HB"
    t.integer "S1HB"
    t.float "PSMLIBUR"
    t.float "SMLIBUR"
    t.float "PSLIBUR"
    t.float "MALAMLIBUR"
    t.float "SORELIBUR"
    t.float "PAGILIBUR"
    t.float "PAGISOREMALAM"
    t.float "SOREMALAM"
    t.float "PAGISORE"
    t.float "MALAM"
    t.float "SORE"
    t.float "PSMHARIRAYA"
    t.float "SMHARIRAYA"
    t.float "PSHARIRAYA"
    t.float "MALAMHARIRAYA"
    t.float "SOREHARIRAYA"
    t.float "PAGIHARIRAYA"
    t.float "TOTALSHIFT"
    t.datetime "TGLAJUAN"
    t.datetime "TGLVALIDASI"
    t.varchar "STATUSVALIDASI", limit: 200
  end

  create_table "UpdateDokterIGD", primary_key: "nourut", id: :integer, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.datetime "TglPelayanan", null: false
    t.char "IdPegawai", limit: 10
    t.char "IdUser", limit: 10, null: false
    t.varchar "apliaksi", limit: 10
  end

  create_table "Verifikasi", id: false, force: :cascade do |t|
    t.smalldatetime "TglRequestVerifikasi", null: false
    t.char "NomorJenisVerifikasi", limit: 10, null: false
    t.integer "KdJenisVerifikasi", limit: 1, null: false
    t.varchar "NoDokumenVerifikasi", limit: 50
    t.char "IdPegawai1", limit: 10
    t.char "IdPegawai2", limit: 10
    t.char "IdPegawai3", limit: 10
    t.char "IdPegawai4", limit: 10
    t.char "NoVerifikasi", limit: 10
    t.smalldatetime "TglACCVerifikasi"
  end

  create_table "VerifikasiJkn", id: :integer, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 20
    t.varchar "NoCM", limit: 20
    t.datetime "CreateDate"
    t.varchar "IdPegawai", limit: 14
    t.varchar "StatusGrouper", limit: 2
    t.datetime "CreateDateGrouper"
    t.varchar "StatusGrouperVerifikator", limit: 2
    t.datetime "CreateDateGrouperVerifikator"
  end

  create_table "VerifikasiMobilisasiDana", id: :integer, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 20
    t.varchar "NoCM", limit: 20
    t.datetime "CreateDate"
    t.varchar "IdPegawai", limit: 14
  end

  create_table "WaktuEtiketResep", primary_key: "KdWaktuEtiket", id: { type: :char, limit: 2 }, force: :cascade do |t|
    t.varchar "WaktuEtiket", limit: 30, null: false
    t.varchar "Singkatan", limit: 15
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 30
    t.integer "StatusEnabled", limit: 1
  end

  create_table "WaktuEtiketResep2", primary_key: "KdWaktuEtiket2", id: { type: :char, limit: 2 }, force: :cascade do |t|
    t.varchar "WaktuEtiket2", limit: 30, null: false
    t.varchar "Singkatan2", limit: 15
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 30
    t.integer "StatusEnabled", limit: 1
  end

  create_table "WaktuInputBkbu", id: false, force: :cascade do |t|
    t.bigint "IdWaktu", null: false
    t.datetime "TglMulai"
    t.datetime "TglAkhir"
    t.datetime "TglBuat"
    t.integer "StatusEnabled", limit: 1
    t.char "IdPegawai", limit: 50
    t.integer "JenisDpa", limit: 1
    t.varchar "Tahun", limit: 50
  end

  create_table "WaktuPelayanan", id: false, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 20
    t.varchar "NoCM", limit: 20
    t.nchar "KdRuangan", limit: 10
    t.datetime "TglMulai"
    t.datetime "TglSelesai"
    t.varchar "IdUser", limit: 20
    t.datetime "CreateDate"
  end

  create_table "WarnaPartograf", id: false, force: :cascade do |t|
    t.char "KdWarna", limit: 2
    t.varchar "KdKontraksi", limit: 20
    t.varchar "Background", limit: 100
    t.varchar "Border", limit: 100
    t.integer "StatusEnabled", limit: 1
  end

  create_table "ZatAktif", id: false, force: :cascade do |t|
    t.char "IdZatAktif", limit: 10
    t.varchar_max "ZatAktif"
    t.integer "StatusEnabled", limit: 1
  end

  create_table "antreanFarmasi", primary_key: "idAntrian", id: :integer, force: :cascade do |t|
    t.varchar "nopendaftaran", limit: 15
    t.varchar "nocm", limit: 15
    t.varchar "angkaAntrean", limit: 15
    t.varchar "nomorAntrean", limit: 15
    t.varchar "statusAntrean", limit: 5
    t.datetime "modifiedDate"
  end

  create_table "antreanFarmasi2", primary_key: "idAntrian", id: :integer, force: :cascade do |t|
    t.varchar "nopendaftaran", limit: 15
    t.varchar "nocm", limit: 15
    t.varchar "angkaAntrean", limit: 15
    t.varchar "nomorAntrean", limit: 15
    t.varchar "statusAntrean", limit: 5
    t.datetime "MulaiDate"
    t.datetime "SelesaiDate"
  end

  create_table "appointmentJKN", force: :cascade do |t|
    t.varchar_max "noRM"
    t.datetime "bookingDate"
    t.varchar_max "poliClinicID"
    t.varchar_max "doctorID"
    t.varchar_max "paymentID"
    t.varchar_max "bookingCode"
    t.varchar_max "queueNumber"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.varchar_max "cancel"
    t.varchar_max "referrralNumber"
    t.varchar_max "bpjsNumber"
    t.varchar_max "startTime"
    t.varchar_max "endTime"
    t.integer "statusID"
    t.integer "waitingNumber"
  end

  create_table "appointments", force: :cascade do |t|
    t.string "noRM"
    t.datetime "bookingDate"
    t.string "policlinicID"
    t.string "doctorID"
    t.string "paymentID"
    t.string "bookingCode"
    t.string "queueNumber"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "cancel"
    t.string "referrralNumber"
    t.string "bpjsNumber"
    t.string "startTime"
    t.string "endTime"
    t.integer "statusID"
    t.integer "waitingNumber"
  end

  create_table "appointments2", force: :cascade do |t|
    t.string "noRM"
    t.datetime "bookingDate"
    t.string "policlinicID"
    t.string "doctorID"
    t.string "paymentID"
    t.string "bookingCode"
    t.string "queueNumber"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "cancel"
    t.string "referrralNumber"
    t.string "bpjsNumber"
    t.string "startTime"
    t.string "endTime"
    t.integer "statusID"
    t.integer "waitingNumber"
  end

  create_table "appointments_JKN", id: :integer, force: :cascade do |t|
    t.varchar_max "noRM"
    t.datetime "bookingDate"
    t.varchar_max "poliClinicID"
    t.varchar_max "doctorID"
    t.varchar_max "paymentID"
    t.varchar_max "bookingCode"
    t.varchar_max "queueNumber"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.varchar_max "cancel"
    t.varchar_max "referrralNumber"
    t.varchar_max "bpjsNumber"
    t.varchar_max "startTime"
    t.varchar_max "endTime"
    t.integer "statusID"
    t.integer "waitingNumber"
  end

  create_table "appuserlog_elab", primary_key: "indeks", id: :integer, force: :cascade do |t|
    t.datetime "createdate"
    t.varchar "ipaddress", limit: 200
    t.varchar "hostname", limit: 200
    t.varchar "pin", limit: 100
    t.varchar "appsessions", limit: 200
    t.varchar "appid", limit: 10
    t.varchar "appname", limit: 200
    t.varchar "appmodule", limit: 200
    t.varchar "appeventname", limit: 200
    t.datetime "appeventtime"
    t.varchar "appnotifikasi", limit: 200
    t.varchar "deskripsi", limit: 2000
  end

  create_table "backupDetailOrderPelayananOANew15022023", id: false, force: :cascade do |t|
    t.char "NoOrder", limit: 10, null: false
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
    t.char "NoPakai", limit: 10
    t.char "IdDokterOrder", limit: 10
    t.char "StatusCito", limit: 1, null: false
    t.varchar "KdBarang", limit: 9, null: false
    t.real "JmlBarang", null: false
    t.varchar "NoResep", limit: 15, null: false
    t.integer "ResepKe", limit: 1, null: false
    t.varchar "AturanPakai", limit: 500
    t.varchar "KeteranganPakai", limit: 50
    t.datetime "TglResep"
    t.char "NoRiwayat", limit: 10
    t.char "NoRetur", limit: 10
    t.integer "JmlRetur", null: false
    t.char "KdPelayananRSUsed", limit: 6
    t.varchar "KeteranganLainnya", limit: 200
    t.char "KdJenisObat", limit: 2, null: false
    t.varchar "KeteranganLainnya2", limit: 200
    t.varchar "StatusOrder", limit: 20
    t.money "harga", precision: 19, scale: 4
    t.varchar_max "keteranganracik"
    t.varchar_max "dosis"
    t.varchar "obatpulang", limit: 50
    t.integer "ID_Resep"
  end

  create_table "backupTemp_ResepNew15022023", primary_key: "ID_Resep", id: :integer, force: :cascade do |t|
    t.varchar "KdBarang", limit: 50
    t.varchar "NoPendaftaran", limit: 50
    t.varchar "NoCm", limit: 10
    t.varchar "JmlBarang", limit: 50
    t.varchar "StatusJmlBarang", limit: 2
    t.varchar "Keterangan", limit: 250
    t.varchar "AturanPakai", limit: 500
    t.datetime "CreateDate"
    t.varchar "IdPegawai", limit: 50
    t.varchar_max "NamaBarang"
    t.varchar "JenisBarang", limit: 50
    t.varchar "StatusVerif", limit: 50
    t.varchar "JmlStok", limit: 50
    t.varchar "JenisObat", limit: 2
    t.varchar "PemberianObat", limit: 2
    t.varchar "KdRuangan", limit: 3
    t.money "Harga", precision: 19, scale: 4
    t.varchar_max "keteranganracik"
    t.varchar_max "dosis"
    t.varchar "KeteranganLain", limit: 500
    t.varchar "obatpulang", limit: 50
  end

  create_table "barcode_gagah", primary_key: "Id", id: :integer, default: nil, force: :cascade do |t|
    t.char "IdPegawai", limit: 10, null: false
    t.varchar "filename", limit: 255
    t.varchar "filepath", limit: 255
  end

  create_table "bkbu", primary_key: "Kd", id: :integer, force: :cascade do |t|
    t.integer "Kode"
    t.varchar "KodeBarang", limit: 50
    t.integer "VolumeKoef1"
    t.integer "VolumeKoef2"
    t.integer "VolumeSatuan"
    t.money "Harga", precision: 19, scale: 4
    t.money "HargaPPN", precision: 19, scale: 4
    t.money "Netto", precision: 19, scale: 4
    t.varchar "Tahun", limit: 50
    t.integer "StatusEnabled", limit: 1
    t.char "KdRekening", limit: 10
    t.char "IdPegawai", limit: 10
    t.datetime "TglInput"
    t.datetime "TglHapus"
    t.char "KdRuangan", limit: 10
    t.char "IdKsp", limit: 10
    t.datetime "TglVerifKsp"
    t.integer "VolumeKsp"
    t.char "IdKabag", limit: 10
    t.datetime "TglVerifKabag"
    t.integer "VolumeKabag"
    t.varchar_max "Ket"
    t.integer "KdSatpel", limit: 1
    t.integer "JenisDpa", limit: 1
    t.varchar_max "KetTolakKsp"
    t.varchar_max "KetTolakKabag"
    t.integer "VolumeUtang"
  end

  create_table "data_claim", id: false, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 50
    t.varchar "NoSEP", limit: 50
    t.varchar "NoKartu", limit: 50
    t.datetime "TglMasuk"
    t.datetime "TglPulang"
    t.varchar_max "Brijing"
    t.datetime "CreateDate"
  end

  create_table "dokters", force: :cascade do |t|
    t.string "idPegawai"
    t.string "kdJnsPegawai"
    t.string "kdTitle"
    t.string "namaLengkap"
    t.string "namaKeluarga"
    t.string "namaPanggilan"
    t.string "jenisKelamin"
    t.string "tempatLahir"
    t.date "tglLahir"
    t.date "tglMasuk"
    t.date "tglKeluar"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "formulirSurveilansHaiPPI", id: false, force: :cascade do |t|
    t.datetime "TglPelayanan", null: false
    t.char "JmlPasien", limit: 3
    t.char "ETT", limit: 3
    t.char "CVL", limit: 3
    t.char "IVL", limit: 3
    t.char "UC", limit: 3
    t.char "PostOprasi", limit: 3
    t.char "VAP", limit: 3
    t.char "Clabsi", limit: 3
    t.char "Phlebitis", limit: 3
    t.char "Cauti", limit: 3
    t.char "IDO", limit: 3
    t.char "KdRuangan", limit: 3
    t.char "Pin", limit: 4
    t.datetime "TglKom", null: false
    t.integer "HariTglPelayanan"
    t.integer "BlnTglPelayanan"
    t.integer "ThnTglPelayanan"
  end

  create_table "general_consent", id: false, force: :cascade do |t|
    t.varchar "NoCM", limit: 50
    t.varchar "keluarga1", limit: 50
    t.varchar "keluarga2", limit: 50
    t.varchar "keluarga3", limit: 50
    t.varchar "keluarga4", limit: 50
    t.varchar "keluarga5", limit: 50
    t.varchar "keluarga6", limit: 50
    t.varchar "privasi-nama", limit: 50
    t.varchar "privasi-profesi", limit: 50
    t.varchar "privasi-a", limit: 50
    t.varchar "privasi-b", limit: 50
    t.varchar "privasi-c", limit: 50
    t.varchar "privasi-lain", limit: 50
    t.datetime "CreateDate"
  end

  create_table "groupy", id: false, force: :cascade do |t|
    t.bigint "ID_GROUPY", null: false
    t.varchar "NAMA", limit: 50
    t.varchar "DESCRIPTION", limit: 100
    t.integer "StatusEnabled", limit: 1
  end

  create_table "his_delete", primary_key: "Id", id: :integer, force: :cascade do |t|
    t.datetime "CreateDate"
    t.varchar "IdUser", limit: 50
    t.varchar_max "NamaUser"
    t.varchar "KdRuangan", limit: 15
    t.varchar_max "Keterangan"
  end

  create_table "history_deleteEMR", id: false, force: :cascade do |t|
    t.varchar_max "NoPendaftaran"
    t.varchar_max "NoCM"
    t.varchar_max "KdDiagnosa"
    t.varchar_max "KdPelayananRS"
    t.varchar_max "IdPegawai"
    t.datetime "CreateDate"
  end

  create_table "ihs_pasien_condition", id: false, force: :cascade do |t|
    t.datetime "createdate"
    t.varchar "NoPendaftaran", limit: 15
    t.varchar "NoCM", limit: 15
    t.varchar "KdRuangan", limit: 3
    t.varchar "TglPeriksa", limit: 20
    t.varchar_max "IdPasien"
    t.varchar_max "NamaPasienIHS"
    t.varchar_max "id_encounter"
    t.varchar_max "id_condition"
    t.varchar_max "condition_rank"
    t.varchar_max "KdDiagnosa"
    t.varchar_max "NamaDiagnosa"
  end

  create_table "ihs_pasien_encounter", id: false, force: :cascade do |t|
    t.datetime "createdate"
    t.varchar "NoPendaftaran", limit: 15
    t.varchar "NoCM", limit: 15
    t.varchar "KdRuangan", limit: 3
    t.varchar "TglPendaftaran", limit: 20
    t.varchar_max "IdPasien"
    t.varchar_max "NamaPasienIHS"
    t.varchar_max "id_encounter"
    t.varchar_max "id_practitioner"
    t.varchar_max "NamaDokterIHS"
    t.varchar_max "id_location"
    t.varchar_max "NamaRuanganIHS"
  end

  create_table "ihs_pasien_fix", id: false, force: :cascade do |t|
    t.datetime "createdate"
    t.varchar "NoPendaftaran", limit: 15
    t.varchar "NoCM", limit: 15
    t.varchar "KdRuangan", limit: 3
    t.varchar_max "IdPasien"
    t.varchar_max "token"
    t.varchar_max "id_encounter"
    t.varchar_max "id_condition_primer"
    t.varchar_max "id_condition_sekunder"
    t.varchar_max "id_observation_nadi"
    t.varchar_max "id_observation_pernafasan"
    t.varchar_max "id_observation_sistol"
    t.varchar_max "id_observation_diastole"
    t.varchar_max "id_observation_suhu"
    t.varchar_max "id_procedure"
    t.varchar_max "id_composition"
    t.varchar_max "id_practitioner"
    t.varchar_max "id_location"
    t.varchar_max "NamaDokterIHS"
    t.varchar_max "NamaRuanganIHS"
    t.varchar_max "NamaPasienIHS"
  end

  create_table "ihs_pasien_lab_fix", id: false, force: :cascade do |t|
    t.datetime "createdate"
    t.varchar "NoPendaftaran", limit: 15
    t.varchar "NoCM", limit: 15
    t.varchar "KdRuangan", limit: 3
    t.datetime "TglPelayanan"
    t.varchar_max "IdPasien"
    t.varchar_max "id_encounter"
    t.varchar "KdLoinc", limit: 100
    t.varchar_max "NamaLoinc"
    t.varchar_max "token_1"
    t.varchar_max "id_service_request"
    t.datetime "createdate_2"
    t.varchar "KdSpesimen", limit: 100
    t.varchar_max "JenisSpesimen"
    t.varchar "KdMetode", limit: 100
    t.varchar_max "NamaMetode"
    t.varchar_max "token_2"
    t.varchar_max "id_spesimen"
    t.datetime "createdate_3"
    t.varchar_max "HasilObservation"
    t.varchar_max "token_3"
    t.varchar_max "id_observation_create"
    t.datetime "createdate_4"
    t.varchar_max "conclusion"
    t.varchar_max "token_4"
    t.varchar_max "id_diagnostic_report"
  end

  create_table "ihs_pasien_obat_fix", id: false, force: :cascade do |t|
    t.datetime "createdate"
    t.varchar "NoPendaftaran", limit: 15
    t.varchar "NoCM", limit: 15
    t.varchar "KdRuangan", limit: 3
    t.varchar "KdBarang", limit: 50
    t.varchar_max "NamaBarang"
    t.varchar_max "IdPasien"
    t.varchar_max "token"
    t.varchar_max "id_encounter"
    t.varchar_max "id_medication_1"
    t.varchar_max "id_medication_request"
    t.varchar_max "id_medication_2"
    t.varchar_max "id_medication_dispense"
    t.datetime "createdate_2"
    t.varchar_max "token_2"
  end

  create_table "ihs_pasien_observation", id: false, force: :cascade do |t|
    t.datetime "createdate"
    t.varchar "NoPendaftaran", limit: 15
    t.varchar "NoCM", limit: 15
    t.varchar "KdRuangan", limit: 3
    t.varchar "TglPeriksa", limit: 20
    t.varchar_max "IdPasien"
    t.varchar_max "NamaPasienIHS"
    t.varchar_max "id_encounter"
    t.varchar_max "id_observation"
    t.varchar_max "observation_code"
    t.varchar_max "Keterangan"
    t.varchar_max "observation_value"
    t.varchar_max "interpretation_code"
    t.varchar_max "interpretation_display"
    t.varchar_max "interpretation_value"
  end

  create_table "ihs_token", primary_key: "Id", id: :integer, force: :cascade do |t|
    t.varchar_max "access_token"
    t.varchar_max "message"
    t.datetime "createdate"
    t.datetime "expired_at"
  end

  create_table "ihs_wstracer", id: false, force: :cascade do |t|
    t.datetime "createdate"
    t.varchar "app_module", limit: 50
    t.varchar_max "wsurl"
    t.varchar "method", limit: 50
    t.varchar_max "wsrequest"
    t.varchar_max "wsresponse"
    t.float "infotime"
    t.varchar "NoPendaftaran", limit: 15
    t.varchar "NoCM", limit: 15
    t.varchar_max "IdPasien"
    t.varchar "httpstatuscode", limit: 3
    t.varchar_max "token"
    t.varchar_max "id_encounter"
    t.varchar_max "id_condition_primer"
    t.varchar_max "id_condition_sekunder"
    t.varchar_max "id_observation_nadi"
    t.varchar_max "id_observation_pernafasan"
    t.varchar_max "id_observation_sistol"
    t.varchar_max "id_observation_diastole"
    t.varchar_max "id_observation_suhu"
    t.varchar_max "id_procedure"
    t.varchar_max "id_composition"
  end

  create_table "ihs_wstracer_lab", id: false, force: :cascade do |t|
    t.datetime "createdate"
    t.varchar "app_module", limit: 50
    t.varchar_max "wsurl"
    t.varchar "method", limit: 50
    t.varchar_max "wsrequest"
    t.varchar_max "wsresponse"
    t.float "infotime"
    t.varchar "NoPendaftaran", limit: 15
    t.varchar "NoCM", limit: 15
    t.varchar_max "IdPasien"
    t.varchar "httpstatuscode", limit: 3
    t.varchar_max "token"
    t.varchar_max "id_encounter"
    t.varchar_max "id_service_request"
    t.varchar_max "id_spesimen"
    t.varchar_max "id_observation"
    t.varchar_max "id_diagnostic_report"
  end

  create_table "ihs_wstracer_obat", id: false, force: :cascade do |t|
    t.datetime "createdate"
    t.varchar "app_module", limit: 50
    t.varchar_max "wsurl"
    t.varchar "method", limit: 50
    t.varchar_max "wsrequest"
    t.varchar_max "wsresponse"
    t.float "infotime"
    t.varchar "NoPendaftaran", limit: 15
    t.varchar "NoCM", limit: 15
    t.varchar_max "IdPasien"
    t.varchar "httpstatuscode", limit: 3
    t.varchar_max "token"
    t.varchar_max "id_encounter"
    t.varchar_max "id_medication_1"
    t.varchar_max "id_medication_request"
    t.varchar_max "id_medication_2"
    t.varchar_max "id_medication_dispense"
  end

  create_table "jadwalOperasi", primary_key: "idJadwalOperasi", id: :integer, force: :cascade do |t|
    t.varchar_max "namaOperasi"
    t.datetime "waktuMulaiOperasi"
    t.datetime "waktuSelesaiOperasi"
    t.datetime "modifiedDate"
    t.varchar "NoPendaftaran", limit: 50
    t.varchar "NoCM", limit: 50
  end

  create_table "jadwal_polies", force: :cascade do |t|
    t.string "kdPraktek"
    t.string "polyclinicID"
    t.string "hari"
    t.string "idDokter"
    t.string "kdRuangan"
    t.string "jamMulai"
    t.string "jamSelesai"
    t.string "keterangan"
    t.string "statusEnabled"
    t.integer "kouta"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "jakis", force: :cascade do |t|
    t.string "noRM"
    t.string "name"
    t.string "birthDate"
    t.string "nik"
    t.string "sex"
    t.string "noJKN"
    t.string "phone"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "login_rencana", id: false, force: :cascade do |t|
    t.bigint "ID_LOGIN", null: false
    t.varchar "PENGGUNA", limit: 50
    t.varchar "PASS", limit: 50
    t.datetime "LAST_LOGIN"
    t.varchar "LAST_IPADDRESS", limit: 50
    t.datetime "LAST_ACTIVE"
    t.integer "IS_LOGIN", limit: 1
    t.integer "StatusEnabled", limit: 1
    t.integer "ENABLE", limit: 1
    t.bigint "ID_GROUPY"
    t.char "IdPegawai", limit: 50
    t.varchar "CURRENT_LOCATION", limit: 50
    t.char "KdRuangan", limit: 50
    t.char "KdSupplier", limit: 10
    t.char "Tahun", limit: 4
    t.integer "KdSatpel", limit: 1
    t.integer "KdKabag", limit: 1
    t.char "KdInstalasi", limit: 10
    t.integer "KdPptk", limit: 1
  end

  create_table "mcu_skbs", primary_key: "id_mcuskbs", id: :integer, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 50
    t.varchar "NoCm", limit: 10
    t.varchar "IdPegawai", limit: 50
    t.varchar "KdRuangan", limit: 3
    t.varchar "keperluan", limit: 50
    t.varchar "keperluanskhpn", limit: 50
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
  end

  create_table "orderpelayananfarmasi_PHP", id: false, force: :cascade do |t|
    t.varchar "KdBarang", limit: 9
    t.varchar "NamaBarang", limit: 150
    t.varchar "KeKuatan", limit: 12
    t.char "KdRuanganAkhir", limit: 3
    t.money "HargaNetto1", precision: 19, scale: 4
    t.float "JmlStok"
    t.char "KdAsal", limit: 2
    t.varchar "JenisBarang", limit: 150
    t.char "stokKdruangan", limit: 3
    t.varchar "NamaAsal", limit: 150
    t.money "HargaNetto2", precision: 19, scale: 4
    t.char "Satuan", limit: 1
    t.char "resepke", limit: 3
    t.char "KdJenisBarang", limit: 2
    t.char "NoPendaftaran", limit: 10
    t.char "NoCM", limit: 15
    t.datetime "TglPendaftaran"
    t.char "KdDetailJenisJasaPelayanan", limit: 2
    t.char "StatusPasien", limit: 4
    t.datetime "TglPulang"
    t.varchar "KdPaket", limit: 3
    t.char "KdKelasAkhir", limit: 2
    t.char "IdPenjamin", limit: 10
    t.char "StatusLimitBPOA", limit: 1
    t.char "StatusLimitBPTM", limit: 1
    t.varchar "HariTglMasuk", limit: 10
    t.varchar "BlnTglMasuk", limit: 10
    t.varchar "ThnTglMasuk", limit: 10
    t.varchar "JenisPasien", limit: 30
    t.char "KdKelompokPasien", limit: 2
    t.integer "JmlTerkecil"
    t.integer "JmlFix"
    t.money "HargaNettoFix", precision: 19, scale: 4
  end

  create_table "patients", force: :cascade do |t|
    t.string "nik"
    t.string "kkNumber"
    t.string "name"
    t.string "gender"
    t.string "placeOfBirth"
    t.date "dateOfBirth"
    t.string "religion"
    t.text "address"
    t.string "provinceCode"
    t.string "regencyCode"
    t.string "districtCode"
    t.string "villageCode"
    t.string "postalCode"
    t.string "rt"
    t.string "rw"
    t.string "insurrance"
    t.string "insurranceNumber"
    t.string "phoneNumber"
    t.string "email"
    t.string "occupation"
    t.string "education"
    t.string "tribe"
    t.string "motherName"
    t.string "patientCompanionRelationship"
    t.string "patientCompanionName"
    t.string "patientCompanionGender"
    t.string "patientCompanionPhoneNumber"
    t.date "patientCompanionDateOfBirth"
    t.string "patientCompanionAddress"
    t.string "identity_file_name"
    t.string "identity_content_type"
    t.integer "identity_file_size"
    t.datetime "identity_updated_at"
    t.text "disabilities"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "maritalStatus"
  end

  create_table "polyclinics", force: :cascade do |t|
    t.string "kdRuangan"
    t.string "namaRuangan"
    t.string "gambarPoli"
    t.string "statusEnabled"
    t.string "kodeSubInstalasi"
    t.string "kodePoliBPJS"
    t.string "kodeInstalasi"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "rawat_jalan_apms", force: :cascade do |t|
    t.string "mrn"
    t.date "tglSJP"
    t.string "jaminan"
    t.string "jnsPelayanan"
    t.string "namaPeserta"
    t.string "bookingCode"
    t.string "queueNumber"
    t.string "diagnosa"
    t.string "informasi"
    t.string "kelasRawat"
    t.string "noRujukan"
    t.string "noSep"
    t.string "penjamin"
    t.string "poli"
    t.string "poliEksekutif"
    t.string "tglSep"
    t.string "asuransi"
    t.string "hakKelas"
    t.string "jnsPeserta"
    t.string "kelamin"
    t.string "noKartu"
    t.string "noMr"
    t.string "tglLahir"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "rawat_jalan_onsites", force: :cascade do |t|
    t.string "noRM"
    t.date "visitDate"
    t.string "policlinicID"
    t.string "doctorID"
    t.string "paymentID"
    t.string "noJKN"
    t.string "kodeJenisPeserta"
    t.string "jenisPeserta"
    t.string "noSEP"
    t.string "noRujukan"
    t.string "kodePPK"
    t.string "kodeDiag"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "rujukan_bpjs", force: :cascade do |t|
    t.string "noRujukan"
    t.date "tgl_rujukan"
    t.string "namaPasien"
    t.string "kode_ppk"
    t.string "nama_rs_ppk"
    t.string "noBpjs"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "siantar_pasien", id: false, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15
    t.varchar "NamaPasien", limit: 100
    t.varchar "Alamat", limit: 100
    t.varchar "NoTelepon", limit: 15
    t.varchar "NoAntrianResep", limit: 3
    t.datetime "TglInputSiantar"
    t.varchar "StatusPengiriman", limit: 20
    t.varchar "RT", limit: 3
    t.varchar "RW", limit: 3
    t.varchar "Kecamatan", limit: 100
    t.varchar "Kelurahan", limit: 100
    t.varchar "KodePos", limit: 5
    t.varchar "NoSiantar", limit: 11
    t.char "IdPegawai", limit: 10
    t.char "IdPegawaiApoteker", limit: 10
    t.datetime "TglValidasiApotik"
    t.char "IdPegawaiKurir", limit: 10
    t.datetime "TglValidasiKurir"
  end

  create_table "staf_medis", id: false, force: :cascade do |t|
    t.varchar "id_pegawai", limit: 15
    t.varchar "gambar", limit: 225
    t.integer "StatusEnabled"
  end

  create_table "status_plg_eklaim", primary_key: "ID_Plg_Eklaim", id: :integer, force: :cascade do |t|
    t.varchar "KdStatusPlg", limit: 50
    t.varchar "KdKondisiPulang", limit: 50
    t.varchar "StatusPulangEklaim", limit: 50
    t.varchar "StatusEnabled", limit: 10
  end

  create_table "suratketerangankontrol", primary_key: "id_skk", id: :integer, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 50, null: false
    t.varchar "NoCM", limit: 10, null: false
    t.varchar "IdUser", limit: 50, null: false
    t.varchar "KdRuangan", limit: 3, null: false
    t.varchar "alasanbelumdapatdikembalikan1", limit: 255
    t.varchar "alasanbelumdapatdikembalikan2", limit: 255
    t.varchar "alasanbelumdapatdikembalikan3", limit: 255
    t.varchar "Rencanatl1", limit: 255
    t.varchar "Rencanatl2", limit: 255
    t.varchar "Rencanatl3", limit: 255
    t.date "tanggaldigunakan"
    t.varchar "noantrian", limit: 50
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
  end

  create_table "suratketerangansehat", primary_key: "id_sks", id: :integer, force: :cascade do |t|
    t.varchar "NoPendaftaran", limit: 50, null: false
    t.varchar "NoCM", limit: 10, null: false
    t.varchar "IdUser", limit: 50, null: false
    t.varchar "KdRuangan", limit: 3, null: false
    t.integer "memerlukanistirahatselama", null: false
    t.varchar "mulaitanggal", limit: 50
    t.varchar "sampaitanggal", limit: 50
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
  end

  create_table "sysdiagrams", primary_key: "diagram_id", id: :integer, force: :cascade do |t|
    t.string "name", null: false
    t.integer "principal_id", null: false
    t.integer "version"
    t.binary "definition"
    t.index ["principal_id", "name"], name: "UK_principal_name", unique: true
  end

  create_table "tRujukanOnlineTerDaftar", id: false, force: :cascade do |t|
    t.varchar "NoKartu", limit: 55, null: false
    t.varchar "NoRujukan", limit: 19, null: false
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NoCM", limit: 15, null: false
  end

  create_table "t_AsesmenKeperawatan_Aktifitas", id: false, force: :cascade do |t|
    t.char "NoCM", limit: 8
    t.char "NoPendaftaran", limit: 10
    t.char "KdRuangan", limit: 3
    t.varchar "MC_KebersihanDiri", limit: 1
    t.varchar "MC_Ambulansi", limit: 1
    t.varchar "MC_MakanMinumSendiri", limit: 1
    t.varchar "MC_ObservasiTTV", limit: 1
    t.varchar "MC_Pengobatan", limit: 1
    t.varchar "MC_PerawatanLuka", limit: 1
    t.varchar "PC_KebersihanDiri", limit: 1
    t.varchar "PC_ObservasiTTV", limit: 1
    t.varchar "PC_Ambulansi", limit: 1
    t.varchar "PC_PengobatanInjeksi", limit: 1
    t.varchar "PC_PasienCateterUrine", limit: 1
    t.varchar "PC_PasienInfus", limit: 1
    t.varchar "TC_SemuaKebutuhan", limit: 1
    t.varchar "TC_ObservasiTTV", limit: 1
    t.varchar "TC_NGT", limit: 1
    t.varchar "TC_TerapiIntravena", limit: 1
    t.varchar "TC_Suction", limit: 1
    t.varchar "TC_Gelisah", limit: 1
    t.varchar "TC_PerawatanKompleks", limit: 1
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
    t.varchar "UserId", limit: 10
  end

  create_table "t_AsesmenKeperawatan_Gizi", id: false, force: :cascade do |t|
    t.char "NoCM", limit: 8
    t.char "NoPendaftaran", limit: 10
    t.char "KdRuangan", limit: 3
    t.varchar "PenurunanBB", limit: 1
    t.varchar "AsupanMakanBerkurang", limit: 1
    t.varchar "MK_JalanNafas", limit: 1
    t.varchar "MK_PerubahanNafas", limit: 1
    t.varchar "MK_GangguanPertukaranGas", limit: 1
    t.varchar "MK_Perfusi", limit: 1
    t.varchar "MK_PenurunanCurahJantung", limit: 1
    t.varchar "MK_Hipertermi", limit: 1
    t.varchar "MK_Nyeri", limit: 1
    t.varchar "MK_KelebihanVolCairan", limit: 1
    t.varchar "MK_KekuranganVolCairan", limit: 1
    t.varchar "MK_PerubahanNutrisi", limit: 1
    t.varchar "MK_IntoleransiAktifitas", limit: 1
    t.varchar "MK_KerusakanMobilitas", limit: 1
    t.varchar "MK_GangguanIntegritas", limit: 1
    t.varchar "MK_ResikoInfeksi", limit: 1
    t.varchar "MK_ResikoPenyebaranInfeksi", limit: 1
    t.varchar "MK_ResikoCedera", limit: 1
    t.varchar "MK_Disfungsi", limit: 1
    t.varchar "MK_PerubahanPolaEliminasi", limit: 1
    t.varchar "MK_Ansietas", limit: 1
    t.varchar "MK_Lainnya", limit: 100
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
    t.varchar "UserId", limit: 10
  end

  create_table "t_AsesmenKeperawatan_StatusMental", id: false, force: :cascade do |t|
    t.char "KdStatusMental", limit: 2
    t.varchar "StatusMental", limit: 50
    t.varchar "StatusEnabled", limit: 1
  end

  create_table "t_BedBook", id: false, force: :cascade do |t|
    t.float "BookID"
    t.char "KdRuangan", limit: 3
    t.char "KdKamar", limit: 4
    t.char "NoBed", limit: 2
    t.varchar "NamaPasien", limit: 100
    t.varchar "NoID", limit: 20
    t.varchar "Pemesanan", limit: 100
    t.varchar "Keterangan", limit: 255
    t.varchar "BookStatus", limit: 1
    t.varchar "BookDesc", limit: 225
    t.varchar "UserId", limit: 20
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
    t.datetime "TglLahir"
  end

  create_table "t_BedRusak", id: false, force: :cascade do |t|
    t.char "KdRuangan", limit: 3
    t.char "KdKamar", limit: 4
    t.char "NoBed", limit: 2
    t.char "Rusak", limit: 1
    t.varchar "UserId", limit: 20
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
  end

  create_table "t_Bed_Gizi_Temp", primary_key: "Indeks", id: :integer, force: :cascade do |t|
    t.datetime "TglOrder"
    t.datetime "TglUpdate"
    t.varchar "NoOrder", limit: 10
    t.char "KdRuangan", limit: 3
    t.char "KdKamar", limit: 4
    t.char "KdJenisWaktu", limit: 3
    t.char "KdJenisMenuDiet", limit: 3
    t.varchar "NoPendaftaran", limit: 10
    t.varchar "NoPakai", limit: 10
    t.text_basic "Keterangan"
    t.varchar "KdKategoryDiet", limit: 3
    t.integer "JmlOrder", limit: 1
    t.char "KdJenisDietTambahan", limit: 3
    t.varchar "JmlTambah", limit: 50
    t.char "IdUser", limit: 10
    t.varchar "StatusKeluar", limit: 1
    t.datetime "TglKeluar"
    t.char "KdJenisMenuDiet_Tmbhn", limit: 3
  end

  create_table "t_HisoriKasir", id: false, force: :cascade do |t|
    t.varchar "PIN", limit: 20
    t.varchar "KdRuangan", limit: 20
    t.varchar "IdPegawai", limit: 10
    t.datetime "TglLogin"
  end

  create_table "t_IntegritasKulit", id: false, force: :cascade do |t|
    t.char "KdIntegritasKulit", limit: 2, null: false
    t.varchar "IntegritasKulit", limit: 50, null: false
    t.varchar "KodeExternal", limit: 15
    t.varchar "NamaExternal", limit: 50
    t.integer "StatusEnabled", limit: 1, null: false
  end

  create_table "t_PasienPMK", id: false, force: :cascade do |t|
    t.char "NoCM", limit: 8, null: false
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NamaPasien", limit: 255
    t.varchar "Alamat", limit: 255
    t.varchar "KdDiagnosa", limit: 7
    t.char "BBAwal", limit: 10
    t.char "BBKeluar", limit: 10
    t.text_basic "Anjuran"
    t.varchar "LokasiGambar", limit: 255
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
    t.datetime "TglKeluar"
    t.char "StatusEnabled", limit: 1
    t.varchar "UserId", limit: 10
    t.varchar "KdKecamatan", limit: 2
    t.varchar "KdKelurahan", limit: 3
    t.varchar "NoTelepon", limit: 30
    t.varchar "PanjangBadan", limit: 5
    t.varchar "UsiaGestasi", limit: 5
    t.varchar "LingkarKepala", limit: 5
    t.varchar "ApgarSatu", limit: 1
    t.varchar "ApgarDua", limit: 1
    t.varchar "KdTindakan", limit: 2
    t.varchar "ROP", limit: 5
    t.varchar "OAE", limit: 5
    t.varchar "StatusAlamat", limit: 20
  end

  create_table "t_PasienPMK_DataDasar", id: false, force: :cascade do |t|
    t.char "NoCM", limit: 8, null: false
    t.char "NoPendaftaran", limit: 10, null: false
    t.char "NIK", limit: 16
    t.char "NamaIbu", limit: 255
    t.char "NoCMIbu", limit: 8, null: false
    t.char "NamaAyah", limit: 255
    t.varchar "Alamat", limit: 255
    t.varchar "KdTindakan", limit: 2
    t.varchar "JenisKelamin", limit: 1
    t.varchar "ApgarSatu", limit: 1
    t.varchar "ApgarDua", limit: 1
    t.char "BBLahir", limit: 10
    t.char "BBAwal", limit: 10
    t.char "PanjangBadan", limit: 5
    t.char "LingkarKepala", limit: 5
    t.char "UUBSagital", limit: 5
    t.char "UUBTransversal", limit: 5
    t.char "UsiaGestasi", limit: 5
    t.varchar "LamaPMK", limit: 50
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
    t.varchar "UserId", limit: 10
    t.nchar "Keterangan", limit: 50
  end

  create_table "t_PemakaianKamar", id: false, force: :cascade do |t|
    t.varchar "KdRuangan", limit: 3
    t.varchar "BedIsi", limit: 3
    t.varchar "BedKosong", limit: 3
    t.varchar "TotalBed", limit: 4
    t.datetime "UpdateDate"
    t.varchar "Pria", limit: 3
    t.varchar "Wanita", limit: 3
  end

  create_table "t_RoomClass", id: false, force: :cascade do |t|
    t.integer "ClassID"
    t.varchar "ClassName", limit: 100
  end

  create_table "t_RuanganPerbaikan", id: false, force: :cascade do |t|
    t.char "KdRp", limit: 10, null: false
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
    t.char "KdRuangan", limit: 3
    t.char "KdKamar", limit: 4
    t.char "NoBed", limit: 2
    t.char "KdRusak", limit: 2
    t.char "StatusRusak", limit: 1
    t.varchar "UserId", limit: 10
  end

  create_table "t_User", id: false, force: :cascade do |t|
    t.varchar "PIN", limit: 20
    t.varchar "NamaPegawai", limit: 255
    t.varchar "Pegawai", limit: 255
    t.varchar "IdPegawai", limit: 20
    t.varchar "NamaPegawaiRS", limit: 100
    t.varchar "UserPassword", limit: 20
    t.varchar "UserSession", limit: 100
    t.varchar "gambar", limit: 255
    t.varchar "PermissionsAllowed", limit: 100
  end

  create_table "t_UserAbsen", id: false, force: :cascade do |t|
    t.varchar "PIN", limit: 20
    t.varchar "NamaPegawai", limit: 255
    t.varchar "Pegawai", limit: 255
    t.varchar "IdPegawai", limit: 20
    t.varchar "NamaPegawaiRS", limit: 100
    t.varchar "UserPassword", limit: 20
    t.varchar "UserSession", limit: 100
    t.varchar "gambar", limit: 255
    t.varchar "PermissionsAllowed", limit: 100
    t.varchar "kd_ruangan", limit: 100
  end

  create_table "t_UserGizi", id: false, force: :cascade do |t|
    t.varchar "PIN", limit: 20
    t.varchar "NamaPegawai", limit: 255
    t.varchar "Pegawai", limit: 255
    t.varchar "IdPegawai", limit: 20
    t.varchar "NamaPegawaiRS", limit: 100
    t.varchar "UserPassword", limit: 20
    t.varchar "UserSession", limit: 100
    t.varchar "gambar", limit: 255
    t.varchar "PermissionsAllowed", limit: 100
  end

  create_table "t_UserJadwalDokter", id: false, force: :cascade do |t|
    t.varchar "PIN", limit: 20
    t.varchar "NamaPegawai", limit: 255
    t.varchar "Pegawai", limit: 255
    t.varchar "IdPegawai", limit: 20
    t.varchar "NamaPegawaiRS", limit: 100
    t.varchar "UserPassword", limit: 20
    t.varchar "UserSession", limit: 100
    t.varchar "gambar", limit: 255
    t.varchar "PermissionsAllowed", limit: 100
  end

  create_table "t_UserPasienMeninggal", id: false, force: :cascade do |t|
    t.varchar "PIN", limit: 20
    t.varchar "NamaPegawai", limit: 255
    t.varchar "Pegawai", limit: 255
    t.varchar "IdPegawai", limit: 20
    t.varchar "NamaPegawaiRS", limit: 100
    t.varchar "UserPassword", limit: 20
    t.varchar "UserSession", limit: 100
    t.varchar "gambar", limit: 255
    t.varchar "PermissionsAllowed", limit: 100
  end

  create_table "t_UserPermission", id: false, force: :cascade do |t|
    t.varchar "IdPermission", limit: 2
    t.varchar "PermissionsAllowed", limit: 100
    t.varchar "Keterangan", limit: 100
    t.varchar "StatusEnabled", limit: 1
  end

  create_table "t_UserPrivilege", id: false, force: :cascade do |t|
    t.varchar "PIN", limit: 20
    t.varchar "KdRuangan", limit: 20
    t.varchar "IdPegawai", limit: 10
  end

  create_table "t_User_Kerja", id: false, force: :cascade do |t|
    t.varchar "PIN", limit: 20
    t.varchar "NamaPegawai", limit: 255
    t.varchar "Pegawai", limit: 255
    t.varchar "IdPegawai", limit: 20
    t.varchar "NamaPegawaiRS", limit: 100
    t.varchar "UserPassword", limit: 20
    t.varchar "UserSession", limit: 100
    t.varchar "gambar", limit: 255
    t.varchar "PermissionsAllowed", limit: 100
  end

  create_table "t_User_Lab", id: false, force: :cascade do |t|
    t.varchar "PIN", limit: 20
    t.varchar "NamaPegawai", limit: 255
    t.varchar "Pegawai", limit: 255
    t.varchar "IdPegawai", limit: 20
    t.varchar "NamaPegawaiRS", limit: 100
    t.char "KdRuangan", limit: 3
    t.varchar "UserPassword", limit: 20
    t.varchar "UserSession", limit: 100
    t.varchar "gambar", limit: 255
    t.varchar "PermissionsAllowed", limit: 100
  end

  create_table "t_User_Verifikasi", id: false, force: :cascade do |t|
    t.varchar "PIN", limit: 20
    t.varchar "NamaPegawai", limit: 255
    t.varchar "Pegawai", limit: 255
    t.varchar "IdPegawai", limit: 20
    t.varchar "NamaPegawaiRS", limit: 100
    t.char "KdRuangan", limit: 3
    t.varchar "UserPassword", limit: 20
    t.varchar "UserSession", limit: 100
    t.varchar "gambar", limit: 255
    t.varchar "PermissionsAllowed", limit: 100
  end

  create_table "t_bed_biayapasienalkes", id: false, force: :cascade do |t|
    t.varchar_max "nocm"
    t.varchar_max "nopendaftaran"
    t.float "biaya"
    t.datetime "updatedate"
  end

  create_table "t_bed_biayapasienbilling", id: false, force: :cascade do |t|
    t.varchar_max "nocm"
    t.varchar_max "nopendaftaran"
    t.float "biaya"
    t.datetime "updatedate"
  end

  create_table "t_rencanarawat", id: false, force: :cascade do |t|
    t.varchar "nocm", limit: 8
    t.varchar "nopendaftaran", limit: 20
    t.datetime "create_date"
    t.datetime "update_date"
    t.varchar "statusdaftar", limit: 1
    t.char "KdRuangan", limit: 3
    t.char "KdKamar", limit: 4
    t.char "NoBed", limit: 2
    t.char "KdRuanganAsal", limit: 3
    t.varchar "pin", limit: 20
  end

  create_table "temp_stokopname", id: false, force: :cascade do |t|
    t.varchar "NoClosing", limit: 50
    t.varchar "KdRuangan", limit: 50
    t.varchar "KdBarang", limit: 50
    t.varchar "KdAsal", limit: 5
    t.varchar "JmlStokSystem", limit: 15
    t.varchar "JmlStokReal", limit: 15
    t.money "HargaNetto1", precision: 19, scale: 4
    t.money "HargaNetto2", precision: 19, scale: 4
    t.varchar "Lokasi", limit: 50
    t.datetime "TglKadaluarsa"
    t.varchar "jmlminimum", limit: 10
    t.varchar_max "keterangan"
    t.datetime "CreateDate"
    t.varchar "IdUser", limit: 50
  end

  create_table "tempresep_2308240220", primary_key: "ID_Resep", id: :integer, force: :cascade do |t|
    t.varchar "KdBarang", limit: 50
    t.varchar "NoPendaftaran", limit: 50
    t.varchar "NoCm", limit: 10
    t.varchar "JmlBarang", limit: 50
    t.varchar "StatusJmlBarang", limit: 2
    t.varchar "Keterangan", limit: 250
    t.varchar "AturanPakai", limit: 500
    t.datetime "CreateDate"
    t.varchar "IdPegawai", limit: 50
    t.varchar_max "NamaBarang"
    t.varchar "JenisBarang", limit: 50
    t.varchar "StatusVerif", limit: 50
    t.varchar "JmlStok", limit: 50
    t.varchar "JenisObat", limit: 2
    t.varchar "PemberianObat", limit: 2
    t.varchar "KdRuangan", limit: 3
    t.money "Harga", precision: 19, scale: 4
    t.varchar_max "keteranganracik"
    t.varchar_max "dosis"
    t.varchar "KeteranganLain", limit: 500
    t.varchar "obatpulang", limit: 50
  end

  create_table "v_statusbed_old", id: false, force: :cascade do |t|
    t.varchar "BlokName", limit: 15
    t.integer "FloorName"
    t.varchar "RoomID", limit: 3
    t.varchar "RoomName", limit: 30
    t.integer "ClassID"
    t.varchar "ClassName", limit: 50
    t.char "KdRuangan", limit: 3
    t.varchar "NamaRuangan", limit: 50
    t.char "KdKelas", limit: 2
    t.varchar "KelasKamar", limit: 50
    t.char "KdKamar", limit: 4
    t.varchar "NmKamar", limit: 50
    t.char "NoBed", limit: 2, null: false
    t.varchar "StatusBed", limit: 6, null: false
    t.integer "StatusBedIsi"
    t.integer "StatusBedKosong", null: false
    t.integer "TotalBed", null: false
    t.integer "StatusEnabled", limit: 1
    t.varchar "JenisKelamin", limit: 1
    t.integer "Pria", null: false
    t.integer "Wanita", null: false
    t.integer "PriaWanita", null: false
    t.char "Rusak", limit: 1, null: false
    t.integer "JmlBedRusak", null: false
  end

  create_table "vclaim_logs", id: false, force: :cascade do |t|
    t.datetime "created_date", default: -> { "getdate()" }
    t.nchar "code", limit: 10
    t.ntext "request_payload"
    t.ntext "response_data"
    t.varchar "nobpjs", limit: 50
    t.ntext "response_message"
    t.ntext "sep_request_payload"
  end

  create_table "vclaim_sep_logs", id: false, force: :cascade do |t|
    t.datetime "created_date", default: -> { "getdate()" }
    t.varchar "no_sep", limit: 50
    t.ntext "sep"
    t.varchar "noPendaftaran", limit: 30
    t.index ["noPendaftaran"], name: "idx_vclaim_sep_logs_noPendaftaran"
  end

  create_table "xCPPT_RANAP_2308240220", primary_key: "IdCPPT", id: :integer, force: :cascade do |t|
    t.char "NoPendaftaran", limit: 10
    t.char "NoCM", limit: 8
    t.char "KdRuangan", limit: 3
    t.char "IdDokter", limit: 10
    t.varchar_max "Instruksi"
    t.varchar_max "Instruksi_Objektif"
    t.varchar_max "Instruksi_Assesment"
    t.varchar_max "Instruksi_Plan"
    t.datetime "CreateDate"
    t.datetime "UpdateDate"
    t.char "StatusVerif", limit: 1
    t.varchar "Instruksi_interuksi", limit: 5000
  end

  create_table "xtemp_resepnew280123", primary_key: "ID_Resep", id: :integer, force: :cascade do |t|
    t.varchar "KdBarang", limit: 50
    t.varchar "NoPendaftaran", limit: 50
    t.varchar "NoCm", limit: 10
    t.varchar "JmlBarang", limit: 50
    t.varchar "StatusJmlBarang", limit: 2
    t.varchar "Keterangan", limit: 250
    t.varchar "AturanPakai", limit: 500
    t.datetime "CreateDate"
    t.varchar "IdPegawai", limit: 50
    t.varchar_max "NamaBarang"
    t.varchar "JenisBarang", limit: 50
    t.varchar "StatusVerif", limit: 50
    t.varchar "JmlStok", limit: 50
    t.varchar "JenisObat", limit: 2
    t.varchar "PemberianObat", limit: 2
    t.varchar "KdRuangan", limit: 3
    t.money "Harga", precision: 19, scale: 4
    t.varchar_max "keteranganracik"
    t.varchar_max "dosis"
    t.varchar "KeteranganLain", limit: 500
    t.varchar "obatpulang", limit: 50
  end

  add_foreign_key "DetailBarangKeluarVerifikasi", "StrukKirim", column: "NoKirim", primary_key: "NoKirim", name: "FK_DetailBarangKeluarVerifikasi_StrukKirim", on_delete: :cascade
  add_foreign_key "DetailOrderPelayananOA", "StrukOrder", column: "NoOrder", primary_key: "NoOrder", name: "FK_DetailOrderPelayananOA_StrukOrder"
  add_foreign_key "DetailPemakaianAlkes", "KelasPelayanan", column: "KdKelas", primary_key: "KdKelas", name: "FK_DetailPemakaianAlkes_KelasPelayanan"
  add_foreign_key "DetailPemakaianAlkes", "KelasPelayanan", column: "KdKelasPenjamin", primary_key: "KdKelas", name: "FK_DetailPemakaianAlkes_KelasPelayanan1"
  add_foreign_key "DetailRujukanAsal", "RujukanAsal", column: "KdRujukanAsal", primary_key: "KdRujukanAsal", name: "FK_DetailRujukanAsal_RujukanAsal", on_delete: :cascade
  add_foreign_key "KelompokPasien", "JenisTarif", column: "KdJenisTarif", primary_key: "KdJenisTarif", name: "FK_KelompokPasien_JenisTarif"
  add_foreign_key "Kelurahan", "Kecamatan", column: "KdKecamatan", primary_key: "KdKecamatan", name: "FK_Kelurahan_Kecamatan1", on_delete: :cascade
  add_foreign_key "KotaKabupaten", "Propinsi", column: "KdPropinsi", primary_key: "KdPropinsi", name: "FK_KotaKabupaten_Propinsi", on_update: :cascade, on_delete: :cascade
  add_foreign_key "PasienIGDKeluar", "KondisiPulang", column: "KdKondisiPulang", primary_key: "KdKondisiPulang", name: "FK_PasienIGDKeluar_KondisiPulang", on_update: :cascade, on_delete: :cascade
  add_foreign_key "PasienMeninggal", "PenyebabKematian", column: "KdPenyebab", primary_key: "KdPenyebabKematian", name: "FK_PasienMeninggal_PenyebabKematian"
  add_foreign_key "RegistrasiIBS", "KelasPelayanan", column: "KdKelas", primary_key: "KdKelas", name: "FK_RegistrasiIBS_KelasPelayanan"
  add_foreign_key "RegistrasiIBS", "RujukanAsal", column: "KdRujukanAsal", primary_key: "KdRujukanAsal", name: "FK_RegistrasiIBS_RujukanAsal"
  add_foreign_key "RegistrasiIEDTA", "KelasPelayanan", column: "KdKelas", primary_key: "KdKelas", name: "PK_RegistrasiIEDTA_KelasPelayanan"
  add_foreign_key "RegistrasiIEDTA", "RujukanAsal", column: "KdRujukanAsal", primary_key: "KdRujukanAsal", name: "PK_RegistrasiIEDTA_RujukanAsal"
  add_foreign_key "RegistrasiRadiology", "KelasPelayanan", column: "KdKelas", primary_key: "KdKelas", name: "FK_RegistrasiRadiology_KelasPelayanan"
  add_foreign_key "RegistrasiRadiology", "RujukanAsal", column: "KdRujukanAsal", primary_key: "KdRujukanAsal", name: "FK_RegistrasiRadiology_RujukanAsal"
  add_foreign_key "SupplierPabrik", "Pabrik", column: "KdPabrik", primary_key: "KdPabrik", name: "FK_SupplierPabrik_Pabrik", on_delete: :cascade
  add_foreign_key "SupplierPabrik", "Supplier", column: "KdSupplier", primary_key: "KdSupplier", name: "FK_SupplierPabrik_Supplier", on_delete: :cascade
end
