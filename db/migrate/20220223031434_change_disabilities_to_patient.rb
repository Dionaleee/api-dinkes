class ChangeDisabilitiesToPatient < ActiveRecord::Migration[6.1]
  def change
    change_column :patients, :disabilities, :text, array:true
  end
end
