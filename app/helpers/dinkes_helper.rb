module DinkesHelper
	# @@key = nil
	@@iv = nil
	@@key = "3Ljfxb445fddVFK69"
	def self.encryption(msg)
		begin
			cipher = OpenSSL::Cipher.new("AES-128-ECB")
			cipher.encrypt()
			cipher.key = @@key
			crypt = cipher.update(msg) + cipher.final()
			crypt_string = (Base64.encode64(crypt))
			return crypt_string.strip
		rescue Exception => exc
			puts ("Message for the encryption log file for message #{msg} = #{exc.message}")
		end
	end

	def self.decryption(msg)
		begin
			cipher = OpenSSL::Cipher.new("AES-128-ECB")
			cipher.decrypt()
			cipher.key = @@key
			tempkey = Base64.decode64(msg)
			crypt = cipher.update(tempkey)
			crypt << cipher.final()
			return crypt
		rescue Exception => exc
			puts ("Message for the decryption log file for message #{msg} = #{exc.message}")
		end
	end

	def self.decode_jwt_reversal(token)

		if token.present?
		  begin
			hmac_secret = "!CIRACAS@202!"
			begin
			  
			  decoded_token = JWT.decode token, hmac_secret, true, { algorithm: 'HS256' }
			  return decoded_token
				
			#   Rails.logger.info "==============> AUTH: #{@status}"
			rescue JWT::VerificationError => ex
			  Rails.logger.info "=======> TOKEN JWT VerificationError: #{ex}"
			  false
			end
		  rescue JWT::ExpiredSignature
			# Handle expired token, e.g. logout user or deny access
			Rails.logger.info "=======> TOKEN JWT EXPIRED: #{token}"
			false
		  end
		else
		  false
		end
	end
end
