class DelMartitalStatusToPatient < ActiveRecord::Migration[6.1]
  def change
    remove_attachment  :patients, :maritalStatus
  end
end
