class Patient < ApplicationRecord
    validates :nik, uniqueness: {
        message: 'data sudah ada'
    }

    validates :nik, :name, :dateOfBirth, :address, :rw, :rt, :insurrance, :email,:maritalStatus, :occupation, :education,:tribe,:motherName, presence: { message: "wajib diisi" }
    # errors.add :nik, :too_plain, message: "harus diisi"
    validates :insurranceNumber, presence: { message: "wajib diisi" }, if: :paid_with?
    
    def paid_with?
        insurrance == "0001"
    end

    validate :cant_no_insurrance
    validate :cant_no_marital   

    def cant_no_insurrance
      if insurrance != '0000' && insurrance != '0001'
        errors.add(:insurrance, "Tipe asuransi tidak valid")
      end
    end

    def cant_no_marital
        if maritalStatus != 'KAWIN' && maritalStatus != 'BELUM_KAWIN' && maritalStatus != 'CERAI_HIDUP' && maritalStatus != 'CERAI_MATI'
            errors.add(:maritalStatus, "Tipe marital status tidak valid")
        end
    end

    # has_attached_file :identity, :path   => 'public/patient/identity/:id/:filename'
    # validates_attachment_content_type :identity, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif", 'application/pdf']
end
