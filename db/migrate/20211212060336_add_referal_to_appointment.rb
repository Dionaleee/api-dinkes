class AddReferalToAppointment < ActiveRecord::Migration[6.1]
  def change
    add_column :appointments, :referrralNumber, :string
    add_column :appointments, :bpjsNumber, :string
    add_column :appointments, :startTime, :string
    add_column :appointments, :endTime, :string
  end
end
