class CreateDokters < ActiveRecord::Migration[6.1]
  def change
    create_table :dokters do |t|
      t.string :idPegawai
      t.string :kdJnsPegawai
      t.string :kdTitle
      t.string :namaLengkap
      t.string :namaKeluarga
      t.string :namaPanggilan
      t.string :jenisKelamin
      t.string :tempatLahir
      t.date :tglLahir
      t.date :tglMasuk
      t.date :tglKeluar

      t.timestamps
    end
  end
end
