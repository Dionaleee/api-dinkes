class ChangeColumnAndTypePatient < ActiveRecord::Migration[6.1]
  def change
    change_column :patients, :kkNumber, :string
    rename_column :patients, :inssurance, :insurrance
    rename_column :patients, :inssuranceNumber, :insurranceNumber
  end
end
