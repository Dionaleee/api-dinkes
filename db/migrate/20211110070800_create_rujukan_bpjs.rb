class CreateRujukanBpjs < ActiveRecord::Migration[6.1]
  def change
    create_table :rujukan_bpjs do |t|
      t.string :noRujukan
      t.date :tgl_rujukan
      t.string :namaPasien
      t.string :kode_ppk
      t.string :nama_rs_ppk
      t.string :noBpjs

      t.timestamps
    end
  end
end
