class CreateJadwalPolies < ActiveRecord::Migration[6.1]
  def change
    create_table :jadwal_polies do |t|
      t.string :kdPraktek
      t.string :polyclinicID
      t.string :hari
      t.string :idDokter
      t.string :kdRuangan
      t.string :jamMulai
      t.string :jamSelesai
      t.string :keterangan
      t.string :statusEnabled
      t.string :kouta

      t.timestamps
    end
  end
end
