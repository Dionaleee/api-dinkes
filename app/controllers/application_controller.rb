class ApplicationController < ActionController::API
    
        def authenticate_jwt
            if !token_jwt_valid?
              render json: { metaData: { message: 'unauthorized access / token', status: 401 } }, status: 401
            end
        end
    
        private
        def token_jwt_valid?
          if token_present?
            Rails.logger.info "=======> TOKEN JWT: #{token}"
            begin
              hmac_secret = "!CIRACAS@202!"
              begin
                decoded_token = JWT.decode token, hmac_secret, true, { algorithm: 'HS256' }
                Rails.logger.info "=======> user: #{decoded_token.first.to_json}"
                api_user = decoded_token.first['data']['api_user']
                api_key = decoded_token.first['data']['api_key']
                if api_user == '21Ciracas!!!' &&  api_key == 'eRFD35sc!sUImj'
                  @myauth = true
                else
                  @myauth = false
                end
                Rails.logger.info "=======> user: #{@myauth}"
                Rails.logger.info "==============> DECODED TOKEN: #{api_user} - #{api_key}"
              rescue JWT::VerificationError => ex
                Rails.logger.info "=======> TOKEN JWT VerificationError: #{ex}"
                false
              end
            rescue JWT::ExpiredSignature
              # Handle expired token, e.g. logout user or deny access
              Rails.logger.info "=======> TOKEN JWT EXPIRED: #{token}"
              false
            end
          else
            false
          end
        end
    
        def token
            request.env['HTTP_TOKEN'].scan(/APIDINKES (.*)$/).flatten.last
        end
    
        def token_present?
            !!request.env.fetch('HTTP_TOKEN','').scan(/APIDINKES/).flatten.first
        end
end
