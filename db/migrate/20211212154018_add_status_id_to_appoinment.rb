class AddStatusIdToAppoinment < ActiveRecord::Migration[6.1]
  def change
    add_column :appointments, :statusID, :integer
  end
end
