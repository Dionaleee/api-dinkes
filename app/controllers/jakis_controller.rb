class JakisController < ApplicationController
  # before_action :set_jaki, only: %i[ show update destroy ]
  before_action :authenticate_jwt, except: [:create_token]

  # GET /jakis
  # GET /jakis.json
  def sisa_antrian
    
    find_appointment = Appointment.where("bookingCode = #{params['bookingCode']}")
  
    if find_appointment.present?
        data = []
        find_appointment.each do |res|
          cek_antrian = Appointment.where(statusID: 1,policlinicID: res['policlinicID'], bookingDate: res['bookingDate']).count
          cek_antrian_saat_ini = Appointment.where(statusID: 1,policlinicID: res['policlinicID'], bookingDate: res['bookingDate']).first
          cek_antrian_masuk = Appointment.where(statusID: 2,policlinicID: res['policlinicID'], bookingDate: res['bookingDate'])
         
          find_patient = Jaki.find_by(noRM: res['noRM'])
          dokter = Dokter.find_by(idPegawai: res['doctorID'])
          polyclinic = Polyclinic.find_by(kdRuangan: res['policlinicID'])
        #  render json: polyclinic
        #  return
          if find_patient.present?
            if dokter.present?
              if polyclinic.present?
                datax = {
                  noMR: res['noRM'],
                  bookingDate: res['bookingDate'],
                  policlinicID: res['policlinicID'],
                  policlinicName: polyclinic.namaRuangan,
                  doctorID: res['doctorID'],
                  doctorName: dokter.namaLengkap,
                  paymentID: res['paymentID'],
                  paymentName: (res['paymentID'] == 00 ? 'BPJS' : 'UMUM'),
                  referrralNumber: res['referrralNumber'],
                  bpjsNumber: res['bpjsNumber'],
                  startTime: res['startTime'],
                  endTime: res['endTime'],
                  bookingCode: res['bookingCode'],
                  queueNumber: res['queueNumber'],
                  queue: {
                    untilMyTurn: (cek_antrian.present? ? cek_antrian.to_i-1 : cek_antrian) ,
                    currentBookingCode: (cek_antrian_saat_ini.present? ? cek_antrian_saat_ini.bookingCode : cek_antrian_saat_ini.bookingCode),
                    currentNumber: (cek_antrian_saat_ini.present? ? cek_antrian_saat_ini.queueNumber.gsub("B","") : cek_antrian_saat_ini.queueNumber),
                  }
                }
                data.push(datax)
              end
            end
          else
            mydata = {
              metaData: {code: 404, message: "Not found. Appointment doesn't exist"},
            }
            render json: mydata, status: :not_found
          end
        end

        mydata = {
          metaData: {code: 200, message: "OK"},
          data: data ,
        }
        render json: mydata, status: :ok
    else
        mydata = {
          metaData: {code: 404, message: "Data antrian tidak ditemukan"},
        }
        render json: mydata, status: :not_found
    end

   
  end

  def get_patient
    data = Patient.all

    render json: data
    return
  end

  def create_patient
    data = {
      nik: params['nik'],
      kkNumber: params['kkNumber'],
      name: params['name'],
      gender: params['gender'],
      placeOfBirth: params['placeOfBirth'],
      dateOfBirth: params['dateOfBirth'],
      religion: params['religion'],
      address: params['address'],
      provinceCode: params['provinceCode'],
      regencyCode: params['regencyCode'],
      districtCode: params['districtCode'],
      villageCode: params['villageCode'],
      postalCode: params['postalCode'],
      rw: params['rw'],
      rt: params['rt'],
      insurrance: params['insurrance'],
      insurranceNumber: params['insurranceNumber'],
      phoneNumber: params['phoneNumber'],
      email: params['email'],
      occupation: params['occupation'],
      education: params['education'],
      tribe: params['tribe'],
      motherName: params['motherName'],
      maritalStatus: params['maritalStatus'],
      patientCompanionRelationship: params['patientCompanionRelationship'],
      patientCompanionName: params['patientCompanionName'],
      patientCompanionGender: params['patientCompanionGender'],
      patientCompanionPhoneNumber: params['patientCompanionPhoneNumber'],
      patientCompanionDateOfBirth: params['patientCompanionDateOfBirth'],
      patientCompanionAddress: params['patientCompanionAddress'],
      # identity: params['identity'],
      disabilities: params['disabilities'],
    }

    cek_data = Patient.where(nik: params['nik'])

    if !cek_data.present?
      daftar = Patient.new(data)

      dataS = {
          patientCompanion: {
                relationship: params['patientCompanionRelationship'],
                name: params['name'],
                gender: params['gender'],
                phoneNumber: params['phoneNumber'],
                dateOfBirth: params['dateOfBirth'],
                address: params['address'],
          },
          nik: params['nik'],
          kkNumber: params['kkNumber'],
          placeOfBirth: params['placeOfBirth'],
          religion: params['religion'],
          maritalStatus: params['maritalStatus'],
          provinceCode: params['provinceCode'],
          regencyCode: params['regencyCode'],
          districtCode: params['districtCode'],
          villageCode: params['villageCode'],
          postalCode: params['postalCode'],
          rw: params['rw'],
          rt: params['rt'],
          insurrance: params['insurrance'],
          insurranceNumber: params['insurranceNumber'],   
        }
      
      if daftar.valid?
        if daftar.save
          myResp = {
            metaData: {
              code: 200,
              message: "OK"
            },
            data: dataS,
          }
          render json: myResp, status: :ok
        else
          myResp = {
            metaData: {
              code: 400,
              message: "Gagal Menyimpan Data"
            },
            data: dataS
          }
        end
      else
        myResp = {
          message: "Data tidak valid",
          errors: daftar.errors.messages,
        }
        render json: myResp, status: :unprocessable_entity
      end
    else
      myResp = {
          message: "Data tidak valid",
          errors: {
            nik: ["NIK Sudah Terdaftar"],
          },
      }
      render json: myResp,  status: :unprocessable_entity
    end

   
  end

  def index
    @jakis = Jaki.all
  end

  def create_token
    api_user = params['api_user']
    api_key = params['api_key']

    if api_user == '21Ciracas!!!' && api_key == 'eRFD35sc!sUImj'
      exp = Time.now.to_i + 10 * 60
      data = {
        api_user: api_user,
        api_key: api_key
      }

      payload = { data: data , exp: exp}
    
        hmac_secret = "!CIRACAS@202!"
        token = JWT.encode payload, hmac_secret, 'HS256'
    
        payload = {
          token: token,
          exp: exp,
        }
        render json: { metaData: { code: 200,message:"ok"}, data:  payload  }
    else
      render json: { metaData: { code: 400,message:"Error, Parameter Tidak Valid"}, data: nil }
    end
   
  end

  def rujukan
    rujuk = RujukanBpj.where(noBpjs: params['bpjs_number'])

    if rujuk.present?
      rujuk.each do |res|
        poli = Polyclinic.where()
        data = {
          no_rujukan: res['no_rujukan'],
          tgl_rujukan: res['tgl_rujukan'],
          kode_ppk: res['kode_ppk'],
          nama_rs_ppk: res['nama_rs_ppk'],
          kode_poli: res['no_rujukan'],
          nama_poli: res['no_rujukan']
        }
      end
      ret = {
        metaData: {code:200,message:"OK"},
        response: rujuk
      }
    else
      ret = {
        metaData: {code:404,message:"Referral Not found"},
        response: nil
      }
    end

    render json: ret
    return
  end

  def jadwal_poli
    date = params['visitDate']
    id = params['id']
    poli = Polyclinic.find_by(id: id)
    
    if poli.present?
      if date.present?
        time = date.to_date
        a = time.strftime("%a")
        

        jadwal_poli = JadwalPoly.where(polyclinicID: poli['kdRuangan'], hari: a)
       
        # jadwal_poli = JadwalPoly.find_by_sql("SELECT DISTINCT hari,idDokter FROM jadwal_polies WHERE polyclinicID = '#{poli['id']}' AND hari = '#{a}';")
        set =[]
        dokter_jdwl = []
        if jadwal_poli.present?
          # dokter = Dokter.find_by_sql("SELECT DISTINCT idPegawai FROM dokters;")
          jadwal_p = JadwalPoly.find_by_sql("SELECT DISTINCT hari,idDokter FROM jadwal_polies WHERE polyclinicID = '#{poli['kdRuangan']}' AND hari = '#{a}';")
          jadwal_p.each do |e|
            dokter = Dokter.where(idPegawai: e['idDokter'])
            poli_jam = JadwalPoly.find_by_sql("SELECT DISTINCT jamMulai AS start_time, jamSelesai AS end_time,kouta AS quota_left FROM jadwal_polies WHERE idDokter = '#{e['idDokter']}' AND hari = '#{a}' UNION SELECT DISTINCT jamMulai AS start_time, jamSelesai AS end_time,kouta AS quota_left  FROM jadwal_polies WHERE idDokter = '#{e['idDokter']}' AND hari = '#{a}'")
            
            dokter.each do |resp|
              cek_apm = Appointment.where(doctorID: resp['idPegawai'],bookingDate: date.to_date.strftime("%F"))
              cek_apm2 = cek_apm.where("statusID != 3")
              
              if cek_apm2.present?
                cek_apm2.each do |e|
                  time = []
                  poli_jam.each do |e|
                    datax = {
                      start_time: e['start_time'],
                      end_time: e['end_time'],
                      quota_left: e['quota_left']-cek_apm2.count
                    }
                    time.push(datax);
                  end
                  data = {
                    doctorId: resp['idPegawai'],
                    doctorName: resp['namaLengkap'],
                    times: time
                    
                  }
                  set.push(data);
                end
              else
                data = {
                    doctorId: resp['idPegawai'],
                    doctorName: resp['namaLengkap'],
                    times: poli_jam
                    
                  }
                  set.push(data);
              end
                
              
            end
          end
         
            
          
          # render json: set
          # return
          # jadwal_poli.each do |res|
          #   dokter = Dokter.find_by_sql("SELECT DISTINCT idPegawai FROM dokters;")
          #   # render json: dokter
          #   # return
          #   # dokter.each do |item|
          #   #   data = {
          #   #     doctorId: item['idPegawai'],
          #   #     # doctorName: item['namaLengkap'],
          #   #     # times: child_data
          #   #   }
          #     set.push(dokter);
          #   # end

          #   jadwal_poli_child = JadwalPoly.find_by_sql("SELECT * FROM jadwal_polies HAVING COUNT(DISTINCT hari,idDokter);")
            
          #   # if jadwal_poli_child.present?
          #   #   jadwal_poli_child.each do |item_child|
          #   #     # poli_jam = JadwalPoly.find_by_sql("SELECT jamMulai , jamSelesai,kouta  FROM jadwal_polies WHERE idDokter = '#{item_child['idDokter']}' UNION SELECT jamMulai , jamSelesai, kouta  FROM jadwal_polies WHERE idDokter = '#{item_child['idDokter']}'")
               
          #   #     if poli_jam.present?
          #   #       poli_jam.each do |child|
          #   #         array_child = {
          #   #           start_time: child.jamMulai,
          #   #           end_time: child.jamSelesai,
          #   #           quota_left: child.kouta,
          #   #         }
          #   #         child_data.push(array_child)
          #   #       end
          #   #     end
                 
          #   #   end
          #   # end
  
          # end
          # render json: set
          # return
          ret = {
            metaData: {code:200,message:"OK"},
            data:  set
          }
        else
          ret = {
            metaData: {code:404,message:"Data Poli & Date Not Found"},
            data: ""
          }
        end
    
      else
        ret = {
          metaData: {code:404,message:"Date Not found"},
          response: {doctorBookingList: ""}
        }
        
      end

    else
      ret = {
        metaData: {code:404,message:"Schedule Not found"},
        response: {doctorBookingList: nil}
      }
    end

    render json: ret
    return
  end

  def registrationrawatjalanCIK
    data = {
     bookingCode: params['bookingCode']
    }
    daftar = RawatJalanApm.new(data)

    if daftar.save
      find_apm = Appointment.find_by(bookingCode: params['bookingCode'])

      if find_apm['paymentID'] == 'BPJS' 
         mydata = {
          metaData: {code: 200 , message: "OK"},
          response: { 
            diagnosa: daftar['diagnosa'],
            informasi: daftar['informasi'],
            jnsPelayanan: daftar['jnsPelayanan'],
            kelasRawat: daftar['kelasRawat'],
            noRujukan: daftar['noRujukan'],
            noSep: daftar['noSep'],
            penjamin: daftar['penjamin'],
            peserta: {
              asuransi: daftar['asuransi'],
              hakKelas: daftar['hakKelas'],
              jnsPeserta: daftar['jnsPeserta'],
              kelamin: daftar['kelamin'],
              nama: daftar['nama'],
              noKartu: daftar['noKartu'],
              noMr: daftar['noMr'],
              tglLahir: daftar['tglLahir'],
            },
            poli: daftar['poli'],
            poliEksekutif: daftar['poliEksekutif'],
            tglSep: daftar['tglSep'],
          },
        }
      else
        mydata = {
          metaData: {code: 200 , message: "OK"},
          data: { 
            mrn:  daftar['mrn'],
            jaminan: daftar['jaminan'],
            tglSJP: daftar['tglSJP'],
            jnsPelayanan: daftar['jnsPelayanan'],
            namaPeserta: daftar['namaPeserta'],
            poli: daftar['poli']
           },
        }
      end
    else
      mydata = {
          metaData: {code: 400, message: "Bad Request, not saving data"}
        }
    end

    render json: mydata
    return

  end

  def registrationrawatjalan
    data = {
      noRM: params['noMR'],
      visitDate: params['visitDate'].to_date,
      policlinicID: params['policlinicID'],
      doctorID: params['doctorID'],
      paymentID: params['paymentID'],
      noJKN: params['noJKN'],
      kodeJenisPeserta: params['kodeJenisPeserta'],
      jenisPeserta: params['jenisPeserta'],
      noSEP: params['noSEP'],
      noRujukan: params['noRujukan'],
      kodePPK: params['kodePPK'],
      kodeDiag: params['kodeDiag']
    }
    daftar = RawatJalanOnsite.new(data)

    if daftar.save
      mydata = {
          metaData: {code: 201 , message: "Object created. Patient have been checked in"},
          data:  {registrationCode: "RJ#{daftar['visitDate'].strftime('%Y%m%d')}#{daftar['doctorID']}", queueNumber: daftar['kodeJenisPeserta']},
        }
    else
      mydata = {
          metaData: {code: 400, message: "Bad Request, not saving data"}
        }
    end

    render json: mydata
    return
  end

  def appointmentsearch
    find_appointment = Appointment.where("noRM = #{params['noMR']}")
  
    if find_appointment.present?
       searchDate = find_appointment.where("bookingDate BETWEEN '#{params['startDate'].to_date}' AND '#{params['endDate'].to_date}'")
      
       if searchDate.present?
        data = []
        searchDate.each do |res|
          
          find_patient = Jaki.find_by(noRM: res['noRM'])

          dokter = Dokter.find_by(idPegawai: res['doctorID'])
          polyclinic = Polyclinic.find_by(id: res['policlinicID'])
          if find_patient.present?
            if dokter.present?
              if polyclinic.present?
                datax = {
                  bookingCode: res['bookingCode'],
                  noMR: res['noRM'],
                  patientName: res['name'],
                  bookingDate: res['bookingDate'],
                  policlinicID: res['policlinicID'],
                  policlinicName: polyclinic.namaRuangan,
                  doctorID: res['doctorID'],
                  doctorName: dokter.namaLengkap,
                  paymentID: res['paymentID'],
                  queueNumber: res['queueNumber'],
                  startTime: res['startTime'],
                  endTime: res['endTime'],
                  waitingNumber: res['waitingNumber'],
                  statusID: res['statusID']
                }
                data.push(datax)
              end
            end
          else
            mydata = {
              metaData: {code: 404, message: "Not found. Appointment doesn't exist"},
            }
            render json: mydata
            return
          end
        end

        mydata = {
          metaData: {code: 200, message: "OK"},
          data: data ,
        }
       else
        mydata = {
          metaData: {code: 404, message: "Not found. Appointment doesn't exist"},
        }
       end
    else
        mydata = {
          metaData: {code: 404, message: "Not found. Appointment doesn't exist"},
        }
    end

    render json: mydata
    return
   

  end
  
  def appointmentcancel
    mr = params['noMR'].to_s
    book = params['bookingCode'].to_s
    find_appointment = Appointment.where("noRM = #{mr} AND bookingCode = '#{book}' AND statusID != 3")
   
    if find_appointment.present?
      data = {
        # noRM: find_appointment['noRM'],
        # bookingDate: find_appointment['bookingDate'],
        # policlinicID: find_appointment['policlinicID'],
        # doctorID: find_appointment['doctorID'],
        # paymentID: find_appointment['paymentID'],
        # bookingCode: find_appointment['bookingCode'],
        # queueNumber: find_appointment['queueNumber'],
        cancel: '3',
        statusID: '3' 
      }
      if find_appointment.update_all(data)
         mydata = {
          metaData: {code: 200, message: "OK. 1 appointment(s) has been canceled  "},
        }
      else
         mydata = {
          metaData: {code: 404, message: "Not Update"},
        }
      end
    else
      mydata = {
          metaData: {code: 404, message: "Not found. Appointment doesn’t exist"},
        }
    end

    render json: mydata
    return
  end

  def appointment

    booking = params['bookingDate']
    policlinicID = params['policlinicID']
    doctorID = params['doctorID']

    # find_booking = Appointment.where(:bookingDate => "#{booking} 00:00:01.549" .. "#{booking} 23:59:59.549",:policlinicID => policlinicID,:doctorID => doctorID).count()
    # find_booking = find_booking+1

    find_booking = Appointment.where(:policlinicID => policlinicID,:doctorID => doctorID).count()
    find_booking = find_booking+1
   
      bookingCode = "0000"+"#{find_booking}"+"#{policlinicID}"
      statusID = 1
      input = {
        noRM: params['noMR'].to_s,
        bookingDate: booking.to_date,
        policlinicID: params['policlinicID'].to_s,
        doctorID: params['doctorID'].to_s,
        paymentID: params['paymentID'].to_s,
        bookingCode: bookingCode.gsub(' ','').to_s,
        queueNumber: (find_booking >= 10 ? "B0"+"#{find_booking}" : "B00"+"#{find_booking}" ),
        referrralNumber: params['referrralNumber'].to_s,
        bpjsNumber: params['bpjsNumber'].to_s,
        startTime: params['startTime'],
        endTime: params['endTime'],
        statusID: statusID

      }
      data = Appointment.new(input)
      
      find_booking_date = Appointment.where("bookingDate = '#{booking.to_date}' AND cancel != 3 AND statusID != 3").count()
      max_booking = 12;

      if find_booking_date > max_booking
        mydata = {
            metaData: {code: 403, message: "Today's appointment has exceeded the limit"}
          }
      else
        cek_apm = Appointment.where(noRM: data.noRM, bookingDate: data.bookingDate, policlinicID: data.policlinicID)
        
        cek_apm.each do |r|
          if r.present? && r['statusID'] != 3
            mydata = {
              metaData: {code: 400, message: "Anda Telah Memiliki Perjanjian Di Tanggal Yang Sama!"}
            }
            render json: mydata
            return
          end
        end
        if params['paymentID'].to_s == '1000'
          mydata = {
            metaData: {code: 400, message: "Pendaftaran online pasien BPJS ke RSUD Ciracas belum dapat dilakukan, mohon melakukan pendaftaran langsung melalui loket secara langsung"}
          }
        else
          if data.save
            find_waiting = Appointment.where("statusID = 1").count()
            waitingNumber = find_waiting-1
            data.waitingNumber = waitingNumber
            data.save
            mydata = {
              metaData: {code: 201, message: "Object created. Appointment has been scheduled "},
              data: { bookingCode: data['bookingCode'], queueNumber: data['queueNumber'], waitingNumber: waitingNumber },
            }
          else
            mydata = {
              metaData: {code: 400, message: "Bad request, Not saving data"}
            }
          end
         
        end 
       
      end

    render json: mydata
    return

  end

  def get_validasi
    jakis = Jaki.find_by_sql("SELECT DISTINCT p.NoCM as noRM, p.NamaLengkap as name, p.TglLahir as birthDate,  p.NoIdentitas as nik, p.JenisKelamin as sex, pa.IdAsuransi as noJKN, p.Telepon as phone FROM PASIEN p
    LEFT JOIN AsuransiPasien pa ON pa.NoCM=p.NoCM
    WHERE noRM = '#{params['NoRm']}' OR NoIdentitas = '#{params['nik']}' OR IdAsuransi  = '#{params['nojkn']}' ")

    data = []
    if jakis.present?
      jakis.each do |jakis|
        datax = {
          noMR: jakis['noRM'],
          name: jakis['name'],
          gender: jakis['sex'],
          birthDate: jakis['birthDate'],
          nik: jakis['nik'],
          noJKN: jakis['noJKN'],
          phone: jakis['phone']
        }
        data.push(datax)
      end
      
      mydata = {
        metaData: {code: 200, message: "OK"},
        data: data[0] ,
      }
    else
      mydata = {
        metaData: {code: 404, message: "Not Found,Patient doesn't exist"},
        data: { patient: data },
      }
    end

    render json: mydata
    return
  end

  # GET /jakis/1
  # GET /jakis/1.json
  def show
  end

  # POST /jakis
  # POST /jakis.json
  def create
    @jaki = Jaki.new(jaki_params)

    if @jaki.save
      render :show, status: :created, location: @jaki
    else
      render json: @jaki.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /jakis/1
  # PATCH/PUT /jakis/1.json
  def update
    if @jaki.update(jaki_params)
      render :show, status: :ok, location: @jaki
    else
      render json: @jaki.errors, status: :unprocessable_entity
    end
  end

  # DELETE /jakis/1
  # DELETE /jakis/1.json
  def destroy
    @jaki.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_jaki
      @jaki = Jaki.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def jaki_params
      params.permit(:noRM, :name, :sex, :birthDate, :nik, :noJKN, :phone)
    end
end
