class CreatePolyclinics < ActiveRecord::Migration[6.1]
  def change
    create_table :polyclinics do |t|
      t.string :kdRuangan
      t.string :namaRuangan
      t.string :gambarPoli
      t.string :statusEnabled
      t.string :kodeSubInstalasi
      t.string :kodePoliBPJS
      t.string :kodeInstalasi

      t.timestamps
    end
  end
end
