class AddWaitingNumberToAppoinment < ActiveRecord::Migration[6.1]
  def change
    add_column :appointments, :waitingNumber, :integer
  end
end
